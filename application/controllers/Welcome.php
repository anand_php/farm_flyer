<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function verifyEmail($email,$user_type)
    {
        if(!empty($email) && !empty($user_type))
        {
            $updateData = array(
                'EmailVerified'=>1,
                'Updated_on'=>date('Y-m-d H:i:s')
            );
            $status = $this->api_model->update('ff_userlogindetails',array('UserEmailID'=>$email),$updateData);
            if($status == true)
            {
                echo "Your Email verified.";
            }
            else
            {
                echo "Your Email not verified.";
            }
        }
    }
}
