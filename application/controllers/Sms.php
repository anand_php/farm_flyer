<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('sms_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['sms'] = $this->sms_model->getAllSmsDetails();
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		// $this->load->view('category/category', $this->data);
		$this->load->view('sms/sms', $this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result =  $this->sms_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$sms_id = $this->uri->segment(3);
		$result = $this->sms_model->delete($sms_id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully!");
			redirect('sms');
		}
	}

	public function edit(){
        $sms_id = $this->uri->segment(3);   
        $this->data['sms'] = $this->sms_model->getSmsById($sms_id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('sms/edit_sms', $this->data);
    }

    public function update(){
    	$this->session->set_flashdata('message',"Record Updated successfully!");
    	$result = $this->sms_model->update();
    	echo json_encode($result);
    }

    public function checkActivate(){
    	
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$id = $_POST['id'];
    	$result = $this->sms_model->checkActivate($id);
    	echo json_encode($result);
    }

   
    public function view(){
    	$sms_id = $this->uri->segment(3);   
        $this->data['sms'] = $this->sms_model->getSmsById($sms_id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('sms/view_sms', $this->data);
    }
}
