<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}
		
		$this->load->view('dashboard');
		$this->load->view('common/script');
	}

	public function profile(){
		$this->data['profile'] = $this->dashboard_model->getProfileDataById();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('profile', $this->data);
	}

	public function update(){
		$result = $this->dashboard_model->update();
		$this->session->set_flashdata('message',"<div style='color:green;'>Record Inserted successfully.<div>");
		redirect('dashboard/profile');
	}

	public function change_password(){
		
		$result = $this->dashboard_model->change_password();
		if($result == 1){
			redirect('dashboard');
		}
	}

	//created by Pooja on 14_09_2019
	public function update_subadmin(){
		$result = $this->dashboard_model->update_subadmin();
		$this->session->set_flashdata('message',"<div style='color:green;'>Record Inserted successfully.<div>");
		redirect('dashboard/profile');
	}
 
}
