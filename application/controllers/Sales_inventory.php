<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_inventory extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('sales_inventory_model');
		$this->load->model('resource_product_model');
		$this->load->model('users_model');
		$this->load->model('category_model');
		$this->load->model('units_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
		error_reporting(0);
	}
	
	public function index()
	{
		$this->data['sales_inventory'] = $this->sales_inventory_model->getAllSalesInventory();
		
		if($this->data['sales_inventory']['count'] > 0){

			for($i = 0; $i < $this->data['sales_inventory']['count']; $i++){
				$base_unit = $this->sales_inventory_model->getUnitAsBaseUnit($this->data['sales_inventory']['result'][$i]->unit_id); 
				
					$quantity = $this->data['sales_inventory']['result'][$i]->product_quantity / $base_unit->unit_ratio;
					$this->data['sales_inventory']['result'][$i]->product_quantity = $quantity;
					
				$this->data['sales_inventory']['result'][$i]->base_unit_name = $base_unit->unit_name;
					
			}
		};
		$this->data['resourc_products'] = $this->resource_product_model->getAllResourceProducts();
		$this->data['sellers'] = $this->users_model->getAllUsers();
		$this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['units'] = $this->units_model->getAllUnits();
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('sales_inventory/inventory',$this->data);
	}

	//created by Pooja on 27_09_2019
	public function add_inventory(){
		// $this->data['sales_inventory'] = $this->sales_inventory_model->getAllSalesInventory();
		
		// if($this->data['sales_inventory']['count'] > 0){

		// 	for($i = 0; $i < $this->data['sales_inventory']['count']; $i++){
		// 		$base_unit = $this->sales_inventory_model->getUnitAsBaseUnit($this->data['sales_inventory']['result'][$i]->unit_id); 
				
		// 			$quantity = $this->data['sales_inventory']['result'][$i]->product_quantity / $base_unit->unit_ratio;
		// 			$this->data['sales_inventory']['result'][$i]->product_quantity = $quantity;
					
		// 		$this->data['sales_inventory']['result'][$i]->base_unit_name = $base_unit->unit_name;
					
		// 	}
		// };
		$this->data['resourc_products'] = $this->resource_product_model->getAllResourceProducts();
		$this->data['sellers'] = $this->users_model->getAllUsers();
		$this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['units'] = $this->units_model->getAllUnits();
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('sales_inventory/add_sales_inventory',$this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result = $this->sales_inventory_model->add();
		echo json_encode($result);
	}

	// public function delete(){	
	// 	$id = $this->uri->segment(3);
	// 	$result = $this->sales_inventory_model->delete($id);
	// 	if($result == 1){
	// 		$this->session->set_flashdata('message',"<div style='color:green;'>Record Deleted successfully.<div>");
	// 		redirect("brand");
	// 	}
	// }

	public function edit(){
		$sale_inventory_id = $this->uri->segment(3);
		$this->data['sale_inventory']= $this->sales_inventory_model->getSaleInventoryById($sale_inventory_id);
		$this->data['sellers'] = $this->users_model->getAllUsers();
		$this->data['resourc_products'] = $this->resource_product_model->getAllResourceProducts();
		$this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['units'] = $this->units_model->getAllUnits();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('sales_inventory/edit_sales_inventory',$this->data);
	}

	public function view(){
		$sale_inventory_id = $this->uri->segment(3);
		$this->data['sale_inventory']= $this->sales_inventory_model->getSaleInventoryById($sale_inventory_id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('sales_inventory/view_sales_inventory',$this->data);	
	}

	public function update(){
		$this->session->set_flashdata('message',"Record Updated successfully!");
		$result = $this->sales_inventory_model->update();
		echo json_encode($result);
	}

	public function getBrandById(){
		$id= $_POST['id'];
		$result = $this->sales_inventory_model->getBrandById($id);
		echo json_encode($result);
	}

	public function checkActivate(){
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$sale_inventory_id = $_POST['sale_inventory_id'];
    	$result = $this->sales_inventory_model->checkActivate($sale_inventory_id);
    	echo json_encode($result);
    }

    public function getProductsBySubcategory(){
    	$subcategory_id = $_POST['subcategory_id'];
    	$result = $this->sales_inventory_model->getProductsBySubcategory($subcategory_id);
    	echo json_encode($result);	
    }

    public function getVarientsByProductId(){
    	$product_id = $_POST['product_id'];
    	$result = $this->sales_inventory_model->getVarientsByProductId($product_id);
    	echo json_encode($result);
    }
   
}
