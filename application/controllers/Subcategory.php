<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('subcategory_model');
		$this->load->database();
		$this->load->model('category_model');
		$this->load->model('dashboard_model');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['subcategories'] = $this->subcategory_model->getAllSubcategories();
		$this->data['categories'] = $this->category_model->getAllCategories();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('subcategory/subcategory', $this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"<div style='color:green;'>Record Inserted successfully.<div>");
		$result = $this->subcategory_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result= $this->subcategory_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"<div style='color:green;'>Record Deleted successfully.<div>");
			redirect("subcategory");
		}
	}

	public function edit(){
		$id = $this->uri->segment(3);
		$this->data['subcategory'] = $this->subcategory_model->getSubcategoryById($id);
		$this->data['categories'] = $this->category_model->getAllCategories();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('subcategory/edit_subcategory', $this->data);
	}

	public function update(){
		$result = $this->subcategory_model->update();
		echo json_encode($result);
	}

	public function checkActivate(){
		
    	$this->session->set_flashdata('message',"<div style='color:green;'>Record activated/deactivated successfully.<div>");
    	$id = $_POST['id'];
    	$result = $this->subcategory_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$sub_cat_name = $_POST['sub_cat_name'];
    	$result = $this->subcategory_model->checkExists($sub_cat_name);
    	echo json_encode($result);	
    }
}
