<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('category_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['categories'] = $this->category_model->getAllCategories();

		for ($i=0; $i < count($this->data['categories']['result']) ; $i++) 
		{ 

			if(!empty($this->data['categories']['result'][$i]->parent_category_id))
			{
				$base_category = $this->category_model->getCategoryById($this->data['categories']['result'][$i]->parent_category_id);

				$this->data['categories']['result'][$i]->parent_category_id = $base_category->category_name;
			}
		}
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('category/category', $this->data);
	}

	public function add(){
		
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result =  $this->category_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result = $this->category_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully!");
			redirect('category');
		}
	}

	public function edit(){
        $id = $this->uri->segment(3);   
        $this->data['category'] = $this->category_model->getCategoryById($id);
        $this->data['categories'] = $this->category_model->getAllCategories();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('category/edit_category', $this->data);
    }

    public function update(){
    	$this->session->set_flashdata('message',"Record Updated successfully!");
    	$result = $this->category_model->update();
    	echo json_encode($result);
    }

    public function checkActivate(){
    	
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$id = $_POST['id'];
    	$result = $this->category_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$category_name = $_POST['category_name'];
    	$result = $this->category_model->checkExists($category_name);
    	echo json_encode($result);
    }

    public function view(){
    	$id = $this->uri->segment(3);   
        $this->data['category'] = $this->category_model->getCategoryById($id);

        if(!empty($this->data['category']->parent_category_id))
		{
			$base_category = $this->category_model->getCategoryById($this->data['category']->parent_category_id);

			$this->data['category']->parent_category_id = $base_category->category_name;
		}

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('category/view_category', $this->data);
    }
}
