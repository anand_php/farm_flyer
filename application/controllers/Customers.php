<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}

	// public function index()
	// {
	// 	$this->load->view('common/header');
	// 	$this->load->view('category');
	// }

	public function view()
	{
		$this->load->view('common/header');
		$this->load->view('view_customer');
	}
}
