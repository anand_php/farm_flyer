<?php
   
// require APPPATH . 'libraries/REST_Controller.php';
// require APPPATH . 'libraries/REST_Controller.php';
require(APPPATH.'libraries/REST_Controller.php');
// require APPPATH . 'libraries/Format.php';
require(APPPATH.'libraries/Format.php');
     
class Api extends REST_Controller {
    
	/**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
       $this->load->model('api_model');
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function getAllStates_post(){
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $data = $this->api_model->getAllStates();
            if($data){
                $this->set_response([
                'status' => 1,
                'message' => 'success',
                'data'    => $data
                ], REST_Controller::HTTP_OK);

                // $this->set_response($data, REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                'status' => 0,
                'message' => 'No states available'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }

        
    }

    //created by Pooja on 11_09_2019 to get cities from state id
    public function getAllCitiesByStateId_post(){
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $state_id = $this->input->post('state_id');
            $data = $this->api_model->getAllCitiesByStateId($state_id);
            if($data){
                $this->set_response([
                'status' => 1,
                'message' => 'success',
                'data'    => $data
                ], REST_Controller::HTTP_OK);

                // $this->set_response($data, REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                'status' => 0,
                'message' => 'No city available'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
        
        
    }

    //Anand check 05_11_2019
    public function getUserTypes_post()
    {
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $userTypes = $this->api_model->selectAllByWhere('ff_UserType',array('status'=>1));

            if(!empty($userTypes))
            {
                $this->set_response([
                'status' => 1,
                'message' => 'success',
                'data'    => $userTypes
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                'status' => 0,
                'message' => 'User type not found.'
                ], REST_Controller::HTTP_OK);
            }
            
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function sendOTP_post()
    {
        $mobile = $this->input->post('mobile');
        $user_type = $this->input->post('user_type');
        $user_name = $this->input->post('user_name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $checkIsMobileExistUnverified = $this->api_model->select('ff_userlogindetails',array('UserMobileNumber'=> $mobile,'OTPVerified'=>0,'UserType'=>$user_type)); //Exist but not verified

            $checkIsMobileExistVerified = $this->api_model->select('ff_userlogindetails',array('UserMobileNumber'=> $mobile,'OTPVerified'=>1,'UserType'=>$user_type)); //Exist and verified

            $checkIsEmailExist = $this->api_model->select('ff_userlogindetails',array('UserEmailID'=> $email,'UserType'=>$user_type)); //Email Exist

            if(!empty($checkIsMobileExistUnverified))
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Mobile no. already exist,please verify it.'
                ], REST_Controller::HTTP_OK);
            }
            elseif(!empty($checkIsMobileExistVerified))
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Mobile no. already exist..please login.'
                ], REST_Controller::HTTP_OK);
            }
            elseif(!empty($checkIsEmailExist))
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Email already exist.'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                
                $otp = mt_rand(1000, 9999);
                $table = 'ff_userlogindetails';
                $data = array(
                    'UserType'=>$user_type,
                    'UserName'=>$user_name,
                    'UserMobileNumber' => $mobile,
                    'UserPassword'=>md5($password), //md5
                    'UserEmailID'=>$email,
                    'OTPCode'   => $otp,
                    'Created_on'=>date('Y-m-d H:i:s')
                );

                 $isMobileExist = $this->api_model->select('ff_userlogindetails',array('UserMobileNumber'=>$mobile));
                
                if(!empty($isMobileExist))
                {
                    $where = array('UserMobileNumber'=>$mobile);
                    $data = array(
                        'UserType'=>$user_type,
                        'UserName'=>$user_name,
                        'UserMobileNumber' => $mobile,
                        'UserPassword'=>md5($password), //md5
                        'UserEmailID'=>$email,
                        'OTPCode'   => $otp,
                        'Updated_on'=>date('Y-m-d H:i:s')
                    );
                    $response = $this->api_model->update($table, $where, $data);
                }
                else
                {
                   $response = $this->api_model->insert_all($table,$data);  
                }

                if($response > 0 || $response == true)
                {
                    //for sms gateway
                    $message = "Your OTP is :". $otp;
                    $smsGatewayUrl="http://www.bulksmsshortcode.com/sendsmsv2.asp?user=gooiipune&password=gooiipune&sender=OIIMSG&sendercdma=919860609000&text=".urlencode($message)."&PhoneNumber=91".trim($mobile)."&track=1";
                    
                    $url = $smsGatewayUrl;
                    
                    $ch = curl_init();
                    
                    curl_setopt($ch, CURLOPT_POST, false);
                    
                    curl_setopt($ch, CURLOPT_URL, $url);
                    
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                    
                    $output = curl_exec($ch);
                    
                    curl_close($ch);
                    //send email verification link
                    $link = 'Click Here To Verify Email <a href='.base_url().'api/verifyEmail/'.$email.'/'.$user_type.'> click here... </a>'; 
                    $subject = "Email Verification";
                    $smtp_details = $this->api_model->selectRow("smtp_details");
                    $host=$smtp_details[0]->smtp_host; 
                    $mailid=$smtp_details[0]->smtp_username; 
                    $mailpass=$smtp_details[0]->smtp_password;
                    $adminmail=$smtp_details[0]->smtp_username;
                    date_default_timezone_set('Asia/Kolkata'); 

                    $this->load->library('email');
                    $this->email->set_newline("\r\n");
                    $this->email->set_crlf("\r\n");
                    $this->email->set_mailtype("html");
                    $this->email->clear();
                    $this->email->from($smtp_details[0]->sender_email_id, $smtp_details[0]->sender_name);
                    $this->email->to($email);
                    $this->email->subject($subject);    
                    $this->email->message($link);
                    $isEmailSend = 0;
                    if($this->email->send())
                    {
                        $isEmailSend = 1;
                    }
                    $this->set_response([
                    'status' => 1,
                    'message' => 'Otp sent on your mobile.',
                    ], REST_Controller::HTTP_OK);
                    
                }
                else
                {
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Something went wrong'
                    ], REST_Controller::HTTP_OK);
                } 
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function verifyOTP_post()
    {
        
        $mobile = $this->input->post('mobile');
        $otp = $this->input->post('otp');
        $token  = $this->input->post('token');
       
        $checkToken = $this->api_model->checkToken($token);
        if($checkToken == 1)
        {
            if(!empty($mobile) && !empty($otp))
            {
                $result = $this->api_model->select('ff_userlogindetails', array('UserMobileNumber'=> $mobile, 'OTPCode'=> $otp));


                if(!empty($result))
                {
                    //update status opt verified and status active
                    $updateData = array(
                        'OTPVerified'=>1,
                        'Status'=>1,
                        'Updated_on'=>date('Y-m-d H:i:s'),
                    );
                    $status = $this->api_model->update('ff_userlogindetails', array('UserMobileNumber'=> $mobile, 'OTPCode'=> $otp),$updateData);
                    if($status == true)
                    {
                        $result = $this->api_model->select('ff_userlogindetails', array('UserMobileNumber'=> $mobile));
                        $this->set_response([
                        'status' => 1,
                        'message' => 'Mobile number verified.',
                        'data'=>$result,
                        ], REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $this->set_response([
                        'status' => 0,
                        'message' => 'Mobile number not verified.'
                        ], REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $this->set_response([
                        'status' => 0,
                        'message' => 'OTP is wrong.'
                    ], REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Please enter mobile and otp.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function reSendOTP_post()
    {
        $mobile = $this->input->post('mobile');
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $otp = mt_rand(1000, 9999);
            
            $data = array(
                'OTPCode'   => $otp,
                'Created_on'=>date('Y-m-d H:i:s')
            );

            $isMobileExist = $this->api_model->select('ff_userlogindetails',array('UserMobileNumber'=>$mobile));
            
            if(!empty($isMobileExist))
            {
                $otp_data = array(
                    'OTPCode' => $otp,
                    'Updated_on'=>date('Y-m-d H:i:s')
                    );
                $response = $this->api_model->update('ff_userlogindetails', array('UserMobileNumber'=>$mobile), $otp_data);
            }
            

            if($response == 1)
            {
                //for sms gateway
                $message = "Your OTP is :". $otp;
                $smsGatewayUrl="http://www.bulksmsshortcode.com/sendsmsv2.asp?user=gooiipune&password=gooiipune&sender=OIIMSG&sendercdma=919860609000&text=".urlencode($message)."&PhoneNumber=91".trim($mobile)."&track=1";
                
                $url = $smsGatewayUrl;
                
                $ch = curl_init();
                
                curl_setopt($ch, CURLOPT_POST, false);
                
                curl_setopt($ch, CURLOPT_URL, $url);
                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                
                $output = curl_exec($ch);
                
                curl_close($ch);

               
                if(!empty($isMobileExist))
                {
                    $this->set_response([
                    'status' => 1,
                    'message' => 'Otp resent on your mobile.',
                    ], REST_Controller::HTTP_OK);
                }
                else
                {
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Otp not resent on your mobile.'
                    ], REST_Controller::HTTP_OK);
                }
                
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Something went wrong'
                ], REST_Controller::HTTP_OK);
            } 
            
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function buyerRegistration_post()
    {
        $CompanyName     = $this->input->post('company_name');
        $PhoneNumber = $this->input->post('alternate_mobile');
        $Address    = $this->input->post('buyer_address');
        $City          = $this->input->post('city_id');
        $Town             = $this->input->post('town');
        $Landmark         = $this->input->post('landmark');
        $PinCode          = $this->input->post('pincode');
        $State         = $this->input->post('state_id');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        //optional
        $GSTNumber       = $this->input->post('gst_number');
        $GSTNumberDoc       = $this->input->post('gst_number_doc');
        $PANCard       = $this->input->post('pan_card_number');
        $PANCardDoc       = $this->input->post('pan_card_doc');
        $TypeOfCompany       = $this->input->post('type_of_company');
        $UpdatedBy       = $this->input->post('user_id');
        $CompanyMainAccountID       = $this->input->post('user_id');
        $token            = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $gstImageName = '';
            if(!empty($GSTNumberDoc))
            {
                ini_set ('gd.jpeg_ignore_warning', 1);
                $gstImageName = 'GST_'.$GSTNumber.'.jpeg';//Anand check 08_07_2019
                $convertImage = base64_decode($GSTNumberDoc);
                $img = imagecreatefromstring($convertImage);
                if($img != false)
                {
                   $path = './uploads/gstDocs/'.$gstImageName;
                   imagejpeg($img, $path);
                }
            }

            $panCardImageName = '';
            if(!empty($PANCardDoc))
            {
                ini_set ('gd.jpeg_ignore_warning', 1);
                $panCardImageName = 'PAN_'.$PANCard.'.jpeg';//Anand check 08_07_2019
                $convertImage = base64_decode($PANCardDoc);
                $img1 = imagecreatefromstring($convertImage);
                if($img1 != false)
                {
                   $path = './uploads/panDocs/'.$panCardImageName;
                   imagejpeg($img1, $path);
                }
            }
            
            $data = array(
                'CompanyName' => $CompanyName,
                'PhoneNumber' => $PhoneNumber,
                'Address'     => $Address, 
                'Location_Map_ID'=>$latitude.','.$longitude,
                'City'        => $City,
                'State'       => $State, 
                'Town'        => $Town,
                'Landmark'    => $Landmark,
                'PinCode'     => $PinCode,
                'GSTNumber'   => $GSTNumber,
                'PANCard'     => $PANCard,
                'TypeOfCompany' => $TypeOfCompany,
                'UpdatedBy'     => $UpdatedBy,
                'CompanyMainAccountID'=> $CompanyMainAccountID,
                'GSTNumberDoc'=>$gstImageName,
                'PANCardDoc'=>$panCardImageName,
                'Created_On'=>date('Y-m-d H:i:s')
            );

            $response = $this->api_model->insert_all('ff_comapnydetails',$data);
            if($response > 0)
            {
                //Insert into ff_companyusers junction table
                $inserData = array(
                    'User_id'=>$CompanyMainAccountID,
                    'Company_id'=>$response
                );
                $insertId = $this->api_model->insert_all('ff_companyusers',$inserData);
                $this->set_response([
                    'status' => 1,
                    'message' => 'Registraion done successfully'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Registraion not done'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
        
    }
    public function login_post()
    {
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if(!empty($username) && !empty($password))
            {
                if(is_numeric($username))
                {
                    $table = 'ff_userlogindetails';
                    $where = array('UserMobileNumber'=> $username, 'UserPassword'=> md5($password));

                    $result = $this->api_model->select($table, $where);
                    if(!empty($result))
                    {
                        $isUserActive = $this->api_model->select($table, array('UserMobileNumber'=> $username, 'Status'=> 1));
                        $isOtpVerified = $this->api_model->select($table, array('UserMobileNumber'=> $username, 'UserPassword'=> md5($password),'OTPVerified'=>1));
                        if(!empty($isUserActive))
                        {
                            if(!empty($isOtpVerified))
                            {
                                //check company profile exist or not
                                $isCompanyProfileExist = $this->api_model->selectRowByWhere('ff_comapnydetails',array('CompanyMainAccountID'=>$isOtpVerified->UserID));
                                if(!empty($isCompanyProfileExist))
                                {
                                    //check company profile is verified by admin or not
                                    $isCompanyProfileApproved = $this->api_model->selectRowByWhere('ff_comapnydetails',array('CompanyMainAccountID'=>$isOtpVerified->UserID,'AdminApproved'=>1));
                                    if(!empty($isCompanyProfileApproved))
                                    {
                                        $GSTNumberDocUrl = '';
                                        if(!empty($isCompanyProfileApproved->GSTNumberDoc))
                                        {
                                            $GSTNumberDocUrl = base_url().'uploads/gstDocs/'.$isCompanyProfileApproved->GSTNumberDoc;
                                        }
                                        $isCompanyProfileApproved->GSTNumberDocUrl = $GSTNumberDocUrl;

                                        $PANCardDocUrl = '';
                                        if(!empty($isCompanyProfileApproved->PANCardDoc))
                                        {
                                            $PANCardDocUrl = base_url().'uploads/panDocs/'.$isCompanyProfileApproved->PANCardDoc;
                                        }
                                        $isCompanyProfileApproved->PANCardDocUrl = $PANCardDocUrl;

                                        $getState = $this->api_model->selectRowByWhere('service_state',array('state_id'=>$isCompanyProfileApproved->State));
                                        $isCompanyProfileApproved->state_name = $getState->state_name;

                                        $getCity = $this->api_model->selectRowByWhere('service_city',array('id'=>$isCompanyProfileApproved->City));
                                        $isCompanyProfileApproved->city_name = $getCity->city_name;

                                        $this->set_response([
                                        'status' => 1,
                                        'message' => 'User log in successfully.',
                                        'data' => $isOtpVerified,
                                        'company_data'=>$isCompanyProfileApproved,
                                        ], REST_Controller::HTTP_OK);
                                    }
                                    else
                                    {
                                        $this->set_response([
                                        'status' => 0,
                                        'is_company_profile_verified'=>0,
                                        'message' => 'Contact Admin,Company profile not verified.',
                                        ], REST_Controller::HTTP_OK);
                                    }
                                   
                                }
                                else
                                {
                                    $this->set_response([
                                    'status' => 0,
                                    'is_company_profile'=>0,
                                    'message' => 'Company profile not complete.',
                                    ], REST_Controller::HTTP_OK);
                                }
                                
                            }
                            else
                            {
                                $this->set_response([
                                    'status' => 3,
                                    'message' => 'Mobile number is not verified.',
                                ], REST_Controller::HTTP_OK);
                            }
                        }
                        else
                        {
                            $this->set_response([
                                'status' => 0,
                                'message' => 'User is not active.',
                            ], REST_Controller::HTTP_OK);
                        }
                       
                    }
                    else
                    {
                        $this->set_response([
                            'status' => 0,
                            'message' => 'Username or password is incorrect.'
                        ], REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $table = 'ff_userlogindetails';
                    $where = array('UserEmailID'=> $username, 'UserPassword'=> md5($password));
                    $result = $this->api_model->select($table, $where);
                    if(!empty($result))
                    {
                        $isUserActive = $this->api_model->select($table, array('UserEmailID'=> $username, 'Status'=> 1));
                        if(!empty($isUserActive))
                        {
                            //check company profile exist or not
                            $isCompanyProfileExist = $this->api_model->selectRowByWhere('ff_comapnydetails',array('CompanyMainAccountID'=>$isUserActive->UserID));
                            if(!empty($isCompanyProfileExist))
                            {
                                //check company profile is verified by admin or not
                                $isCompanyProfileApproved = $this->api_model->selectRowByWhere('ff_comapnydetails',array('CompanyMainAccountID'=>$isUserActive->UserID,'AdminApproved'=>1));
                                if(!empty($isCompanyProfileApproved))
                                {
                                    $GSTNumberDocUrl = '';
                                    if(!empty($isCompanyProfileApproved->GSTNumberDoc))
                                    {
                                        $GSTNumberDocUrl = base_url().'uploads/gstDocs/'.$isCompanyProfileApproved->GSTNumberDoc;
                                    }
                                    $isCompanyProfileApproved->GSTNumberDocUrl = $GSTNumberDocUrl;

                                    $PANCardDocUrl = '';
                                    if(!empty($isCompanyProfileApproved->PANCardDoc))
                                    {
                                        $PANCardDocUrl = base_url().'uploads/panDocs/'.$isCompanyProfileApproved->PANCardDoc;
                                    }
                                    $isCompanyProfileApproved->PANCardDocUrl = $PANCardDocUrl;

                                    $this->set_response([
                                    'status' => 1,
                                    'message' => 'User log in successfully.',
                                    'data' => $isOtpVerified,
                                    'company_data'=>$isCompanyProfileApproved,
                                    ], REST_Controller::HTTP_OK);
                                }
                                else
                                {
                                    $this->set_response([
                                    'status' => 0,
                                    'is_company_profile_verified'=>0,
                                    'message' => 'Contact Admin,Company profile not verified.',
                                    ], REST_Controller::HTTP_OK);
                                }
                            }
                            else
                            {
                                $this->set_response([
                                'status' => 0,
                                'is_company_profile'=>0,
                                'data'=>$isUserActive,
                                'message' => 'Company profile not complete.',
                                ], REST_Controller::HTTP_OK);
                            }
                        }
                        else
                        {
                            $this->set_response([
                                'status' => 0,
                                'message' => 'User is not active.',
                            ], REST_Controller::HTTP_OK);
                        }
                    }
                    else
                    {
                        $this->set_response([
                            'status' => 0,
                            'message' => 'Username or password is incorrect.'
                        ], REST_Controller::HTTP_OK);
                    }
                }
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Enter usersname and password'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    //Anand check 06_11_2019
    public function updateProfile_post()
    {
        $ID     = $this->input->post('ID');
        $CompanyName     = $this->input->post('company_name');
        $PhoneNumber = $this->input->post('alternate_mobile');
        $Address    = $this->input->post('buyer_address');
        $City          = $this->input->post('city_id');
        $Town             = $this->input->post('town');
        $Landmark         = $this->input->post('landmark');
        $PinCode          = $this->input->post('pincode');
        $State         = $this->input->post('state_id');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        //optional
        $GSTNumber       = $this->input->post('gst_number');
        $GSTNumberDoc       = $this->input->post('gst_number_doc');
        $PANCard       = $this->input->post('pan_card_number');
        $PANCardDoc       = $this->input->post('pan_card_doc');
        $TypeOfCompany       = $this->input->post('type_of_company');
        $UpdatedBy       = $this->input->post('user_id');
        $CompanyMainAccountID       = $this->input->post('user_id');
        $token            = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $companyData = $this->api_model->selectRowByWhere('ff_comapnydetails',array('ID'=>$ID));
            $gstImageName = $companyData->GSTNumberDoc;
            if(!empty($GSTNumberDoc))
            {
                @unlink("./uploads/gstDocs/".$companyData->GSTNumberDoc);
                ini_set ('gd.jpeg_ignore_warning', 1);
                $gstImageName = 'GST_'.$GSTNumber.'.jpeg';//Anand check 08_07_2019
                $convertImage = base64_decode($GSTNumberDoc);
                $img = imagecreatefromstring($convertImage);
                if($img != false)
                {
                   $path = './uploads/gstDocs/'.$gstImageName;
                   imagejpeg($img, $path);
                }
            }

            $panCardImageName = $companyData->PANCardDoc;
            if(!empty($PANCardDoc))
            {
                @unlink("./uploads/panDocs/".$companyData->PANCardDoc);
                ini_set ('gd.jpeg_ignore_warning', 1);
                $panCardImageName = 'PAN_'.$PANCard.'.jpeg';//Anand check 08_07_2019
                $convertImage = base64_decode($PANCardDoc);
                $img1 = imagecreatefromstring($convertImage);
                if($img1 != false)
                {
                   $path = './uploads/panDocs/'.$panCardImageName;
                   imagejpeg($img1, $path);
                }
            }
            
            $data = array(
                'CompanyName' => $CompanyName,
                'PhoneNumber' => $PhoneNumber,
                'Address'     => $Address, 
                'Location_Map_ID'=>$latitude.','.$longitude,
                'City'        => $City,
                'State'       => $State, 
                'Town'        => $Town,
                'Landmark'    => $Landmark,
                'PinCode'     => $PinCode,
                'GSTNumber'   => $GSTNumber,
                'PANCard'     => $PANCard,
                'TypeOfCompany' => $TypeOfCompany,
                'UpdatedBy'     => $UpdatedBy,
                'CompanyMainAccountID'=> $CompanyMainAccountID,
                'GSTNumberDoc'=>$gstImageName,
                'PANCardDoc'=>$panCardImageName,
                'AdminApproved'=>0,//unverified
                'Updated_On'=>date('Y-m-d H:i:s')
            );

            $response = $this->api_model->update_all('ff_comapnydetails',array('ID'=>$ID),$data);
            if($response == true)
            {
                $this->set_response([
                    'status' => 1,
                    'message' => 'Profile updated.'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Profile not updated.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function sendOTPChangePassword_post()
    {
        $mobile = $this->input->post('mobile');
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $otp = mt_rand(1000, 9999);
            $isMobileExist = $this->api_model->select('ff_userlogindetails',array('UserMobileNumber'=>$mobile));
            
            if(!empty($isMobileExist))
            {
                $otp_data = array(
                    'OTPCode' => $otp,
                    'Updated_on'=>date('Y-m-d H:i:s')
                    );
                $response = $this->api_model->update('ff_userlogindetails', array('UserMobileNumber'=>$mobile), $otp_data);
                if($response == 1)
                {
                    //for sms gateway
                    $message = "Your OTP is :". $otp;
                    $smsGatewayUrl="http://www.bulksmsshortcode.com/sendsmsv2.asp?user=gooiipune&password=gooiipune&sender=OIIMSG&sendercdma=919860609000&text=".urlencode($message)."&PhoneNumber=91".trim($mobile)."&track=1";
                    
                    $url = $smsGatewayUrl;
                    
                    $ch = curl_init();
                    
                    curl_setopt($ch, CURLOPT_POST, false);
                    
                    curl_setopt($ch, CURLOPT_URL, $url);
                    
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                    
                    $output = curl_exec($ch);
                    
                    curl_close($ch);

                   
                    if(!empty($isMobileExist))
                    {
                        $this->set_response([
                        'status' => 1,
                        'message' => 'Otp sent on your mobile.',
                        ], REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $this->set_response([
                            'status' => 0,
                            'message' => 'Otp not sent on your mobile.'
                        ], REST_Controller::HTTP_OK);
                    }
                }
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Mobile number does not exist.'
                ], REST_Controller::HTTP_OK);
            } 
            
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function resetPassword_post()
    {
        $mobile = $this->input->post('mobile');
        $newPassword = $this->input->post('newPassword');
        $token  = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $updateData = array(
                'UserPassword' => md5($newPassword),
                'Updated_on'=>date('Y-m-d H:i:s')
                );
            $response = $this->api_model->update_password('ff_userlogindetails', array('UserMobileNumber'=>$mobile), $updateData);

            if($response == true)
            {
                $this->set_response([
                'status' => 1,
                'message' => 'Password reset successfully.',
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Password not reset successfully.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    //Anand check 06_11_2019
    //Anand check 07_11_2019
    public function addDeliveryAddress_post()
    {
        $company_id     = $this->input->post('company_id');
        $address_nick_name = $this->input->post('address_nick_name');
        $address    = $this->input->post('address');
        $pincode          = $this->input->post('pincode');
        $user_id         = $this->input->post('user_id');
        $contact_person = $this->input->post('contact_person');
        $contact_number = $this->input->post('contact_number');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $token            = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $data = array(
                'company_id' => $company_id,
                'address_nick_name' => $address_nick_name,
                'address'     => $address, 
                'pincode'=>$pincode,
                'added_by_user_id'=> $user_id,
                'contact_person_name'=> $contact_person, 
                'contact_person_number'=> $contact_number,
                'location_id'=>$latitude.','.$longitude,
                'created_on'    => date('Y-m-d H:i:s'),
            );

            $response = $this->api_model->insert_all('ff_comapnydeleveryaddress',$data);
            if($response > 0)
            {
                $this->set_response([
                    'status' => 1,
                    'message' => 'Address added.'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Address not added.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function getAllDeliveryAddressOfBuyer_post()
    {
        $company_id     = $this->input->post('company_id');
        $user_id         = $this->input->post('user_id');
        $token            = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $deliveryAddresses = $this->api_model->selectAllByWhere('ff_comapnydeleveryaddress',array('added_by_user_id'=>$user_id,'company_id'=>$company_id));

            if(!empty($deliveryAddresses))
            {
                $this->set_response([
                    'status' => 1,
                    'message' => 'Success',
                    'data'=>$deliveryAddresses
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'Address not found.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    //Anand check 07_11_2019
    //Anand check 08_11_2019
    public function getAllUserRoleTypes_post()
    {
        $token  = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
           $allUserRoleType = $this->api_model->selectAllByWhere('ff_usertyperole',array('Status'=>1));

            if(!empty($allUserRoleType))
            {
                $this->set_response([
                    'status' => 1,
                    'message' => 'Success',
                    'data'=>$allUserRoleType
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'User role type not found.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    public function addUserByRoleType_post()
    {
        $token  = $this->input->post('token');
        $Company_id  = $this->input->post('company_id');
        $UserName  = $this->input->post('name');
        $UserMobileNumber  = $this->input->post('mobile');
        $UserPassword  = $this->input->post('password');
        $UserEmailID  = $this->input->post('email');
        $UserRoleID  = $this->input->post('UserRoleID');
        $UserType  = $this->input->post('UserType');
       
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $insertData = array(
                'UserType'=>$UserType,
                'UserName'=>$UserName,
                'UserMobileNumber'=>$UserMobileNumber,
                'UserEmailID'=>$UserEmailID,
                'UserPassword'=>md5($UserPassword),
                'OTPVerified'=>1,
                'EmailVerified'=>1,
                'Status'=>1,
                'Created_on'=>date('Y-m-d H:i:s')
            );
            $insertId = $this->api_model->insert_all('ff_userlogindetails',$insertData);

            if($insertId > 0)
            {
                //insert data into ff_companyusers
                $insertDataCompanyUsers = array(
                    'User_id'=>$insertId,
                    'Company_id'=>$Company_id,
                    'UserRoleID'=>$UserRoleID
                );
                $insert_id = $this->api_model->insert_all('ff_companyusers',$insertDataCompanyUsers);
                if($insert_id > 0)
                {
                     $this->set_response([
                    'status' => 1,
                    'message' => 'User added.'
                    ], REST_Controller::HTTP_OK);
                }
                else
                {
                    $this->set_response([
                    'status' => 0,
                    'message' => 'User not added.'
                    ], REST_Controller::HTTP_OK);
                }
               
            }
            else
            {
                $this->set_response([
                    'status' => 0,
                    'message' => 'User not added.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    //Anand check 08_11_2019
    // created by Pooja on 11_09_2019 for buyer registration
    /*public function sendOTP_post(){
        $mobile = $this->input->post('mobile');
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $table2 = 'buyers';
            $where = array('mobile'=> $mobile);
            $checkMobile = $this->api_model->select($table2,$where);

            if(!empty($checkMobile)){
                $this->set_response([
                    'status' => 0,
                    'message' => 'Mobile number exists'
                ], REST_Controller::HTTP_OK);
            }else{
                $table2 = 'users';
                $where = array('mobile'=> $mobile);
                $checkMobileforSeller = $this->api_model->select($table2,$where);
                if($checkMobileforSeller != ''){
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Mobile number exists'
                    ], REST_Controller::HTTP_OK);
                }else{
                    $table = 'otp_table';
                    $otp = mt_rand(1000, 9999);
                    $data = array(
                        'mobile' => $mobile,
                        'otp'   => $otp
                    );

                    $res = $this->api_model->checkMobileExists($mobile);
                    
                    if($res){
                        $where = array('mobile'=>$mobile);
                        $otp_data = array('otp' => $otp);
                        $response = $this->api_model->update($table, $where, $otp_data);
                        
                    }else{
                       $response = $this->api_model->insert($table,$data);  
                    }
                    if($response == 1){

                        //for sms gateway
                        $message = "Your OTP is :". $otp;
                        $smsGatewayUrl="http://www.bulksmsshortcode.com/sendsmsv2.asp?user=gooiipune&password=gooiipune&sender=OIIMSG&sendercdma=919860609000&text=".urlencode($message)."&PhoneNumber=91".trim($mobile)."&track=1";
                        
                        $url = $smsGatewayUrl;
                        
                        $ch = curl_init();
                        
                        curl_setopt($ch, CURLOPT_POST, false);
                        
                        curl_setopt($ch, CURLOPT_URL, $url);
                        
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                        
                        $output = curl_exec($ch);
                        
                        curl_close($ch);

                        $where = array('mobile'=> $mobile);
                        $data2 = $this->db->select($table, $where);
                        if($data2){
                            $this->set_response([
                                'status' => 1,
                                'message' => 'success',
                                'data'    => $data
                            ], REST_Controller::HTTP_OK);
                        }else{
                            $this->set_response([
                                'status' => 0,
                                'message' => 'Record could not be saved'
                            ], REST_Controller::HTTP_OK);
                        }
                        
                    }else{
                        $this->set_response([
                            'status' => 0,
                            'message' => 'Something went wrong'
                        ], REST_Controller::HTTP_OK);
                    } 
                }
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
        
        // $res = $this->api_model->checkMobileExists($mobile);

        
       
    }*/

    //created by Pooja on 13_09_2019 to verify otp
   /* public function verifyOTP_post(){
        $mobile = $this->input->post('mobile');
        $otp = $this->input->post('otp');
        $token  = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);
        if($checkToken == 1){
            if($mobile != '' && $otp != ''){
                $table = 'otp_table';
                $where = array('mobile'=> $mobile, 'otp'=> $otp);

                $result = $this->api_model->select($table, $where);
                if(!empty($result)){
                    $this->set_response([
                        'status' => 1,
                        'message' => 'OTP verified'
                    ], REST_Controller::HTTP_OK);
                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'OTP does not verified'
                    ], REST_Controller::HTTP_OK);
                }
            }else{
                $this->set_response([
                    'status' => 0,
                    'message' => 'Please enter mobile and otp'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }*/

    //created by Pooja on 13_09_2019
    /*public function buyerRegistration_post(){

        $mobile           = $this->input->post('mobile');
        $buyer_name       = $this->input->post('buyer_name');
        $company_name     = $this->input->post('company_name');
        $alternate_mobile = $this->input->post('alternate_mobile');
        $email            = $this->input->post('email');
        $password         = $this->input->post('password');
        $buyer_address    = $this->input->post('buyer_address');
        $city_id          = $this->input->post('city_id');
        $town             = $this->input->post('town');
        $landmark         = $this->input->post('landmark');
        $pincode          = $this->input->post('pincode');
        $state_id         = $this->input->post('state_id');
        $gst_number       = $this->input->post('gst_number');
        $token            = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            if($email != '' && $password != ''){
                $table = 'buyers';
                $where = array('mobile' => $mobile); 
                // $or_where = array('mobile'=> $mobile);
                $result = $this->api_model->select($table, $where);

                if($result == ''){ 
                    $where = array('email'=> $email);
                    $result2 = $this->api_model->select($table, $where);
                    if($result2 == ''){
                        $data = array(
                            'mobile'           => $mobile,
                            'buyer_name'       => $buyer_name,
                            'company_name'     => $company_name, 
                            'alternate_mobile' => $alternate_mobile,
                            'email'            => $email, 
                            'password'         => $password,
                            'buyer_address'    => $buyer_address,
                            'city_id'          => $city_id,
                            'town'             => $town,
                            'landmark'         => $landmark,
                            'pincode'          => $pincode,
                            'state_id'         => $state_id,
                            'gst_number'       => $gst_number
                        );

                        $response = $this->api_model->insert($table,$data);
                        if($response == 1){
                            $this->set_response([
                                'status' => 1,
                                'message' => 'Registraion done successfully'
                            ], REST_Controller::HTTP_OK);
                        }else{
                            $this->set_response([
                                'status' => 0,
                                'message' => 'Registraion not done'
                            ], REST_Controller::HTTP_OK);
                        }
                    }else{
                        $this->set_response([
                            'status' => 0,
                            'message' => 'Email id already exist'
                        ], REST_Controller::HTTP_OK);
                    }

                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Mobile already exists'
                    ], REST_Controller::HTTP_OK);
                }
            }else{
                $this->set_response([
                    'status' => 0,
                    'message' => 'Something went to wrong'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
        
    }*/

    //created by Pooja on 13_09_2019 for buyer login
 /*   public function login_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $mobile = $this->input->post('mobile');
            $password = $this->input->post('password');
            if($mobile != '' && $password != ''){
                $table = 'buyers';
                $where = array('mobile'=> $mobile, 'password'=> $password);
                $result = $this->api_model->select($table, $where);
                if($result != ''){
                    $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                        ], REST_Controller::HTTP_OK);
                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                    ], REST_Controller::HTTP_OK);
                }
            }else{
                $this->set_response([
                    'status' => 0,
                    'message' => 'Enter mobile and password'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }

    }*/

    // Modified By Ganesh 30/10/2019
/*     public function login_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $mobile = $this->input->post('mobile');
            $password = $this->input->post('password');
            if($mobile != '' && $password != ''){
                $table = 'buyers';
                $where = array('mobile'=> $mobile, 'password'=> $password);
                $result = $this->api_model->select($table, $where);

                $table1 = 'users';
                $where1 = array('user_mobile'=> $mobile, 'user_password'=> $password);
                $result1 = $this->api_model->select($table1, $where1);

                $sellerarray = array();
                if($result != ''){
                    $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                        ], REST_Controller::HTTP_OK);
                }
                elseif($result1!=''){
                    $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result1
                        ], REST_Controller::HTTP_OK);
                }
                else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                    ], REST_Controller::HTTP_OK);
                }
            }else{
                $this->set_response([
                    'status' => 0,
                    'message' => 'Enter mobile and password'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }

    }*/

    //created by Pooja on 13_09_2019
    public function sellerRegistration_post(){
        $user_mobile     = $this->input->post('user_mobile');
        $user_name       = $this->input->post('user_name');
        $user_alt_mobile = $this->input->post('user_alt_mobile');
        $user_email      = $this->input->post('user_email');
        $user_password   = $this->input->post('user_password');
        $user_address    = $this->input->post('user_address');
        $user_city_id    = $this->input->post('user_city_id');
        $user_town       = $this->input->post('user_town');
        $user_landmark   = $this->input->post('user_landmark');
        $user_pincode    = $this->input->post('user_pincode');
        $user_state_id   = $this->input->post('user_state_id');
        $token           = $this->input->post('token');

        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            if($user_email != '' && $user_password != ''){
                $table = 'users';
                $where = array('user_mobile' => $user_mobile); 
                // $or_where = array('mobile'=> $mobile);
                $result = $this->api_model->select($table, $where);

                if($result == ''){ 
                    $where = array('user_email'=> $user_email);
                    $result2 = $this->api_model->select($table, $where);
                    if($result2 == ''){
                        $data = array(
                            'user_mobile'     => $user_mobile,
                            'user_name'       => $user_name,
                            'user_alt_mobile' => $user_alt_mobile,
                            'user_email'      => $user_email, 
                            'user_password'   => $user_password,
                            'user_address'    => $user_address,
                            'user_city_id'    => $user_city_id,
                            'user_town'       => $user_town,
                            'user_landmark'   => $user_landmark,
                            'user_pincode'    => $user_pincode,
                            'user_state_id'   => $user_state_id,
                        );

                        $response = $this->api_model->insert($table,$data);
                        if($response == 1){

                            $this->set_response([
                                'status' => 1,
                                'message' => 'Registraion done successfully'
                            ], REST_Controller::HTTP_OK);
                        }else{
                            $this->set_response([
                                'status' => 0,
                                'message' => 'Registraion not done'
                            ], REST_Controller::HTTP_OK);
                        }
                    }else{
                        $this->set_response([
                            'status' => 0,
                            'message' => 'Email id already exist'
                        ], REST_Controller::HTTP_OK);
                    }

                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Mobile already exists'
                    ], REST_Controller::HTTP_OK);
                }
            }else{
                $this->set_response([
                    'status' => 0,
                    'message' => 'Something went to wrong'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
        
    }

    //Created By Ganesh 30/10/2019
    public function getBuyerProfileDetails_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);
        if($checkToken == 1){
            $userid = $this->input->post('userid');
            $table = 'buyers';
            $where = array('buyer_id'=> $userid);
            $result = $this->api_model->selectbuyerprofile($table, $where);
            if(!empty($result)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }
        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }

    //Created By Ganesh 30/10/2019
    public function getSellerProfileDetails_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);
        if($checkToken == 1){
            $userid = $this->input->post('userid');
            $table = 'users';
            $where = array('user_id'=> $userid);
            $result = $this->api_model->selectsellerprofile($table, $where);
            if(!empty($result)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }
        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }

    public function getCategoryList_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $table = 'manage_resources_category';
            $where = array('status_choice'=>'true','is_deleted'=>'0');
            $result = $this->api_model->selectCategory($table, $where);
            if(!empty($result)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }

        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }

    }

    /*public function getCategoryListById_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);
        $category_url = base_url().'uploads/category/';

        if($checkToken == 1){
            $categoryId = $this->input->post('categoryid');
            $table = 'manage_resources_category';
            $where = array('status_choice'=>'true','is_deleted'=>'0','id'=>$categoryId);
            $result = $this->api_model->getCategoryListById($table, $where);
            for ($i=0; $i <count($result); $i++) { 
               $result[$i]->categoryImage = $category_url.$result[$i]->category_image;
            }
            if(!empty($result)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }

        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        } 
    }*/
    //Anand check 05_11_2019
   /* public function getProductListByCategoryId_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);
        $category_url = base_url().'uploads/category/';

        if($checkToken == 1){
            $categoryId = $this->input->post('categoryid');
            $table = 'manage_resources_product';
            $table2 = 'manage_resources_category';
            $joinOn = 'manage_resources_product.product_category=manage_resources_category.id';
            $where = array('manage_resources_product.status_choice'=>'true','manage_resources_product.is_deleted'=>'0','product_category'=>$categoryId);
            $result = $this->api_model->getProductListByCategoryId($table, $table2, $joinOn, $where);
            echo "<pre>";
            print_r($result);
            exit();
            for ($i=0; $i <count($result); $i++) 
            { 
               $result[$i]->unit_name = $category_url.$result[$i]->category_image;
            }
            if(!empty($result)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }

        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        } 
    }*/
    //Anand check 05_11_2019


    public function getProductDetailsById_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);
        $product_url = base_url().'uploads/resource_product/';

        if($checkToken == 1){
            $productid = $this->input->post('productid');
            $table1 = 'manage_resources_product';
            $table2 = 'manage_resources_unit';
            $on = 'manage_resources_unit.id = manage_resources_product.unit_fk_key';
            $where = array('manage_resources_product.status_choice'=>'true','manage_resources_product.is_deleted'=>'0','manage_resources_product.id'=>$productid);
            $result = $this->api_model->getProductDetailsById($table1,$table2,$on,$where);
            for ($i=0; $i <count($result); $i++) { 
               $result[$i]->productImage = $product_url.$result[$i]->product_image;
            }
            if(!empty($result)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }

        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        } 
    }

    /*public function getProductList_post(){
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $table = 'manage_resources_product';
            $where = array('status_choice'=>'true','is_deleted'=>'0');
            $result = $this->api_model->selectProduct($table, $where);
            if(!empty($result)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }

        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }

    }*/
    //Anand check 13_11_2019
    public function getProductList_post()
    {
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);
        $subcategoryid = $this->input->post('subcategoryid');
        $user_type = $this->input->post('user_type');
        $product_url = base_url().'uploads/resource_product/';

        if($checkToken == 1)
        {
            $table = 'manage_resources_product';

            $where = array('sub_category_fk_id'=>$subcategoryid,'status_choice'=>'true','is_deleted'=>'0');
            $result = $this->api_model->selectProduct($table, $where);

            for ($i=0; $i <count($result); $i++) 
            {
                $result[$i]->productImage = $product_url.$result[$i]->product_image;
                
                if(!empty($result[$i]->product_varient))
                {
                    $result[$i]->product_varient = $result[$i]->product_varient;
                }
                else
                {
                    $result[$i]->product_varient = '';
                }
                
                if(!empty($result[$i]->product_varient))
                {
                    $result[$i]->variant_status = '1';
                }
                else
                {
                    $result[$i]->variant_status = '0';
                }

                $vskucode = '';
                if(!empty($result[$i]->id))
                {
                    $variantData = $this->api_model->selectRowByWhere('ff_prouduct_variant',array('product_id'=>$result[$i]->id));
                    if(!empty($variantData))
                    {
                        $vskucode = $variantData->product_sku_code;
                    }
                }
                $result[$i]->VSKU_code = $vskucode;

                //Anand check 14_11_2019
                $product_unit = '';
                if(!empty($result[$i]->unit_fk_key))
                {
                    $unitData = $this->api_model->selectRowByWhere('manage_resources_unit',array('id'=>$result[$i]->unit_fk_key));
                    if(!empty($unitData))
                    {
                        $product_unit = $unitData->unit_name;
                    }
                }
                $result[$i]->product_unit = $product_unit;
                //Anand check 14_11_2019
                
                if($user_type =='1')
                {
                    $table1 = 'ff_pricing_selling';
                    $where1 = array('product_id'=>$result[$i]->id);
                    $res = $this->api_model->selectPricing($table1, $where1);
                    foreach ($res->result() as $rows) {
                        if($rows){
                            $result[$i]->productprice = $rows->sell_price;
                        }
                    }
                    
                }
                else
                {
                    $table2 = 'ff_pricing_buying';
                    $where2 = array('product_id'=>$result[$i]->id);
                    $res1 = $this->api_model->selectPricing($table2, $where2);
                    foreach ($res1->result() as $row) {
                        if($row){
                            $result[$i]->productprice = $row->buy_price;
                        }
                    }
                }

            }
            if(!empty($result))
            {
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $result
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }
        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }

    }
    public function generateCart()
    {
        mt_srand((double)microtime()*10000);
        $charid = md5(uniqid(rand(), true));
        $c = unpack("C*",$charid);
        $c = implode("",$c);
        return substr($c,0,5);
    }
    public function addToCart_post()
    {
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $company_id = $this->input->post('company_id');
        $product_id = $this->input->post('product_id');
        $sku_code = $this->input->post('sku_code');
        $vsku_code = $this->input->post('vsku_code');
        $product_name = $this->input->post('product_name');
        $variant_name = $this->input->post('variant_name');
        $qty = $this->input->post('qty');
        $product_qty_count = $this->input->post('product_qty_count');
        $unit = $this->input->post('unit');
        $price = $this->input->post('price');
        
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $isCartExist = $this->api_model->selectRowByWhere('ff_tempcart',array('CompanyID'=>$company_id,'UserID'=>$user_id));
            if(empty($isCartExist))
            {
                $insertData = array(
                    'CartID'=>'CART'.$this->generateCart(),//generate for user and company if not generate
                    'CompanyID'=>$company_id,
                    'UserID'=>$user_id,
                    'Created_on'=>date('Y-m-d H:i:s')
                );
                $insertId = $this->api_model->insert_all('ff_tempcart',$insertData);
                if($insertId > 0)
                {
                    $isProductExist = $this->api_model->selectRowByWhere('ff_tempcartitem',array('Product_id'=>$product_id,'Company_id'=>$company_id));
                    if(empty($isProductExist))
                    {
                        //add data into ff_TempCartItem
                        $insertDataForItem = array(
                            'CartID'=>$insertId,
                            'Company_id'=>$company_id,
                            'SKUCode'=>$sku_code,
                            'VSKUCode'=>$vsku_code,
                            'Product_id'=>$product_id,
                            'ProductName'=>$product_name,
                            'VariantName'=>$variant_name,
                            'Qty'=>$qty,
                            'Product_Qty_Count'=>$product_qty_count,
                            'Unit'=>$unit,
                            'Price'=>$price,
                            'Created_on'=>date('Y-m-d H:i:s')
                        );
                        $insertId_Item = $this->api_model->insert_all('ff_tempcartitem',$insertDataForItem);
                        if($insertId_Item > 0)
                        {
                            $this->set_response([
                                'status' => 1,
                                'message' => 'Product added in cart.'
                            ], REST_Controller::HTTP_OK);
                        }
                        else
                        {
                            $this->set_response([
                            'status' => 0,
                            'message' => 'Product not added in cart.'
                            ], REST_Controller::HTTP_OK);
                        }
                    }
                    else
                    {
                        $this->set_response([
                        'status' => 0,
                        'message' => 'Product already added in cart.',
                        'temp_cart_item_id'=>$isProductExist->ID
                        ], REST_Controller::HTTP_OK);
                    }
                }
            }
            else
            {
               $isProductExist = $this->api_model->selectRowByWhere('ff_tempcartitem',array('Product_id'=>$product_id,'Company_id'=>$company_id));
                if(empty($isProductExist))
                {
                    //add data into ff_TempCartItem
                    $insertDataForItem = array(
                        'CartID'=>$isCartExist->ID,
                        'Company_id'=>$company_id,
                        'SKUCode'=>$sku_code,
                        'VSKUCode'=>$vsku_code,
                        'Product_id'=>$product_id,
                        'ProductName'=>$product_name,
                        'VariantName'=>$variant_name,
                        'Qty'=>$qty,
                        'Product_Qty_Count'=>$product_qty_count,
                        'Unit'=>$unit,
                        'Price'=>$price,
                        'Created_on'=>date('Y-m-d H:i:s')
                    );
                    $insertId_Item = $this->api_model->insert_all('ff_tempcartitem',$insertDataForItem);
                    if($insertId_Item > 0)
                    {
                        $this->set_response([
                            'status' => 1,
                            'message' => 'Product added in cart.'
                        ], REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $this->set_response([
                        'status' => 0,
                        'message' => 'Product not added in cart.'
                        ], REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Product already added in cart.',
                        'temp_cart_item_id'=>$isProductExist->ID
                    ], REST_Controller::HTTP_OK);
                }
            }
        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }

    }
    public function getAllCartItems_post()
    {
        $token = $this->input->post('token');
        $user_id = $this->input->post('user_id');
        $company_id = $this->input->post('company_id');
        
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            $cartData = $this->api_model->getAllCartItems($user_id,$company_id);

            if(!empty($cartData))
            {
                
                $product_url = base_url().'uploads/resource_product/';
                for ($i=0; $i < count($cartData) ; $i++) 
                { 
                    $productImageData = $this->api_model->selectRowByWhere('manage_resources_product',array('id'=>$cartData[$i]->Product_id));
                    if(!empty($productImageData->product_image))
                    {
                        $product_image =  $productImageData->product_image;
                        $cartData[$i]->product_image = $product_url.$product_image;
                    }
                    else
                    {
                        $cartData[$i]->product_image = '';
                    }
                    
                }
                
                $this->set_response([
                    'status' => 1,
                    'message' => 'Success',
                    'data'=>$cartData
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                'status' => 0,
                'message' => 'No item found.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    //Anand check 13_11_2019


    //Anand check 14_11_2019
    public function generateNextOrderID($user_type)
    {
        $getMaxId = $this->api_model->selectMaxId($user_type);
    
        if(!empty($getMaxId))
        {
            //split order id
            $last4digit = substr ($getMaxId, -4);
            $number = str_pad($last4digit+1, 4, "0", STR_PAD_LEFT);
        }
        else
        {
            $input = 1;
            $number = str_pad($input, 4, "0", STR_PAD_LEFT);
        }
        return $number;
    }
    public function placeOrder_post()
    {
        $token = $this->input->post('token');
        $user_type = $this->input->post('user_type'); //1=buyer,2=seller
        $CartID = $this->input->post('CartID');
        $company_id = $this->input->post('company_id');
        $order_type = $this->input->post('order_type');//1=priority_order,2=advanced_order
        $address = $this->input->post('address');
        $pincode = $this->input->post('pincode');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        $order_placed_by = $this->input->post('order_placed_by');//Name of the user logged in
        $order_total_item = $this->input->post('order_total_item');//No of item in the Cart while ordering
        $total_cost = $this->input->post('total_cost');//Total Cost of the Item before GST
        $payment_status = $this->input->post('payment_status');//PAID / COD / Credit
        $gst_amount = $this->input->post('gst_amount');//gst amount
        $dispatch_depot_id = $this->input->post('dispatch_depot_id');
        $warehouse_id = $this->input->post('warehouse_id');

        $product_ids = $this->input->post('product_ids');
        
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1)
        {
            if(!empty($payment_status))
            {
                if($user_type == 1)//1=buyer,2=seller
                {
                    $last4digit = $this->generateNextOrderID($user_type);
                    //insert data into ff_buyorder
                    $orderId = 'BOID'.date('Ymd').$last4digit;
                    $insertOrderData = array(
                        'OrderID'=>$orderId,//BOID201911130001 if its Selll Order it will start with S
                        
                        'CompanyID'=>$company_id,
                        'Address'=>$address,
                        'Pincode'=>$pincode,
                        'City'=>$city,
                        'State'=>$state,
                        'Locationmapid'=>$latitude.','.$longitude,
                        'OrderPlacedBy'=>$order_placed_by,
                        'DateTime'=>date('Y-m-d H:i:s'),
                        'OrderStatus'=>'pending',
                        'OrderType'=>$order_type,
                        'OrderTotalItem'=>$order_total_item,
                        'TotalCost'=>$total_cost,
                        'GSTAmount'=>$gst_amount,
                        'PaymentStatus'=>$payment_status,
                        'DispatchedCenter'=>$dispatch_depot_id,
                    );
                    $insert_id = $this->api_model->insert_all('ff_buyorder',$insertOrderData);
                    $getMaxId = $this->api_model->selectMaxId($user_type);

                    if(!empty($getMaxId))
                    {
                        //insert data into ff_buyorderitem
                        $productArray = explode(',', $product_ids);

                        for ($i=0; $i < count(array_values(array_filter($productArray))) ; $i++) 
                        { 
                            //get product data by product id and company id
                            $productData = $this->api_model->selectRowByWhere('ff_tempcartitem',array('CartID'=>$CartID,'Company_id'=>$company_id,'Product_id'=>$productArray[$i]));
                           
                            if(!empty($productData))
                            {
                                $insertProductData = array(
                                'OrderID'=>$getMaxId,
                                'SKUCode'=>$productData->SKUCode,
                                'VSKUCode'=>$productData->VSKUCode,
                                'ProductName'=>$productData->ProductName,
                                'VariantName'=>$productData->VariantName,
                                'Qty'=>$productData->Qty,
                                'Unit'=>$productData->Unit,
                                'Price'=>$productData->Price,
                                'Created_on'=>date('Y-m-d H:i:s'),
                                );
                                $insertId = $this->api_model->insert_all('ff_buyorderitem',$insertProductData);
                            }
                        }

                        if($insertId > 0)
                        {
                            //delete the cart items by CartID
                            for ($j=0; $j < count(array_values(array_filter($productArray))); $j++) 
                            { 
                                $deleteCartItem = $this->api_model->delete_all('ff_tempcartitem',array('CartID'=>$CartID,'Product_id'=>$productArray[$j],'Company_id'=>$company_id));
                            }
                            
                            $this->set_response([
                            'status' => 1,
                            'message' => 'Order Placed Successfully.',
                            ], REST_Controller::HTTP_OK);
                        }
                        else
                        {
                            $this->set_response([
                            'status' => 1,
                            'message' => 'Order Placed.'
                            ], REST_Controller::HTTP_OK);
                        }
                    }
                }
                elseif($user_type == 2) 
                {
                    $last4digit = $this->generateNextOrderID($user_type);
                    $orderId = 'SOID'.date('Ymd').$last4digit;
                    //insert data into ff_buyorder
                    $insertOrderData = array(
                        'OrderID'=>$orderId,//SOID201911130001 
                        
                        'CompanyID'=>$company_id,
                        'Address'=>$address,
                        'Pincode'=>$pincode,
                        'City'=>$city,
                        'State'=>$state,
                        'Locationmapid'=>$latitude.','.$longitude,
                        'OrderPlacedBy'=>$order_placed_by,
                        'DateTime'=>date('Y-m-d H:i:s'),
                        'OrderStatus'=>'pending',
                        'OrderType'=>$order_type,
                        'OrderTotalItem'=>$order_total_item,
                        'TotalCost'=>$total_cost,
                        'GSTAmount'=>$gst_amount,
                        'PaymentStatus'=>$payment_status,
                        'DispatchedCenter'=>$dispatch_depot_id,
                    );
                    $insert_id = $this->api_model->insert_all('ff_buyorder',$insertOrderData);
                    $getMaxId = $this->api_model->selectMaxId($user_type);
                    
                    if(!empty($getMaxId))
                    {
                        //insert data into ff_buyorderitem
                        $productArray = explode(',', $product_ids);

                        for ($i=0; $i < count(array_values(array_filter($productArray))) ; $i++) 
                        { 
                            //get product data by product id and company id
                            $productData = $this->api_model->selectRowByWhere('ff_tempcartitem',array('CartID'=>$CartID,'Company_id'=>$company_id,'Product_id'=>$productArray[$i]));
                          
                            if(!empty($productData))
                            {
                                $insertProductData = array(
                                'OrderID'=>$getMaxId,
                                'SKUCode'=>$productData->SKUCode,
                                'VSKUCode'=>$productData->VSKUCode,
                                'ProductName'=>$productData->ProductName,
                                'VariantName'=>$productData->VariantName,
                                'Qty'=>$productData->Qty,
                                'Unit'=>$productData->Unit,
                                'Price'=>$productData->Price,
                                'Created_on'=>date('Y-m-d H:i:s'),
                                );
                                $insertId = $this->api_model->insert_all('ff_buyorderitem',$insertProductData);
                            }
                        }

                        if($insertId > 0)
                        {
                            //delete the cart items by CartID
                            for ($j=0; $j < count(array_values(array_filter($productArray))); $j++) 
                            { 
                                $deleteCartItem = $this->api_model->delete_all('ff_tempcartitem',array('CartID'=>$CartID,'Product_id'=>$productArray[$j],'Company_id'=>$company_id));
                            }

                            $this->set_response([
                            'status' => 1,
                            'message' => 'Order Placed Successfully.',
                            ], REST_Controller::HTTP_OK);
                        }
                        else
                        {
                            $this->set_response([
                            'status' => 1,
                            'message' => 'Order Placed.'
                            ], REST_Controller::HTTP_OK);
                        }
                    }
                }
            }
            else
            {
                $this->set_response([
                'status' => 0,
                'message' => 'Please select payment type.'
                ], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }
    //Anand check 14_11_2019


   /* public function updateProfile_post(){
        $user_id       = $this->input->post('user_id');
        $user_name       = $this->input->post('name');
        $mobile       = $this->input->post('mobile');
        $user_alt_mobile = $this->input->post('alternative_mobile');
        $user_email      = $this->input->post('email');
        $user_address    = $this->input->post('address');
        $state_id    = $this->input->post('state_id');
        $city_id    = $this->input->post('city_id');
        $town    = $this->input->post('town');
        $landmark    = $this->input->post('landmark');
        $pincode    = $this->input->post('pincode');
        $token = $this->input->post('token');
        $checkToken = $this->api_model->checkToken($token);

        if($checkToken == 1){
            $table ='users';
            $data = array(
                            'user_mobile'     => $mobile,
                            'user_name'       => $user_name,
                            'user_alt_mobile' => $user_alt_mobile,
                            'user_email'      => $user_email, 
                            'user_address'    => $user_address,
                            'user_city_id'    => $city_id,
                            'user_town'       => $town,
                            'user_landmark'   => $landmark,
                            'user_pincode'    => $pincode,
                            'user_state_id'   => $state_id,
                        );

            $where = array('user_id'=>$user_id);

            $response = $this->api_model->update($table,$where,$data);
            if(!empty($response)){
                $this->set_response([
                            'status' => 1,
                            'message' => 'success',
                            'data' => $response
                ], REST_Controller::HTTP_OK);
            }
             else{
                $this->set_response([
                        'status' => 0,
                        'message' => 'something went to wrong'
                ], REST_Controller::HTTP_OK);
            }
        }
        else{
            $this->set_response([
                'status' => 0,
                'message' => 'Unauthorized request'
            ], REST_Controller::HTTP_OK);
        }
    }*/

}