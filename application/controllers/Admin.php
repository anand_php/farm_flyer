<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('role_model');
		$this->load->model('dashboard_model');
		$this->load->model('warehouse_model');
		$this->load->model('cold_storage_model');
		$this->load->model('dispatch_depot_model');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['admins'] = $this->admin_model->getAllAdmins();
		$this->data['roles'] = $this->role_model->getAllRoles();
		//to load sidebar accoding to permissions
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}

		// $this->load->view('common/header');
		$this->load->view('admin/admin',$this->data);
	}

	//created by Pooja on 27_09_2019
	public function add_admin(){
		$this->data['admins'] = $this->admin_model->getAllAdmins();
		$this->data['roles'] = $this->role_model->getAllRoles();
		
		//to load sidebar accoding to permissions
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}

		$this->load->view('admin/add_admin',$this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result = $this->admin_model->add();
		echo json_encode($result);
	}

	public function delete(){	
		$id = $this->uri->segment(3);
		$result = $this->admin_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully!");
			redirect("admin");
		}
	}

	public function edit(){
		$id = $this->uri->segment(3);
		$this->data['admin'] = $this->admin_model->getAdminById($id);
		$this->data['roles'] = $this->role_model->getAllRoles();
		
		//to load sidebar accoding to permissions
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}

		$this->load->view('admin/edit_admin',$this->data);
	}

	public function update(){
		$this->session->set_flashdata('message',"Record Updated successfully!");
		$result = $this->admin_model->update();
		echo json_encode($result);
	}

	public function getBrandById(){
		$id= $_POST['id'];
		$result = $this->admin_model->getBrandById($id);
		echo json_encode($result);
	}

	public function checkActivate(){
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$id = $_POST['id'];
    	$result = $this->admin_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$username = $_POST['username'];
    	$result = $this->admin_model->checkExists($username);
    	echo json_encode($result);
    }

    public function view(){
		$id = $this->uri->segment(3);
		$this->data['admin'] = $this->admin_model->getAdminById($id);
		//to get warehouse names
		if($this->data['admin']->warehouse_id == ''){
			
		}else{
			$warehouses = explode(',', $this->data['admin']->warehouse_id);
			foreach($warehouses as $row){
				$table = "warehouse";
				$ware = $this->admin_model->getLocationByID($table,$row);
				$this->data['admin']->warehouse .= $ware->warehouse_name.", ";
				
			}
		}

		//to get cold storages
		if($this->data['admin']->cold_storage_id == ''){
			
		}else{
			$storages = explode(',', $this->data['admin']->cold_storage_id);
			foreach($warehouses as $row){
				$ware = $this->admin_model->getStorageByID($row);
				$this->data['admin']->cold_storage .= $ware->name.", ";
				
			}
		}

		//to get dispatch depots name
		if($this->data['admin']->dispatch_depot_id == ''){
			
		}else{
			$depots = explode(',', $this->data['admin']->dispatch_depot_id);
			foreach($depots as $row){
				$table = "dispatch_depot";
				$depo = $this->admin_model->getLocationByID($table,$row);
				$this->data['admin']->dispatch_depot .= $depo->dispatch_depot_name.",  ";
				
			}
		}
		

		//to load sidebar accoding to permissions
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
					}
					
				}
			}
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('admin/view_admin',$this->data);
	}

	//created by Pooja on 16_09_2019 
	public function assign_locations(){
		$id = $this->uri->segment(3);

		$this->data['subadmin'] = $this->admin_model->getAdminById($id);
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		$this->data['storages'] = $this->cold_storage_model->getAllStorages();
		$this->data['dispatch_depots'] = $this->dispatch_depot_model->getAllDispathDepots();

		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}

		$this->load->view('admin/assign_locations', $this->data);
	}

	//created by Pooja on 16_09_2019
	public function assign(){
		$result = $this->admin_model->assign();
		if($result == 1){
			$this->session->set_flashdata('message',"Locations assigned Successfully!");
			redirect('admin');
		}
	}
}
