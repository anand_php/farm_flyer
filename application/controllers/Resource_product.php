<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resource_product extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('resource_product_model');
		$this->load->model('category_model');
		$this->load->model('brand_model');
		$this->load->model('units_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['resourc_products'] = $this->resource_product_model->getAllResourceProducts();
		$this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['brands'] = $this->brand_model->getAllBrands();
		$this->data['units'] = $this->units_model->getAllUnits();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		// $this->load->view('category/category', $this->data);
		$this->load->view('resource_product/resource_product', $this->data);
	}

	public function add(){
		
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result =  $this->resource_product_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$this->session->set_flashdata('message',"Record Deleted successfully!");

		$id = $_POST['id'];

		$result = $this->resource_product_model->delete($id);
		echo json_encode($result);
	}

	public function edit(){
        $id = $this->uri->segment(3);   
        $this->data['resource_product'] = $this->resource_product_model->getResourceProductById($id);
        $this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['brands'] = $this->brand_model->getAllBrands();
		$this->data['units'] = $this->units_model->getAllUnits();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('resource_product/edit_resource_product', $this->data);
    }

    public function update(){
    	$this->session->set_flashdata('message',"Record Updated successfully!"); 
    	$result = $this->resource_product_model->update();
    	echo json_encode($result);
    }

    public function checkActivate(){
    	
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$id = $_POST['id'];
    	$result = $this->resource_product_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$category_name = $_POST['category_name'];
    	$result = $this->resource_product_model->checkExists($category_name);
    	echo json_encode($result);
    }

    public function getSubcategoriesByCategory(){
    	$product_category = $_POST['product_category'];
    	$result = $this->resource_product_model->getSubcategoriesByCategory($product_category);
    	echo json_encode($result);
    }

    public function view(){
    	$id = $this->uri->segment(3);   
        $this->data['resource_product'] = $this->resource_product_model->getResourceProductById($id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('resource_product/view_product', $this->data);
    }
}
