<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('warehouse_model');
		$this->load->model('service_state_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		}else{
			$warehouses = array();
			$warehouses2 = $this->warehouse_model->getParticularUserWarehouses();
			if($warehouses2->warehouse_id != ''){
				$warehouse_id = explode(',', $warehouses2->warehouse_id);
				foreach($warehouse_id as $row){
					$ware = $this->warehouse_model->getWarehouseById($row);
					 array_push($warehouses, $ware);
				}
			}
			$this->data['warehouses'] = $warehouses;
		}

		$this->data['cities'] = $this->warehouse_model->getAllCities();
		$this->data['states'] = $this->service_state_model->getAllStates();

		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('warehouse/warehouse',$this->data);
	}

	//created by Pooja 27_09_2019
	public function add_warehouse(){

		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		}else{
			$warehouses = array();
			$warehouses2 = $this->warehouse_model->getParticularUserWarehouses();
			if($warehouses2->warehouse_id != ''){
				$warehouse_id = explode(',', $warehouses2->warehouse_id);
				foreach($warehouse_id as $row){
					$ware = $this->warehouse_model->getWarehouseById($row);
					 array_push($warehouses, $ware);
				}
			}
			$this->data['warehouses'] = $warehouses;
		}

		$this->data['cities'] = $this->warehouse_model->getAllCities();
		$this->data['states'] = $this->service_state_model->getAllStates();

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('warehouse/add_warehouse', $this->data);
	}

	public function add(){
		$result = $this->warehouse_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result = $this->warehouse_model->delete($id);
		if($result == 1){
			redirect("warehouse");
		}
	}

	public function checkActivate(){
		$id = $_POST['id'];
		$result = $this->warehouse_model->checkActivate($id);
		echo json_encode($result);
	}

	public function edit(){
		$id = $this->uri->segment(3);
		$this->data['warehouse'] = $this->warehouse_model->getWarehouseById($id);
		$this->data['cities'] = $this->warehouse_model->getAllCities();
		$this->data['states'] = $this->service_state_model->getAllStates();
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('warehouse/edit_warehouse',$this->data);
	}

	public function update(){
		$result = $this->warehouse_model->update();
		echo json_encode($result);
	}

	public function checkExists(){
		$warehouse_code = $_POST['warehouse_code'];
		$result = $this->warehouse_model->checkExists($warehouse_code);
		echo json_encode($result);
	}

	public function getCityByStateId(){
		$state_id = $_POST['state_id'];
    	$result = $this->warehouse_model->getCityByStateId($state_id);
    	echo json_encode($result);
	}

	public function view(){
		$id = $this->uri->segment(3);
		$this->data['warehouse'] = $this->warehouse_model->getWarehouseById($id);
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('warehouse/view_warehouse',$this->data);
	}
}
