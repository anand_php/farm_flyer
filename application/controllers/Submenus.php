<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submenus extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('submenus_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['menus'] = $this->submenus_model->getAllMenus();
		$this->data['submenus'] = $this->submenus_model->getAllSubmenus();
		//to load sidebar accoding to permissions
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}

		$this->load->view('submenus/submenus',$this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully.");
		$result = $this->submenus_model->add();
		echo json_encode($result);
	}

	public function delete(){	
		$id = $this->uri->segment(3);
		$result = $this->submenus_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully.");
			redirect("submenus");
		}
	}

	public function edit(){
		$id = $this->uri->segment(3);
		$this->data['menus'] = $this->submenus_model->getAllMenus();
		$this->data['submenu'] = $this->submenus_model->getSubmenuById($id);
		//to load sidebar accoding to permissions
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}

		$this->load->view('submenus/edit_submenus',$this->data);
	}

	public function update(){
		
		$result = $this->submenus_model->update();
		echo json_encode($result);
	}

	public function checkActivate(){
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully.");
    	$id = $_POST['id'];
    	$result = $this->submenus_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$submenu_name = $_POST['submenu_name'];
    	$result = $this->submenus_model->checkExists($submenu_name);
    	echo json_encode($result);
    }	

    public function view(){
    	$id = $this->uri->segment(3);
		$this->data['submenu'] = $this->submenus_model->getSubmenuById($id);
		//to load sidebar accoding to permissions
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}
		
		$this->load->view('submenus/view_submenus',$this->data);
    }
}
