<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('buyer_model');
		$this->load->model('dashboard_model');
		$this->load->model('service_state_model');
		$this->load->model('service_city_model');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	//Anand check 07_11_2019
	public function index()
	{
		//$this->data['buyers'] = $this->buyer_model->getAllBuyers();
		$this->data['buyers'] = $this->buyer_model->getAllCompanyBuyers();
	
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1)
		{ 
			$this->load->view('common/header');
		}
		else
		{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			
			for($i = 0; $i< count($menues); $i++)
			{
				$menu = $this->dashboard_model->getMenuById($menues[$i]);
				$this->data['permissions']->menues .= $menu->menu_name .",";


				$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
				
				if($submenu == '')
				{
					
				}
				else
				{
					for($j = 0; $j<count($submenu); $j++)
					{
						$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
					}	
				}
			}

		$this->load->view('common/header', $this->data);
		}
		//Anand check 13_11_2019
		for ($i=0; $i < count($this->data['buyers']) ; $i++) 
		{ 
			if(!empty($this->data['buyers'][$i]->account_manager_id))
			{
				$accountManager = $this->buyer_model->getAccountManagerData($this->data['buyers'][$i]->account_manager_id,$this->data['buyers'][$i]->ID);
			}
			$acName = isset($accountManager)?$accountManager->UserName:'';
			$this->data['buyers'][$i]->accountManagerName = $acName;
		}
		//Anand check 13_11_2019
		
		/*echo "<pre>";
		print_r($this->data);
		exit();*/
		$this->load->view('buyer/company_buyers',$this->data);
	}
	//Anand check 13_11_2019
	public function sellers()
	{
		$this->data['sellers'] = $this->buyer_model->getAllCompanySellers();
	
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1)
		{ 
			$this->load->view('common/header');
		}
		else
		{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			
			for($i = 0; $i< count($menues); $i++)
			{
				$menu = $this->dashboard_model->getMenuById($menues[$i]);
				$this->data['permissions']->menues .= $menu->menu_name .",";


				$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
				
				if($submenu == '')
				{
					
				}
				else
				{
					for($j = 0; $j<count($submenu); $j++)
					{
						$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
					}	
				}
			}

		$this->load->view('common/header', $this->data);
		}
		for ($i=0; $i < count($this->data['sellers']) ; $i++) 
		{ 
			if(!empty($this->data['sellers'][$i]->account_manager_id))
			{
				$accountManager = $this->buyer_model->getAccountManagerData($this->data['sellers'][$i]->account_manager_id,$this->data['sellers'][$i]->ID);
			}
			$acName = isset($accountManager)?$accountManager->UserName:'';
			$this->data['sellers'][$i]->accountManagerName = $acName;
		}
		/*echo "<pre>";
		print_r($this->data);
		exit();*/
		$this->load->view('sellers/company_sellers',$this->data);
	}
	public function viewSeller($id)
	{
		if($this->session->userdata("logged_in")["user_role"] == 1)
		{ 
			$this->load->view('common/header');
		}
		else
		{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			
			for($i = 0; $i< count($menues); $i++)
			{
				$menu = $this->dashboard_model->getMenuById($menues[$i]);
				$this->data['permissions']->menues .= $menu->menu_name .",";


				$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
				
				if($submenu == '')
				{
					
				}
				else
				{
					for($j = 0; $j<count($submenu); $j++)
					{
						$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
					}	
				}
			}
			$this->load->view('common/header', $this->data);
		}
		$this->data['buyerData'] = $this->buyer_model->getBuyerRecord($id);
		$this->data['buyers'] = $this->buyer_model->getAllUsersOfCompany($id);

		$this->data['allDeliveryAddressOfCompany'] = $this->buyer_model->selectAllByWhere('ff_comapnydeleveryaddress',array('company_id'=>$id));
		$this->data['accountManager'] = $this->buyer_model->getAccountManagers();
		
		//Get warehouse if have seller
		$this->data['warehouse'] = $this->buyer_model->getWarehouse('warehouse');

		if(!empty($this->data['buyerData']->warehouse_id))
		{
			$warehouse = $this->buyer_model->getWarehouseData($this->data['buyerData']->warehouse_id,$id);
		}
		$warehouse_name = isset($warehouse)?$warehouse->warehouse_name:'';
		$this->data['buyerData']->warehouse_name = $warehouse_name;

		if(!empty($this->data['buyerData']->account_manager_id))
		{
			$accountManager = $this->buyer_model->getAccountManagerData($this->data['buyerData']->account_manager_id,$id);
		}
		$acName = isset($accountManager)?$accountManager->UserName:'';
		$this->data['buyerData']->accountManagerName = $acName;
		
		/*echo "<pre>";
		print_r($this->data);
		exit();*/
		$this->load->view('buyer/customer_view',$this->data);
	}
	public function viewBuyer($id)
	{
		if($this->session->userdata("logged_in")["user_role"] == 1)
		{ 
			$this->load->view('common/header');
		}
		else
		{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			
			for($i = 0; $i< count($menues); $i++)
			{
				$menu = $this->dashboard_model->getMenuById($menues[$i]);
				$this->data['permissions']->menues .= $menu->menu_name .",";


				$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
				
				if($submenu == '')
				{
					
				}
				else
				{
					for($j = 0; $j<count($submenu); $j++)
					{
						$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
					}	
				}
			}
			$this->load->view('common/header', $this->data);
		}
		$this->data['buyerData'] = $this->buyer_model->getBuyerRecord($id);
		$this->data['buyers'] = $this->buyer_model->getAllUsersOfCompany($id);
		$this->data['allDeliveryAddressOfCompany'] = $this->buyer_model->selectAllByWhere('ff_comapnydeleveryaddress',array('company_id'=>$id));
		
		//Get dispatch depos if have buyer 
		$this->data['dispatchDepos'] = $this->buyer_model->getDispatchDepos('dispatch_depot');
		//Account Manager
		$this->data['accountManager'] = $this->buyer_model->getAccountManagers();
		//Get warehouse if have seller

		
		if(!empty($this->data['buyerData']->account_manager_id))
		{
			$accountManager = $this->buyer_model->getAccountManagerData($this->data['buyerData']->account_manager_id,$id);
		}
		$acName = isset($accountManager)?$accountManager->UserName:'';
		$this->data['buyerData']->accountManagerName = $acName;
		/*echo "<pre>";
		print_r($accountManager);
		print_r($this->data['buyerData']);
		exit();*/
		$this->load->view('buyer/customer_view',$this->data);
	}
	
	public function updateCustomerDepot()
	{
		$id = $this->input->post('id');
		$company_id = $this->input->post('company_id');
		$updateData = array(
			'dispatch_depot_id'=>$id,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$company_id),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function updateAccountManager()
	{
		$account_manager = $this->input->post('account_manager');
		$company_id = $this->input->post('company_id');
		$updateData = array(
			'account_manager_id'=>$account_manager,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$company_id),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function updateWarehouse()
	{
		$warehouse = $this->input->post('warehouse');
		$company_id = $this->input->post('company_id');
		$updateData = array(
			'warehouse_id'=>$warehouse,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$company_id),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function editBuyerNew($id)
	{
		if($this->session->userdata("logged_in")["user_role"] == 1)
		{ 
			$this->load->view('common/header');
		}
		else
		{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			
			for($i = 0; $i< count($menues); $i++)
			{
				$menu = $this->dashboard_model->getMenuById($menues[$i]);
				$this->data['permissions']->menues .= $menu->menu_name .",";


				$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
				
				if($submenu == '')
				{
					
				}
				else
				{
					for($j = 0; $j<count($submenu); $j++)
					{
						$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
					}	
				}
			}
			$this->load->view('common/header', $this->data);
		}
		$this->data['buyerData'] = $this->buyer_model->getBuyerRecord($id);
		$this->data['buyers'] = $this->buyer_model->getAllUsersOfCompany($id);
		$this->data['allDeliveryAddressOfCompany'] = $this->buyer_model->selectAllByWhere('ff_comapnydeleveryaddress',array('company_id'=>$id));
		
		//Get dispatch depos if have buyer 
		$this->data['dispatchDepos'] = $this->buyer_model->getDispatchDepos('dispatch_depot');
		//Account Manager
		$this->data['accountManager'] = $this->buyer_model->getAccountManagers();
		//Get warehouse if have seller

		
		/*echo "<pre>";
		print_r($this->data);
		exit();*/
		$this->load->view('buyer/customer_edit',$this->data);
	}
	public function editSellerNew($id)
	{
		if($this->session->userdata("logged_in")["user_role"] == 1)
		{ 
			$this->load->view('common/header');
		}
		else
		{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			
			for($i = 0; $i< count($menues); $i++)
			{
				$menu = $this->dashboard_model->getMenuById($menues[$i]);
				$this->data['permissions']->menues .= $menu->menu_name .",";


				$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
				
				if($submenu == '')
				{
					
				}
				else
				{
					for($j = 0; $j<count($submenu); $j++)
					{
						$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
					}	
				}
			}
			$this->load->view('common/header', $this->data);
		}
		$this->data['buyerData'] = $this->buyer_model->getBuyerRecord($id);
		$this->data['buyers'] = $this->buyer_model->getAllUsersOfCompany($id);
		$this->data['allDeliveryAddressOfCompany'] = $this->buyer_model->selectAllByWhere('ff_comapnydeleveryaddress',array('company_id'=>$id));
		$this->data['accountManager'] = $this->buyer_model->getAccountManagers();
		
		//Get warehouse if have seller
		$this->data['warehouse'] = $this->buyer_model->getWarehouse('warehouse');

		if(!empty($this->data['buyerData']->warehouse_id))
		{
			$warehouse = $this->buyer_model->getWarehouseData($this->data['buyerData']->warehouse_id,$id);
		}
		$warehouse_name = isset($warehouse)?$warehouse->warehouse_name:'';
		$this->data['buyerData']->warehouse_name = $warehouse_name;


		/*echo "<pre>";
		print_r($this->data);
		exit();*/
		$this->load->view('buyer/customer_edit',$this->data);
	}
	//Anand check 13_11_2019
	//Anand check 12_11_2019
	public function approvePanDoc()
	{
		$ID = $this->input->post('company_id');
		$updateData = array(
			'is_pan_approved'=>1,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$ID),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function disapprovePanDoc()
	{
		$ID = $this->input->post('company_id');
		$updateData = array(
			'is_pan_approved'=>0,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$ID),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function approveGstDoc()
	{
		$ID = $this->input->post('company_id');
		$updateData = array(
			'is_gst_approved'=>1,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$ID),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function disapproveGstDoc()
	{
		$ID = $this->input->post('company_id');
		$updateData = array(
			'is_gst_approved'=>0,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$ID),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function approveCompanyProfile()
	{
		$ID = $this->input->post('company_id');
		$updateData = array(
			'AdminApproved'=>1,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$ID),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		
		echo json_encode($response);
	}
	public function disapproveCompanyProfile()
	{
		$ID = $this->input->post('company_id');
		$updateData = array(
			'AdminApproved'=>0,
			'Updated_On'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_comapnydetails',array('ID'=>$ID),$updateData);
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	public function changeUserStatus()
	{
		$UserID = $this->input->post('UserID');
		$status = $this->input->post('status');
		if($status == 'false')
		{
			$Status = 0;
		}
		else
		{
			$Status = 1;
		}
		
		$updateData = array(
			'Status'=>$Status,
			'Updated_on'=>date('Y-m-d H:i:s')
		);
		$status = $this->buyer_model->update_all('ff_userlogindetails',array('UserID'=>$UserID),$updateData);
		
		if($status == true)
		{
			$response = array('status'=>true);
		}
		else
		{
			$response = array('status'=>false);
		}
		echo json_encode($response);
	}
	//Anand check 07_11_2019
	
	/*public function index()
	{
		$this->data['buyers'] = $this->buyer_model->getAllBuyers();
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
							// echo"<pre>";print_r($submenu[$j]->submenu_name);
						}	
						// $this->data['permissions']->submenues .= $submenu->submenu_name .",";
					}
					// echo"<pre>";print_r($submenu); 
				}
			// }
			$this->load->view('common/header', $this->data);
		}

		
		$this->load->view('buyer/buyers',$this->data);
	}*/

	//created by Pooja on 27_09_2019 
	public function add_buyer(){
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
							// echo"<pre>";print_r($submenu[$j]->submenu_name);
						}	
						// $this->data['permissions']->submenues .= $submenu->submenu_name .",";
					}
					// echo"<pre>";print_r($submenu); 
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('buyer/add_buyer',$this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result = $this->buyer_model->add();
		echo json_encode($result);
	}

	public function delete(){	
		$buyer_id = $this->uri->segment(3);
		$result = $this->buyer_model->delete($buyer_id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully!");
			redirect("buyer");
		}
	}

	public function edit(){
		$buyer_id = $this->uri->segment(3);
		$this->data['buyer'] = $this->buyer_model->getBuyerById($buyer_id);
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
							// echo"<pre>";print_r($submenu[$j]->submenu_name);
						}	
						// $this->data['permissions']->submenues .= $submenu->submenu_name .",";
					}
					// echo"<pre>";print_r($submenu); 
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('buyer/edit_buyer',$this->data);
	}

	public function update(){
		
		$result = $this->buyer_model->update();
		echo json_encode($result);
	}

	
	public function checkActivate(){
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$buyer_id = $_POST['buyer_id'];
    	$result = $this->buyer_model->checkActivate($buyer_id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$username = $_POST['username'];
    	$result = $this->buyer_model->checkExists($username);
    	echo json_encode($result);
    }	

    public function getCityByStateId(){
    	$state_id = $_POST['state_id'];
    	$result = $this->buyer_model->getCityByStateId($state_id);
    	echo json_encode($result);
    }

    public function view(){
    	$buyer_id = $this->uri->segment(3);
    	$this->data['buyer'] = $this->buyer_model->getBuyerById($buyer_id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
							// echo"<pre>";print_r($submenu[$j]->submenu_name);
						}	
						// $this->data['permissions']->submenues .= $submenu->submenu_name .",";
					}
					// echo"<pre>";print_r($submenu); 
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('buyer/view_buyer',$this->data);
    }
}
