<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cold_storage extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('cold_storage_model');
		$this->load->model('warehouse_model');
		$this->load->model('dashboard_model');
		$this->load->model('service_state_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		// $this->data['storages'] = $this->cold_storage_model->getAllStorages();
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		$this->data['states'] = $this->service_state_model->getAllStates();

		//added by Pooja on 17_09_2019 to get accessible cold storages for subuser
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->data['storages'] = $this->cold_storage_model->getAllStorages();
		}else{
			$storages = array();
			$storages2 = $this->cold_storage_model->getParticularUserStorages();

			if($storages2->cold_storage_id != ''){
				$storage_id = explode(',', $storages2->cold_storage_id);
				foreach($storage_id as $row){
					$ware = $this->cold_storage_model->getStorageById($row);
					 array_push($storages, $ware);
				}
			}
			$this->data['storages'] = $storages;
		}
		
		//to show only accessible menues
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			
			if($this->data['permissions']->menu_id != ''){
				$menu_id = explode(',', $this->data['permissions']->menu_id);
				$submenu_id = explode(',', $this->data['permissions']->submenu_id);
				//to get permission menues
				for($i=0; $i<count($menu_id); $i++){
					$permission_menues =  $this->dashboard_model->getMenuById($menu_id[$i]);
					$this->data['permissions']->menues .= $permission_menues->menu_name.",";
				}
				//to get permission submenues
				for($j=0; $j<count($submenu_id); $j++){
					$permission_submenues =  $this->dashboard_model->getSubmenuById($submenu_id[$j]);
					$this->data['permissions']->submenues .= $permission_submenues->submenu_name.",";
					
				}
			}

			//to get all menus and submenus
			$this->data['menus'] = $this->dashboard_model->getAllMenues();
			for($k = 0; $k < count($this->data['menus']); $k++){
				$menus = $this->dashboard_model->getSubmenuByMenuId($this->data['menus'][$k]->id);
				if(empty($menus)){

				}else{
					foreach($menus as $menu){
						$this->data['menus'][$k]->submenus .= $menu->submenu_name.",";
						$this->data['menus'][$k]->sub_link .= $menu->link.",";
						// echo"<pre>"; print_r($menu->submenu_name);
					}
					
				}
				// $this->data['menus'][$k]->submenus = $menus->submenu_name;
				
			}
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('cold_storage/cold_storage',$this->data);
	}

	//created by Pooja on 27_09_2019
	public function add_cold_storage(){
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->cold_storage_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		// echo"<pre>"; print_r($this->data); die();
		$this->load->view('cold_storage/add_cold_storage',$this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result = $this->cold_storage_model->add();
		echo json_encode($result);
	}

	public function edit(){
		$cold_storage_id = $this->uri->segment(3);
		$this->data['storage'] = $this->cold_storage_model->getStorageById($cold_storage_id);
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->cold_storage_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('cold_storage/edit_cold_storage',$this->data);
	}

	public function update(){
		$this->session->set_flashdata('message',"Record Updated successfully!");
		$result = $this->cold_storage_model->update();
		echo json_encode($result);
	}

	public function delete(){
		$this->session->set_flashdata('message',"Record Deleted successfully!");
		$cold_storage_id = $this->uri->segment(3);
		$result = $this->cold_storage_model->delete($cold_storage_id);	
		if($result == 1){
			redirect('cold_storage');
		}
	}

	public function view(){
		$cold_storage_id = $this->uri->segment(3);
		$this->data['storage'] = $this->cold_storage_model->getStorageById($cold_storage_id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('cold_storage/view_cold_storage',$this->data);
	}
}
