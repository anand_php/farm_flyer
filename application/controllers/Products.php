<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->model('dashboard_model');
		$this->load->model('products_model');
		$this->load->model('resource_product_model');
		$this->load->model('category_model');
		$this->load->model('brand_model');
		$this->load->model('units_model');
		$this->load->model('warehouse_model');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
		error_reporting(0);
	}
	
	public function index()
	{

		$this->data['products'] = $this->products_model->getAllProducts();
		$this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['brands'] = $this->brand_model->getAllBrands();
		$this->data['units'] = $this->units_model->getAllUnits();
		$this->data['taxes'] = $this->products_model->getAllTaxes();
		$this->data['last_product'] = $this->products_model->getLastRecord();
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();

		//added by Pooja on 02_10_2019
		$this->data['variants'] = $this->products_model->getAllVariants();

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		// echo"<pre>"; print_r(); die()
		$this->load->view('products/products', $this->data);
	}

	public function edit()
	{
		$id = $this->uri->segment(3);
		$this->data['product'] = $this->products_model->getProductById($id);
		$this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['brands'] = $this->brand_model->getAllBrands();
		$this->data['units'] = $this->units_model->getAllUnits();
		$this->data['taxes'] = $this->products_model->getAllTaxes();

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		// echo"<pre>"; print_r($this->data['product']); die(); 
		$this->load->view('products/edit_product', $this->data);
	}

	//created by Pooja on 26_09_2019
	public function getSellingUnits(){
		$unit_fk_key = $_POST['unit_fk_key'];
		$result = $this->products_model->getSellingUnits($unit_fk_key);
		echo json_encode($result);
	}

	//created by Pooja on 26_09_2019
	public function add(){

		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result =  $this->products_model->add();
		echo json_encode($result);
		// if($result == 1){
		// 	redirect('products');
		// }
	}

	//created by Pooja on 26_09_2019
	public function getSubcategoriesByCategory(){
		$product_category = $_POST['product_category'];
		$result = $this->products_model->getSubcategoriesByCategory($product_category);
		echo json_encode($result);
	}

	//created by Pooja on 26_09_2019
	public function checkExists(){
		$product_name = $_POST['product_name'];
		$result = $this->products_model->checkExists($product_name);
		echo json_encode($result);
	}

	//created by Pooja on 26_09_2019
	public function delete(){
		$id = $this->uri->segment(3);
		$result= $this->products_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Product Deleted successfully.");
			redirect('products');
		}
	}

	//created by Pooja on 26_09_2019
	public function getProductName(){
		$product_sku_code = $_POST['product_sku_code'];
		$result= $this->products_model->getProductName($product_sku_code);
		echo json_encode($result);
	}

	//created by Pooja on 26_09_2019
	public function getVarient(){
		$result = $this->products_model->getVarient();
		echo json_encode($result);
	}

	//created by Pooja on 26_09_2019
	//changed by Pooja on 02_10_2019
	public function addVariant(){
		
		$result = $this->products_model->addVarient();
		echo json_encode($result);
		// if($result == 1){
		// 	$this->session->set_flashdata('message',"Varients Added successfully.");
		// 	redirect('products');
		// }
	}

	//created by Pooja on 26_09_2019
	public function getProductVarients(){
		$product_id = $_POST['product_id'];
		$result = $this->products_model->getProductVarients($product_id);
		echo json_encode($result);
	}

	//created by Pooja on 26_09_2019
	public function addPricing(){
		$result = $this->products_model->addPricing();
		if($result == 1){
			$this->session->set_flashdata('message',"Pricing Added successfully.");
			redirect('products');
		}
	}

	//created by Pooja on 27_09_2019
	public function updateProductBasic(){
		
		$result = $this->products_model->updateProductBasic();
		echo json_encode($result);
		// if($result == 1){
		// 	$this->session->set_flashdata('message',"Product Details Updated successfully.");
		// 	redirect('products');
		// }
	}

	//created by Pooja on 27_09_2019
	public function updateProduct2(){
		$result = $this->products_model->updateProduct2();
		if($result == 1){
			$this->session->set_flashdata('message',"Product Details Updated successfully.");
			redirect('products');
		}
	}

	//created by Pooja on 27_09_2019
	public function product_pricing(){
		$table = 'warehouse';
		$this->data['pricing'] = $this->products_model->getProductPricing();
		$this->data['products'] = $this->products_model->getAllProducts2();
		// $new_array = array();
  //       foreach ($this->data['products']['result'] as $item)
  //         if (!array_key_exists($item->id, $new_array))
  //           $new_array[$item->id] = $item;

  //       foreach($new_array as $row){
  //       	$pricing = $this->products_model->getProductPricing($row->id, $row->varient_sku_code);
  //       	echo"<pre>"; print_r($pricing);
  //       }
  //        die();

		$this->data['warehouses'] = $this->products_model->getAllWarehouses();
		// $this->data['varients'] = $this->products_model->getAllVarients();
		
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('products/pricing', $this->data);
	}

	//created by Ganesh on 24_10_2019
	public function buy_pricing(){
		$table = 'warehouse'; 
		$city = $_POST['city_id'];
		$state = $_POST['state_id'];
		$this->data['pricing'] = $this->products_model->getBuyProductPricing();
		$this->data['products'] = $this->products_model->getAllProducts2();

		
			if(!isset($_POST['search']) && $this->session->userdata('city_id')){
				$this->data['warehouses'] = $this->products_model->getAllWarehouses();
			}
			else{
				$this->data['warehouses'] = $this->products_model->getAllWarehousesByCity($city);
				//wecho "<pre>"; print_r($this->data['warehouses']);exit;
			}

			if(isset($_POST['reset_btn']))
			{
				unset($_SESSION['city_id']);
			}

		$this->data['base_unit'] = $this->products_model->getBuyBaseUnit();
		$this->data['states'] = $this->products_model->getAllStates();
		$this->data['cities'] = $this->products_model->getAllCities();

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('products/buypricing', $this->data);
	}

	//created by Ganesh on 24_10_2019
	public function sell_pricing(){
		$table = 'dispatch_depot';
		$city = $_POST['city_id'];
		$this->data['pricing'] = $this->products_model->getSellProductPricing();
		$this->data['products'] = $this->products_model->getAllProducts2();

			if(!isset($_POST['search']) && $this->session->userdata('city_id')){
				$this->data['dispatchdepo'] = $this->products_model->getAllDispatchdepo();
			}
			else{
				$this->data['dispatchdepo'] = $this->products_model->getAllDispatchdepoByCity($city);
			}

			if(isset($_POST['reset_btn']))
			{
				unset($_SESSION['city_id']);
			}
		$this->data['cities'] = $this->products_model->getAllDDCities();
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('products/sellpricing', $this->data);
	}

	//created by Pooja on 27_09_2019
	public function addProduct(){
		$this->data['products'] = $this->products_model->getAllProducts();
		$this->data['categories'] = $this->category_model->getAllCategories();
		$this->data['brands'] = $this->brand_model->getAllBrands();
		$this->data['units'] = $this->units_model->getAllUnits();
		$this->data['taxes'] = $this->products_model->getAllTaxes();
		$this->data['last_product'] = $this->products_model->getLastRecord();
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		// echo"<pre>"; print_r(); die()
		$this->load->view('products/add_products', $this->data);
	}

	public function updatePricing(){
		
		$this->session->set_flashdata('message',"Product Pricing Updated successfully.");
		$result = $this->products_model->updatePricing();
		echo json_encode($result);
	}
	public function updateSellPricing(){
		
		$this->session->set_flashdata('message',"Product Pricing Updated successfully.");
		$result = $this->products_model->updateSellPricing();
		echo json_encode($result);
	}
	public function updateBuyPricing(){
		
		$this->session->set_flashdata('message',"Product Pricing Updated successfully.");
		$result = $this->products_model->updateBuyPricing();
		echo json_encode($result);
	}

	//created by Pooja on 30_09_2019
	public function addInventory(){
		$result = $this->products_model->addInventory();
		if($result == 1){
			$this->session->set_flashdata('message',"Inventory Added successfully.");
			redirect('products');
		}
	}

	//created by Pooja on 30_09_2019
	public function getProductVarientsBySkuCode(){
		$product_sku_code = $_POST['product_sku_code'];
		$result = $this->products_model->getProductVarientsBySkuCode($product_sku_code);
		echo json_encode($result);
	}

	//created by Pooja on 30_09_2019
	public function viewInventory(){

		// $table = "warehouse";
		$this->data['products'] = $this->products_model->getAllProducts2();
		
		$this->data['inventory'] = $this->products_model->getAllInventory();
		
		$this->data['warehouses'] = $this->products_model->getAllWarehouses();
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		// echo"<pre>"; print_r(); die()
		$this->load->view('products/viewInventory', $this->data);
	}

	//created by Pooja on 30_09_2019
	public function updateInaventory(){
		// echo"<pre>"; print_r($_POST); die();
		$result = $this->products_model->updateInaventory();
		echo json_encode($result);
		// if($result == 1){
		// 	$this->session->set_flashdata('message',"Inventory Updated successfully.");
		// 	redirect('products');
		// }
	}

	//created by Pooja on 30_09_2019
	public function getProductSubCategory(){
		$product_category = $_POST['product_category'];
		$result = $this->products_model->getProductSubCategory($product_category);
		echo json_encode($result);
	}

	//created by Pooja on 30_09_2019
	public function addProduct2(){
		echo"<pre>";print_r($_POST); die();
	}

	//created by Pooja on 02_10_2019
	public function view(){
		$id = $this->uri->segment(3);
		$this->data['product'] = $this->products_model->getProductById($id);

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			for($i = 0; $i< count($menues); $i++){
				$menu = $this->dashboard_model->getMenuById($menues[$i]);
				$this->data['permissions']->menues .= $menu->menu_name .",";


				$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
				
				if($submenu == ''){
					
				}else{
					for($j = 0; $j<count($submenu); $j++){
						$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
					}	;
				}
			}
			$this->load->view('common/header', $this->data);
		}

		$this->load->view('products/view_product', $this->data);
	}
}
