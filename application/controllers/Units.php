<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Units extends CI_Controller {

	// public function __construct(){
	// 	parent::__construct();
	// 	$this->laod->model('units_model');
	// 	$this->load->database();
	// 	$this->load->library('session');
	// }

	public function __construct(){
		parent::__construct();
		$this->load->model('units_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('session');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}

	public function index()
	{
		$tableName = "manage_resources_unit";
		$this->data['units'] = $this->units_model->getAllUnits();
		if($this->data['units']['count'] > 0){
			for ($i=0; $i < count($this->data['units']['result']) ; $i++) 
			{ 
				if(!empty($this->data['units']['result'][$i]->base_unit_name))
				{
					
					$base_unit = $this->units_model->getUnitNameByID($this->data['units']['result'][$i]->base_unit_name);
					$this->data['units']['result'][$i]->base_unit = $base_unit->base_unit;
				}
			}
		}
		
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('units/units',$this->data);
		// $this->load->view('common/script');
	}

	public function add(){
		// echo"Hello"; print_r($_POST); die();
		$result = $this->units_model->add();
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		echo json_encode($result);
	}

	public function update(){
		$result = $this->units_model->update();
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		echo json_encode($result);
	}

	public function edit(){
		$id= $this->uri->segment(3);
		$this->data['unit'] = $this->units_model->getUnitById($id);
		$this->data['units'] = $this->units_model->getAllUnits();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('units/edit_unit',$this->data);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result = $this->units_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully!");
			redirect('units');
		}
	}

	public function checkActivate(){

    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$id = $_POST['id'];
    	$result = $this->units_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$unit_name = $_POST['unit_name'];
    	$result = $this->units_model->checkExists($unit_name);
    	echo json_encode($result);
    }

    public function view(){
    	$id= $this->uri->segment(3);
		$this->data['unit'] = $this->units_model->getUnitById($id);
		
		if(!empty($this->data['unit']->base_unit_name))
		{
			$base_unit = $this->units_model->getUnitNameByID($this->data['unit']->base_unit_name);
			$this->data['unit']->base_unit = $base_unit->base_unit;
		}
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('units/view_units',$this->data);
    }
}
