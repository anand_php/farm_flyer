<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('login_model');
	}
	
	public function index()
	{
		$this->load->view('login');
	}

	public function login(){
		$result = $this->login_model->checkUserExists();
		
			if($result){
				if($result->user_role == 1){
					$data = array(
						'id' => $result->admin_id,
						'fname' => $result->admin_fname,
						'lname' => $result->admin_lname,
						'user_role' => $result->user_role,
						'image' => $result->admin_image_path,
						'email' => $result->admin_email,
						'csrf_token' => $this->security->get_csrf_token_name()
					);
					
				}else{
					$data = array(
						'id' => $result->id,
						'fname' => $result->name,
						'lname' => $result->name,
						'user_role' => $result->user_role,
						'image' => $result->admin_image,
						'email' => $result->email,
						'csrf_token' => $this->security->get_csrf_token_name()
					);
				}
				$this->session->set_userdata('logged_in', $data);
				redirect('dashboard');
			}else{
				$res = $this->login_model->checkSubadminExists();
				
				if($res != ''){
					$data = array(
						'id' => $res->id,
						'fname' => $res->name,
						'user_role' => $res->role,
						'image' => $result->admin_image,
						'email' => $result->email,
						'csrf_token' => $this->security->get_csrf_token_name()
					);
					$this->session->set_userdata('logged_in', $data);
					redirect('dashboard');
				}else{
					redirect('login');
				}
			}
		
		// if($result){
			
		// 	$data = array(
		// 		'id' => $result->admin_id,
		// 		'fname' => $result->admin_fname,
		// 		'lname' => $result->admin_lname,
		// 		'image' => $result->admin_image_path,
		// 		'email' => $result->admin_email,
		// 		'csrf_token' => $this->security->get_csrf_token_name()
		// 	);
			
		// 	$this->session->set_userdata('logged_in', $data);
		// 	redirect('dashboard');
		// }else{
		// 	redirect('login');
		// }
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
