<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('users_model');
		$this->load->model('service_state_model');
		$this->load->model('service_city_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['users'] = $this->users_model->getAllUsers();
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('users/users',$this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"<div style='color:green;'>Record Inserted successfully.<div>");
		$result = $this->users_model->add();
		echo json_encode($result);
	}

	public function delete(){	
		$user_id = $this->uri->segment(3);
		$result = $this->users_model->delete($user_id);
		if($result == 1){
			$this->session->set_flashdata('message',"<div style='color:green;'>Record Deleted successfully.<div>");
			redirect("users");
		}
	}

	public function edit(){
		$id = $this->uri->segment(3);
		$this->data['user'] = $this->users_model->getUserById($id);
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('users/edit_user',$this->data);
	}

	public function update(){
		$this->session->set_flashdata('message',"<div style='color:green;'>Record Updated successfully.<div>");
		$result = $this->users_model->update();
		echo json_encode($result);
	}

	public function view(){
		$user_id = $this->uri->segment(3);
		$this->data['user'] = $this->users_model->getUserById($user_id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		
		$this->load->view('users/view_user',$this->data);
	}

	public function checkActivate(){
    	$this->session->set_flashdata('message',"<div style='color:green;'>Record activated/deactivated successfully.<div>");
    	$user_id = $_POST['user_id'];
    	$result = $this->users_model->checkActivate($user_id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$username = $_POST['username'];
    	$result = $this->users_model->checkExists($username);
    	echo json_encode($result);
    }

    public function getCityByStateId(){
    	$user_state_id = $_POST['user_state_id'];
    	$result = $this->users_model->getCityByStateId($user_state_id);
    	echo json_encode($result);
    }
}
