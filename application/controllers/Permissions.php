<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('permissions_model');
		$this->load->model('menus_model');
		$this->load->model('submenus_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		// $this->data['permissions'] = $this->permissions_model->getAllPermissions();
		// $this->data['admins'] = $this->permissions_model->getAllAdmins();
		// if($this->data['admins']['count'] > 0){
			// for($i = 0; $i<count($this->data['admins']['result']); $i++){
			// 	if(!empty($this->data['admins']['result'][$i]->admin_menus_id)){
			// 		$menu_id = explode(',', $this->data['admins']['result'][$i]->admin_menus_id);
			// 		for($j=0; $j<count($menu_id); $j++){
			// 			$menus = $this->menus_model->getMenuById($menu_id[$j]);
			// 			$this->data['admins']['result'][$i]->menus .= $menus->menu_name.',';
			// 		}
			// 		if(!empty($this->data['admins']['result'][$i]->admin_submenus_id)){
			// 			$submenu_id = explode(',', $this->data['admins']['result'][$i]->admin_submenus_id);

			// 			for($k=0; $k<count($submenu_id); $k++){
			// 				$submenus = $this->permissions_model->getSubmenuById($submenu_id[$k]);
							
			// 				$this->data['admins']['result'][$i]->submenus .= $submenus->submenu_name.',';
			// 			}
			// 		}
			// 	}
			// }
		// }
		
		//added by Pooja on 13_09_2019 as per new changes
		$this->data['permissions'] = $this->permissions_model->getAllPermissions();
		$this->data['roles'] = $this->permissions_model->getAllRoles();
		$this->data['menues'] = $this->permissions_model->getAllMenues();
		for($i = 0; $i< count($this->data['menues']); $i++){
			$submenus = $this->permissions_model->getSubMenusById($this->data['menues'][$i]->id);
			
			for($j = 0; $j < count($submenus); $j++){
				$this->data['menues'][$i]->submenu .= $submenus[$j]->submenu_name.',';
				$this->data['menues'][$i]->submenu_id .= $submenus[$j]->id.',';
			}
			
		}
		if($this->data['permissions']['count'] > 0){
			for($k = 0; $k < count($this->data['permissions']['result']); $k++){
				$menu_id = explode(',', $this->data['permissions']['result'][$k]->menu_id);
				$submenu_id = explode(',', $this->data['permissions']['result'][$k]->submenu_id);
				
				if($menu_id != ''){
					for($l = 0; $l < count($menu_id); $l++){
						$menus = $this->permissions_model->getMenuById($menu_id[$l]);
						$this->data['permissions']['result'][$k]->menus .= $menus[0]->menu_name .",";
						
					}
				}

				if($submenu_id != ''){
					for($m = 0; $m < count($submenu_id); $m++){

						$submenus = $this->permissions_model->getSubmenuById($submenu_id[$m]);
						$this->data['permissions']['result'][$k]->submenues .= $submenus->submenu_name .",";

					}
				}
			}
		}
		
		// echo"<pre>"; print_r($this->data['permissions']); die();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('permissions/permissions',$this->data);
	}

	public function add(){
		
		$this->session->set_flashdata('message',"Record Inserted successfully.");
		$result = $this->permissions_model->add();
		echo json_encode($result);
	}

	public function delete(){	
		$id = $this->uri->segment(3);
		$result = $this->permissions_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully.");
			redirect("permissions");
		}
	}

	public function edit(){
		$id = $this->uri->segment(3);
		// $this->data['admin'] = $this->permissions_model->getAdminById();
		$this->data['permission'] = $this->permissions_model->getPermissionById($id);
		$this->data['roles'] = $this->permissions_model->getAllRoles();
		$this->data['menues'] = $this->permissions_model->getAllMenues();
		for($i = 0; $i< count($this->data['menues']); $i++){
			$submenus = $this->permissions_model->getSubMenusById($this->data['menues'][$i]->id);
			
			for($j = 0; $j < count($submenus); $j++){
				$this->data['menues'][$i]->submenu .= $submenus[$j]->submenu_name.',';
				$this->data['menues'][$i]->submenu_id .= $submenus[$j]->id.',';
			}
			
		}

		$menu_id = explode(',', $this->data['permission']->menu_id);
		$submenu_id = explode(',', $this->data['permission']->submenu_id);
		
		if($menu_id != ''){
			for($l = 0; $l < count($menu_id); $l++){
				
				$menus = $this->permissions_model->getMenuById($menu_id[$l]);
				$this->data['permission']->menus .= $menus[0]->menu_name .",";
				
			}
		}

		if($submenu_id != ''){
			for($m = 0; $m < count($submenu_id); $m++){

				$submenus = $this->permissions_model->getSubmenuById($submenu_id[$m]);
				$this->data['permission']->submenues .= $submenus->submenu_name .",";

			}
		}
		
		// echo"<pre>";print_r($this->data['permission']->menus); die();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('permissions/edit_permissions',$this->data);
	}

	public function update(){
		$this->session->set_flashdata('message',"Record Updated successfully.");
		$result = $this->permissions_model->update();
		echo json_encode($result);
	}

	public function view(){
		$id = $this->uri->segment(3);
		// $this->data['admin'] = $this->permissions_model->getAdminById();
		$this->data['permission'] = $this->permissions_model->getPermissionById($id);
		$this->data['roles'] = $this->permissions_model->getAllRoles();
		$this->data['menues'] = $this->permissions_model->getAllMenues();
		for($i = 0; $i< count($this->data['menues']); $i++){
			$submenus = $this->permissions_model->getSubMenusById($this->data['menues'][$i]->id);
			
			for($j = 0; $j < count($submenus); $j++){
				$this->data['menues'][$i]->submenu .= $submenus[$j]->submenu_name.',';
				$this->data['menues'][$i]->submenu_id .= $submenus[$j]->id.',';
			}
			
		}

		$menu_id = explode(',', $this->data['permission']->menu_id);
		$submenu_id = explode(',', $this->data['permission']->submenu_id);
		
		if($menu_id != ''){
			for($l = 0; $l < count($menu_id); $l++){
				
				$menus = $this->permissions_model->getMenuById($menu_id[$l]);
				$this->data['permission']->menus .= $menus[0]->menu_name .",";
				
			}
		}

		if($submenu_id != ''){
			for($m = 0; $m < count($submenu_id); $m++){

				$submenus = $this->permissions_model->getSubmenuById($submenu_id[$m]);
				$this->data['permission']->submenues .= $submenus->submenu_name .",";

			}
		}
		
		// echo"<pre>";print_r($this->data['permission']->menus); die();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('permissions/view_permissions',$this->data);
	}

	public function getSubMenus(){
		$menu_id = $_POST['menu_id'];
		$result = $this->permissions_model->getSubMenus($menu_id);
		echo json_encode($result);
	}

	public function checkActivate(){
		$this->session->set_flashdata('message',"status changed successfully.");
		$id = $_POST['id'];
		$result = $this->permissions_model->checkActivate($id);
		echo json_encode($result);
	}

}
