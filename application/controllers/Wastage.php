<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wastage extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('wastage_model');
		$this->load->model('dashboard_model');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['wastages'] = $this->wastage_model->getAllWastageTypes();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('wastage_types/wastage', $this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result =  $this->wastage_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result = $this->wastage_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully!");
			redirect('wastage');
		}
	}

	public function edit(){
        $id = $this->uri->segment(3);   
        $this->data['wastage'] = $this->wastage_model->getWastageById($id);

		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('wastage_types/edit_wastage', $this->data);
    }

    public function update(){
    	$this->session->set_flashdata('message',"Record Updated successfully!");
    	$result = $this->wastage_model->update();
    	echo json_encode($result);
    }

    public function checkActivate(){
    	
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$id = $_POST['id'];
    	$result = $this->wastage_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$wastage_type_name = $_POST['wastage_type_name'];
    	$result = $this->wastage_model->checkExists($wastage_type_name);
    	echo json_encode($result);
    }

    public function view(){
    	$id = $this->uri->segment(3);   
        $this->data['wastage'] = $this->wastage_model->getWastageById($id);
        
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('wastage_types/view_wastage', $this->data);
    }
}
