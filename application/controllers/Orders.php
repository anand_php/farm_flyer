<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('form_validation');
		$this->load->model('dashboard_model');
		$this->load->model('orders_model');
		$this->load->model('buyer_model');
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}
	
	public function index()
	{
		$this->data['orders'] = $this->orders_model->getAllOrders();
		$this->data['buyers'] = $this->buyer_model->getAllBuyers();
		$this->load->view('common/header');
		$this->load->view('orders/orders',$this->data);
	}

	public function view()
	{
		$order_id = $this->uri->segment(3);
		$this->data['order'] = $this->orders_model->getOrderById($order_id);
		$this->data['order_details'] = $this->orders_model->getOrderDetailsById($order_id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('orders/view_orders', $this->data);
	}

	// public function edit(){
	// 	$order_id = $this->uri->segment(3);
	// 	$this->data['order'] = $this->orders_model->getOrderById($order_id);
	// 	$this->data['order_details'] = $this->orders_model->getOrderDetailsById($order_id);
	// }	
}
