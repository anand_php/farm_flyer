<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Varient extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('varient_model');
		$this->load->model('dashboard_model');
	}
	
	public function index()
	{
		$this->data['varients'] = $this->varient_model->getAllVarients();
		$this->data['products'] = $this->varient_model->getAllProducts();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('varient/varient',$this->data);
	}

	public function add(){
		$result = $this->varient_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result = $this->varient_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"<div style='color:green;'>Record Deleted successfully.<div>");
			redirect('varient');
		}
	}

	public function edit(){
        $id = $this->uri->segment(3);   
        $this->data['varient'] = $this->varient_model->getVarientById($id);
        $this->data['products'] = $this->varient_model->getAllProducts();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('varient/edit_varient', $this->data);
    }

    public function update(){
    	$result = $this->varient_model->update();
    	echo json_encode($result);
    }

    public function checkActivate(){
    	
    	$this->session->set_flashdata('message',"<div style='color:green;'>Record activated/deactivated successfully.<div>");
    	$id = $_POST['id'];
    	$result = $this->varient_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$variant_name = $_POST['variant_name'];
    	$result = $this->varient_model->checkExists($variant_name);
    	echo json_encode($result);
    }
}
