<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_city extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->model('service_city_model');
		$this->load->model('service_state_model');
		$this->load->model('dashboard_model');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}

	
	public function index()
	{
		$this->data['cities'] = $this->service_city_model->getAllCities();
		$this->data['states'] = $this->service_state_model->getAllStates();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('service_city/service_city', $this->data);
	}

	public function add(){
		$this->session->set_flashdata('message',"Record Inserted successfully!");
		$result =  $this->service_city_model->add();
		echo json_encode($result);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result = $this->service_city_model->delete($id);
		if($result == 1){
			$this->session->set_flashdata('message',"Record Deleted successfully!");
			redirect('service_city');
		}
	}

	public function edit(){
        $id = $this->uri->segment(3);   
        $this->data['city'] = $this->service_city_model->getServiceCityById($id);
        $this->data['states'] = $this->service_state_model->getAllStates();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('service_city/edit_service_city', $this->data);
    }

    public function update(){ 
    	$result = $this->service_city_model->update();
    	echo json_encode($result);
    }

    public function checkActivate(){
    	
    	$this->session->set_flashdata('message',"Record activated/deactivated successfully!");
    	$id = $_POST['id'];
    	$result = $this->service_city_model->checkActivate($id);
    	echo json_encode($result);
    }

    public function checkExists(){
    	$city_name = $_POST['city_name'];
    	$result = $this->service_city_model->checkExists($city_name);
    	echo json_encode($result);
    }

    public function view(){
    	$id = $this->uri->segment(3);   
        $this->data['city'] = $this->service_city_model->getServiceCityById($id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('service_city/view_service_city', $this->data);
    }
}
