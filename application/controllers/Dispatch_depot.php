<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dispatch_depot extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('dispatch_depot_model');
		$this->load->model('service_city_model');
		$this->load->model('service_state_model');
		$this->load->model('dashboard_model');
		$this->load->model('warehouse_model');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata("logged_in") == ''){ 
			redirect("login");
		}
	}

	
	public function index()
	{
		// $this->data['dispatch_depots'] = $this->dispatch_depot_model->getAllDispathDepots();
		$this->data['cities'] = $this->service_city_model->getAllCities();
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		$this->data['states'] = $this->service_state_model->getAllStates();

		//added by Pooja on 17_09_2019
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->data['dispatch_depots'] = $this->dispatch_depot_model->getAllDispathDepots();
		}else{
			$depots = array();
			$depots2 = $this->dispatch_depot_model->getParticularUserDepot();

			if($depots2->dispatch_depot_id != ''){
				$dispatch_depot_id = explode(',', $depots2->dispatch_depot_id);
				foreach($dispatch_depot_id as $row){
					$ware = $this->dispatch_depot_model->getDepotById($row);
					array_push($depots, $ware);
				}
			}
			$this->data['dispatch_depots'] = $depots;
		}
		
		//to show only accessible menues
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('dispatch_depot/dispatch_depot',$this->data);
	}

	//created by Pooja on 27_09_2019
	public function add_dispatch_depot(){
		$this->data['cities'] = $this->service_city_model->getAllCities();
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		$this->data['states'] = $this->service_state_model->getAllStates();

		//added by Pooja on 17_09_2019
		if($this->session->userdata("logged_in")["user_role"] == 1){
			$this->data['dispatch_depots'] = $this->dispatch_depot_model->getAllDispathDepots();
		}else{
			$depots = array();
			$depots2 = $this->dispatch_depot_model->getParticularUserDepot();

			if($depots2->dispatch_depot_id != ''){
				$dispatch_depot_id = explode(',', $depots2->dispatch_depot_id);
				foreach($dispatch_depot_id as $row){
					$ware = $this->dispatch_depot_model->getDepotById($row);
					array_push($depots, $ware);
				}
			}
			$this->data['dispatch_depots'] = $depots;
		}
		
		//to show only accessible menues
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('dispatch_depot/add_dispatch_depot',$this->data);
	}

	public function add(){
		$result = $this->dispatch_depot_model->add();
		echo json_encode($result);
	}

	public function edit(){
		$id = $this->uri->segment(3);
		$this->data['depot'] = $this->dispatch_depot_model->getDepotById($id);
		$this->data['cities'] = $this->service_city_model->getAllCities();
		$this->data['states'] = $this->service_state_model->getAllStates();
		$this->data['warehouses'] = $this->warehouse_model->getAllWarehouses();
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('dispatch_depot/edit_dispatch_depot',$this->data);
	}

	public function update(){
		$result = $this->dispatch_depot_model->update();
		echo json_encode($result);
	}

	public function checkActivate(){
		$id = $_POST['id'];
		$result = $this->dispatch_depot_model->checkActivate($id);
		echo json_encode($result);
	}

	public function delete(){
		$id = $this->uri->segment(3);
		$result = $this->dispatch_depot_model->delete($id);
		if($result){
			redirect("dispatch_depot");
		}
	}

	public function checkExists(){
		$dispatch_depot_code = $_POST['dispatch_depot_code'];
		$result = $this->dispatch_depot_model->checkExists($dispatch_depot_code);
		echo json_encode($result);
	}

	public function view(){
		$id = $this->uri->segment(3);
		$this->data['depot'] = $this->dispatch_depot_model->getDepotById($id);
		if($this->session->userdata("logged_in")["user_role"] == 1){ 
			$this->load->view('common/header');
		}else{
			$this->data['permissions'] = $this->dashboard_model->getPermissions();
			$menues = explode(',', $this->data['permissions']->menu_id);
			$submenues = explode(',', $this->data['permissions']->submenu_id);
			// if($this->data['permissions']->menu_id != ''){
				for($i = 0; $i< count($menues); $i++){
					$menu = $this->dashboard_model->getMenuById($menues[$i]);
					$this->data['permissions']->menues .= $menu->menu_name .",";


					$submenu = $this->dashboard_model->getSubmenuByMenuId($menues[$i]);
					
					if($submenu == ''){
						
					}else{
						for($j = 0; $j<count($submenu); $j++){
							$this->data['permissions']->submenues .= $submenu[$j]->submenu_name .",";
						}	;
					}
				}
			// }
			$this->load->view('common/header', $this->data);
		}
		$this->load->view('dispatch_depot/view_dispatch_depot',$this->data);
	}

}
