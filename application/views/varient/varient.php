            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Verient
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="#">Verient</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <div class="alert alert-success " id="successMessage" role="alert"> 
                                        <?= $this->session->flashdata('message') ?> </div>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Verient</strong> integration</h2>
                            <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Varients</p>

                        <div class="table-responsive">
                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Verient Name</th>
                                        <th class="text-center">Product Name</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php if($varients['count'] > 0 ){
                                        $i = 1;
                                        foreach($varients['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->varient_name ?></td>
                                            <td><?php echo $row->product ?></td>
                                            <td>
                                                <?php if($row->status_choice == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            </td>
                                            <td>                                                
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url().'varient/edit/'.$row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url().'varient/delete/'.$row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php $i++; } }?>                      
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

                <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>
            <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
      <div class="modal-dialog animation-slideDown">

        <!-- Modal content-->
        <div class="modal-content CustomModal">
          <div class="modal-body">
            <div id="loginCustom">
                <!-- Login Title -->
                <div class="login-title text-right">
                    <h1>
                        <i class="Unitsss gi gi-playing_dices pull-left"></i>
                        <strong>Manage</strong> Varient<br>
                        <br>
                    </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block">
                        <form method="post" class="form-horizontal form-bordered" id="varient_form">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Product Name</label>
                                <div class="col-md-9">
                                    <select id="product_name" name="product_name" class="select-chosen" data-placeholder="Choose Product" style="width: 250px;">
                                        <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                         <?php foreach($products['result'] as $row) { ?>
                                            <option value="<?php echo $row->id ?>"><?php echo $row->product_name ?></option>
                                        <?php }?>
                                    </select>
                                    <span id="head"></span>
                                    <span class="help-block">Please select Product</span>
                                </div>
                            </div>
                             
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Varient Name</label>
                                <div class="col-md-9">
                                    <input type="text" id="variant_name" name="variant_name" class="form-control" placeholder="Enter varient name">
                                    <span id="head1"></span>
                                    <span class="help-block">Please enter varient name</span>
                                </div>
                            </div>
                           <div class="form-group">
                                <input type="hidden" name="status_choice" id="somefield" value="true">
                                <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <label class="switch switch-primary">
                                        <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $('#statusCheck').on('change',function(){
        alert();
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });
$(document).ready(function(){

    $("#varient_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#varient_form")[0]);
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('varient/add') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
    });


    function formValidation() 
    {
        // Make quick references to our fields.
        var product_name = $("#product_name").val();
        var variant_name = document.getElementById('variant_name');
  
  
        // To check empty form fields.  
        if (product_name == '') {
            document.getElementById('head').innerText = "Please Select Product Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            sub_cat_name.focus();
            return false;
        }
        if (variant_name.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            sub_cat_description.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(variant_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


        // Function that checks whether input text is an alphabetic character or not.
        function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
        return true;
        } else {
        document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
        $('#p1').delay(2000).hide('slow');
        $('#p1').css('display','block');
        //alert(alertMsg);
        inputtext.focus();
        return false;
        }
        }

    $("#variant_name").on('blur', function(){
        var isValid = false;
        var regex = /^[a-zA-Z ]*$/;
        isValid = regex.test($("#variant_name").val());
        $("#spnError2").css("display", !isValid ? "block" : "none");
        if(isValid == false){
            $("#variant_name").val('');
        }
        return isValid;
    })
});

    function checkActivate(id){
        $.ajax({
            url:'<?php echo base_url('varient/checkActivate') ?>',
            type:"post",
            data:{'id':id},
            success: function(data){
                alert("Record activated/deactivated successfully!");
            }
        });
    }

   

    $("#variant_name").on('blur', function(){
        var variant_name = $(this).val();
        $.ajax({
            url:'<?php echo base_url('varient/checkExists') ?>',
            type:"post",
            data:{'variant_name':variant_name},
            success: function(data){
                if(data == 'true'){
                    $("#variant_name").val('');
                    alert("Verient already exist!");
                }
            }
        });
    });

    $("#reset").on('click', function(){
        $('#myModal').modal('hide');
    });

</script>
