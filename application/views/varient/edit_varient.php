          
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Varient
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Varients</li>
                        <li><a href="<?php echo base_url('varient') ?>">Varient</a></li>
                        <li><a href="#">Edit Varient</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Varient</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Varients</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Varient<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form method="post" class="form-horizontal form-bordered" id="edit_varient_form">
                                <input type="hidden" name="id" value="<?php echo $varient->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Name</label>
                                    <div class="col-md-9">
                                        <select id="product_name" name="product_name" class="select-chosen" data-placeholder="Choose Product" style="width: 250px;" required>
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                             <?php foreach($products['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $varient->product_name ) echo 'selected="selected"'; ?>><?php echo $row->product_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head"></span>
                                        <span class="help-block">Please select Product</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Varient Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="varient_name" name="varient_name" class="form-control" placeholder="Enter varient name" value="<?php echo $varient->varient_name ?>">
                                        <span id="head1"></span>
                                        <span class="help-block">Please enter varient name</span>
                                         <span id="spnError2" style="color: Red; display: none">Please enter only characters</span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($varient->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $varient->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        $("#edit_varient_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#edit_varient_form")[0]);
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('varient/update') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        window.location.href="<?php echo base_url('varient') ?>"
                    }
                }
            });
        }
    });


    function formValidation() 
    {
        // Make quick references to our fields.
        var product_name = $("#product_name").val();
        var variant_name = document.getElementById('varient_name');
  
  
        // To check empty form fields.  
        if (product_name == '') {
            document.getElementById('head').innerText = "Please Select Product Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            sub_cat_name.focus();
            return false;
        }
        if (variant_name.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            variant_name.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(variant_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }

        $("#unit_name").on('blur', function(){ 
            var isValid = false;
            var regex = /^[a-zA-Z ]*$/;
            isValid = regex.test($("#unit_name").val());
            $("#spnError2").css("display", !isValid ? "block" : "none");
            if(isValid == false){
                $(".name").val('');  
            }
            return isValid;
        })
    </script>
    