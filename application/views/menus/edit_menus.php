            <style type="text/css">
    .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
    .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
</style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Menus
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Menus</li>
                        <li><a href="<?php echo base_url('menus') ?>">Menus</a></li>
                        <li><a href="#">Edit Menu</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Menus</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Menus and Assign Menus</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Menus<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="edit_menu_form">
                                <input type="hidden" name="id" value="<?php echo $menu->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="menu_name" name="menu_name" class="form-control menu_name" placeholder="Enter menu name" value="<?php echo $menu->menu_name ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Link<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="link" name="link" class="form-control link" placeholder="Enter menu link" value="<?php echo $menu->menu_name ?>" >
                                        <span id="head1"></span>
                                        <!-- <span class="help-block">Please enter menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Icon<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="file" id="icon" name="icon" class="form-control icon" placeholder="Enter menu icon" required>
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please enter menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($menu->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $menu->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });

            $('#edit_menu_form').submit(function(e){

                e.preventDefault();
                var formData = new FormData($("#edit_menu_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:'<?php echo base_url("menus/update")?>',
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                alert("Record updated successfully!");
                                window.location.href="<?php echo base_url('menus') ?>";
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
    {
        // Make quick references to our fields.
        var menu_name = document.getElementById('menu_name');
  
        // To check empty form fields.  
        if (menu_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter menu Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            menu_name.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(menu_name, "* Please enter valid menu name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }

    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#p1').delay(2000).hide('slow');
            $('#p1').css('display','block');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }  

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('menus') ?>";
        // $('#myModal').modal('hide');
    });  
</script>
    