            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Menus
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Menus</li>
                        <li><a href="#">Menus</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <script>
                                        swal({
                                            title: "Done",
                                            text: "<?php echo $this->session->flashdata('message'); ?>",
                                            timer: 2000,
                                            showConfirmButton: false,
                                            type: 'success'
                                        });
                                    </script>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Menus</strong> integration</h2>
                            <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                        </div>
                        <p>Admin Can Add / edit and Delete Menus and Assign Menus</p>

                        <div class="table-responsive">

                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php 
                                        if($menus['count'] > 0){
                                        $i = 1; 
                                        foreach($menus['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->menu_name ?></td>
                                            <td>
                                                <?php if($row->status_choice == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            </td>
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url().'menus/view/'. $row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url().'menus/edit/'. $row->id ?>" data-toggle="Edit" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'menus/delete/' . $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" id="delete"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    <?php $i++; } } ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>
    <!-- <script src="<?php echo base_url('scripts/add_brand.js')?>"></script> -->
            <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
      <div class="modal-dialog animation-slideDown">

        <!-- Modal content-->
        <div class="modal-content CustomModal">
          <div class="modal-body">
            <div id="loginCustom">
                <!-- Login Title -->
                <div class="login-title text-right">
                <div class="login-title text-right">
                    <h1>
                        <i class="Unitsss fa fa-star-o pull-left"></i>
                        <strong>Manage</strong> Menus<br>
                        <br>
                    </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block">
                        <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="menu_form">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="menu_name" name="menu_name" class="form-control menu_name" placeholder="Enter menu name">
                                    <span id="head"></span>
                                    <!-- <span class="help-block">Please enter menu name</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Link<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="link" name="link" class="form-control link" placeholder="Enter menu link">
                                    <span id="head1"></span>
                                    <!-- <span class="help-block">Please enter menu name</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Icon<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="file" id="icon" name="icon" class="form-control icon" placeholder="Enter menu icon" required>
                                    <span id="head2"></span>
                                    <!-- <span class="help-block">Please enter menu name</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="status_choice" id="somefield" value="true">
                                    <div class="col-md-9">
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#menu_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#menu_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("menus/add")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            alert("Record Inserted Successfully!");
                            window.location.href="<?php echo base_url('menus'); ?>"
                        }
                    }
                });
            }
        });
    });

        function formValidation() 
    {
        // Make quick references to our fields. 
        var menu_name = document.getElementById('menu_name');
        var link = document.getElementById('link');
        var icon = document.getElementById('icon');
  
        // To check empty form fields.  
        if (menu_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter manu Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            menu_name.focus();
            return false;
        }
        if (link.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter manu Link"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            link.focus();
            return false;
        }
        
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(menu_name, "* Please enter valid menu name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[A-z\d\-_\s]+$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
$('#p1').delay(2000).hide('slow');
$('#p1').css('display','block');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

        function checkActivate(id){
            $.ajax({
                // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/brand/checkActivate',
                url:'<?php echo base_url('menus/checkActivate') ?>',
                type:"post",
                data:{'id':id},
                success: function(data){
                    alert("Record activated/deactivated successfully!");
                }
            });
        }

        $("#menu_name").on('blur', function(){
            var menu_name = $(this).val();

            $.ajax({
                url:'<?php echo base_url('menus/checkExists') ?>',
                type:"post",
                data:{'menu_name':menu_name},
                success: function(data){
                    if(data == 'true'){
                        $("#menu_name").val('');
                        alert("Menu name already exists!");
                    }
                }
            });
        });

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('menus') ?>";
            // $('#myModal').modal('hide');
        });
</script>