            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Admin
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Subadmin</li>
                        <li><a href="<?php echo base_url('admin') ?>">Admin</a></li>
                        <li><a href="#">View Admin</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Admin</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Admin</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Admin<br>
                                <br>
                            </h1>
                            <?php //echo"<pre>"; print_r($admin); ?>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Role</td>
                                            <td><?php echo $admin->role ?></td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo $admin->name ?></td>
                                        </tr>
                                         <tr>
                                            <td>Email</td>
                                            <td><?php echo $admin->email ?></td>
                                        </tr>
                                         <tr>
                                            <td>Mobile</td>
                                            <td><?php echo $admin->mobile ?></td>
                                        </tr>
                                        <tr>
                                            <td>Username</td>
                                            <td><?php echo $admin->username ?></td>
                                        </tr>
                                        <tr>
                                            <td>Gender</td>
                                            <td><?php echo $admin->gender ?></td>
                                        </tr>
                                         <tr>
                                            <td>Address</td>
                                            <td><?php echo $admin->address ?></td>
                                        </tr>
                                        <tr>
                                            <td>Warehouse</td>
                                            <td><?php echo $admin->warehouse?></td>
                                        </tr>
                                        <tr>
                                            <td>Cold storage</td>
                                            <td><?php echo $admin->cold_storage?></td>
                                        </tr>
                                        <tr>
                                            <td>Dispatch Depot</td>
                                            <td><?php echo $admin->dispatch_depot?></td>
                                        </tr>
                                        <tr>
                                            <td>Image</td>
                                            <td><img src="<?php echo base_url().'uploads/subadmin/'.$admin->admin_image ?>" style="height: 100px; width: 100px;"></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td><?php if($admin->status_choice == 'true'){ ?>
                                                 <label class="switch switch-primary">
                                                    <input type="checkbox" checked><span></span>
                                                </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" ><span></span>
                                                    </label>
                                                <?php }?>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>