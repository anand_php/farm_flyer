            <style type="text/css">
    .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
    .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
</style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Admin
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('admin') ?>">Admin</a></li>
                        <li><a href="#">Edit Admin</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Admin</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Brands</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Admin<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="admin_edit_form">
                                <input type="hidden" name="id" value="<?php echo $admin->id ?>">
                                    <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">User Role</label>
                                    <div class="col-md-9">
                                        <select id="user_role" name="user_role" class="select-chosen" data-placeholder="Choose Role" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($roles['result'] as $row) { ?>
                                                <option value="<?php echo $row->role_id ?>" <?php if($row->role_id == $admin->user_role ) echo 'selected="selected"';?> ><?php echo $row->name ?></option>
                                            <?php }?>
                                        </select>
                                        <!-- <span class="help-block">Please select category</span> -->
                                        <span id="head7"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="name" name="name" class="form-control name" placeholder="Enter name" value="<?php echo $admin->name ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Email<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="email" id="email" name="email" class="form-control email" placeholder="Enter email" value="<?php echo $admin->email ?>">
                                        <span id="head1"></span>
                                        <!-- <span class="help-block">Please enter email</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Username</label>
                                    <div class="col-md-9">
                                        <input type="text" id="username" name="username" class="form-control email" placeholder="Enter username" value="<?php echo $admin->username ?>">
                                        <span id="head6"></span>
                                        <!-- <span class="help-block">Please enter username</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text"> Mobile<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="mobile" name="mobile" class="form-control mobile" placeholder="Enter mobile" value="<?php echo $admin->mobile ?>">
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please enter mobile</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Gender<span>*</span></label>
                                    <div class="col-md-9">
                                        <!-- <input type="text" id="brand_name" name="brand_name" class="form-control brand_name" placeholder="Enter brand name"> -->
                                        <select class="form-control gender" name="gender" id="gender">
                                            <option value="">Please Select gender</option>
                                            <option value="male" <?php if($admin->gender == 'male' ) echo 'selected="selected"';?> >Male</option>
                                            <option value="female" <?php if($admin->gender == 'female' ) echo 'selected="selected"';?>>Female</option>
                                        </select>
                                        <span id="head3"></span>
                                        <!-- <span class="help-block">Please select gender</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Password<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="password" id="password" name="password" class="form-control password" placeholder="Enter password" >
                                        <span id="head4"></span>
                                        <!-- <span class="help-block">Please enter password</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Address<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="address" name="address" class="form-control address" placeholder="Enter address" value="<?php echo $admin->address ?>">
                                        <span id="head5"></span>
                                        <!-- <span class="help-block">Please enter address</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Image<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="file" id="admin_image" name="admin_image" class="admin_image">
                                        <img src="<?php echo base_url() .'uploads/subadmin/'. $admin->admin_image ?>" id="profile-img-tag" width="200px">
                                        <span id="head6"></span>
                                        <!-- <span class="help-block">Please upload admin image</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="status_choice" id="somefield" value="true">
                                        <?php if($admin->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="submit" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#admin_edit_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#admin_edit_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("admin/update")?>',
                    // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/brand/add',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            window.location.href = "<?php echo base_url('admin') ?>"
                        }
                    }
                });
            }
        });
    });

        function formValidation() 
    {
        // Make quick references to our fields.
        var name = document.getElementById('name');
        // var username = document.getElementById('username');
        var email = document.getElementById('email');
        var mobile = document.getElementById('mobile');
        var gender = document.getElementById('gender');
        // var password = document.getElementById('password');
        var address = document.getElementById('address');
        // var category_image = document.getElementById('category_image');
  
        // To check empty form fields.  
        if (name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            name.focus();
            return false;
        }
        // if (username.value.length == 0) {
        //     document.getElementById('head6').innerText = "Please Enter Name"; // This segment displays the validation rule for all fields
        //     $('#head6').delay(2000).hide('slow');
        //     $('#head6').css('display','block');
        //     $('#head6').css('color','red');
        //     username.focus();
        //     return false;
        // }
        if (email.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Email"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            email.focus();
            return false;
        }
        if (mobile.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter Mobil"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            mobile.focus();
            return false;
        }
        if (gender == '') {
            document.getElementById('head3').innerText = "Please select gender"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            gender.focus();
            return false;
        }
        // if (password.value.length == 0) {
        //     document.getElementById('head4').innerText = "Please Enter Password"; // This segment displays the validation rule for all fields
        //     $('#head4').delay(2000).hide('slow');
        //     $('#head4').css('display','block');
        //     $('#head4').css('color','red');
        //     password.focus();
        //     return false;
        // }
        if (address.value.length == 0) {
            document.getElementById('head5').innerText = "Please Enter Address"; // This segment displays the validation rule for all fields
            $('#head5').delay(2000).hide('slow');
            $('#head5').css('display','block');
            $('#head5').css('color','red');
            address.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(name, "* Please enter valid name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}
        function readURL(input) {
            if (input.files && input.files[0]) {
                 
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#profile-img-tag').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".admin_image").change(function() {
           
          readURL(this);
        });

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('resource_product') ?>";
        });

        $("#mobile").on("blur", function(){
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;

        if (filter.test(mobNum)) {
            if(mobNum.length==10){
                 
            } else {
                document.getElementById('head2').innerText = "Please enter 10  digit mobile number";
                $('#head2').delay(2000).hide('slow');
                $('#head2').css('display','block');
                $('#head2').css('color','red');
                $("#mobile").val('');
                return false;
            }
        }else{
            document.getElementById('head2').innerText = "Please enter valid number";
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            $("#mobile").val('');
            return false;
        }
    });

</script>
    