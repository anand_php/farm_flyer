            <style type="text/css">
    .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
    .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
</style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Admin
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('admin') ?>">Admin</a></li>
                        <li><a href="#">Assign Locations</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Admin</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete admin</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Admin<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" action="<?php echo base_url('admin/assign') ?>">

                                <?php //echo"<pre>"; print_r($this->data); 
                                // print_r($subadmin);
                                $warehouse_ids = explode(',', $subadmin->warehouse_id);
                                $cold_storage_ids = explode(',', $subadmin->cold_storage_id);
                                $dispatch_depot_ids = explode(',', $subadmin->dispatch_depot_id);
                                ?>
                                <input type="hidden" name="id" value="<?php echo $subadmin->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Warehouse</label>
                                    <div class="col-md-9">
                                        <?php foreach($warehouses['result'] as $row) { ?>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="warehouse_id[]" value="<?php echo $row->id ?>" <?php if(in_array($row->id, $warehouse_ids)){ echo 'checked="checked"'; }?>><?php echo $row->warehouse_name ?></label>
                                            </div>
                                        <?php }?>
                                        <!-- <span class="help-block">Please select category</span> -->
                                        <span id="head7"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Cold Storage</label>
                                    <div class="col-md-9">
                                        <?php foreach($storages['result'] as $row) { ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="<?php echo $row->cold_storage_id ?>" name="cold_storage_id[]" <?php if(in_array($row->cold_storage_id, $cold_storage_ids)){ echo 'checked="checked"'; }?> >
                                                    <?php echo $row->name ?>
                                                </label>
                                            </div>
                                        <?php }?>
                                        <!-- <span class="help-block">Please select category</span> -->
                                        <span id="head7"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Dispatch Depot</label>
                                    <div class="col-md-9">
                                        <?php foreach($dispatch_depots['result'] as $row) { ?>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="dispatch_depot_id[]" value="<?php echo $row->id ?>" <?php if(in_array($row->id, $dispatch_depot_ids)){ echo 'checked="checked"'; }?> ><?php echo $row->dispatch_depot_name ?>
                                                </label>
                                            </div>
                                        <?php }?>
                                        <!-- <span class="help-block">Please select category</span> -->
                                        <span id="head7"></span>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){   
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });
        });
        
    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('admin') ?>";
    });


</script>
    