            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            Service City
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Manage Resources</li>
                    <li><a href="#">Service City</a></li>
                </ul>
                <!-- END Datatables Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <div>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <script>
                                    swal({
                                        title: "Done",
                                        text: "<?php echo $this->session->flashdata('message'); ?>",
                                        timer: 2000,
                                        showConfirmButton: false,
                                        type: 'success'
                                    });
                                </script>
                            <?php }else{} ?>    
                        </div>
                        <h2><strong>Service City</strong> integration</h2>
                        <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                    </div>
                    <p>Admin Can Add / edit and Delete Units and Assign Service cities</p>

                    <div class="table-responsive">
                        <table id="example" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">City Name</th>
                                    <th class="text-center">State Name</th>
                                    <th class="text-center">City Description</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php if($cities['count'] > 0){
                                    $i=1; 
                                    foreach($cities['result'] as $row){ ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $row->city_name ?></td>
                                        <td><?php echo $row->state_name ?></td>
                                        <td><?php echo $row->description ?></td>
                                        <td>
                                            <?php if($row->status_choice == 'true'){?>
                                                <label class="switch switch-primary">
                                                    <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                </label>
                                            <?php }else{ ?>
                                                <label class="switch switch-primary">
                                                    <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                </label>
                                            <?php } ?>
                                            
                                        </td>
                                        <td>                                                
                                            <div class="btn-group">
                                                <a href="<?php echo base_url().'service_city/view/'.$row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                <a href="<?php echo base_url().'service_city/edit/'.$row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                <a href="<?php echo base_url().'service_city/delete/'.$row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php $i++; } } ?>               
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <?php $this->load->view('common/footer.php'); ?>
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->

<?php $this->load->view('common/script.php'); ?>

        <!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
  <div class="modal-dialog animation-slideDown">

    <!-- Modal content-->
    <div class="modal-content CustomModal">
      <div class="modal-body">
        <div id="loginCustom">
            <!-- Login Title -->
            <div class="login-title text-right">
                <h1>
                    <i class="Unitsss fa fa-bank pull-left"></i>
                    <strong>Manage</strong> Service City<br>
                    <br>
                </h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block">
                <form method="post" class="form-horizontal form-bordered" id="city_form" novalidate>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">State<span>*</span></label>
                        <div class="col-md-9">
                            <select id="state_id" name="state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;" required>
                                    <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                     <?php foreach($states['result'] as $state) { ?>
                                        <option value="<?php echo $state->state_id ?>"><?php echo $state->state_name ?></option>
                                    <?php }?>
                            </select>
                            <span id="head"></span>
                            <!-- <span class="help-block">Please select menu name</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">City<span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="city_name" name="city_name" class="form-control" placeholder="Enter city name">
                            <span id="head"></span>
                            <!-- <span class="help-block">Please enter city name</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text"> Description<span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="description" name="description" class="form-control" placeholder="Enter description">
                            <span id="head2"></span>
                            <!-- <span class="help-block">Please enter city description</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="status_choice" id="somefield" value="true">
                        <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input id="statusCheck" type="checkbox" checked=""><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Login Block -->
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });
    $("#city_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#city_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('service_city/add') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        var city_name = document.getElementById('city_name');
        var description = document.getElementById('description');
        var state_id = $("#state_id").val();
  
        // To check empty form fields.  
        if (city_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter State Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            city_name.focus();
            return false;
        }
        if (description.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            description.focus();
            return false;
        }
        if (state_id == '') {
            document.getElementById('head3').innerText = "Please Select State"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            state_id.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(city_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }

// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[A-z\d\-_\s]+$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
$('#p1').delay(2000).hide('slow');
$('#p1').css('display','block');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

function checkActivate(id){
    $.ajax({
        url:'<?php echo base_url('service_city/checkActivate') ?>',
        type:"post",
        data:{'id':id},
        success: function(data){
            alert("Record activated/deactivated successfully!");
        }
    });
}

 $("#city_name").on('blur', function(){
    var city_name = $(this).val();
    $.ajax({
        url:'<?php echo base_url('service_city/checkExists') ?>',
        type:"post",
        data:{'city_name':city_name},
        success: function(data){
            if(data == 'true'){
                $("#city_name").val('');
                alert("City name is already exist!");
            }
        }
    });
});

$("#reset").on('click', function(){
    window.location.href="<?php echo base_url('service_city') ?>";
    // $('#myModal').modal('hide');
});
</script>