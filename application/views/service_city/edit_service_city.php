            <style type="text/css">
                form .error {
                    color: #ff0000;
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Service City
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Varients</li>
                        <li><a href="<?php echo base_url('service_city') ?>">Service City</a></li>
                        <li><a href="#">Edit Service City</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Service City</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Service Cities</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Service City<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form method="post" class="form-horizontal form-bordered" id="edit_service_city">
                                <input type="hidden" name="id" value="<?php echo $city->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">State Name <span>*</span></label>
                                    <div class="col-md-9">
                                        <select id="state_id" name="state_id" class="select-chosen" data-placeholder="Choose Menu" style="width: 250px;" required>
                                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                 <?php foreach($states['result'] as $state) { ?>
                                                    <option value="<?php echo $state->state_id ?>" <?php if($state->state_id == $city->state_id ) echo 'selected="selected"'; ?> ><?php echo $state->state_name ?></option>
                                                <?php }?>
                                        </select>
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please select menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="city_name" name="city_name" class="form-control" placeholder="Enter city name" value="<?php echo $city->city_name ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter city name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Description<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Enter description" value="<?php echo $city->description ?>">
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please enter city description</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($city->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $city->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
   <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        $('#edit_service_city').validate({
            rules: {
                city_name: "required",
                description: "required"
            },
            // Specify validation error messages
            messages: {
                city_name: "Please enter city name",
                description: "Please Enter city description"
            },
            submitHandler: function(form) {
                $.ajax({
                    url: '<?php echo base_url('service_city/update') ?>',
                    type: 'post',
                    data: $(form).serialize(),
                    success: function(response) {
                       if(response == 'true'){
                            // alert("Record Inserted Successfully!");
                            window.location.href = '<?php echo base_url("service_city") ?>';
                        }
                    }            
                });
            }
        });

        $("#city_name").on('blur', function(){ 
            var isValid = false;
            var regex = /^[a-zA-Z ]*$/;
            isValid = regex.test($("#unit_name").val());
            $("#spnError2").css("display", !isValid ? "block" : "none");
            if(isValid == false){
                $(".name").val('');  
            }
            return isValid;
        })

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('service_city') ?>";
        });
    </script>
    