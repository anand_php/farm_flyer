            <!-- Page content -->
            <div id="page-content">
                <!-- eCommerce Products Header -->
                <div class="content-header">
                    <ul class="nav-horizontal text-center">
                        <li>
                            <a href="<?php echo base_url('ecom_dashboard') ?>"><i class="fa fa-bar-chart"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="gi gi-shop_window"></i> Orders</a>
                        </li>
                        <li>
                            <a href="#"><i class="gi gi-shopping_cart"></i> Order View</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo base_url('products') ?>"><i class="gi gi-shopping_bag"></i> Products</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('products/addProduct') ?>"><i class="gi gi-pencil"></i> Product Edit</a>
                        </li>
                        <li>
                             <a href="<?php echo base_url('customers/view') ?>"><i class="gi gi-user"></i> Customer View</a>
                        </li>
                    </ul>
                </div>
                <!-- END eCommerce Products Header -->

                <!-- Quick Stats -->
                <div class="row text-center">
                    <div class="col-sm-6 col-lg-3">
                        <a href="javascript:void(0)" class="widget widget-hover-effect2">
                            <div class="widget-extra themed-background-success">
                                <h4 class="widget-content-light"><strong>Add New</strong> Product</h4>
                            </div>
                            <div class="widget-extra-full">
                                <span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="javascript:void(0)" class="widget widget-hover-effect2">
                            <div class="widget-extra themed-background-danger">
                                <h4 class="widget-content-light"><strong>Out of</strong> Stock</h4>
                            </div>
                            <div class="widget-extra-full"><span class="h2 text-danger animation-expandOpen">71</span></div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="javascript:void(0)" class="widget widget-hover-effect2">
                            <div class="widget-extra themed-background-dark">
                                <h4 class="widget-content-light"><strong>Top</strong> Sellers</h4>
                            </div>
                            <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">20</span></div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <a href="javascript:void(0)" class="widget widget-hover-effect2">
                            <div class="widget-extra themed-background-dark">
                                <h4 class="widget-content-light"><strong>All</strong> Products</h4>
                            </div>
                            <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">4.982</span></div>
                        </a>
                    </div>
                </div>
                <!-- END Quick Stats -->
                <div>
                    <?php if ($this->session->flashdata('message')) { ?>
                        <script>
                            swal({
                                title: "Done",
                                text: "<?php echo $this->session->flashdata('message'); ?>",
                                timer: 2000,
                                showConfirmButton: false,
                                type: 'success'
                            });
                        </script>
                    <?php }else{} ?>    
                </div>
                <!-- All Products Block -->
                <div class="block full">
                    <!-- All Products Title -->
                    <div class="block-title">
                        <div class="block-options pull-right">
                            <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                        </div>
                        <h2><strong>All</strong> Products</h2>
                        
                    </div>
                    <!-- END All Products Title -->

                    <!-- All Products Content -->
                    <table id="example" class="table table-bordered table-striped table-vcenter">
                        
                        <tbody>
                            <?php //echo"<pre>"; print_r($product); ?>
                            <tr>
                                <td>Product SKU Code</td>
                                <td><?php echo $product->product_sku_code ?></td>
                            </tr>
                            <tr>
                                <td>Product Name</td>
                                <td><?php echo $product->product_name ?></td>
                            </tr>
                            <tr>
                                <td>Product Category</td>
                                <td><?php echo $product->category_name ?></td>
                            </tr>
                            <tr>
                                <td>Product Sub Category</td>
                                <td><?php echo $product->subcategory_name ?></td>
                            </tr>
                            <tr>
                                <td>Brand Brand</td>
                                <td><?php echo $product->brand_name ?></td>
                            </tr>
                            <tr>
                                <td>Product Unit</td>
                                <td><?php echo $product->unit_name ?></td>
                            </tr>
                            <tr>
                                <td>Product Description</td>
                                <td><?php echo $product->product_description ?></td>
                            </tr>
                            <tr>
                                <td>Product Specification</td>
                                <td><?php echo $product->product_specofication ?></td>
                            </tr>
                            <tr>
                                <td>Selling Unit</td>
                                <td><?php echo $product->selling_unit_name ?></td>
                            </tr>
                            <tr>
                                <td>Tax</td>
                                <td><?php echo $product->tax_name ?></td>
                            </tr>
                            <tr>
                                <td>Product Alert Quantity</td>
                                <td><?php echo $product->alert_quantity ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- END All Products Content -->
                </div>
                <!-- END All Products Block -->
            </div>
            <!-- END Page Content -->

            <!-- Modal -->
            
            <!-- Modal -->
            <div id="myModal3" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Varient</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" method="POST" id="variant_form">
                                <input type="hidden" name="product_id" id="product_id" value="">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">Product:</label>
                                    <div class="col-sm-10">
                                         <!-- <div class="form-group"> -->
                                           <select id="product_sku_code" name="product_sku_code" class="select-chosen form-control" data-placeholder="Choose Product" >
                                                <option>Select Product</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                <?php foreach($products['result'] as $row) { ?>
                                                    <option value="<?php echo $row->product_sku_code ?>" ><?php echo $row->product_sku_code ?></option>
                                                <?php }?>
                                            </select>
                                            <span id="head"></span>
                                        <!-- </div> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="pwd">Product Varient:</label>
                                    <div class="col-sm-10">          
                                        <select id="varient_id" name="varient_id" class="select-chosen form-control" data-placeholder="Choose Variant" >
                                            <option>Select Variant</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($variants['result'] as $row) { ?>
                                                <option value="<?php echo $row->varient_id ?>" ><?php echo $row->varient_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head1"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="pwd">Variant SKU Code:</label>
                                    <div class="col-sm-10">          
                                       <input type="text" class="form-control" name="varient_sku_code" id="varient_sku_code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="status_choice" id="somefield" value="true">
                                        <div class="col-md-9">
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">        
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

           
            <!-- END Login Block -->
            <!-- End add inventory modal -->
            <!-- END Login Block -->
        </div>
      </div>
    </div>

           <?php $this->load->view('common/footer.php'); ?>
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->
<!--  -->

<?php include('common/script.php'); ?>

<script type="text/javascript">
    function addVariantModal(productId){
        $("#product_id").val(productId);    
        $('#myModal3').modal('show');
    }
// $('.trash').on('click',function(){
//     //get cover id
//     var id=$(this).data('id');
//     $("#inventory_id").val(id);
//     //set href for cancel button
//     $('#myModal4').modal();
// })

$(document).ready(function(){  
    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    $("#variant_form").submit(function(e){

        e.preventDefault();
        var formData = new FormData($("#variant_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('products/addVariant') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields. 
        // var product_category = document.getElementById('product_category');
        var product_sku_code = $("#product_sku_code").val();
        var varient_id = $("#varient_id").val();
        // var varient_sku_code = document.getElementById('varient_sku_code');
        
        // To check empty form fields.  
        if(product_sku_code == ''){
            document.getElementById('head').innerText = "Please Select Product SKU"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            product_sku_code.focus();
            return false;
        }
        if(varient_id == ''){
            document.getElementById('head1').innerText = "Please Select Product Varient"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            varient_id.focus();
            return false;
        }
       
  
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(product_name, "* Please enter valid product name.")) {
            // if (isImageValid(product_image, "* Please image in valid format.")) {
                return true;
            // }
        // }
        return false;
    }

    
    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('head3').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#product_image").change(function() {  
        readURL(this);
    });
});

$("#product_sku").on('change', function(){
    var product_sku_code = $(this).val();
    $.ajax({
        url:'<?php echo base_url('products/getProductVarientsBySkuCode') ?>',
        type:"post",
        data:{'product_sku_code':product_sku_code},
        success: function(data){
            $("#varient_sku").empty();
            if(data == 'false'){
                $("#varient_sku").empty();
            }else{
                var res = $.parseJSON(data);
                $.each(res,function(key, value)
                {
                    // $("#varient_id").empty();
                    $("#varient_sku").append('<option value=' + value.varient_sku_code + '>' + value.varient_name + '</option>');
                });
            }
        }
    });
});

$("#product_sku_code").on('change', function(){
    var product_sku_code = $(this).val();
    // alert(product_sku_code);
    $.ajax({
        url:'<?php echo base_url('products/getVarient') ?>',
        type:"post",
        data:{'product_sku_code':product_sku_code},
        dataType: 'JSON',
        success: function(data){
            if(data == false){
                var i = 1;
                var code = product_sku_code +'-'+ i;
                $("#varient_sku_code").val(code);
            }else{
                var code = product_sku_code +'-'+ (parseInt(data) + 1) ;
                $("#varient_sku_code").val(code);
            }
        }
    });
});

// $("#product_code").on('change',function(){
//     var product_sku_code = $(this).val();
//     $.ajax({
//         url:'<?php echo base_url('products/getProductName') ?>',
//         type:"post",
//         data:{'product_sku_code':product_sku_code},
//         success: function(data){
//             var res = $.parseJSON(data);
//             $("#varient_name").val(res.product_name + " - ");
//         }
//     });

//     $.ajax({
//         url:'<?php echo base_url('products/getVarient') ?>',
//         type:"post",
//         success: function(data){
//             var res = $.parseJSON(data);
//             $("#varient_sku_code").val(product_sku_code+'-'+res);
//         }
//     });
// })

$("#product_name").on('blur', function(){
    var product_name = $(this).val();
    $.ajax({
        url:'<?php echo base_url('products/checkExists') ?>',
        type:"post",
        data:{'product_name':product_name},
        success: function(data){
            if(data == 'true'){
                $("#product_name").val('');
                alert("Product Name is already exists!");
            }
        }
    });
});

$("#unit_fk_key").on('change', function(){
    var unit_fk_key = $(this).val();
    $.ajax({
        url:'<?php echo base_url('products/getSellingUnits') ?>',
        type:"post",
        data:{'unit_fk_key':unit_fk_key},
        success: function(data){
            $("#selling_unit").empty();
            if(data == 'false'){
                $("#selling_unit").empty();
            }else{
                var res = $.parseJSON(data);
                $.each(res,function(key, value)
                {
                    $("#selling_unit").append('<option value=' + value.id + '>' + value.unit_name + '</option>');
                });
            }
        }
    });
});

$("#product_category").on('change', function(){
    var product_category = $(this).val();
    $.ajax({
        url:'<?php echo base_url('products/getSubcategoriesByCategory') ?>',
        type:"post",
        data:{'product_category':product_category},
        success: function(data){
            $("#sub_category_fk_id").empty();
            // if(data == 'false'){
            //     $("#sub_category_fk_id").empty();
            // }else{
                var res = $.parseJSON(data);
                $.each(res,function(key, value)
                {
                    $("#sub_category_fk_id").append('<option value=' + value.id + '>' + value.category_name + '</option>');
                });
            // }
        }
    });
});

function checkActivate(id){
    $.ajax({
        url:'<?php echo base_url('products/checkActivate') ?>',
        type:"post",
        data:{'id':id},
        success: function(data){
            if(data){
                window.location.href="<?php echo base_url('products') ?>"
            }
        }
    });
}

$("#reset").on('click', function(){
    window.location.href="<?php echo base_url('products') ?>";
});
</script>
