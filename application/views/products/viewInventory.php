                    <!-- Page content -->
                    <div id="page-content">
                        <!-- eCommerce Products Header -->
                        <div class="content-header">
                            <ul class="nav-horizontal text-center">
                                <li>
                                    <a href="<?php echo base_url('ecom_dashboard') ?>"><i class="fa fa-bar-chart"></i> Dashboard</a>
                                </li>
                                <li>
                                    <a href="#"><i class="gi gi-shop_window"></i> Orders</a>
                                </li>
                                <li>
                                    <a href="#"><i class="gi gi-shopping_cart"></i> Order View</a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url('products') ?>"><i class="gi gi-shopping_bag"></i> Products</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('products/product_pricing') ?>"><i class="gi gi-pencil"></i> Product Edit</a>
                                </li>
                                <li>
                                     <a href="<?php echo base_url('customers/view') ?>"><i class="gi gi-user"></i> Customer View</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END eCommerce Products Header -->

                        <!-- Quick Stats -->
                        <div class="row text-center">
                            <div class="col-sm-6 col-lg-3">
                                <a href="page_ecom_product_edit.html" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-success">
                                        <h4 class="widget-content-light"><strong>Add New</strong> Product</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-danger">
                                        <h4 class="widget-content-light"><strong>Out of</strong> Stock</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 text-danger animation-expandOpen">71</span></div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-dark">
                                        <h4 class="widget-content-light"><strong>Top</strong> Sellers</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">20</span></div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-dark">
                                        <h4 class="widget-content-light"><strong>All</strong> Products</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">4.982</span></div>
                                </a>
                            </div>
                        </div>
                        <!-- END Quick Stats -->

                        <!-- <form action="" method="post"> -->
                            <!-- All Products Block -->
                            <div class="block full table-responsive">
                                <!-- All Products Title -->
                                <div class="block-title">
                                    <div class="block-options pull-right">
                                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                                    </div>
                                    <h2><strong>All</strong> Products Pricing</h2>
                                </div>
                                <form method="post" id="import_csv" enctype="multipart/form-data">
                                    <div class="row text-right">
                                        <div class="col-lg-12" style="margin-bottom: 10px;">
                                            <!-- <input type="file" name="csv_file" id="csv_file" required accept=".csv" /> -->
                                            <button type="submit" class="btn btn-primary">Update Inventory</button>
                                        </div>
                                    </div>
                                </form>
                                
                                <!-- END All Products Title -->

                                <!-- All Products Content -->
                            <table id="example" class="table table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th class="customhead" rowspan="2">Product SKU/Name</th>
                                        <th class="customhead" rowspan="2">Variant SKU</th>
                                        <?php if(!empty($warehouses)){
                                            foreach($warehouses as $warehouse){ ?>
                                                <th class="customhead">WH Code /Location</th>
                                            <?php }
                                        } ?>
                                        <!-- <th class="customhead">WH Code /Location</th>
                                        <th class="customhead">WH Code /Location</th> -->
                                        <!-- <th class="customhead">Action</th> -->
                                    </tr>
                                    <tr>
                                        <?php if(!empty($warehouses)){
                                            foreach($warehouses as $row){ ?>
                                                <th class="customhead"><?php echo $row->warehouse_code ?> (<?php echo $row->city_name ?>)</th>
                                            <?php }
                                        } ?>
                                        
                                        <!-- <th class="customhead">WH101 (Pune)</th> -->
                                        <!-- <th class="customhead">WH102 (surat)</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($products['count'] > 0){
                                        foreach($products['result'] as $product){ ?>
                                            <tr>
                                                <td><?php echo $product->product_sku_code ?>(<?php echo $product->product_name ?>)</td>
                                                <td><?php echo $product->varient_sku_code ?></td>
                                                <?php if(!empty($warehouses)){
                                                    //echo "<pre>"; print_r($warehouses);
                                                    foreach($warehouses as $warehouse){ 
                                                        // $product->id varientId
                                                        
                                                        ?>

                                                        <td>
                                                            <input type="hidden" class="varient_id" name="varient_sku" value="<?php echo $product->varient_sku_code ?>">
                                                            <input type="hidden" class="warehouse_id" name="warehouse_id" value="<?php echo $warehouse->id?>">
                                                            <input type="hidden" class="product_id" name="product_id" value="<?php echo $product->product_sku_code ?>">
                                                            <?php 
                                                                $where = array('product_sku'=>$product->product_sku_code, 'varient_sku'=>$product->varient_sku_code, 'warehouse_id'=>$warehouse->id);
                                                                $query = $this->db->select('quantity')->from('ff_inventory')->where($where)->get()->row();

                                                                if(!empty($query)){ ?>
                                                                <input type="text" name="quantity" class="quantity" value="<?php echo $query->quantity ?>">
                                                            <?php }else{ ?>
                                                                <input type="text" name="quantity" class="quantity">
                                                            <?php }
                                                            ?>
                                                              
                                                            
                                                        </td>
                                                <?php }
                                                } ?>
                                                
                                            </tr>
                                        <?php }
                                    } ?>
                                </tbody>
                            </table>
                            <!-- END All Products Content -->
                            </div>
                            <!-- END All Products Block -->
                        <!-- </form> -->
                        
                    </div>
                    <!-- END Page Content -->

                    <!-- Footer -->
                    <?php $this->load->view('common/footer.php'); ?>
                    <!-- END Footer -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <?php $this->load->view('common/settings') ?>
        <!-- END User Settings -->

        <?php $this->load->view('common/script.php'); ?>
        <script type="text/javascript">
            $('tbody tr td').change(function () {
                var warehouse_id = $(this).find('.warehouse_id').val();
                var product_id = $(this).find('.product_id').val();
                var varient_id = $(this).find('.varient_id').val();
                var quantity = $(this).find('.quantity').val();
                
                $.ajax({
                    url:"<?php echo base_url('products/updateInaventory') ?>",
                    type:"post",
                    data : {'quantity':quantity, 'product_id':product_id, 'warehouse_id':warehouse_id, 'varient_id':varient_id},
                    success: function(data){
                        if(data == 'true'){
                            // alert("Record Inserted Successfully!");
                            // location.reload();
                        }
                    }
                });
                
            });

            $(document).ready(function() {
                $('#example').DataTable();
                $('input[type="search"]').attr('placeholder', 'Search here');
                $('input[type="search"]').addClass('searchh');

            } );
        </script>
    </body>
</html>