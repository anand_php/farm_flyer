                    <!-- Page content -->
                    <div id="page-content">
                        <!-- eCommerce Products Header -->
                        <div class="content-header">
                            <ul class="nav-horizontal text-center">
                                <li>
                                    <a href="<?php echo base_url('ecom_dashboard') ?>"><i class="fa fa-bar-chart"></i> Dashboard</a>
                                </li>
                                <li>
                                    <a href="#"><i class="gi gi-shop_window"></i> Orders</a>
                                </li>
                                <li>
                                    <a href="#"><i class="gi gi-shopping_cart"></i> Order View</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('products') ?>"><i class="gi gi-shopping_bag"></i> Products</a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url('products/product_pricing') ?>"><i class="gi gi-pencil"></i> Product Pricing Edit</a>
                                </li>
                                <li>
                                     <a href="<?php echo base_url('customers/view') ?>"><i class="gi gi-user"></i> Customer View</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END eCommerce Products Header -->

                        <!-- Quick Stats -->
                        <div class="row text-center">
                            <div class="col-sm-6 col-lg-3">
                                <a href="<?php echo base_url('products/addProduct') ?>" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-success">
                                        <h4 class="widget-content-light"><strong>Add New</strong> Product</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-danger">
                                        <h4 class="widget-content-light"><strong>Out of</strong> Stock</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 text-danger animation-expandOpen">71</span></div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-dark">
                                        <h4 class="widget-content-light"><strong>Top</strong> Sellers</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">20</span></div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget widget-hover-effect2">
                                    <div class="widget-extra themed-background-dark">
                                        <h4 class="widget-content-light"><strong>All</strong> Products</h4>
                                    </div>
                                    <div class="widget-extra-full"><span class="h2 themed-color-dark animation-expandOpen">4.982</span></div>
                                </a>
                            </div>
                        </div>
                        <!-- END Quick Stats -->

                        <form action="#" method="post" id="updatePricingForm">
                            <!-- All Products Block -->
                            <div class="block full table-responsive">
                                <!-- All Products Title -->
                                <div class="block-title">
                                    <div class="block-options pull-right">
                                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
                                    </div>
                                    <h2><strong>All</strong> Products Pricing</h2>
                                </div>
                                <div class="row text-right">
                                    <div class="col-lg-12" style="margin-bottom: 10px;">
                                        <button type="submit" class="btn btn-primary">Update Live Pricing</button>
                                    </div>
                                </div>
                                <!-- END All Products Title -->
                                <div class="row text-right">
                                    <form>
                                    <!-- <div class="col-lg-4" style="margin-bottom: 10px;">
                                    
                                       <select id="state" name="state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;">
                                            <option></option>
                                            <?php foreach($states['result'] as $row) { ?>
                                                <option value="<?php echo $row->state_id ?>" ><?php echo $row->state_name ?></option>
                                            <?php }?>
                                        </select>
                                    </div> -->
                                    <div class="col-lg-4" style="margin-bottom: 10px;">
                                        <select id="city" name="city_id" class="city select-chosen" data-placeholder="Choose City" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($cities['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" ><?php echo $row->city_name ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div style="margin-right: 595px;">
                                        <input type="submit" name="search" value="Search" class="btn btn-success" >
                                        <input type="submit" name="reset_btn" value="Reset" class="btn btn-info">
                                    </div>

                                </form>
                                </div>
                                <!-- All Products Content -->
                                <table id="example" class="table table-bordered table-striped table-vcenter">
                                    <thead>
                                    <tr>
                                        <th class="customhead" rowspan="3">Product SKU/Name</th>
                                        <th class="customhead" rowspan="3">Variant SKU</th>
                                        <th class="customhead" rowspan="3">Base Unit</th>
                                        <?php 
                                            if(!empty($dispatchdepo)){
                                                foreach($dispatchdepo as $row){ ?>
                                                    <th class="customhead">DD Code /Location</th>
                                                <?php }
                                            }
                                        ?>
                                    </tr>
                                    <tr>
                                        <?php if(!empty($dispatchdepo)){
                                            foreach($dispatchdepo as $row){ ?>
                                                <th class="customhead"><?php echo $row->dispatch_depot_code ?> (<?php echo $row->city_name?>)</th>
                                            <?php }
                                        } ?>
                                        
                                    </tr>
                                    <tr>
                                        <?php if(!empty($dispatchdepo)){
                                            foreach($dispatchdepo as $row){ ?>
                                                <th class="customhead">Sell Price</th>
                                            <?php }
                                        } ?>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($products['count'] > 0){
                                        foreach($products['result'] as $row){ 
                                            
                                         ?>
                                            <tr>
                                                <td><?php echo $row->product_sku_code ?>(<?php echo $row->product_name ?>)</td>
                                                <td><?php echo $row->varient_sku_code ?></td>
                                                <td><?php echo $row->unit_name ?></td>
                                                <?php if(!empty($dispatchdepo)){
                                                foreach($dispatchdepo as $row2){ 
                                                    // $row2->id
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $where = array('product_id'=>$row->id, 'varient_id'=> $row->varientId, 'dispatch_depot_id'=> $row2->id);
                                                        $query = $this->db->select('sell_price')->from('ff_pricing_selling')->where($where)->get()->row();
                                                        ?>
                                                        <input type="hidden" class="varient_id" name="varient_id" value="<?php echo $row->varientId?>">
                                                        <input type="hidden" class="dispatch_depot_id" name="dispatch_depot_id" value="<?php echo $row2->id?>">
                                                        <input type="hidden" class="product_id" name="product_id" value="<?php echo $row->id ?>">
                                                        <?php if(!empty($query)){ ?>
                                                            <input type="text" name="sell_price" class="sell_price" value="<?php echo $query->sell_price ?>">
                                                        <?php }else{ ?>
                                                            <input type="text" name="sell_price" class="sell_price" >
                                                        <?php } ?>
                                                        
                                                       
                                                    </td>
                                                       
                                                        
                                                <?php } } ?>
                                               
                                            </tr>
                                        <?php  }
                                    } ?>
                                    
                                </tbody>
                            </table>
                                <!-- END All Products Content -->
                        </div>
                            <!-- END All Products Block -->
                    </form>
                        
                </div>
                    <!-- END Page Content -->

                    <!-- Footer -->
                    <?php $this->load->view('common/footer.php'); ?>
                    <!-- END Footer -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <?php $this->load->view('common/settings') ?>
        <!-- END User Settings -->

        <?php $this->load->view('common/script.php'); ?>
        <script type="text/javascript">
            $('tbody tr td').change(function () {
                var dispatch_depot_id = $(this).find('.dispatch_depot_id').val();
                var product_id = $(this).find('.product_id').val();
                var varient_id = $(this).find('.varient_id').val();
                //var buy_price = $(this).find('.buy_price').val();
                var sell_price = $(this).find('.sell_price').val();
                
                $.ajax({
                    url:"<?php echo base_url('products/updateSellPricing') ?>",
                    type:"post",
                    data : {'product_id':product_id, 'dispatch_depot_id':dispatch_depot_id, 'varient_id':varient_id, "sell_price":sell_price},
                    success: function(data){
                        if(data == 'true'){
                        }
                    }
                });
                
            });
        </script>
    </body>
</html>