    
                    <style type="text/css">
                        input[type=file]{
                          display: inline;
                        }
                        #image_preview{
                          border: 1px solid black;
                          padding: 10px;
                        }
                        #image_preview img{
                          width: 200px;
                          padding: 5px;
                        }
                    </style>
                    <!-- Page content -->
                    <div id="page-content">
                        <!-- eCommerce Product Edit Header -->
                        <div class="content-header">
                            <ul class="nav-horizontal text-center">
                                <li>
                                    <a href="<?php echo base_url('ecom_dashboard') ?>"><i class="fa fa-bar-chart"></i> Dashboard</a>
                                </li>
                                <li>
                                    <a href="#"><i class="gi gi-shop_window"></i> Orders</a>
                                </li>
                                <li>
                                    <a href="#"><i class="gi gi-shopping_cart"></i> Order View</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('products') ?>"><i class="gi gi-shopping_bag"></i> Products</a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url('products/product_pricing') ?>"><i class="gi gi-pencil"></i> Product Edit</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('customers/view') ?>"><i class="gi gi-user"></i> Customer View</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END eCommerce Product Edit Header -->

                        <!-- Product Edit Content -->
                        <!-- <form action="<?php echo base_url('products/add') ?>" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data"> -->
                        <form method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="product_form">
                        <div class="row">
                            <div class="col-lg-6">
                                <!-- General Data Block -->
                                <div class="block">
                                    <!-- General Data Title -->
                                    <div class="block-title">
                                        <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                                    </div>
                                    <!-- END General Data Title -->

                                    <!-- General Data Content -->
                                    
                                        <?php 
                                            $f = 'FF';
                                            $id = $last_product+1;
                                            $number = 1000 . $id;
                                            $sku = $f.$id.$number;
                                        ?>
                                        <!-- <input type="hidden" name="id" value="<?php echo $product->id ?>"> -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="product-id">PID</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly="readonly" id="product_sku_code" name="product_sku_code" class="form-control" value="<?php echo $sku; ?>" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="product-name">Name</label>
                                            <div class="col-md-9">
                                                <input type="text" id="product_name" name="product_name" class="form-control" placeholder="Enter product name.." >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="product-short-description">Product Description</label>
                                            <div class="col-md-9">
                                                <textarea id="product_description" name="product_description" class="form-control" rows="3" placeholder="Enter short description"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="product-description">Specification</label>
                                            <div class="col-md-9">
                                                <!-- CKEditor, you just need to include the plugin (see at the bottom of this page) and add the class 'ckeditor' to your textarea -->
                                                <!-- More info can be found at http://ckeditor.com -->
                                                <textarea id="product_specofication" name="product_specofication" class="ckeditor"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-chosen-multiple">Category</label>
                                            <div class="col-md-9">
                                                <select id="product_category" name="product_category" class="select-chosen form-control" data-placeholder="Choose Category" >
                                                    <option>Select Category</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    <?php foreach($categories['result'] as $row) { ?>
                                                        <option value="<?php echo $row->id ?>" <?php if($product->product_category == $row->id) echo 'selected="selected"'; ?> ><?php echo $row->category_name ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-chosen-multiple">Sub-Category</label>
                                            <div class="col-md-9">
                                                <select id="sub_category_fk_id" name="sub_category_fk_id" class="select-chosen form-control" data-placeholder="Choose Subcategory" >
                                                    <option>Select Subcategory</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-hf-text">Brand Name</label>
                                            <div class="col-md-9">
                                                <select id="brand_fk_id" name="brand_fk_id" class="select-chosen form-control" data-placeholder="Choose Brand">
                                                    <option>Select Brands</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    
                                                    <?php foreach($brands['result'] as $row) { ?>
                                                        <option value="<?php echo $row->id ?>" ><?php echo $row->brand_name ?></option>
                                                    <?php }?>
                                                </select>
                                                <!-- <span class="help-block">Please select brand</span> -->
                                                <span id="head2"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                            <div class="col-md-9">
                                                <input type="hidden" name="status_choice" id="somefield" value="true">
                                                <div class="col-md-9">
                                                    <label class="switch switch-primary">
                                                        <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label">Published?</label>
                                            <div class="col-md-9">
                                                <label class="switch switch-primary">
                                                    <input type="checkbox" id="status" name="status" checked><span></span>
                                                </label>
                                            </div>
                                        </div> -->
                                        <!-- <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Cancel</button>
                                            </div>
                                        </div> -->
                                    <!-- </form> -->
                                    <!-- END General Data Content -->
                                </div>
                                <!-- END General Data Block -->
                            </div>
                            <div class="col-lg-6">
                                <!-- Meta Data Block -->
                                <!-- Product Images Block -->
                                <div class="block">
                                    <!-- Product Images Title -->
                                    <div class="block-title">
                                        <h2><i class="fa fa-picture-o"></i> <strong>Product</strong> Images</h2>
                                    </div>
                                    <!-- END Product Images Title -->

                                    <!-- Product Images Content -->
                                    <div class="block-section">
                                        <!-- Dropzone.js, You can check out https://github.com/enyo/dropzone/wiki for usage examples -->
                                        <!-- <form action="page_ecom_product_edit.html" class="dropzone"></form> -->
                                        <input type="file" name="files[]" id="uploadFile" multiple="multiple">
                                    </div>
                                    <table class="table table-bordered table-striped table-vcenter">
                                        <tbody id="image_preview">
                                            
                                        </tbody>
                                    </table>
                                    <!-- END Product Images Content -->
                                </div>
                                <!-- END Product Images Block -->
                                <!-- Meta Data Block -->
                                <div class="block">
                                    <!-- Meta Data Title -->
                                    <div class="block-title">
                                        <h2><i class="gi gi-cogwheel"></i> <strong>Product</strong> Settings</h2>
                                    </div>
                                    <!-- END Meta Data Title -->

                                    <!-- Meta Data Content -->
                                    <!-- <form action="<?php //echo base_url('products/addProduct2') ?>" method="post" class="form-horizontal form-bordered"> -->
                                        <input type="hidden" name="id" value="<?php echo $product->id ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-chosen">Unit</label>
                                            <div class="col-md-9">
                                                <select id="unit_fk_key" name="unit_fk_key" class="select-chosen form-control" data-placeholder="Choose Unit" >
                                                    <option>Select Unit</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    <?php foreach($units['result'] as $row) { ?>
                                                        <option value="<?php echo $row->id ?>" <?php if($product->unit_fk_key == $row->id) echo 'selected="selected"'; ?> ><?php echo $row->unit_name ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-chosen">Default Sale Unit</label>
                                            <div class="col-md-9">
                                                <select id="selling_unit" name="selling_unit" class="select-chosen form-control" data-placeholder="Choose selling unit" >
                                                    <option>Select Selling Unit</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-chosen">Product Tax</label>
                                            <div class="col-md-9">
                                                <select id="tax" name="tax" class="select-chosen form-control" data-placeholder="Choose selling unit" >
                                                    <option>Select Tax</option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                    
                                                    <?php foreach($taxes['result'] as $row) { ?>
                                                        <option value="<?php echo $row->tax_id ?>" <?php if($product->tax == $row->tax_id) echo 'selected="selected"'; ?> ><?php echo $row->tax_name ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="example-hf-alert quantity">Alert Quantity</label>
                                            <div class="col-md-9">
                                                <input type="alert quantity" id="alert_quantity" name="alert_quantity" class="form-control" placeholder="Enter alert quantity..">
                                                <span class="help-block">Please enter your alert quantity</span>
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                                            </div>
                                        </div>
                                    <!-- END Meta Data Content -->
                                </div>
                                <!-- END Meta Data Block -->
                            </div>
                        </div>

                        </form>
                        <!-- END Product Edit Content -->
                    </div>
                    <!-- END Page Content -->

                    <!-- Footer -->
                    <?php $this->load->view('common/footer.php'); ?>
                    <!-- END Footer -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <?php $this->load->view('common/settings') ?>
        <!-- END User Settings -->
<script type="text/javascript">
    $("#uploadFile").change(function(){
        $('#image_preview').html("");
        var total_file=document.getElementById("uploadFile").files.length;
        for(var i=0;i<total_file;i++){

            $('#image_preview').append("<tr id='"+i+"'><td style='width: 20%;'><img src='"+URL.createObjectURL(event.target.files[i])+"' class='img-responsive center-block' style='max-width: 110px;'></td><td class='text-center'><label class='switch switch-primary'><input type='checkbox' name='myCheckbox' id='myCheckbox' value='' onclick='selectOnlyThis(this)' class='myCheckbox'><span></span></label>Cover</td><td class='text-center' style='display:none';>"+document.getElementById("uploadFile").files[i].name+"</td> <td id='test' class='text-center' style='display:none'; value=''></td> <td class='text-center'><a href='javascript:void(0)' class='btn btn-xs btn-danger'><i class='fa fa-trash-o'></i> Delete</a></tr>");
        }
    });

    function selectOnlyThis(id){
        var myCheckbox = document.getElementsByName("myCheckbox");
        Array.prototype.forEach.call(myCheckbox,function(el){
            el.checked = false;
            // document.getElementById("test").value = "normal";
            
            // $("#myCheckbox").val('normal');
        });
            id.checked = true;
            if(id.checked = true){
                document.getElementById('myCheckbox').value = "cover";
            }else{
                document.getElementById('myCheckbox').value = "normal";
            }
            
    }

    function storeTblValues()
        {
              var TableData = new Array();
              var i=0;

              $('.table tr').each(function(row, tr){

                // if($(tr).find('input[type="checkbox"]:checked')){
                //     var image_status = 'cover';
                // }else{
                //     var image_status = 'normal';
                // }

                var chkbox = $(tr).find('td:eq(1) input[type="checkbox"]');
                if(chkbox.length) {
                    var status = chkbox.prop('checked');
                    if(status == true){
                        var image_status = 'cover';
                    }else{
                        var image_status = 'normal';
                    }
                }
                
                  TableData[row]={

                        "image_status" : image_status
                        ,
                       "image_name" : $(tr).find('td:eq(2)').text()
                       
                  } 
                  i++;   
              }); 
              TableData.shift();  // first row will be empty - so remove
              // alert(TableData);
              return TableData;
        }

    $("#product_form").submit(function(e){

        e.preventDefault();
        

        var TableData = [];
        var groupNameData = [];
        TableData = storeTblValues();

        var x = [];
       
        alert(TableData);
        $.each(TableData, function(key,value) {
            if(value['image_name'] != '')
            {
                
                x.push(value);
            }
           
        });

        var product_sku_code      = document.getElementById('product_sku_code').value;
        var product_name          = document.getElementById('product_name').value;
        var product_description   = document.getElementById('product_description').value;
        var product_specofication = document.getElementById('product_specofication').value;
        var product_category      = document.getElementById('product_category').value;
        var sub_category_fk_id    = document.getElementById('sub_category_fk_id').value;
        var brand_fk_id           = document.getElementById('brand_fk_id').value;
        // var status_choice         = document.getElementById('status_choice').value;
        var unit_fk_key           = document.getElementById('unit_fk_key').value;
        var selling_unit          = document.getElementById('selling_unit').value;
        var tax                   = document.getElementById('tax').value;
        var alert_quantity        = document.getElementById('alert_quantity').value;

        // var formData = new FormData($("#product_form")[0]);
        var status = $("#statusCheck").val();

        var formData = new FormData($("#product_form")[0]);
        formData.append("product_sku_code", product_sku_code);
        formData.append("product_name", product_name);
        formData.append("product_description", product_description);
        formData.append("product_specofication", product_specofication);
        formData.append("product_category", product_category);
        formData.append("sub_category_fk_id", sub_category_fk_id);
        formData.append("brand_fk_id", brand_fk_id);
        // formData.append("status_choice", status_choice);
        formData.append("unit_fk_key", unit_fk_key);
        formData.append("selling_unit", selling_unit);
        formData.append("tax", tax);
        formData.append("alert_quantity", alert_quantity);
        formData.append("cover_image", JSON.stringify(x));

        // var formData = new FormData();

        // formData.append("cover_image", JSON.stringify(x));

        // alert(formData);
        if(TableData != ''){
            $.ajax({
                url:"<?php echo base_url('products/add') ?>",
                type:"post",
                data:formData,
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){ {"id" : 1, "myJSArray" : JSON.stringify(myArray)},
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
        
    });

    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    $("#product_category").on('change',function(){
        var product_category = $(this).val();
        $.ajax({
            url:'<?php echo base_url('products/getProductSubCategory') ?>',
            type:"post",
            data:{'product_category':product_category},
            success: function(data){
                $("#sub_category_fk_id").empty();
                if(data == 'false'){
                    $("#sub_category_fk_id").empty();
                }else{
                    var res = $.parseJSON(data);

                    $.each(res,function(key, value)
                    {
                        $("#sub_category_fk_id").append('<option name="sub_category_fk_id" value=' + value.id + '>' + value.category_name + '</option>');
                        $('#sub_category_fk_id').trigger("chosen:updated");
                    });
                }
            }
        });
    });

    $("#unit_fk_key").on('change', function(){
    var unit_fk_key = $(this).val();
    $.ajax({
        url:'<?php echo base_url('products/getSellingUnits') ?>',
        type:"post",
        data:{'unit_fk_key':unit_fk_key},
        success: function(data){
            // $("#selling_unit").empty();
            if(data == 'false'){
                $("#selling_unit").empty();
            }else{
                var res = $.parseJSON(data);

                $.each(res,function(key, value)
                {
                    $("#selling_unit").append('<option name="selling_unit" value=' + value.id + '>' + value.unit_name + '</option>');
                    $('#selling_unit').trigger("chosen:updated");
                });
            }
        }
    });
});
</script>
        <?php $this->load->view('common/script.php');   