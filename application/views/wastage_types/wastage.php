            <style type="text/css">
                .alert {
                   height:40px;    
                }
                form .error {
                    color: #ff0000;
                }
            </style>          
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            Wastage Type
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Manage Resources</li>
                    <li><a href="#">Wastage Type</a></li>
                </ul>
                <!-- END Datatables Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <div>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <script>
                                    swal({
                                        title: "Done",
                                        text: "<?php echo $this->session->flashdata('message'); ?>",
                                        timer: 2000,
                                        showConfirmButton: false,
                                        type: 'success'
                                    });
                                </script>
                            <?php }else{} ?>    
                        </div>
                        <h2><strong>Wastage Type</strong> integration</h2>
                        <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                    </div>
                    <p>Admin Can Add / edit and Delete Units and Assign Wastage Type</p>

                    <div class="table-responsive">
                        <table id="example" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php if($wastages['count'] > 0 ){
                                    $i = 1; 
                                    foreach($wastages['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->wastage_type_name ?></td>
                                            <td><?php echo $row->description ?></td>
                                            <td>
                                                <?php if($row->status_choice == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                                
                                            </td>
                                            <td>                                              
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url() .'wastage/view/'. $row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url() .'wastage/edit/'. $row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'wastage/delete/'. $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                <?php $i++; } }?>                       
                            </tbody>
                        </table>
                    </div>
                    
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <?php  $this->load->view('common/footer.php'); ?> 
            
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->

<?php $this->load->view('common/script.php'); ?>

        <!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
  <div class="modal-dialog animation-slideDown">

    <!-- Modal content-->
    <div class="modal-content CustomModal">
      <div class="modal-body">
        <div id="loginCustom">
            <!-- Login Title -->
            <div class="login-title text-right">
                <h1>
                    <i class="Unitsss fa fa-hourglass pull-left"></i>
                    <strong>Manage</strong> Wastage Type<br>
                    <br>
                </h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block">
                <form method="post" class="form-horizontal form-bordered" name="waste_type_form" id="waste_type_form">

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="wastage_type_name" name="wastage_type_name" class="form-control name"  placeholder="Please enter name" required>
                            <!-- <span class="help-block">Please enter waste type</span> -->
                            <span id="spnError2" style="color: red; display: none">Please enter only characters</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Description<span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="description" name="description" class="form-control" placeholder="Please enter description" required>
                            <!-- <span class="help-block">Please enter description</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="status_choice" id="somefield" value="true">
                        <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                            </label>
                        </div>  
                    </div>
                    
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Login Block -->
        </div>
      </div>
    </div>

  </div>
</div>
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script> -->
<script type="text/javascript">
    
    
   $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    $('#waste_type_form').validate({
        rules: {
            wastage_type_name: "required",
            description: "required"
        },
        // Specify validation error messages
        messages: {
            wastage_type_name: "Please enter your wastage type name",
            description: "Please enter your wastage type description",
        },
        submitHandler: function(form) {
            $.ajax({
                url: '<?php echo base_url('wastage/add') ?>',
                type: 'post',
                data: $(form).serialize(),
                success: function(response) {
                   if(response == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }            
            });
        }
    });

    $(".name").on('blur', function(){
        var isValid = false;
        var regex = /^[a-zA-Z ]*$/;
        isValid = regex.test($(".name").val());
        $("#spnError2").css("display", !isValid ? "block" : "none");
        if(isValid == false){
            $(".name").val('');  
        }
        return isValid;
    })


function checkActivate(id){
    $.ajax({
        url:'<?php echo base_url('wastage/checkActivate') ?>',
        type:"post",
        data:{'id':id},
        success: function(data){
            alert("Record activated/deactivated successfully!");
        }
    });
}

    $("#wastage_type_name").on('blur', function(){
        var wastage_type_name = $(this).val();
        
        $.ajax({
            url:'<?php echo base_url('wastage/checkExists') ?>',
            type:"post",
            data:{'wastage_type_name':wastage_type_name},
            success: function(data){
                if(data == 'true'){
                    $("#wastage_type_name").val('');
                    alert("Wastage type name already exist!");
                }
            }
        });
    });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('wastage') ?>";
        // $('#myModal').modal('hide');
    });
   
</script>
    