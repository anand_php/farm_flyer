            <style type="text/css">
                .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
                .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
                form .error {
                    color: #ff0000;
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Wastage Type</h1>
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('wastage') ?>">Wastage Type</a></li>
                        <li><a href="#">Edit Wastage Type</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Wastage Type</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Wastage Type</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Wastage Type<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form method="post" class="form-horizontal form-bordered" id="waste_type_form">
                                <input type="hidden" name="id" value="<?php echo $wastage->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="wastage_type_name" name="wastage_type_name" class="form-control name"  placeholder="Please enter name" value="<?php echo $wastage->wastage_type_name ?>" required>
                                        <!-- <span class="help-block">Please enter waste type</span> -->
                                        <span id="spnError2" style="color: red; display: none">Please enter only characters</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Description<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Please Enter description" value="<?php echo $wastage->description ?>" required>
                                        <!-- <span class="help-block">Please enter description</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <?php if($wastage->status_choice == 'true'){ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                        </label>
                                    <?php }else{ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox"><span></span>
                                        </label>
                                    <?php } ?>
                                    <input type="hidden" name="status_choice" id="somefield" value="<?php echo $wastage->status_choice ?>">
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    
    <script type="text/javascript">
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        $('#waste_type_form').validate({
            rules: {
                wastage_type_name: "required",
                description: "required"
            },
            // Specify validation error messages
            messages: {
                wastage_type_name: "Please enter your wastage type name",
                description: "Please enter your wastage type description",
            },
            submitHandler: function(form) {
                $.ajax({
                    url: '<?php echo base_url('wastage/update') ?>',
                    type: 'post',
                    data: $(form).serialize(),
                    success: function(response) {
                       if(response == 'true'){
                            // alert("Record Inserted Successfully!");
                            window.location.href = '<?php echo base_url("wastage") ?>';
                        }
                    }            
                });
            }
        });
        $(".name").on('blur', function(){ 
            var isValid = false;
            var regex = /^[a-zA-Z ]*$/;
            isValid = regex.test($(".name").val());
            $("#spnError2").css("display", !isValid ? "block" : "none");
            return isValid;
        })

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('wastage') ?>";
            // $('#myModal').modal('hide');
        });
    </script>