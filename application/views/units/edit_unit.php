            <style type="text/css">
                form .error {
                    color: #ff0000;
                }
            </style>          
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Unit
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('units') ?>">Unit</a></li>
                        <li><a href="#">Edit Unit</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Unit</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Units</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Unit<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form method="post" class="form-horizontal form-bordered" id="edit_unit_form">
                                <input type="hidden" name="id" value="<?php echo $unit->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="unit_name" name="unit_name" class="form-control" placeholder="Please enter unit name" value="<?php echo $unit->unit_name ?>">
                                        <!-- <span class="help-block">Please enter unit name</span> -->
                                        <span id="spnError2" style="color: red; display: none">Please enter only characters</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Base Unit</label>
                                    <div class="col-md-9">
                                        <select id="base_unit_name" name="base_unit_name" class="select-chosen" data-placeholder="Choose Base Unit" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($units['result'] as $row){ ?> 
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $unit->base_unit_name ) echo 'selected="selected"'; ?> ><?php echo $row->unit_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <!-- <span class="help-block">Please select base unit</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Ratio<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="unit_ratio" name="unit_ratio" class="form-control" placeholder="Please enter unit name" value="<?php echo $unit->unit_ratio ?>">
                                        <!-- <span class="help-block">Please enter unit ratio</span> -->
                                         
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($unit->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input  value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $unit->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">

        // $("#base_unit_name").on('change', function(){
        //     var base_unit_name = $( "#base_unit_name option:selected" ).text();
        //     var unit_name = $("#unit_name").val();

        //     $("#base_unit").val(base_unit_name);

        //     if(base_unit_name == 'gramm'){
        //         if(unit_name == 'kilo' || unit_name == 'kg' || unit_name == 'killogram'){
        //             var ratio = 1000;
        //             $("#unit_ratio").val(ratio);
        //         }else if(unit_name == 'tonn'){
        //             var ratio = 1000000
        //             $("#unit_ratio").val(ratio);
        //         }else if(unit_name == 'quintal'){
        //             var ratio = 100000;
        //             $("#unit_ratio").val(ratio);
        //         }else{
        //             $("#unit_ratio").val('');
        //         }
        //     }else if(base_unit_name == 'kilo' || base_unit_name == 'kg' || base_unit_name == 'kilogram'){
        //         if(unit_name == 'tonn' || unit_name == 'Ton'){
        //             var ratio = 1000;
        //             $("#unit_ratio").val(ratio);
        //         }else if(unit_name == 'quintal'){
        //             var ratio = 100;
        //             $("#unit_ratio").val(ratio);
        //         }else{
        //             $("#unit_ratio").val('');
        //         }
        //         // $("#unit_ratio").val('');
        //     }else if(base_unit_name == 'Ton'){

        //         if(unit_name == 'quintal'){
        //             var ratio = 0.110231;
        //             $("#unit_ratio").val(ratio);
        //         }else{
        //             $("#unit_ratio").val('');
        //         }
                
        //     }else if(base_unit_name == 'mg' || base_unit_name == 'miligram'){
        //         if(unit_name == 'gram'){
        //             var ratio = 1000;
        //             $("#unit_ratio").val(ratio);
        //         }else if(unit_name == 'kg' || unit_name == 'kilo' || unit_name == 'kilogram'){
        //             var ratio = 1000000;
        //             $("#unit_ratio").val(ratio);
        //         }else if(unit_name == 'tonn'){
        //             var ratio = 1000000000;
        //             $("#unit_ratio").val(ratio);
        //         }else if(unit_name == 'quintal'){
        //             var ratio = 100000000;
        //             $("#unit_ratio").val(ratio);
        //         }else{
        //             $("#unit_ratio").val('');
        //         }
        //     }
        // });

        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        $('#edit_unit_form').validate({
            rules: {
                unit_name: "required"
            },
            // Specify validation error messages
            messages: {
                unit_name: "Please enter unit name"
            },
            submitHandler: function(form) {
                $.ajax({
                    url: '<?php echo base_url('units/update') ?>',
                    type: 'post',
                    data: $(form).serialize(),
                    success: function(response) {
                       if(response == 'true'){
                            // alert("Record Inserted Successfully!");
                            window.location.href = '<?php echo base_url("units") ?>';
                        }
                    }            
                });
            }
        });

        $("#unit_name").on('blur', function(){ 
            var isValid = false;
            var regex = /^[a-zA-Z ]*$/;
            isValid = regex.test($("#unit_name").val());
            $("#spnError2").css("display", !isValid ? "block" : "none");
            if(isValid == false){
                $(".name").val('');  
            }
            return isValid;
        })

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('units') ?>";
            // $('#myModal').modal('hide');
        });
    </script>
    