             <style type="text/css">
                .alert {
                   height:40px;    
                }
                form .error {
                    color: #ff0000;
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Units
                    </div>
                    
                </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources </li>
                        <li><a href="#">Units</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <script>
                                        swal({
                                            title: "Done",
                                            text: "<?php echo $this->session->flashdata('message'); ?>",
                                            timer: 2000,
                                            showConfirmButton: false,
                                            type: 'success'
                                        });
                                    </script>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Units</strong> integration</h2>
                            <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                        </div>
                       
                        <p>Admin Can Add / edit and Delete Units and Assign Base Units</p>

                        <div class="table-responsive">
                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Units Name</th>
                                        <th class="text-center">Base Units</th>
                                        <th class="text-center">Ratio</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php if($units['count'] > 0 ){ 
                                        $i = 1;
                                        foreach($units['result'] as $row){ 
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->unit_name ?></td>
                                            <td><?php if($row->base_unit_name){
                                                    echo $row->base_unit;
                                                 }else{ } ?>
                                            </td>
                                            <td><?php echo $row->unit_ratio ?></td>
                                            <td>
                                                <?php if($row->status_choice == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            <td>                                                
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url() .'units/view/'. $row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url() .'units/edit/'. $row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'units/delete/'. $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php $i++; } }  ?>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <?php $this->load->view('common/settings') ?>

    <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
   <?php $this->load->view('common/script.php') ?>      
   
    <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
      <div class="modal-dialog animation-slideDown">

        <!-- Modal content-->
        <div class="modal-content CustomModal">
          <div class="modal-body">
            <div id="loginCustom">
                <!-- Login Title -->
                <div class="login-title text-right">
                    <h1>
                        <i class="Unitsss fa fa-cube pull-left"></i>
                        <strong>Manage</strong> Units<br>
                        <br>
                    </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block">
                        <form method="post" class="form-horizontal form-bordered" id="unit_form">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="unit_name" name="unit_name" class="form-control" placeholder="Please enter unit name" required>
                                    <!-- <span class="help-block">Please enter unit name</span> -->
                                    <span id="spnError2" style="color: red; display: none">Please enter only characters</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Base Unit</label>
                                <div class="col-md-9">
                                    <select id="base_unit_name" name="base_unit_name" class="select-chosen" data-placeholder="Choose Base Unit" style="width: 250px;">
                                        <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                        <?php foreach($units['result'] as $row){ ?> 
                                            <option value="<?php echo $row->id ?>"><?php echo $row->unit_name ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" name="base_unit" id="base_unit" value="">
                                    <!-- <span class="help-block">Please select base unit</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Ratio<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="unit_ratio" name="unit_ratio" class="form-control" placeholder="Enter unit ratio">
                                    <!-- <span class="help-block">Please enter unit ratio</span> -->
                                     
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="status_choice" id="somefield" value="true">
                                <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <label class="switch switch-primary">
                                        <input id="statusCheck" value="" type="checkbox" checked=""><span></span>
                                    </label>
                                </div>
                                <!-- <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <label class="switch switch-primary">
                                        <input type="checkbox" checked="" name="status_choice"><span></span></label>
                                </div> -->
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-9 col-md-offset-3">  
                                    <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    
        $( document ).ready(function() {
            // $("#unit_form").submit(function (event) {
            // e.preventDefault();
            // var formData = new FormData($("#unit_form")[0]);
            
            // $.ajax({
            //     url:"<?php echo base_url('units/add') ?>",
            //     // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/units/add',
            //     type:"post",
            //     data:new FormData(this),
            //     processData:false,
            //     contentType:false,
            //     cache:false,
            //     async:false,
            //     success: function(data){
            //         if(data == true){
            //             alert("Record Inserted Successfully!");
            //             location.reload();
            //         }
            //     }
            // });
        // });
    });

    // $("#base_unit_name").on('change', function(){
    //     var base_unit_name = $( "#base_unit_name option:selected" ).text();
    //     var unit_name = $("#unit_name").val();

    //     $("#base_unit").val(base_unit_name);

    //     if(base_unit_name == 'gram'){
    //         if(unit_name == 'kilo' || unit_name == 'kg' || unit_name == 'kilogram'){
    //             var ratio = 1000;
    //             $("#unit_ratio").val(ratio);
    //         }else if(unit_name == 'ton'){
    //             var ratio = 1000000
    //             $("#unit_ratio").val(ratio);
    //         }else if(unit_name == 'quintal'){
    //             var ratio = 100000;
    //             $("#unit_ratio").val(ratio);
    //         }else{
    //             $("#unit_ratio").val('');
    //         }
    //     }else if(base_unit_name == 'kilo' || base_unit_name == 'kg' || base_unit_name == 'kilogram'){
    //         if(unit_name == 'ton' || unit_name == 'Ton'){
    //             var ratio = 1000;
    //             $("#unit_ratio").val(ratio);
    //         }else if(unit_name == 'quintal'){
    //             var ratio = 100;
    //             $("#unit_ratio").val(ratio);
    //         }else{
    //             $("#unit_ratio").val('');
    //         }
    //         // $("#unit_ratio").val('');
    //     }else if(base_unit_name == 'ton'){

    //         if(unit_name == 'quintal'){
    //             var ratio = 0.110231;
    //             $("#unit_ratio").val(ratio);
    //         }else{
    //             $("#unit_ratio").val('');
    //         }
            
    //     }else if(base_unit_name == 'mg' || base_unit_name == 'miligram'){
    //         if(unit_name == 'gram'){
    //             var ratio = 1000;
    //             $("#unit_ratio").val(ratio);
    //         }else if(unit_name == 'kg' || unit_name == 'kilo' || unit_name == 'kilogram'){
    //             var ratio = 1000000;
    //             $("#unit_ratio").val(ratio);
    //         }else if(unit_name == 'ton'){
    //             var ratio = 1000000000;
    //             $("#unit_ratio").val(ratio);
    //         }else if(unit_name == 'quintal'){
    //             var ratio = 100000000;
    //             $("#unit_ratio").val(ratio);
    //         }else{
    //             $("#unit_ratio").val('');
    //         }
    //     }else{
            
    //         $("#unit_name").val('');
    //         alert("Please enter valid unit names");
    //     }
    // });
       
    $('#unit_form').validate({
        rules: {
            unit_name: "required",
        },
        // Specify validation error messages
        messages: {
            unit_name: "Please enter unit name",
        },
        submitHandler: function(form) {
            $.ajax({
                url: '<?php echo base_url('units/add') ?>',
                type: 'post',
                data: $(form).serialize(),
                success: function(response) {
                   if(response == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }            
            });
        }
    });

    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    function checkActivate(id){
        $.ajax({
            // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/units/checkActivate',
            url:'<?php echo base_url('units/checkActivate') ?>',
            type:"post",
            data:{'id':id},
            success: function(data){
                // alert("Record activated/deactivated successfully!");
            }
        });
    }

    $("#unit_name").on('blur', function(){
        var unit_name = $(this).val();
        $.ajax({
            url:'<?php echo base_url('units/checkExists') ?>',
            type:"post",
            data:{'unit_name':unit_name},
            success: function(data){
                if(data == 'true'){
                    $("#unit_name").val('');
                    alert("Unit name already exists!");
                }
            }
        });

        var isValid = false;
        var regex = /^[a-zA-Z ]*$/;
        isValid = regex.test($("#unit_name").val());
        $("#spnError2").css("display", !isValid ? "block" : "none");
        if(isValid == false){
            $("#unit_name").val('');  
        }
        return isValid;
    });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('units') ?>";
        // $('#myModal').modal('hide');
    });
   
</script>
