            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Products
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('resource_product') ?>">Products</a></li>
                        <li><a href="#">View Product</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Products</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign products</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Product<br>
                                <br>
                            </h1>
                            <!-- <?php echo"<pre>"; print_r($resource_product); ?>  -->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Product Category</td>
                                            <td><?php echo $resource_product->category_name ?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Subcategory</td>
                                            <td><?php echo $resource_product->sub_cat_name ?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Brand</td>
                                            <td><?php echo $resource_product->brand_name?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Varient</td>
                                            <td><?php echo $resource_product->product_varient?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Name</td>
                                            <td><?php echo $resource_product->product_name?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Wastage</td>
                                            <td><?php echo $resource_product->product_wastage?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Unit</td>
                                            <td><?php echo $resource_product->unit_name?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Description</td>
                                            <td><?php echo $resource_product->product_description?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Specification</td>
                                            <td><?php echo $resource_product->product_specofication?></td>
                                        </tr>
                                         <tr>
                                            <td>Product Image</td>
                                            <td>
                                                <img src="<?php echo base_url().'uploads/resource_product/'. $resource_product->product_image?>" style="height: 100px; width: 100px;">
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>Product Status</td>
                                            <td><?php if($resource_product->status_choice == 'true'){ ?>
                                                 <label class="switch switch-primary">
                                                    <input type="checkbox" checked><span></span>
                                                </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" ><span></span>
                                                    </label>
                                                <?php }?>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>