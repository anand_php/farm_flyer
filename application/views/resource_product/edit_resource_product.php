            <style type="text/css">
                .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
                .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            Product
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('resource_product') ?>"> Product</a></li>
                        <li><a href="#">Edit Product</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong> Product</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Categories</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Product<br>
                                <br>
                            </h1>
                            <?php ?>
                            <form method="post" class="form-horizontal form-bordered" id="resourcce_product_edit_form" enctype="multipart/form-data" >
                                <input type="hidden" name="id" value="<?php echo $resource_product->id?>">
                                 <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Category Name </label>
                                    <div class="col-md-9">
                                        <select id="product_category" name="product_category" class="select-chosen" data-placeholder="Choose Category" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($categories['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $resource_product->product_category ) echo 'selected="selected"'; ?> ><?php echo $row->category_name ?></option>
                                            <?php }?>
                                        </select>
                                        
                                        <span class="help-block">Please enter Product name</span>
                                        <span id="head3"></span>
                                        <span id="spnError2" style="color: Red; display: none">Please enter only characters</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Sub Category Name</label>
                                    <div class="col-md-9">
                                        <select id="sub_category_fk_id" name="sub_category_fk_id" class="select-chosen" data-placeholder="Choose Sub category" style="width: 250px;">
                                            <!-- <option></option>Required for data-placeholder attribute to work with Chosen plugin -->
                                            <option value="">Select Subcategory</option>
                                            
                                        </select>
                                        <span class="help-block">Please select sub category</span>
                                        <span id="head1"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Brand Name</label>
                                    <div class="col-md-9">
                                        <select id="brand_fk_id" name="brand_fk_id" class="select-chosen" data-placeholder="Choose Category" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($brands['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $resource_product->brand_fk_id ) echo 'selected="selected"'; ?> ><?php echo $row->brand_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span class="help-block">Please select brand</span>
                                        <span id="head2"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Name </label>
                                    <div class="col-md-9">
                                        <input type="text" id="product_name" name="product_name" class="form-control" placeholder="Enter product name" value="<?php echo $resource_product->product_name ?>">
                                        <span class="help-block">Please enter Product name</span>
                                        <span id="head3"></span>
                                        <span id="spnError2" style="color: Red; display: none">Please enter only characters</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Description</label>
                                    <div class="col-md-9">
                                        <input type="text" id="product_description" name="product_description" class="form-control" placeholder="Enter product description" value="<?php echo $resource_product->product_description ?>">
                                        <span id="head4"></span>
                                        <span class="help-block">Please enter product description</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Specification</label>
                                    <div class="col-md-9">
                                        <input type="text" id="product_specofication" name="product_specofication" class="form-control" placeholder="Enter product specification" value="<?php echo $resource_product->product_specofication?>">
                                        <span id="head5"></span>
                                        <span class="help-block">Please enter product specification</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Unit</label>
                                    <div class="col-md-9">
                                        <select id="unit_fk_key" name="unit_fk_key" class="select-chosen" data-placeholder="Choose Unit" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($units['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $resource_product->unit_fk_key ) echo 'selected="selected"'; ?> ><?php echo $row->unit_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id=""></span>
                                        <span class="help-block">Please enter product unit</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Varient</label>
                                    <div class="col-md-9" id="list">
                                        <?php 
                                            $varients = explode(',',$resource_product->product_varient);
                                            for($i =0; $i< count($varients); $i++){ ?>
                                                <input type="text" id="product_varient" name="product_varient[]" class="form-control" placeholder="Enter product Varient" value="<?php echo $varients[$i] ?>">
                                        <?php } 
                                        ?>
                                        
                                        <button type="button" onclick="addMore();">Add</button>
                                        <span id=""></span>
                                        <span class="help-block">Please enter product varient</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Image</label>
                                    <div class="col-md-9">
                                        <input type="file" id="product_image" class="product_image" name="product_image" value="<?php echo $resource_product->product_image?>">
                                        <img src="<?php echo base_url().'uploads/resource_product/'. $resource_product->product_image ?>" id="profile-img-tag" width="200px">
                                        <span id="head865"></span>
                                        <span class="help-block">Please upload product image</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="status_choice" id="somefield" value="true">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <?php if($resource_product->status_choice == 'true'){ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" type="checkbox" checked><span></span>
                                        </label>
                                    <?php }else{ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" type="checkbox"><span></span>
                                        </label>
                                    <?php } ?>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
               
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->
    <?php $this->load->view('common/script.php'); ?>
   
    <script type="text/javascript">

        function addMore() {
            var fieldHTML = '<input type="text" id="product_varient" name="product_varient[]" class="form-control" placeholder="Enter product Varient">'
          var container = $('#list');
          $("#list").append(fieldHTML);
        }
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        $("#product_category").on('change',function(){
            var t = $(this);
            var product_category = $(this).val();
            $.ajax({
                url:"<?php echo base_url('resource_product/getSubcategoriesByCategory') ?>",
                type:"post",
                data:{'product_category' : product_category},
                success: function(data){
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                        $('#sub_category_fk_id').append('<option value="' + res[i].id + '">' + res[i].sub_cat_name + '</option>');

                        $('#sub_category_fk_id').trigger("chosen:updated");
                        
                        
                    }
                    // $("#sub_category_fk_id").append(options);
                    // $('#sub_category_fk_id').append (options);
                }
            });
        });

        $("#resourcce_product_edit_form").submit(function(e){
            e.preventDefault();
            var formData = new FormData($("#resourcce_product_edit_form")[0]);
            var status = $("#statusCheck").val();
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:"<?php echo base_url('resource_product/update') ?>",
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            alert("Record Updated Successfully!");
                            window.location.href = "<?php echo base_url('resource_product') ?>";
                        }
                    }
                });
            }
        });

    function formValidation() 
    {
        // Make quick references to our fields. 
        var product_category = document.getElementById('product_category');
        var sub_category_fk_id = document.getElementById('sub_category_fk_id');
        var brand_fk_id = document.getElementById('brand_fk_id');
        var product_name = document.getElementById('product_name');
        var product_description = document.getElementById('product_description');
        var product_specofication = document.getElementById('product_specofication');
        var product_image = document.getElementById('product_image');
        
        // To check empty form fields.  
        if(product_category == ''){
            document.getElementById('head').innerText = "Please Select Category"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            product_category.focus();
            return false;
        }
        // if(sub_category_fk_id == ''){
        //     document.getElementById('head1').innerText = "Please Select subcategory"; // This segment displays the validation rule for all fields
        //     $('#head1').delay(2000).hide('slow');
        //     $('#head1').css('display','block');
        //     $('#head1').css('color','red');
        //     sub_category_fk_id.focus();
        //     return false;
        // }
        if(brand_fk_id == ''){
            document.getElementById('head2').innerText = "Please Select Brand"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            brand_fk_id.focus();
            return false;
        }
        if (product_name.value.length == 0) {
            document.getElementById('head3').innerText = "Please Enter Product Name"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            product_name.focus();
            return false;
        }
        if (product_description.value.length == 0) {
            document.getElementById('head4').innerText = "Please Enter Product Description"; // This segment displays the validation rule for all fields
            $('#head4').delay(2000).hide('slow');
            $('#head4').css('display','block');
            $('#head4').css('color','red');
            product_description.focus();
            return false;
        }
        if (product_specofication.value.length == 0) {
            document.getElementById('head5').innerText = "Please Enter Product Specification"; // This segment displays the validation rule for all fields
            $('#head5').delay(2000).hide('slow');
            $('#head5').css('display','block');
            $('#head5').css('color','red');
            product_specofication.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(product_name, "* Please enter valid product name.")) {
            // if (isImageValid(product_image, "* Please image in valid format.")) {
                return true;
            // }
        }
    return false;
}

function isImageValid(img,alertMsg)
{
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($("#product_image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        //alert("Only formats are allowed : "+fileExtension.join(', '));
        $("#head6").html("<span style='color:red;'>Formats Allowd are : </span>"+fileExtension.join(', '));
        $('#head6').delay(2000).hide('slow');
        $('#head6').css('display','block');

        return false;
    }
    else
    {
        //$('#basicForm')[0].submit();
        return  true;
    }
}


    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
        return true;
        } else {
            document.getElementById('head3').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }
        function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#product_image").change(function() {
       
      readURL(this);
    });

    $("#reset").on('click', function(){
        // $(this).closest('form').find("input[type=text], textarea, select, input[type=file]").val("");
       
        window.location.href="<?php echo base_url('resource_product') ?>";
    });
</script>