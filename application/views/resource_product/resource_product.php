             
            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            Products
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Manage Resources</li>
                    <li><a href="#">Product</a></li>
                </ul>
                <!-- END Datatables Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <div>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <script>
                                    swal({
                                        title: "Done",
                                        text: "<?php echo $this->session->flashdata('message'); ?>",
                                        timer: 2000,
                                        showConfirmButton: false,
                                        type: 'success'
                                    });
                                </script>
                               <!--  <div class="alert alert-success alert-dismissible" id="successMessage" role="alert"> 
                                    
                                    <?= $this->session->flashdata('message') ?> </div> -->
                            <?php }else{} ?>    
                        </div>
                        <div>
                           <h2><strong>Products</strong> integration</h2> 
                           <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                        </div>
                        
                    </div>
                    <p>Admin Can Add / edit and Delete Products and Assign Products</p>
                    
                    <div class="table-responsive">
                        <table id="example" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Product Name</th>
                                    <th class="text-center">Product Category</th>
                                    <th class="text-center">Product Subcategory</th>
                                    <th class="text-center">Product Brand</th>
                                    <!-- <th class="text-center">Product Description</th> -->
                                    <th class="text-center">Product Specification</th>
                                    <th class="text-center">Product Quantity</th>
                                    <th class="text-center">Product Image</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php if($resourc_products['count'] > 0 ){
                                    $i =1;  
                                    foreach($resourc_products['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->product_name ?></td>
                                            <td><?php echo $row->category_name ?></td>
                                            <td><?php echo $row->sub_cat_name ?></td>
                                            <td><?php echo $row->brand_name ?></td>
                                            <!-- <td><?php echo $row->product_description ?></td> -->
                                            <td><?php echo $row->product_specofication ?></td>
                                            <td><?php echo $row->unit_name ?></td>
                                            <td>
                                                <img src="<?php echo base_url() . 'uploads/resource_product/'. $row->product_image ?>" alt="avatar" class="img-circle" style="height: 80px; width:100px">
                                            </td>
                                            <td>
                                                <?php if($row->status_choice == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                                
                                            </td>
                                            <td>                                              
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url() .'resource_product/view/'. $row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url() .'resource_product/edit/'. $row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                    <!-- <a href="<?php echo base_url() .'resource_product/delete/'. $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a> -->
                                                   
                                                    <a type="button" data-toggle="tooltip" title="Delete" value="<?php echo $row->id ?>" class="btn btn-xs btn-danger remove" onclick="remove(<?php echo $row->id ?>)"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                <?php $i++; } }?>                       
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <?php  $this->load->view('common/footer.php'); 
            // include('common/footer.php'); 
            ?> 
            <!-- <?php  echo base_url().'views/common/footer.php' ?> -->
            
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->

<?php $this->load->view('common/script.php'); ?>


        <!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
  <div class="modal-dialog animation-slideDown">

    <!-- Modal content-->
    <div class="modal-content CustomModal">
      <div class="modal-body">
        <div id="loginCustom">
            <!-- Login Title -->
            <div class="login-title text-right">
                <h1>
                    <i class="Unitsss fa fa-hourglass pull-left"></i>
                    <strong>Manage</strong> Products<br>
                    <br>
                </h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block">
                <form method="post" class="form-horizontal form-bordered" id="resourcce_product_form" enctype="multipart/form-data" >
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Category Name</label>
                        <div class="col-md-9">
                            <select id="product_category" name="product_category" class="select-chosen" data-placeholder="Choose Category" style="width: 250px;">
                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                <?php foreach($categories['result'] as $row) { ?>
                                    <option value="<?php echo $row->id ?>" ><?php echo $row->category_name ?></option>
                                <?php }?>
                            </select>
                            <span class="help-block">Please select category</span>
                            <span id="head"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Sub Category Name</label>
                        <div class="col-md-9">
                            <select id="sub_category_fk_id" name="sub_category_fk_id" class="select-chosen" data-placeholder="Choose Sub category" style="width: 250px;">
                                <!-- <option></option>Required for data-placeholder attribute to work with Chosen plugin -->
                                <option id="test_subcategory">Select Subcategory</option>
                            </select>
                            <span class="help-block">Please select sub category</span>
                            <span id="head1"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Brand Name</label>
                        <div class="col-md-9">
                            <select id="brand_fk_id" name="brand_fk_id" class="select-chosen" data-placeholder="Choose Brand" style="width: 250px;">
                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                <?php foreach($brands['result'] as $row) { ?>
                                    <option value="<?php echo $row->id ?>" ><?php echo $row->brand_name ?></option>
                                <?php }?>
                            </select>
                            <span class="help-block">Please select brand</span>
                            <span id="head2"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Product Name </label>
                        <div class="col-md-9">
                            <input type="text" id="product_name" name="product_name" class="form-control" placeholder="Enter product name">
                            <span class="help-block">Please enter Product name</span>
                            <span id="head3"></span>
                            <span id="spnError2" style="color: Red; display: none">Please enter only characters</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Product Description</label>
                        <div class="col-md-9">
                            <input type="text" id="product_description" name="product_description" class="form-control" placeholder="Enter product description">
                            <span id="head4"></span>
                            <span class="help-block">Please enter product description</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Product Specification</label>
                        <div class="col-md-9">
                            <input type="text" id="product_specofication" name="product_specofication" class="form-control" placeholder="Enter product specification">
                            <span id="head5"></span>
                            <span class="help-block">Please enter product specification</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Product Unit</label>
                        <div class="col-md-9">
                            <select id="unit_fk_key" name="unit_fk_key" class="select-chosen" data-placeholder="Choose Unit" style="width: 250px;">
                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                <?php foreach($units['result'] as $row) { ?>
                                    <option value="<?php echo $row->id ?>" ><?php echo $row->unit_name ?></option>
                                <?php }?>
                            </select>
                            <span id="head6"></span>
                            <span class="help-block">Please enter product unit</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Product Varient</label>
                        <div class="col-md-9" id="list">
                            <input type="text" id="product_varient" name="product_varient[]" class="form-control" placeholder="Enter product Varient">
                            <button type="button" onclick="addMore();">Add</button>
                            <span id=""></span>
                            <span class="help-block">Please enter product varient</span>
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Product Image</label>
                        <div class="col-md-9">
                            <input type="file" id="product_image" class="product_image" name="product_image">
                            <img src="<?php echo base_url('assets/img/placeholders/avatars/avatar2.jpg')?>" id="profile-img-tag" width="200px">
                            <span id="head6"></span>
                            <span class="help-block">Please upload product image</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="status_choice" id="somefield" value="true">
                        <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Login Block -->
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    function remove(id){
        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
               url: '<?php echo base_url("resource_product/delete") ?>',
               type: 'post',
               data: {'id':id},
               error: function() {
                  alert('Something is wrong');
               },
               success: function(data) {
                    if(data == 'true'){ 
                        window.location.href="<?php echo base_url('resource_product') ?>";
                    }
               }
            });
        }  
    }
    
function closeMe(element) {
  $(element).parent().remove();
}

function addMore() {
    var fieldHTML = '<input type="text" id="product_varient" name="product_varient[]" class="form-control" placeholder="Enter product Varient">'
  var container = $('#list');
  $("#list").append(fieldHTML);
}
   
$(document).ready(function(){
     

    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    $("#resourcce_product_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#resourcce_product_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('resource_product/add') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields. 
        // var product_category = document.getElementById('product_category');
        var product_category = $("#product_category").val();
        var sub_category_fk_id = $("#sub_category_fk_id").val();
        var brand_fk_id = $("#brand_fk_id").val();
        var unit_fk_key = $("#unit_fk_key").val();
       
        var product_name = document.getElementById('product_name');
        var product_description = document.getElementById('product_description');
        var product_specofication = document.getElementById('product_specofication');
        var product_image = document.getElementById('product_image');
        // var product_varient = document.getElementById('product_varient');
        
        // To check empty form fields.  
        if(product_category == ''){
            document.getElementById('head').innerText = "Please Select Category"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            product_category.focus();
            return false;
        }
        if(sub_category_fk_id == ''){
            document.getElementById('head1').innerText = "Please Select subcategory"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            sub_category_fk_id.focus();
            return false;
        }
        if(brand_fk_id == ''){
            document.getElementById('head2').innerText = "Please Select Brand"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            brand_fk_id.focus();
            return false;
        }
        if(unit_fk_key == ''){
            document.getElementById('head6').innerText = "Please Select Brand"; // This segment displays the validation rule for all fields
            $('#head6').delay(2000).hide('slow');
            $('#head6').css('display','block');
            $('#head6').css('color','red');
            unit_fk_key.focus();
            return false;
        }
        if (product_name.value.length == 0) {
            document.getElementById('head3').innerText = "Please Enter Product Name"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            product_name.focus();
            return false;
        }
        
        if (product_description.value.length == 0) {
            document.getElementById('head4').innerText = "Please Enter Product Description"; // This segment displays the validation rule for all fields
            $('#head4').delay(2000).hide('slow');
            $('#head4').css('display','block');
            $('#head4').css('color','red');
            product_description.focus();
            return false;
        }
        if (product_specofication.value.length == 0) {
            document.getElementById('head5').innerText = "Please Enter Product Specification"; // This segment displays the validation rule for all fields
            $('#head5').delay(2000).hide('slow');
            $('#head5').css('display','block');
            $('#head5').css('color','red');
            product_specofication.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(product_name, "* Please enter valid product name.")) {
            if (isImageValid(product_image, "* Please image in valid format.")) {
                return true;
            }
        }
    return false;
}

function isImageValid(img,alertMsg)
{
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($("#product_image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        //alert("Only formats are allowed : "+fileExtension.join(', '));
        $("#head6").html("<span style='color:red;'>Formats Allowd are : </span>"+fileExtension.join(', '));
        $('#head6').delay(2000).hide('slow');
        $('#head6').css('display','block');

        return false;
    }
    else
    {
        //$('#basicForm')[0].submit();
        return  true;
    }
}


// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[A-z\d\-_\s]+$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head3').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head3').delay(2000).hide('slow');
$('#head3').css('display','block');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

    function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#product_image").change(function() {
       
      readURL(this);
    });
});

function checkActivate(id){
    $.ajax({
        url:'<?php echo base_url('resource_product/checkActivate') ?>',
        type:"post",
        data:{'id':id},
        success: function(data){
            if(data){
                window.location.href="<?php echo base_url('resource_product') ?>"
            }
        }
    });
}

$("#reset").on('click', function(){
    // $(this).closest('form').find("input[type=text], textarea, select, input[type=file]").val("");
    // // $("#product_image").val('');
    // // $("#product_category").val('');
    // // $("#product_image").val("");

    // $('#myModal').modal('hide');
    window.location.href="<?php echo base_url('resource_product') ?>";
});
   
</script>
    