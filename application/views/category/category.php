            <style type="text/css">
                form .error {
                    color: #ff0000;
                }
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            Category
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Manage Resources</li>
                    <li><a href="#">Category</a></li>
                </ul>
                <!-- END Datatables Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <div>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <script>
                                    swal({
                                        title: "Done",
                                        text: "<?php echo $this->session->flashdata('message'); ?>",
                                        timer: 2000,
                                        showConfirmButton: false,
                                        type: 'success'
                                    });
                                </script>
                            <?php }else{} ?>    
                        </div>
                        <h2><strong>Category</strong> integration</h2>
                        <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                    </div>
                    <p>Admin Can Add / edit and Delete Units and Assign Categories</p>

                    <div class="table-responsive">
                        <table id="example" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Parent Category</th>
                                    <th class="text-center">Descroption</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php if($categories['count'] > 0 ){
                                    $i =1; 
                                    foreach($categories['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->category_name ?></td>
                                            <td><?php echo $row->parent_category_id ?></td>
                                            <td><?php echo $row->description ?></td>
                                            <td>
                                                <img src="<?php echo base_url() . 'uploads/category/'. $row->category_image ?>" alt="avatar" class="img-circle" style="height: 80px; width:100px">
                                            </td>
                                            <td>
                                                <?php if($row->status_choice == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                                
                                            </td>
                                            <td>                                              
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url() .'category/view/'. $row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url() .'category/edit/'. $row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'category/delete/'. $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                <?php $i++; } }?>                       
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <?php  $this->load->view('common/footer.php'); ?> 
            
            
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->

<?php $this->load->view('common/script.php'); ?>

        <!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
  <div class="modal-dialog animation-slideDown">

    <!-- Modal content-->
    <div class="modal-content CustomModal">
      <div class="modal-body">
        <div id="loginCustom">
            <!-- Login Title -->
            <div class="login-title text-right">
                <h1>
                    <i class="Unitsss fa fa-hourglass pull-left"></i>
                    <strong>Manage</strong> Category<br>
                    <br>
                </h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block">
                <form method="post" class="form-horizontal form-bordered" id="category_form" enctype="multipart/form-data" novalidate>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Parent Category</label>
                        <div class="col-md-9">
                            <select id="parent_category_id" name="parent_category_id" class="select-chosen" data-placeholder="Choose Category" style="width: 250px;" required>
                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                 <?php foreach($categories['result'] as $row) { ?>
                                    <option value="<?php echo $row->id ?>"><?php echo $row->category_name ?></option>
                                <?php }?>
                            </select>
                            <span id="head3"></span>
                            <!-- <span class="help-block">Please select category</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Name <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="category_name" name="category_name" class="form-control" placeholder="Enter category name">
                            <!-- <span class="help-block">Please enter category name</span> -->
                            <span id="head"></span>
                            <span id="spnError2" style="color: Red; display: none">Please enter only characters</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Description <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="description" name="description" class="form-control" placeholder="Enter category description">
                            <span id="head2"></span>
                            <!-- <span class="help-block">Please enter category description</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Image <span>*</span></label>
                        <div class="col-md-9">
                            <input type="file" id="category_image" class="category_image" name="category_image" required>
                            <img src="<?php echo base_url('assets/img/placeholders/avatars/avatar2.jpg')?>" id="profile-img-tag" width="200px" >
                            <span id="head"></span>
                            <!-- <span class="help-block">Please upload category image</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="status_choice" id="somefield" value="true">
                        <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Login Block -->
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    $("#category_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#category_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('category/add') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        // var parent_category_id = $('#parent_category_id').val();
        var category_name = document.getElementById('category_name');
        var description = document.getElementById('description');
        var category_image = document.getElementById('category_image');
  
        // To check empty form fields.  
        // if (parent_category_id == '') {
        //     document.getElementById('head3').innerText = "Please Select Parent Category"; // This segment displays the validation rule for all fields
        //     $('#head3').delay(2000).hide('slow');
        //     $('#head3').css('display','block');
        //     $('#head3').css('color','red');
        //     // parent_category_id.focus();
        //     return false;
        // }
        if (category_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Category Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            category_name.focus();
            return false;
        }
        if (description.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            category_name.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(category_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
            return true;
        // }
    }
    return false;
}

function isImageValid(img,alertMsg)
{
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($("#category_image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        //alert("Only formats are allowed : "+fileExtension.join(', '));
        $("#head").html("<span style='color:red;'>Formats Allowd are : </span>"+fileExtension.join(', '));
        $('#head').delay(2000).hide('slow');
        $('#head').css('display','block');

        return false;
    }
    else
    {
        //$('#basicForm')[0].submit();
        return  true;
    }
}


// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}


    $("#category_name").on('blur', function(){
        var isValid = false;
        var regex = /^[a-zA-Z ]*$/;
        isValid = regex.test($("#category_name").val());
        $("#spnError2").css("display", !isValid ? "block" : "none");
        if(isValid == false){
            $("#category_name").val('');
        }
        return isValid;
    })
});

function checkActivate(id){
    $.ajax({
        // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/category/checkActivate',
        url:'<?php echo base_url('category/checkActivate') ?>',
        type:"post",
        data:{'id':id},
        success: function(data){
            alert("Record activated/deactivated successfully!");
        }
    });
}

    function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".category_image").change(function() {
       
      readURL(this);
    });

    $("#category_name").on('blur', function(){
        var category_name = $(this).val();
        $.ajax({
            url:'<?php echo base_url('category/checkExists') ?>',
            type:"post",
            data:{'category_name':category_name},
            success: function(data){
                if(data == 'true'){
                    $("#category_name").val('');
                    alert("Category already exist!");
                }
            }
        });
    });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('category') ?>";
        // $('#myModal').modal('hide');
    });

   
</script>
    