            <style type="text/css">
                .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
                .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Category
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('category') ?>">Category</a></li>
                        <li><a href="#">Edit Category</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Category</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Categories</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Category<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="edit_category_form" novalidate>
                                <input type="hidden" name="id" value="<?php echo $category->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Parent Category</label>
                                    <div class="col-md-9">
                                        <select id="parent_category_id" name="parent_category_id" class="select-chosen" data-placeholder="Choose Category" style="width: 250px;" required>
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                             <?php foreach($categories['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $category->parent_category_id ) echo 'selected="selected"'; ?> ><?php echo $row->category_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head3"></span>
                                        <!-- <span class="help-block">Please select category</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name <span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="category_name" name="category_name" class="form-control" placeholder="Please enter category name" value="<?php echo $category->category_name ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter category name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Description<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Please enter category description" value="<?php echo $category->description ?>">
                                        <!-- <span class="help-block">Please enter category description</span> -->
                                        <span id="head1"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Image</label>
                                    <div class="col-md-9">
                                        <input type="file" id="example-hf-text" name="category_image" class="form-control category_image" placeholder="Please select file" value="<?php echo $category->category_image ?>">
                                        <img src="<?php echo base_url(). 'uploads/category/'. $category->category_image ?>" id="profile-img-tag" width="200px" />
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please select file</span> -->
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <?php if($category->status_choice == 'true'){ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                        </label>
                                    <?php }else{ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox"><span></span>
                                        </label>
                                    <?php } ?>
                                    
                                    <input type="hidden" name="status_choice" id="somefield" value="<?php echo $category->status_choice ?>">
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    
    <script type="text/javascript">
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".category_image").change(function() {
       
      readURL(this);
    });

    $("#edit_category_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#edit_category_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('category/update') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Updated Successfully!");
                        window.location.href = '<?php echo base_url("category") ?>';
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        // var parent_category_id = $("#parent_category_id").val();
        var category_name = document.getElementById('category_name');
        var description = document.getElementById('description');
        var category_image = document.getElementById('category_image');
  
        // To check empty form fields.   
        // if (parent_category_id == '') {
        //     document.getElementById('head3').innerText = "Please Select Parent Category "; // This segment displays the validation rule for all fields
        //     $('#head3').delay(2000).hide('slow');
        //     $('#head3').css('display','block');
        //     $('#head3').css('color','red');
        //     category_name.focus();
        //     return false;
        // }
        if (category_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Category Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            category_name.focus();
            return false;
        }
        if (description.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            category_name.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(category_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }




// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('category') ?>";
    });

    </script>