            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Category
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('category') ?>">Category</a></li>
                        <li><a href="#">View Category</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Category</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Category</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Category<br>
                                <br>
                            </h1>
                            <!-- <?php //echo"<pre>"; print_r($unit) ?> -->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo $category->category_name ?></td>
                                        </tr>
                                        <tr>
                                            <td>Base Category </td>
                                            <td><?php echo $category->parent_category_id ?></td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td><?php echo $category->description ?></td>
                                        </tr>
                                        <tr>
                                            <td>Image</td>
                                            <td><img src="<?php echo base_url() . 'uploads/category/' . $category->category_image ?>" style="height: 100px; width: 100px;"></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td><?php if($category->status_choice == 'true'){ ?>
                                                 <label class="switch switch-primary">
                                                    <input type="checkbox" checked><span></span>
                                                </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" ><span></span>
                                                    </label>
                                                <?php }?>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>