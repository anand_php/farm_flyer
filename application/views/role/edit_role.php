            <style type="text/css">
                .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
                .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Role
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Roles</li>
                        <li><a href="<?php echo base_url('role') ?>">Role</a></li>
                        <li><a href="#">Edit Role</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Role</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Role</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Role<br>
                                <br>
                            </h1>
                            
                            <form method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="edit_role_form" novalidate>
                                <input type="hidden" name="role_id" value="<?php echo $role->role_id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name <span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="name" name="name" class="form-control" placeholder="Please enter role name" value="<?php echo $role->name ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter category name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Description<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="description" name="description" class="form-control" placeholder="Please enter description" value="<?php echo $role->description ?>">
                                        <!-- <span class="help-block">Please enter category description</span> -->
                                        <span id="head1"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Image</label>
                                    <div class="col-md-9">
                                        <input type="file" id="example-hf-text" name="icon" class="form-control icon" placeholder="Please select file" value="<?php echo $role->icon ?>">
                                        <img src="<?php echo base_url(). 'uploads/roles/'. $role->icon ?>" id="profile-img-tag" width="200px" />
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please select file</span> -->
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <?php if($role->status == 'true'){ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                        </label>
                                    <?php }else{ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox"><span></span>
                                        </label>
                                    <?php } ?>
                                    
                                    <input type="hidden" name="status_choice" id="somefield" value="<?php echo $role->status ?>">
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    
    <script type="text/javascript">
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".icon").change(function() {
       
      readURL(this);
    });

    $("#edit_role_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#edit_role_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('role/update') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Updated Successfully!");
                        window.location.href = '<?php echo base_url("role") ?>';
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        // var parent_category_id = $("#parent_category_id").val();
        var name = document.getElementById('name');
        var description = document.getElementById('description');
        var icon = document.getElementById('icon');
  
        // To check empty form fields.
        if (name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Role Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            name.focus();
            return false;
        }
        if (description.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter role Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            description.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(category_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        // }
        return false;
    }




// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('role') ?>";
    });

    </script>