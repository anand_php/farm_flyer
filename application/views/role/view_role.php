            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Role
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Roles</li>
                        <li><a href="<?php echo base_url('role') ?>">Role</a></li>
                        <li><a href="#">View Role</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Roles</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete role</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Role<br>
                                <br>
                            </h1>
                            <!-- <?php //echo"<pre>"; print_r($unit) ?> -->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo $role->name ?></td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td><?php echo $role->description ?></td>
                                        </tr>
                                        <tr>
                                            <td>Image</td>
                                            <td><img src="<?php echo base_url() . 'uploads/roles/' . $role->icon ?>" style="height: 100px; width: 100px;"></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td><?php if($role->status == 'true'){ ?>
                                                 <label class="switch switch-primary">
                                                    <input type="checkbox" checked><span></span>
                                                </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" ><span></span>
                                                    </label>
                                                <?php }?>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>