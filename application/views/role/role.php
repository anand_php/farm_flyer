            <style type="text/css">
                form .error {
                    color: #ff0000;
                }
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            Roles
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Manage Roles</li>
                    <li><a href="#">Role</a></li>
                </ul>
                <!-- END Datatables Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <div>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <script>
                                    swal({
                                        title: "Done",
                                        text: "<?php echo $this->session->flashdata('message'); ?>",
                                        timer: 2000,
                                        showConfirmButton: false,
                                        type: 'success'
                                    });
                                </script>
                            <?php }else{} ?>    
                        </div>
                        <h2><strong>Role</strong> integration</h2>
                        <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                    </div>
                    <p>Admin Can Add / edit and Delete Roles</p>

                    <div class="table-responsive">
                        <table id="example" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php if($roles['count'] > 0 ){
                                    $i =1; 
                                    foreach($roles['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->name ?></td>
                                            <td><?php echo $row->description ?></td>
                                            <td>
                                                <img src="<?php echo base_url() . 'uploads/roles/'. $row->icon ?>" alt="avatar" class="img-circle" style="height: 80px; width:100px">
                                            </td>
                                            <td>
                                                <?php if($row->status == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->role_id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->role_id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                                
                                            </td>
                                            <td>                                              
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url() .'role/view/'. $row->role_id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url() .'role/edit/'. $row->role_id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'role/delete/'. $row->role_id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                <?php $i++; } }?>                       
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <?php  $this->load->view('common/footer.php'); ?> 
            
            
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->

<?php $this->load->view('common/script.php'); ?>

        <!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
  <div class="modal-dialog animation-slideDown">

    <!-- Modal content-->
    <div class="modal-content CustomModal">
      <div class="modal-body">
        <div id="loginCustom">
            <!-- Login Title -->
            <div class="login-title text-right">
                <h1>
                    <i class="Unitsss fa fa-hourglass pull-left"></i>
                    <strong>Manage</strong> Roles<br>
                    <br>
                </h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block">
                <form method="post" class="form-horizontal form-bordered" id="role_form" enctype="multipart/form-data" novalidate>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Name <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Enter role name">
                            <span id="head"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Description <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="description" name="description" class="form-control" placeholder="Enter category description">
                            <span id="head2"></span>
                            <!-- <span class="help-block">Please enter category description</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Image <span>*</span></label>
                        <div class="col-md-9">
                            <input type="file" id="category_image" class="icon" name="icon" required>
                            
                            <span id="head3"></span>
                            <!-- <span class="help-block">Please upload category image</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="status_choice" id="somefield" value="true">
                        <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Login Block -->
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    $("#role_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#role_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('role/add') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        // var parent_category_id = $('#parent_category_id').val();
        var name = document.getElementById('name');
        var description = document.getElementById('description');
        var icon = document.getElementById('icon');
  
        // To check empty form fields.  
        // if (parent_category_id == '') {
        //     document.getElementById('head3').innerText = "Please Select Parent Category"; // This segment displays the validation rule for all fields
        //     $('#head3').delay(2000).hide('slow');
        //     $('#head3').css('display','block');
        //     $('#head3').css('color','red');
        //     // parent_category_id.focus();
        //     return false;
        // }
        if (name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            name.focus();
            return false;
        }
        if (description.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            description.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(name, "* Please enter valid role name.")) {
                // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        // }
        return false;
    }
});

// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

function checkActivate(role_id){
    $.ajax({
        // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/category/checkActivate',
        url:'<?php echo base_url('role/checkActivate') ?>',
        type:"post",
        data:{'role_id':role_id},
        success: function(data){
            if(data == 'true'){
                window.location.href = "<?php echo base_url('role') ?>";
            }
            
        }
    });
}

    function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".category_image").change(function() {
       
      readURL(this);
    });

    // $("#category_name").on('blur', function(){
    //     var category_name = $(this).val();
    //     $.ajax({
    //         url:'<?php echo base_url('category/checkExists') ?>',
    //         type:"post",
    //         data:{'category_name':category_name},
    //         success: function(data){
    //             if(data == 'true'){
    //                 $("#category_name").val('');
    //                 alert("Category already exist!");
    //             }
    //         }
    //     });
    // });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('role') ?>";
        // $('#myModal').modal('hide');
    });

   
</script>
    