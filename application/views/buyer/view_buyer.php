            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Buyers
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Users</li>
                        <li><a href="<?php echo base_url('buyer') ?>">Buyers</a></li>
                        <li><a href="#">View User</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Buyers</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete buyers</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Buyers<br>
                                <br>
                            </h1>
                            <!-- <?php //echo"<pre>"; print_r($buyer); ?>  -->
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Role</td>
                                            <td><?php echo $buyer->buyer_role?></td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo $buyer->buyer_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?php echo $buyer->email?></td>
                                        </tr>
                                        <tr>
                                            <td>Usaername</td>
                                            <td><?php echo $buyer->username?></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile</td>
                                            <td><?php echo $buyer->mobile?></td>
                                        </tr>
                                        <tr>
                                            <td>Alternate Mobile</td>
                                            <td><?php echo $buyer->alternate_mobile ?></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td><?php echo $buyer->buyer_address?></td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td><?php echo $buyer->state_name?></td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td><?php echo $buyer->city_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Town</td>
                                            <td><?php echo $buyer->town?></td>
                                        </tr>
                                        <tr>
                                            <td>Pincode</td>
                                            <td><?php echo $buyer->pincode?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>