           <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Buyers
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Users</li>
                        <li><a href="<?php echo base_url('buyer') ?>">Buyers</a></li>
                        <li><a href="#">Edit Buyers</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Buyers</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Buyers</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Buyers<br>
                                <br>
                            </h1>
                            <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="buyer_edit_form" novalidate>
                                <input type="hidden" name="buyer_id" value="<?php echo $buyer->buyer_id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Role<span>*</span></label>
                                    <div class="col-md-9">
                                        <select id="buyer_role" name="buyer_role" class="form-control buyer_role">
                                            <option>Select Role</option>
                                            <option value="purchasor" <?php if($buyer->buyer_role == 'purchasor') echo"selected : 'selected'" ?> >Purchasor</option>
                                            <option value="stockist" <?php if($buyer->buyer_role == 'stockist') echo"selected : 'selected'" ?> >Stockist</option>
                                        </select>
                                        <!-- <input type="text" id="buyer_name" name="buyer_name" class="form-control name" placeholder="Enter Buyer name"> -->
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter buyer name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="buyer_name" name="buyer_name" class="form-control name" placeholder="Please enter Buyer name" value="<?php echo $buyer->buyer_name ?>">
                                        <span id="head1"></span>
                                        <!-- <span class="help-block">Please enter buyer name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Mobile<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Please enter mobile" value="<?php echo $buyer->mobile ?>">
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please enter buyer mobile</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Alternate Mobile</label>
                                    <div class="col-md-9">
                                        <input type="text" id="alternate_mobile" name="alternate_mobile" class="form-control alternate_mobile" placeholder="Please enter alternate mobile" value="<?php echo $buyer->alternate_mobile ?>">
                                        <!-- <span class="help-block">Please enter altername mobile</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Email<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="email" name="email" class="form-control email" placeholder="Please enter Email" value="<?php echo $buyer->email ?>">
                                        <span id="head3"></span>
                                        <!-- <span class="help-block">Please enter email</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Username<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="username" name="username" class="form-control username" placeholder="Please enter username" value="<?php echo $buyer->username ?>">
                                        <span id="head4"></span>
                                        <!-- <span class="help-block">Please enter username</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Password<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="password" id="password" name="password" class="form-control password" placeholder="Please enter password">
                                        <span id="head5"></span>
                                        <!-- <span class="help-block">Please enter password</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" id="buyer_address" name="buyer_address" class="form-control buyer_address" placeholder="Please enter address" value="<?php echo $buyer->buyer_address ?>">
                                        <span id="head6"></span>
                                        <!-- <span class="help-block">Please enter address</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">State Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <select id="state_id" name="state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;" required>
                                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                 <?php foreach($states['result'] as $state) { ?>
                                                    <option value="<?php echo $state->state_id ?>" <?php if($state->state_id == $buyer->state_id ) echo 'selected="selected"'; ?> ><?php echo $state->state_name ?></option>
                                                <?php }?>
                                        </select>
                                        <span id="head7"></span>
                                        <!-- <span class="help-block">Please select menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">City<span>*</span></label>
                                    <div class="col-md-9">
                                        <select name="city_id" id="city_id" class="select-chosen" data-placeholder="Choose City" style="width: 250px;" required>
                                            <option></option>
                                            <?php foreach($cities['result'] as $city) { ?>
                                                <option value="<?php echo $city->id ?>" <?php if($city->id == $buyer->city_id ) echo 'selected="selected"'; ?> ><?php echo $city->city_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head8"></span>
                                        <!-- <span class="help-block">Please select city</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Town<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="town" name="town" class="form-control town" placeholder="Please enter town" value="<?php echo $buyer->town ?>">
                                        <span id="head9"></span>
                                        <!-- <span class="help-block">Please enter town</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Pincode<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="pincode" name="pincode" class="form-control pincode" placeholder="Please enter pincode" value="<?php echo $buyer->pincode ?>">
                                        <span id="head10"></span>
                                        <!-- <span class="help-block">Please enter pincode</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($buyer->status_choice == '1'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $buyer->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

<script type="text/javascript">
    $("#state_id").on('change',function(){
        var state_id = $("#state_id").val();
        
        $.ajax({
            url:'<?php echo base_url("buyer/getCityByStateId")?>',
            type:"post",
            data:{'state_id' : state_id},
            success: function(data){
                if(data == 'false'){
                    $("#city_id").empty();
                    $('#city_id').trigger("chosen:updated");
                }else{
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#city_id').append('<option value="' + res[i].id + '">' + res[i].city_name + '</option>');
                         $('#city_id').trigger("chosen:updated");
                        
                    }
                }
            }
        });
    });
        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        $('#buyer_edit_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#buyer_edit_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("buyer/update")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            window.location.href="<?php echo base_url('buyer') ?>";
                        }
                    }
                });
            }
        });
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        var buyer_name = document.getElementById('buyer_name');
        var mobile = document.getElementById('mobile');
        var email = document.getElementById('email');
        var username = document.getElementById('username');
        var state_id = $("#state_id").val();
        var city_id = $("#city_id").val();
        // var password = document.getElementById('password');
        var buyer_address = document.getElementById('buyer_address');
        var town = document.getElementById('town');
        var pincode = document.getElementById('pincode');
  
        // To check empty form fields.  
        if (buyer_name.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Name"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            buyer_name.focus();
            return false;
        }
        if (mobile.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter Mobile"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            mobile.focus();
            return false;
        }
        if (email.value.length == 0) {
            document.getElementById('head3').innerText = "Please Enter Email"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            email.focus();
            return false;
        }
        if (username.value.length == 0) {
            document.getElementById('head4').innerText = "Please Enter Username"; // This segment displays the validation rule for all fields
            $('#head4').delay(2000).hide('slow');
            $('#head4').css('display','block');
            $('#head4').css('color','red');
            username.focus();
            return false;
        }
        // if (password.value.length == 0) {
        //     document.getElementById('head5').innerText = "Please Enter Password"; // This segment displays the validation rule for all fields
        //     $('#head5').delay(2000).hide('slow');
        //     $('#head5').css('display','block');
        //     $('#head5').css('color','red');
        //     password.focus();
        //     return false;
        // }
        if (buyer_address.value.length == 0) {
            document.getElementById('head6').innerText = "Please Enter Address"; // This segment displays the validation rule for all fields
            $('#head6').delay(2000).hide('slow');
            $('#head6').css('display','block');
            $('#head6').css('color','red');
            buyer_address.focus();
            return false;
        }
        if (state_id == '') {
            document.getElementById('head7').innerText = "Please Select State"; // This segment displays the validation rule for all fields
            $('#head7').delay(2000).hide('slow');
            $('#head7').css('display','block');
            $('#head7').css('color','red');
            state_id.focus();
            return false;
        }
        if (city_id == '') {
            document.getElementById('head8').innerText = "Please Select City"; // This segment displays the validation rule for all fields
            $('#head8').delay(2000).hide('slow');
            $('#head8').css('display','block');
            $('#head8').css('color','red');
            city_id.focus();
            return false;
        }
        if (town.value.length == 0) {
            document.getElementById('head9').innerText = "Please Enter Town"; // This segment displays the validation rule for all fields
            $('#head9').delay(2000).hide('slow');
            $('#head9').css('display','block');
            $('#head9').css('color','red');
            buyer_address.focus();
            return false;
        }
        if (pincode.value.length == 0) {
            document.getElementById('head10').innerText = "Please Enter pincode"; // This segment displays the validation rule for all fields
            $('#head10').delay(2000).hide('slow');
            $('#head10').css('display','block');
            $('#head10').css('color','red');
            pincode.focus();
            return false;
        }

        // Check each input in the order that it appears in the form.
        if (inputAlphabet(buyer_name, "* Please enter valid name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
        return true;
        } else {
        document.getElementById('head1').innerText = alertMsg; // This segment displays the validation rule for name.
        $('#head1').delay(2000).hide('slow');
        $('#head1').css('display','block');
        //alert(alertMsg);
        inputtext.focus();
        return false;
        }
    }

    $("#mobile").on("blur", function(){
        
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;

        if (filter.test(mobNum)) {
            if(mobNum.length==10){
                 
            } else {
                document.getElementById('head2').innerText = "Please put 10  digit mobile number";
                $('#head2').delay(2000).hide('slow');
                $('#head2').css('display','block');
                $('#head2').css('color','red');
                $("#user_mobile").val('');
                return false;
            }
        }else{
            document.getElementById('head2').innerText = "Please enter valid number";
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            $("#user_mobile").val('');
            return false;
        }
    });
    
    //function to reset or redirect after click on cancel button
    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('buyer') ?>"
    });
</script>
    