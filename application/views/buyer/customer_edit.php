<style type="text/css">
    .tick {
  background: url(http://wiki.pandorafms.com/images/d/d6/Yes_tick.png) no-repeat;
  width: 20px;
  height: 20px;
  position: absolute;
  margin: 5px 20px;
  display: none;
  margin-top: -20px;
  margin-left: 84px;
}
</style>

                    <!-- Page content -->
                    <div id="page-content">
                        <!-- eCommerce Product Edit Header -->
                        <div class="content-header">
                            <ul class="nav-horizontal text-center">
                                <li>
                                    <a href="page_ecom_dashboard.html"><i class="fa fa-bar-chart"></i> Dashboard</a>
                                </li>
                                <li>
                                    <a href="page_ecom_orders.html"><i class="gi gi-shop_window"></i> Orders</a>
                                </li>
                                <li>
                                    <a href="page_ecom_order_view.html"><i class="gi gi-shopping_cart"></i> Order View</a>
                                </li>
                                <li>
                                    <a href="page_ecom_products.html"><i class="gi gi-shopping_bag"></i> Products</a>
                                </li>
                                <li>
                                    <a href="page_ecom_product_edit.html"><i class="gi gi-pencil"></i> Product Edit</a>
                                </li>
                                <li class="active">
                                    <a href="page_ecom_customer_view.html"><i class="gi gi-user"></i> Customer View</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END eCommerce Product Edit Header -->

                        <!-- Customer Content -->
                        <div class="row">
                            <div class="col-lg-4">
                                <!-- Customer Info Block -->
                                <div class="block">
                                    <!-- Customer Info Title -->
                                    <div class="block-title">
                                        <?php if($buyerData->UserType == 1){ ?>
                                        <h2><i class="fa fa-file-o"></i> <strong>Buyer</strong> Info</h2>
                                        <?php }elseif ($buyerData->UserType == 2) { ?>
                                            <h2><i class="fa fa-file-o"></i> <strong>Seller</strong> Info</h2>
                                        <?php } ?>
                                    </div>
                                    <!-- END Customer Info Title -->

                                    <!-- Customer Info -->
                                    <div class="block-section text-center">
                                       
                                        <a href="javascript:void(0)">
                                           <!--  <img src="img/placeholders/avatars/avatar4@2x.jpg" alt="avatar" class="img-circle"> -->
                                        </a>
                                        <h3>
                                            <strong><?php echo $buyerData->CompanyName;?></strong><br><small></small>
                                        </h3>
                                    </div>
                                    <table class="table table-borderless table-striped table-vcenter">
                                        <tbody>
                                            <tr>
                                                <td class="text-right" style="width: 50%;"><strong>PhoneNumber</strong></td>
                                                <td><?php echo $buyerData->PhoneNumber;?></td>
                                            </tr>
                                             <tr>
                                                <td class="text-right"><strong>PANCard</strong></td>
                                                <td><?php echo $buyerData->PANCard;?>
                                                    <?php if(!empty($buyerData->is_pan_approved ==1)){ ?>
                                                    <span class="tick" style="display: block;"></span>
                                                    <?php } ?>
                                                </td>

                                                <td>  
                                                    <?php if(!empty($buyerData->PANCardDoc)){ ?>
                                                    <a href="#"  onclick="$('#myModal').modal('show');" alt="Image description" class="btn btn-xs btn-default"  ><i class="fa fa-eye"></i></a>
                                                    <?php } else{ ?>
                                                    <a href="#" alt="Image description" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>GSTNumber</strong></td>
                                                <td><?php echo $buyerData->GSTNumber;?>
                                                     <?php if(!empty($buyerData->is_gst_approved ==1)){ ?>
                                                    <span class="tick" style="display: block;"></span>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php if(!empty($buyerData->GSTNumberDoc)){ ?>
                                                    <a href="#" onclick="$('#myModal1').modal('show');" alt="Image description" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                    <?php } else{ ?>
                                                    <a href="#" alt="Image description" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Address</strong></td>
                                                <td><?php echo $buyerData->Address;?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>State</strong></td>
                                                <td><?php echo $buyerData->state_name;?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>City</strong></td>
                                                <td><?php echo $buyerData->city_name;?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Town</strong></td>
                                                <td><?php echo $buyerData->Town;?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Landmark</strong></td>
                                                <td><?php echo $buyerData->Landmark;?></td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="text-right"><strong>Admin Approved</strong></td>
                                                <?php if($buyerData->AdminApproved == 1){ ?>
                                                    <td><?php echo 'Approved';?></td>
                                                <?php }else{ ?>
                                                    <td><?php echo 'Disapproved';?></td>
                                                <?php }?>
                                            </tr>
                                            <tr>
                                                
                                                <div id="dispatch_msg" style="color:green;"></div>
                                                <div id="dispatch_msg_error" style="color:red;"></div>
                                                <?php if($buyerData->UserType == 1){ ?>
                                                <td class="text-right"><strong>Select Dispatch Depot</strong></td>
                                                <td class="dispatch_depo">
                                                    <select  name="dispatch_depo" id="dispatch_depo">
                                                    <option value="">Select</option>
                                                    <?php 
                                                    foreach ($dispatchDepos as $e) { ?>
                                                    <option value="<?php echo $e->id ?>"<?= $e->id == $buyerData->dispatch_depot_id ? ' selected="selected"' : '';?>><?php echo $e->dispatch_depot_name;?></option>
                                                    <?php } ?>
                                                    </select>
                                                </td>
                                            <?php }?>
                                                

                                            <tr>
                                              
                                               
                                                 <?php if($buyerData->UserType == 1){ ?>
                                                <td class="text-right"><strong>Select A/C Manager</strong></td>
                                                <td class="account_manager">
                                                    <select  name="account_manager" id="account_manager">
                                                    <option value="">Select</option>
                                                     <?php 
                                                    foreach ($accountManager as $a) { ?>
                                                    <option value="<?php echo $a->ID ?>"<?= $a->ID == $buyerData->account_manager_id ? ' selected="selected"' : '';?>><?php echo $a->UserName;?></option>
                                                    <?php } ?>
                                                    </select>
                                                    
                                                </td>
                                            <?php } ?>
                                            
                                            </tr>

                                            <?php if($buyerData->UserType == 2){ ?>
                                                <td class="text-right"><strong>Select Warehouse</strong></td>
                                                <td class="warehouse">
                                                    <select  name="warehouse" id="warehouse">
                                                    <option value="">Select</option>
                                                     <?php 
                                                    foreach ($warehouse as $w) { ?>
                                                    <option value="<?php echo $w->id ?>"<?= $w->id == $buyerData->warehouse_id ? ' selected="selected"' : '';?>><?php echo $w->warehouse_name;?></option>
                                                    <?php } ?>
                                                    </select>
                                                    <td><a href="#" data-toggle="Edit" title="Edit" class="btn btn-xs btn-default" id="editWarehouseId"><i class="fa fa-pencil"></i></a></td>
                                                </td>
                                            <?php } ?>

                                            </tr>
                                             
                                            <tr>
                                                <?php if($buyerData->UserType == 2){ ?>
                                                <td class="text-right"><strong>Select A/C Manager</strong></td>
                                                <td class="account_manager">
                                                    <select  name="account_manager" id="account_manager">
                                                    <option value="">Select</option>
                                                     <?php 
                                                    foreach ($accountManager as $a) { ?>
                                                    <option value="<?php echo $a->ID ?>"<?= $a->ID == $buyerData->account_manager_id ? ' selected="selected"' : '';?>><?php echo $a->UserName;?></option>
                                                    <?php } ?>
                                                    </select>
                                                    <td><a href="#" data-toggle="Edit" title="Edit" class="btn btn-xs btn-default" id="editACManagerSeller"><i class="fa fa-pencil"></i></a></td>
                                                </td>
                                            <?php } ?>
                                            </tr>
                                            
                                            
                                            <tr>
                                                <td class="text-right">
                                                    <?php if($buyerData->AdminApproved == 0){ ?>
                                                    <button type="button" id="AdminApprove" class="btn btn-sm btn btn-success"> Approve</button>
                                                    <?php }else{ ?>
                                                         <button type="button" disabled="" class="btn btn-sm btn btn-success"> Approve</button>
                                                    <?php }?>
                                                </td>
                                                <td>
                                                    <?php if($buyerData->AdminApproved == 1){ ?>
                                                    <button type="button" id="AdminDisApprove" class="btn btn-sm btn btn-danger"> Disapprove</button>
                                                    <?php }else{ ?>
                                                          <button type="button" disabled="" class="btn btn-sm btn btn-danger"> Disapprove</button>
                                                    <?php }?>
                                                </td>
                                            </tr>
                                             
                                          
                                        </tbody>

                                    </table>

                                    <!-- END Customer Info -->
                                </div>
                                <!-- END Customer Info Block -->

                                <!-- Quick Stats Block -->
                                <div class="block">
                                    <!-- Quick Stats Title -->
                                    <div class="block-title">
                                        <h2><i class="fa fa-line-chart"></i> <strong>Quick</strong> Stats</h2>
                                    </div>
                                    <!-- END Quick Stats Title -->

                                    <!-- Quick Stats Content -->
                                    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                                        <div class="widget-simple">
                                            <div class="widget-icon pull-right themed-background">
                                                <i class="fa fa-truck"></i>
                                            </div>
                                            <h4 class="text-left">
                                                <strong>4</strong><br><small>Orders in Total</small>
                                            </h4>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                                        <div class="widget-simple">
                                            <div class="widget-icon pull-right themed-background-success">
                                                <i class="fa fa-usd"></i>
                                            </div>
                                            <h4 class="text-left text-success">
                                                <strong>₹ 2.125,00</strong><br><small>Orders Value</small>
                                            </h4>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                                        <div class="widget-simple">
                                            <div class="widget-icon pull-right themed-background-warning">
                                                <i class="fa fa-shopping-cart"></i>
                                            </div>
                                            <h4 class="text-left text-warning">
                                                <strong>3</strong> (₹ 517,00)<br><small>Products in Cart</small>
                                            </h4>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                                        <div class="widget-simple">
                                            <div class="widget-icon pull-right themed-background-info">
                                                <i class="fa fa-group"></i>
                                            </div>
                                            <h4 class="text-left text-info">
                                                <strong>2</strong><br><small>Referred Members</small>
                                            </h4>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                                        <div class="widget-simple">
                                            <div class="widget-icon pull-right themed-background-danger">
                                                <i class="fa fa-heart"></i>
                                            </div>
                                            <h4 class="text-left text-danger">
                                                <strong>15</strong><br><small>Favorite Products</small>
                                            </h4>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                                        <div class="widget-simple">
                                            <div class="widget-icon pull-right themed-background-muted">
                                                <i class="fa fa-ticket"></i>
                                            </div>
                                            <h4 class="text-left text-muted">
                                                <strong>2</strong><br><small>Tickets</small>
                                            </h4>
                                        </div>
                                    </a>
                                    <!-- END Quick Stats Content -->
                                </div>
                                <!-- END Quick Stats Block -->
                            </div>
                           
                            <div class="col-lg-8">
                                <!-- Orders Block -->
                                <div class="block">
                                    <!-- Orders Title -->
                                    <div class="block-title">
                                        <div class="block-options pull-right">
                                            
                                        </div>
                                        <h2><i class="fa fa-user"></i> <strong>Users</strong><?php echo '('.count($buyers).')'; ?></h2>
                                        <div id="Statusmsg" style="color: green;"></div>
                                        <div id="Statuserrormsg" style="color: red;"></div>
                                    </div>
                                    <!-- END Orders Title -->

                                    <!-- Orders Content -->
                                    <table class="table table-bordered table-striped table-vcenter">
                                        <tbody>
                                             <?php 
                                        if(count($buyers) > 0){ 
                                        $i = 1;
                                        foreach($buyers as $row){ ?>
                                            <tr>
                                                <td class="text-center" style="width: 50px;"><?php echo $i ?></td>
                                                <td class="hidden-xs" style="width: 15%;"><?php echo $row->UserName; ?></td>
                                                <td class="text-right hidden-xs" style="width: 10%;"><?php echo $row->UserMobileNumber; ?></td>
                                                <td><?php echo $row->UserEmailID; ?></td>
                                                <td><?php echo isset($row->UserRoleName)?$row->UserRoleName:''; ?></td>
                                                <td><?php echo date('d-m-Y',strtotime($row->Created_on)); ?></td>
                                               <?php if($row->Status == '1'){ ?>
                                                <td>
                                                    <input type="hidden" name="status_choice" class="somefield" value="true">
                                                     <input type="hidden" name="UserID" class="UserID" value="<?php echo $row->UserID ?>">
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" class="checkStatus" checked onclick="checkActivate(this,<?php echo $row->UserID ?>)"><span></span>
                                                    </label>
                                                    <td>
                                                <?php }else{ ?>
                                                    <td>
                                                    <input type="hidden" name="status_choice" class="somefield" value="true">
                                                     <input type="hidden" name="UserID" class="UserID" value="<?php echo $row->UserID ?>">
                                                    <label class="switch switch-primary">
                                                         <input type="checkbox" class="checkStatus"  onclick="checkActivate(this,<?php echo $row->UserID ?>)"><span></span>
                                                    </label>
                                                    <td>

                                                <?php } ?>
                                            </tr>
                                            <?php $i++; } } ?>
                                        </tbody>
                                    </table>
                                    <!-- END Orders Content -->
                                </div>
                                <!-- END Orders Block -->

                                <!-- Products in Cart Block -->
                                <div class="block">
                                    <!-- Products in Cart Title -->
                                    <div class="block-title">
                                        <div class="block-options pull-right">
                                            <span class="label label-success"><strong>₹ 517,00</strong></span>
                                        </div>
                                        <h2><i class="fa fa-shopping-cart"></i> <strong>Products</strong> in Cart (3)</h2>
                                    </div>
                                    <!-- END Products in Cart Title -->

                                    <!-- Products in Cart Content -->
                                    <table class="table table-bordered table-striped table-vcenter">
                                        <tbody>
                                            <tr>
                                                <td class="text-center" style="width: 100px;"><a href="page_ecom_product_edit.html"><strong>PID.8715</strong></a></td>
                                                <td class="hidden-xs" style="width: 15%;"><a href="page_ecom_product_edit.html">Product #98</a></td>
                                                <td class="text-right hidden-xs" style="width: 10%;"><strong>₹399,00</strong></td>
                                                <td><span class="label label-success">Available (479)</span></td>
                                                <td class="text-center" style="width: 70px;">
                                                    <a href="page_ecom_product_edit.html" data-toggle="tooltip" title="" class="btn btn-xs btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center"><a href="page_ecom_product_edit.html"><strong>PID.8725</strong></a></td>
                                                <td class="hidden-xs"><a href="page_ecom_product_edit.html">Product #98</a></td>
                                                <td class="text-right hidden-xs"><strong>₹59,00</strong></td>
                                                <td><span class="label label-success">Available (163)</span></td>
                                                <td class="text-center">
                                                    <a href="page_ecom_product_edit.html" data-toggle="tooltip" title="" class="btn btn-xs btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center"><a href="page_ecom_product_edit.html"><strong>PID.8798</strong></a></td>
                                                <td class="hidden-xs"><a href="page_ecom_product_edit.html">Product #98</a></td>
                                                <td class="text-right hidden-xs"><strong>₹59,00</strong></td>
                                                <td><span class="label label-danger">Out of Stock</span></td>
                                                <td class="text-center">
                                                    <a href="page_ecom_product_edit.html" data-toggle="tooltip" title="" class="btn btn-xs btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END Products in Cart Content -->
                                </div>
                                <!-- END Products in Cart Block -->

                                <!-- Customer Addresses Block -->
                                <div class="block">
                                    <!-- Customer Addresses Title -->
                                    <div class="block-title">
                                        <h2><i class="fa fa-building-o"></i> <strong>Customer</strong> Addresses (<?php echo count($allDeliveryAddressOfCompany); ?>)</h2>
                                    </div>
                                    <!-- END Customer Addresses Title -->

                                    <!-- Customer Addresses Content -->
                                     <?php 
                                        if(count($allDeliveryAddressOfCompany) > 0){ 
                                        $i = 1;
                                        foreach($allDeliveryAddressOfCompany as $row){ ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <!-- Billing Address Block -->
                                            <div class="block">
                                                <!-- Billing Address Title -->
                                                <div class="block-title">
                                                    <h2>Billing Address</h2>
                                                </div>
                                                <!-- END Billing Address Title -->

                                                <!-- Billing Address Content -->
                                                <h4><strong><?php echo $row->address_nick_name; ?></strong></h4>
                                                <address>
                                                    <?php echo $row->contact_person_name; ?>
                                                    <?php echo $row->address; ?><br>

                                                    <?php echo $row->pincode; ?><br>
                                                    
                                                    <i class="fa fa-phone"></i> <?php echo $row->contact_person_number; ?><br>
                                                  <!--   <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">amitagarwal@example.com</a> -->
                                                </address>
                                                <!-- END Billing Address Content -->
                                            </div>
                                            <!-- END Billing Address Block -->
                                        </div>
                                        
                                    </div>
                                <?php }} ?>
                                    <!-- END Customer Addresses Content -->
                                </div>
                                <!-- END Customer Addresses Block -->

                                <!-- Private Notes Block -->
                                <div class="block full">
                                    <!-- Private Notes Title -->
                                    <div class="block-title">
                                        <h2><i class="fa fa-file-text-o"></i> <strong>Private</strong> Notes</h2>
                                    </div>
                                    <!-- END Private Notes Title -->

                                    <!-- Private Notes Content -->
                                    <div class="alert alert-info">
                                        <i class="fa fa-fw fa-info-circle"></i> This note will be displayed to all employees but not to customers.
                                    </div>
                                    <form action="page_ecom_customer_view.html" method="post" onsubmit="return false;">
                                        <textarea id="private-note" name="private-note" class="form-control" rows="4" placeholder="Your note.."></textarea>
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Note</button>
                                    </form>
                                    <!-- END Private Notes Content -->
                                </div>
                                <!-- END Private Notes Block -->
                            </div>
                        </div>
                        <!-- END Customer Content -->
                    </div>
                    <!-- END Page Content -->

                    <!-- Footer -->
                  <?php $this->load->view('common/footer.php'); ?>
                    <!-- END Footer -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
                            <fieldset>
                                <legend>Vital Info</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Username</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static">Admin</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" id="user-settings-email" name="user-settings-email" class="form-control" value="admin@example.com">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
                                    <div class="col-md-8">
                                        <label class="switch switch-primary">
                                            <input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Please choose a complex one..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="..and confirm it!">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->

        <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
        <?php $this->load->view('common/script.php'); ?>
        <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
            <div class="modal-dialog animation-slideDown">

            <!-- Modal content-->
            <div class="modal-content CustomModal">
              <div class="modal-body">
                <div id="loginCustom">
                    <!-- Login Title -->
                    <div class="login-title text-right">
                        <h1>
                            <i class="Unitsss fa fa-hourglass pull-left"></i>
                            <strong>View</strong>Image<br>
                            <br>
                        </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block form-horizontal">
                         <!--  <form method="post" class="form-horizontal form-bordered" id="category_form" enctype="multipart/form-data" novalidate> -->
                            <div id="msg" style="color: green;"></div>
                            <div id="errormsg" style="color: red;"></div>
                            <input type="hidden" id="company_id" value="<?php echo $buyerData->ID; ?>">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">PAN Card Image</label>
                                <div class="col-md-9">
                                    <img src="<?php echo base_url();?>uploads/panDocs/<?php echo $buyerData->PANCardDoc; ?>" id="profile-img-tag" width="200px" >
                                    <span id="head"></span>
                                    <!-- <span class="help-block">Please upload category image</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3" style="padding-bottom: 10px;">
                                   <!--  <button type="button" id="Approve" class="btn btn-sm btn btn-success"> Approve</button> -->
                                    <?php if($buyerData->is_pan_approved == 0){ ?>
                                    <button type="button" id="Approve" class="btn btn-sm btn btn-success"> Approve</button>
                                    <?php }else{ ?>
                                    <button type="button" disabled="" class="btn btn-sm btn btn-success"> Approve</button>
                                    <?php }?>
                                    <?php if($buyerData->is_pan_approved == 1){ ?>
                                    <button type="button" id="Disapprove" class="btn btn-sm btn btn-danger"></i> Disapprove</button>
                                     <?php }else{ ?>
                                    <button type="button" disabled="" class="btn btn-sm btn btn-danger"></i> Disapprove</button>
                                    <?php } ?>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                       <!-- </form> -->
                    </div>
                    <!-- END Login Block -->
                </div>
              </div>
            </div>

        </div>
        </div>
         <div id="myModal1" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
            <div class="modal-dialog animation-slideDown">

            <!-- Modal content-->
            <div class="modal-content CustomModal">
              <div class="modal-body">
                <div id="loginCustom">
                    <!-- Login Title -->
                    <div class="login-title text-right">
                        <h1>
                            <i class="Unitsss fa fa-hourglass pull-left"></i>
                            <strong>View</strong>Image<br>
                            <br>
                        </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block form-horizontal">
                         <!--  <form method="post" class="form-horizontal form-bordered" id="category_form" enctype="multipart/form-data" novalidate> -->
                            <div id="msg" style="color: green;"></div>
                            <div id="errormsg" style="color: red;"></div>
                            <input type="hidden" id="company_id" value="<?php echo $buyerData->ID; ?>">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">GST Image</label>
                                <div class="col-md-9">
                                    <img src="<?php echo base_url();?>uploads/gstDocs/<?php echo $buyerData->GSTNumberDoc; ?>" id="profile-img-tag" width="200px" >
                                    <span id="head"></span>
                                    <!-- <span class="help-block">Please upload category image</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3" style="padding-bottom: 10px;">
                                    <?php if($buyerData->is_gst_approved == 0){ ?>
                                    <button type="button" id="Approve1" class="btn btn-sm btn btn-success"> Approve</button>
                                     <?php }else{ ?>
                                    <button type="button" disabled="" class="btn btn-sm btn btn-success"> Approve</button>
                                    <?php }?>
                                    <?php if($buyerData->is_gst_approved == 1){ ?>
                                    <button type="button" id="Disapprove1" class="btn btn-sm btn btn-danger"></i> Disapprove</button>
                                      <?php }else{ ?>
                                    <button type="button" disabled="" class="btn btn-sm btn btn-danger"></i> Disapprove</button>
                                    <?php }?>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset1"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                       <!-- </form> -->
                    </div>
                    <!-- END Login Block -->
                </div>
              </div>
            </div>

        </div>
        </div>
       <!--  <script src="js/vendor/jquery.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>
        <script src="js/helpers/ckeditor/ckeditor.js"></script> -->
    </body>
    <script type="text/javascript">
         $("#editWarehouse").click(function(e){
            var company_id = $("#company_id").val();
            window.location.href='<?php echo base_url() ?>buyer/editBuyerNew/'+company_id;
         });
         $("#editACManager").click(function(e){
            var company_id = $("#company_id").val();
            window.location.href='<?php echo base_url() ?>buyer/editBuyerNew/'+company_id;
         });
         $("#editWarehouseId").click(function(e){
            var company_id = $("#company_id").val();
            window.location.href='<?php echo base_url() ?>buyer/editSellerNew/'+company_id;
         });
        $('select[name="dispatch_depo"]').change(function() {
            
            var id = $("#dispatch_depo").val();
            var company_id = $("#company_id").val();
            
            $.ajax({
                url:"<?php echo base_url('buyer/updateCustomerDepot') ?>",
                type:"post",
                data:{id:id,company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card approved.");
                        document.getElementById('dispatch_msg').innerText = 'Dispatch Depot updated.'; 
                        $('#dispatch_msg').delay(2000).hide('slow');
                        $('#dispatch_msg').css('display','block');
                        //location.reload();
                    }
                    else
                    {
                        //alert("PAN card not approved.");
                        document.getElementById('dispatch_msg_error').innerText = 'Dispatch Depot not updated.'; 
                        $('#dispatch_msg_error').delay(2000).hide('slow');
                        $('#dispatch_msg_error').css('display','block');
                        //location.reload();
                    }
                }
            });
        });
        $('select[name="account_manager"]').change(function() {
            
            var account_manager = $("#account_manager").val();
            var company_id = $("#company_id").val();
            
            $.ajax({
                url:"<?php echo base_url('buyer/updateAccountManager') ?>",
                type:"post",
                data:{account_manager:account_manager,company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card approved.");
                        document.getElementById('dispatch_msg').innerText = 'A/C Manager updated.'; 
                        $('#dispatch_msg').delay(2000).hide('slow');
                        $('#dispatch_msg').css('display','block');
                        //location.reload();
                    }
                    else
                    {
                        //alert("PAN card not approved.");
                        document.getElementById('dispatch_msg_error').innerText = 'A/C Manager not updated.'; 
                        $('#dispatch_msg_error').delay(2000).hide('slow');
                        $('#dispatch_msg_error').css('display','block');
                        //location.reload();
                    }
                }
            });
        });
        $('select[name="warehouse"]').change(function() {
            
            var warehouse = $("#warehouse").val();
            var company_id = $("#company_id").val();
            
            $.ajax({
                url:"<?php echo base_url('buyer/updateWarehouse') ?>",
                type:"post",
                data:{warehouse:warehouse,company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card approved.");
                        document.getElementById('dispatch_msg').innerText = 'Warehouse updated.'; 
                        $('#dispatch_msg').delay(2000).hide('slow');
                        $('#dispatch_msg').css('display','block');
                        //location.reload();
                    }
                    else
                    {
                        //alert("PAN card not approved.");
                        document.getElementById('dispatch_msg_error').innerText = 'Warehouse not updated.'; 
                        $('#dispatch_msg_error').delay(2000).hide('slow');
                        $('#dispatch_msg_error').css('display','block');
                        //location.reload();
                    }
                }
            });
        });
        function checkActivate(e,id)
        {
            var checkStatus = $(e).find('.checkStatus');
            var UserID = e.parentElement.parentElement.children.UserID.value;
            var status = e.checked;

            $.ajax({
                url:"<?php echo base_url('buyer/changeUserStatus') ?>",
                type:"post",
                data:{status:status,UserID:UserID},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card approved.");
                        document.getElementById('Statusmsg').innerText = 'Status updated.'; 
                        $('#Statusmsg').delay(2000).hide('slow');
                        $('#Statusmsg').css('display','block');
                        
                        //location.reload();
                    }
                    else
                    {
                        //alert("PAN card not approved.");
                        document.getElementById('Statuserrormsg').innerText = 'Status not updated.'; 
                        $('#Statuserrormsg').delay(2000).hide('slow');
                        $('#Statuserrormsg').css('display','block');
                        
                        //location.reload();
                    }
                }
            });
        }
    $(document).ready(function(){
       
       
         $("#reset").click(function(e){
            $('#myModal').modal('toggle');
         });
         $("#reset1").click(function(e){
            $('#myModal1').modal('toggle');
         });

        $("#Approve").click(function(e){
            var company_id = $("#company_id").val();
            $.ajax({
                url:"<?php echo base_url('buyer/approvePanDoc') ?>",
                type:"post",
                data:{company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card approved.");
                        document.getElementById('msg').innerText = 'PAN card approved.'; 
                        $('#msg').delay(2000).hide('slow');
                        $('#msg').css('display','block');
                        $('#myModal').modal('toggle');
                        location.reload();
                    }
                    else
                    {
                        //alert("PAN card not approved.");
                        document.getElementById('errormsg').innerText = 'PAN card not approved.'; 
                        $('#errormsg').delay(2000).hide('slow');
                        $('#errormsg').css('display','block');
                        $('#myModal').modal('toggle');
                        location.reload();
                    }
                }
            });
        });
        $("#Disapprove").click(function(e){
            var company_id = $("#company_id").val();
            $.ajax({
                url:"<?php echo base_url('buyer/disapprovePanDoc') ?>",
                type:"post",
                data:{company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card disapproved.");
                        document.getElementById('msg').innerText = 'PAN card disapproved.'; 
                        $('#msg').delay(2000).hide('slow');
                        $('#msg').css('display','block');
                        $('#myModal').modal('toggle');
                        location.reload();
                    }
                    else
                    {
                        //alert("PAN card not disapproved.");
                        document.getElementById('errormsg').innerText = 'PAN card not disapproved.'; 
                        $('#errormsg').delay(2000).hide('slow');
                        $('#errormsg').css('display','block');
                        $('#myModal').modal('toggle');
                        location.reload();
                    }
                }
            });
        });
        $("#Approve1").click(function(e){
            var company_id = $("#company_id").val();
            $.ajax({
                url:"<?php echo base_url('buyer/approveGstDoc') ?>",
                type:"post",
                data:{company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card approved.");
                        document.getElementById('msg').innerText = 'GST approved.'; 
                        $('#msg').delay(2000).hide('slow');
                        $('#msg').css('display','block');
                        $('#myModal1').modal('toggle');
                        location.reload();
                    }
                    else
                    {
                        //alert("PAN card not approved.");
                        document.getElementById('errormsg').innerText = 'GST not approved.'; 
                        $('#errormsg').delay(2000).hide('slow');
                        $('#errormsg').css('display','block');
                        $('#myModal1').modal('toggle');
                        location.reload();
                    }
                }
            });
        });
        $("#Disapprove1").click(function(e){
            var company_id = $("#company_id").val();
            $.ajax({
                url:"<?php echo base_url('buyer/disapproveGstDoc') ?>",
                type:"post",
                data:{company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card disapproved.");
                        document.getElementById('msg').innerText = 'GST disapproved.'; 
                        $('#msg').delay(2000).hide('slow');
                        $('#msg').css('display','block');
                        $('#myModal1').modal('toggle');
                        location.reload();
                    }
                    else
                    {
                        //alert("PAN card not disapproved.");
                        document.getElementById('errormsg').innerText = 'GST not disapproved.'; 
                        $('#errormsg').delay(2000).hide('slow');
                        $('#errormsg').css('display','block');
                        $('#myModal1').modal('toggle');
                        location.reload();
                    }
                }
            });
        });
        $("#AdminApprove").click(function(e){
            var company_id = $("#company_id").val();
            $.ajax({
                url:"<?php echo base_url('buyer/approveCompanyProfile') ?>",
                type:"post",
                data:{company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card approved.");
                        document.getElementById('msg').innerText = 'Profile approved.'; 
                        $('#msg').delay(2000).hide('slow');
                        $('#msg').css('display','block');
                        
                        location.reload();
                    }
                    else
                    {
                        //alert("PAN card not approved.");
                        document.getElementById('errormsg').innerText = 'Profile not approved.'; 
                        $('#errormsg').delay(2000).hide('slow');
                        $('#errormsg').css('display','block');
                        
                        location.reload();
                    }
                }
            });
        });
        $("#AdminDisApprove").click(function(e){
            var company_id = $("#company_id").val();
            $.ajax({
                url:"<?php echo base_url('buyer/disapproveCompanyProfile') ?>",
                type:"post",
                data:{company_id:company_id},
                success: function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj['status'] == true)
                    {
                        //alert("PAN card disapproved.");
                        document.getElementById('msg').innerText = 'Profile disapproved.'; 
                        $('#msg').delay(2000).hide('slow');
                        $('#msg').css('display','block');
                        location.reload();
                    }
                    else
                    {
                        //alert("PAN card not disapproved.");
                        document.getElementById('errormsg').innerText = 'Profile not disapproved.'; 
                        $('#errormsg').delay(2000).hide('slow');
                        $('#errormsg').css('display','block');
                        location.reload();
                        
                    }
                }
            });
        });
    });

    </script>
</html>