            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Warehouse
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Warehouse</li>
                        <li><a href="<?php echo base_url('warehouse') ?>">Warehouse</a></li>
                        <li><a href="#">Edit Warehouse</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Warehouse</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Warehouse and Assign Warehouse</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Warehouse<br>
                                <br>
                            </h1>
                            <form action="#" method="post" class="form-horizontal form-bordered" id="warehouse_edit_form">
                                <input type="hidden" name="id" value="<?php echo $warehouse->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Code<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="warehouse_code" name="warehouse_code" class="form-control" placeholder="Enter warehouse code" value="<?php echo $warehouse->warehouse_code ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter warehouse code</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="warehouse_name" name="warehouse_name" class="form-control" placeholder="Enter warehouse name" value="<?php echo $warehouse->warehouse_name ?>">
                                        <span id="head1"></span>
                                        <!-- <span class="help-block">Please enter warehouse name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Address<span>*</span></label>
                                    <div class="col-md-9">
                                        <textarea type="text" rows="3" cols="3" id="warehouse_address" name="warehouse_address" class="form-control" placeholder="Enter warehouse address"><?php echo $warehouse->warehouse_address ?></textarea>
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please enter warehouse address</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">State<span>*</span></label>
                                    <div class="col-md-9">
                                        <select id="state_id" name="state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                           <?php foreach($states['result'] as $state) { ?>
                                                <option value="<?php echo $state->state_id ?>" <?php if($state->state_id == $warehouse->state_id ) echo 'selected="selected"'; ?> ><?php echo $state->state_name?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head5"></span>
                                        <!-- <span class="help-block">Select Warehouse City</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">City<span>*</span></label>
                                    <div class="col-md-9">
                                        <select id="city_id" name="city_id" class="select-chosen" data-placeholder="Choose Warehouse" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->cities
                                            <?php foreach($cities as $city) { ?>
                                                <option value="<?php echo $city->id ?>" <?php if($city->id == $warehouse->city_id ) echo 'selected="selected"'; ?> ><?php echo $city->city_name?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head3"></span>
                                        <!-- <span class="help-block">Select Warehouse City</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">MAP ID<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="map_id" name="map_id" class="form-control" placeholder="Enter warehouse map id" value="<?php echo $warehouse->latitude .' , '. $warehouse->longitude  ?>">
                                        <span id="head4"></span>
                                        <!-- <span class="help-block">Please enter warehouse map id</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($warehouse->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $warehouse->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyvW_3aj4NtaihWGsT3GojA2I6ba0rD5w&amp;libraries=geometry,places"></script> -->
<script type="text/javascript">
$("#warehouse_address").on('blur', function(){
    var geocoder = new google.maps.Geocoder();
    var address = $("#warehouse_address").val();

    geocoder.geocode( { 'address': address}, function(results, status) {

    if (status == google.maps.GeocoderStatus.OK) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        document.getElementById("map_id").value = latitude +","+ longitude;
        } 
    }); 
});
</script>
    <script type="text/javascript">
        $("#state_id").on('change',function(){
        var state_id = $("#state_id").val();
        
        $.ajax({
            url:'<?php echo base_url("warehouse/getCityByStateId")?>',
            type:"post",
            data:{'state_id' : state_id},
            success: function(data){
                if(data == 'false'){
                    $("#city_id").empty();
                    $('#city_id').trigger("chosen:updated");
                }else{
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#city_id').append('<option value="' + res[i].id + '">' + res[i].city_name + '</option>');
                         $('#city_id').trigger("chosen:updated");
                        
                    }
                }
            }
        });
    });

        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });

            $('#warehouse_edit_form').submit(function(e){

                e.preventDefault();
                var formData = new FormData($("#brand_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:'<?php echo base_url("warehouse/update")?>',
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                alert("Record updated successfully!");
                                window.location.href="<?php echo base_url('warehouse') ?>";
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
        {
            // Make quick references to our fields.
            var warehouse_code = document.getElementById('warehouse_code');
            var warehouse_name = document.getElementById('warehouse_name');
            var warehouse_address = document.getElementById('warehouse_address');
            var city_id = $('#city_id').val();
            var state_id = $('#state_id').val();
            var map_id = document.getElementById('map_id');
      
            // To check empty form fields.  
            if (warehouse_code.value.length == 0) {
                document.getElementById('head').innerText = "Please Enter Warehouse Code"; // This segment displays the validation rule for all fields
                $('#head').delay(2000).hide('slow');
                $('#head').css('display','block');
                $('#head').css('color','red');
                warehouse_code.focus();
                return false;
            }
            if (warehouse_name.value.length == 0) {
                document.getElementById('head1').innerText = "Please Enter Warehouse Name"; // This segment displays the validation rule for all fields
                $('#head1').delay(2000).hide('slow');
                $('#head1').css('display','block');
                $('#head1').css('color','red');
                warehouse_name.focus();
                return false;
            }
            if (warehouse_address.value.length == 0) {
                document.getElementById('head2').innerText = "Please Enter Warehouse Address"; // This segment displays the validation rule for all fields
                $('#head2').delay(2000).hide('slow');
                $('#head2').css('display','block');
                $('#head2').css('color','red');
                warehouse_address.focus();
                return false;
            }
            if (city_id == '') {
                document.getElementById('head3').innerText = "Please Select City Name"; // This segment displays the validation rule for all fields
                $('#head3').delay(2000).hide('slow');
                $('#head3').css('display','block');
                $('#head3').css('color','red');
                city_id.focus();
                return false;
            }
            if (state_id == '') {
                document.getElementById('head5').innerText = "Please Select State Name"; // This segment displays the validation rule for all fields
                $('#head5').delay(2000).hide('slow');
                $('#head5').css('display','block');
                $('#head5').css('color','red');
                city_id.focus();
                return false;
            }
            if (map_id.value.length == 0) {
                document.getElementById('head4').innerText = "Please Enter map id"; // This segment displays the validation rule for all fields
                $('#head4').delay(2000).hide('slow');
                $('#head4').css('display','block');
                $('#head4').css('color','red');
                map_id.focus();
                return false;
            }
            // Check each input in the order that it appears in the form.
            if (inputAlphabet(warehouse_name, "* Please enter valid warehouse name.")) {
                // if (isImageValid(category_image, "* Please image in valid format.")) {
                    return true;
                // }
            }
            return false;
        }

        // Function that checks whether input text is an alphabetic character or not.
        function inputAlphabet(inputtext, alertMsg) {
            var alphaExp = /^[A-z\d\-_\s]+$/;
            if (inputtext.value.match(alphaExp)) {
                return true;
            } else {
                document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
                $('#p1').delay(2000).hide('slow');
                $('#p1').css('display','block');
                //alert(alertMsg);
                inputtext.focus();
                return false;
            }
        }

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('warehouse') ?>";
        });

    </script>
    