            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Warehouse
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Locations</li>
                        <li><a href="<?php echo base_url('warehouse') ?>">Warehouse</a></li>
                        <li><a href="#">View Warehouse</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Warehouse</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Warehouse</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Warehouse<br>
                                <br>
                            </h1>
                            <!-- <?php //echo"<pre>"; print_r($warehouse); ?> -->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>Warehouse Code</td>
                                            <td><?php echo $warehouse->warehouse_code ?></td>
                                        </tr>
                                         <tr>
                                            <td>Name</td>
                                            <td><?php echo $warehouse->warehouse_name ?></td>
                                        </tr>
                                         <tr>
                                            <td>Address</td>
                                            <td><?php echo $warehouse->warehouse_address ?></td>
                                        </tr>
                                         <tr>
                                            <td>State</td>
                                            <td><?php echo $warehouse->state_name ?></td>
                                        </tr>
                                         <tr>
                                            <td>City</td>
                                            <td><?php echo $warehouse->city_name ?></td>
                                        </tr>
                                         <tr>
                                            <td>Latitude</td>
                                            <td><?php echo $warehouse->latitude?></td>
                                        </tr>
                                         <tr>
                                            <td>Longitude</td>
                                            <td><?php echo $warehouse->longitude?></td>
                                        </tr>
                                        
                                         <tr>
                                            <td>Product Status</td>
                                            <td><?php if($warehouse->status_choice == 'true'){ ?>
                                                 <label class="switch switch-primary">
                                                    <input type="checkbox" checked><span></span>
                                                </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" ><span></span>
                                                    </label>
                                                <?php }?>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>