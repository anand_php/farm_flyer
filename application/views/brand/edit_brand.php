            <style type="text/css">
    .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
    .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
</style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Brand
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('brand') ?>">Brand</a></li>
                        <li><a href="#">Edit Brand</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Brand</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Brands</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Brand<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="edit_brand_form">
                                <input type="hidden" name="id" value="<?php echo $brand->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="brand_name" name="brand_name" class="form-control brand_name" placeholder="Please enter brand name" value="<?php echo $brand->brand_name ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter brand name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Image</label>
                                    <div class="col-md-9">
                                        <input type="file" id="example-file-input" name="brand_image" class="category_image">
                                        <img src="<?php echo base_url().'uploads/brand/'. $brand->brand_image ?>" id="profile-img-tag" width="200px">
                                        <!-- <span class="help-block">Please upload brand image</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($brand->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $brand->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });

            $('#edit_brand_form').submit(function(e){

                e.preventDefault();
                var formData = new FormData($("#brand_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:'<?php echo base_url("brand/update")?>',
                        // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/brand/add',
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                alert("Record updated successfully!");
                                window.location.href="<?php echo base_url('brand') ?>";
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
    {
        // Make quick references to our fields.
        var brand_name = document.getElementById('brand_name');
        // var category_image = document.getElementById('category_image');
  
        // To check empty form fields.  
        if (brand_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Category Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            brand_name.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(brand_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

        function readURL(input) {
            if (input.files && input.files[0]) {
                 
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#profile-img-tag').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".category_image").change(function() {
           
          readURL(this);
        });

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('brand')?>";
            // $('#myModal').modal('hide');
        });
    </script>
    