            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sales Inventory
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Inventory</li>
                        <li><a href="<?php echo base_url('sales_inventory') ?>">Sales Inventory</a></li>
                        <li><a href="#">View Sales Inventory</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Sales Inventory</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Sales Inventory</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Sales Inventory<br>
                                <br>
                            </h1>
                            <?php //echo"<pre>"; print_r($sale_inventory); ?>  
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Seller Name</td>
                                            <td><?php echo $sale_inventory->user_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Category Name</td>
                                            <td><?php echo $sale_inventory->category_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Subcategory Name</td>
                                            <td><?php echo $sale_inventory->sub_cat_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Product Name</td>
                                            <td><?php echo $sale_inventory->product_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Product Varient</td>
                                            <td><?php echo $sale_inventory->product_varient ?></td>
                                        </tr>
                                        <tr>
                                            <td>Product Quantiry</td>
                                            <td><?php echo $sale_inventory->product_quantity?></td>
                                        </tr>
                                        <tr>
                                            <td>Unit</td>
                                            <td><?php echo $sale_inventory->unit_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Order Type</td>
                                            <td><?php echo $sale_inventory->order_type?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>