           <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
           <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sales Inventory
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Inventory</li>
                        <li><a href="#">Sales Inventory</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <script>
                                        swal({
                                            title: "Done",
                                            text: "<?php echo $this->session->flashdata('message'); ?>",
                                            timer: 2000,
                                            showConfirmButton: false,
                                            type: 'success'
                                        });
                                    </script>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Sales Inventory</strong> integration</h2>
                            <p class="AddNew"><b><a href="<?php echo base_url('sales_inventory/add_inventory') ?>">ADD NEW +</a> </b></p>
                        </div>
                        <p>Admin Can Add / edit and Delete Sales Inventory  </p>

                        <div class="table-responsive">

                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Seller Name</th>
                                        <th class="text-center">Product Name</th>
                                        <th class="text-center">Product Quantity</th>
                                        <th class="text-center">Order Type</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php 
                                        if($sales_inventory['count'] > 0){ 
                                        $i = 1;
                                        foreach($sales_inventory['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->user_name ?></td>
                                            <td><?php echo $row->product_name ?></td>
                                            <td><?php 
                                                // $quantity = $row->product_quantity % $row->unit_ratio;
                                                // if($quantity == 0){
                                                //     echo $quantity . ' ' . $row->unit_name; 
                                                // }else{
                                                //     echo $quantity . ' ' . $row->base_unit_name; 
                                                // }
                                                echo $row->product_quantity .'  '. $row->base_unit_name 
                                            ?></td>
                                            <td><?php if($row->order_type == 'priority_sale'){
                                                echo "Priority Sale";
                                            }else{
                                                echo"Store for future";
                                            }  ?></td>
                                            <td>
                                                <?php if($row->status_choice == '1'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->sale_inventory_id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->sale_inventory_id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            </td>
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url() .'sales_inventory/view/' . $row->sale_inventory_id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-danger" id="view"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url().'sales_inventory/edit/'. $row->sale_inventory_id ?>" data-toggle="Edit" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-pencil"></i></a>
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    <?php $i++; } } ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings.php') ?>

   <?php $this->load->view('common/script.php'); ?>
    <!-- <script src="<?php echo base_url('scripts/add_brand.js')?>"></script> -->
            <!-- Modal -->
   
<script type="text/javascript">
        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#sales_inventory_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#sales_inventory_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("sales_inventory/add")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            
                            window.location.href = "<?php echo base_url('sales_inventory') ?>";
                        }
                    }
                });
            }
        });
    });

        function formValidation() 
    {
        // Make quick references to our fields.
        var product_id = $('#product_id').val();
        var product_quantity = document.getElementById('product_quantity');
        var seller_id = $('#seller_id').val();
        var category_id = $('#category_id').val();
  
        // To check empty form fields.   
        if (product_id == '') {
            document.getElementById('head').innerText = "Please select product"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            product_id.focus();
            return false;
        }
        if (product_quantity.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Quantity"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            product_quantity.focus();
            return false;
        }
        if (seller_id == '') {
            document.getElementById('head2').innerText = "Please Select Seller"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            seller_id.focus();
            return false;
        }
        if (category_id == '') {
            document.getElementById('head3').innerText = "Please select Category"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            category_id.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(brand_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        // }
        return false;
    }

        function checkActivate(sale_inventory_id){
            $.ajax({
                url:'<?php echo base_url('sales_inventory/checkActivate') ?>',
                type:"post",
                data:{'sale_inventory_id':sale_inventory_id},
                success: function(data){
                    if(data == 'true'){
                        window.location.href="<?php echo base_url('sales_inventory') ?>";
                    }
                }
            });
        }

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('sales_inventory')?>";
            // $('#myModal').modal('hide');
        });

        $("#category_id").on('change',function(){
            var category_id = $(this).val();
            $.ajax({
                url:"<?php echo base_url('resource_product/getSubcategoriesByCategory') ?>",
                type:"post",
                data:{'product_category' : category_id},
                success: function(data){
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#subcategory_id').append('<option value="' + res[i].id + '">' + res[i].sub_cat_name + '</option>');
                         $('#subcategory_id').trigger("chosen:updated");
                        
                    }
                }
            });
        })

        $("#subcategory_id").on('change',function(){
            var subcategory_id = $(this).val();
            $.ajax({
                url:"<?php echo base_url('sales_inventory/getProductsBySubcategory') ?>",
                type:"post",
                data:{'subcategory_id' : subcategory_id},
                success: function(data){
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#product_id').append('<option value="' + res[i].id + '">' + res[i].product_name + '</option>');
                         $('#product_id').trigger("chosen:updated");
                        
                    }
                }
            });
        })

        $("#product_id").on('change',function(){
            var product_id = $(this).val();
            $.ajax({
                url:"<?php echo base_url('sales_inventory/getVarientsByProductId') ?>",
                type:"post",
                data:{'product_id' : product_id},
                success: function(data){
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        var arr = (res[i].product_varient).split(',');
                        for(var j =0; j < arr.length; j++){
                            
                            // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                            $('#product_varient').append('<option value="' + arr[j] + '">' + arr[j] + '</option>');
                            $('#product_varient').trigger("chosen:updated");
                        }
                        
                        
                    }
                }
            });
        })
</script>