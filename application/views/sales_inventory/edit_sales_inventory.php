           <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sales Inventory
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Inventory</li>
                        <li><a href="<?php echo base_url('sales_inventory') ?>">Sales Inventory</a></li>
                        <li><a href="#">Edit Sales Inventory</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Sales Inventory</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Sales Inventory</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Sales Inventory<br>
                                <br>
                            </h1>
                            
                            <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="edit_sales_inventory_form">
                                <input type="hidden" name="sale_inventory_id" value="<?php echo $sale_inventory->sale_inventory_id?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Seller Name</label>
                                    <div class="col-md-9">
                                        <select id="seller_id" name="seller_id" class="select-chosen" data-placeholder="Choose Product" style="width: 250px;">
                                            <option></option>
                                            <?php foreach($sellers['result'] as $seller){ ?>
                                                <option value="<?php echo $seller->user_id ?>" <?php if($seller->user_id == $sale_inventory->seller_id ) echo 'selected="selected"'; ?>><?php echo $seller->user_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="head2"></span>
                                        <span class="help-block">Please select Seller</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Category</label>
                                    <div class="col-md-9">
                                        <select id="category_id" name="category_id" class="select-chosen" data-placeholder="Choose Category" style="width: 250px;">
                                            <option></option>
                                            <?php foreach($categories['result'] as $category){ ?>
                                                <option value="<?php echo $category->id ?>" <?php if($category->id == $sale_inventory->category_id ) echo 'selected="selected"'; ?> ><?php echo $category->category_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="head3"></span>
                                        <span class="help-block">Please select Product Category</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Sub category</label>
                                    <div class="col-md-9">
                                        <select id="subcategory_id" name="subcategory_id" class="select-chosen" data-placeholder="Choose subcategory" style="width: 250px;">
                                            <option></option>
                                        </select>
                                        <span id="head3"></span>
                                        <span class="help-block">Please select Product subcategory</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Name</label>
                                    <div class="col-md-9">
                                        <select id="product_id" name="product_id" class="select-chosen" data-placeholder="Choose Product" style="width: 250px;">
                                            <option></option>
                                            
                                        </select>
                                        <span id="head"></span>
                                        <span class="help-block">Please select product</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Varients</label>
                                    <div class="col-md-9">
                                        <select id="product_varient" name="product_varient" class="select-chosen" data-placeholder="Choose Product varient" style="width: 250px;">
                                            <option></option>
                                            
                                        </select>
                                        <span id="head"></span>
                                        <span class="help-block">Please select product</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Product Quantity</label>
                                    <div class="col-md-9">
                                        <input type="text" id="product_quantity" name="product_quantity" class="form-control" value="<?php echo $sale_inventory->product_quantity?>">
                                        <span id="head1"></span>
                                        <span class="help-block">Please enter product quantity</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Unit</label>
                                    <div class="col-md-9">
                                        <select id="unit_id" name="unit_id" class="select-chosen" data-placeholder="Choose Unit" style="width: 250px;">
                                            <option></option>
                                            <?php foreach($units['result'] as $unit){ ?>
                                                <option value="<?php echo $unit->id ?>"><?php echo $unit->unit_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="head3"></span>
                                        <span class="help-block">Please select Product Category</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Order Type</label>
                                    <?php if($sale_inventory->order_type == 'priority_sale'){ ?>
                                        <div class="col-md-9">
                                            <label><input type="radio" name="order_type" value="priority_sale" checked>Priority Sale</label>
                                            <label><input type="radio" name="order_type" value="future_sale">Sale for Future</label>
                                            <span id="head2"></span>
                                            <span class="help-block">Please Select order type</span>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="col-md-9">
                                            <label><input type="radio" name="order_type" value="priority_sale">Priority Sale</label>
                                            <label><input type="radio" name="order_type" value="future_sale" checked>Sale for Future</label>
                                            <span id="head2"></span>
                                            <span class="help-block">Please Select order type</span>
                                        </div>
                                    <?php } ?>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="status_choice" id="somefield" value="true">
                                        <!-- <label class="col-md-3 control-label" for="example-hf-text">Status</label> -->
                                        <div class="col-md-9">
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });

            $('#edit_sales_inventory_form').submit(function(e){

                e.preventDefault();
                var formData = new FormData($("#edit_sales_inventory_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:'<?php echo base_url("sales_inventory/update")?>',
                        // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/brand/add',
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                
                                window.location.href="<?php echo base_url('sales_inventory') ?>";
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
    {
        // Make quick references to our fields.
        var product_id = document.getElementById('product_id');
        var product_quantity = document.getElementById('product_quantity');
        // var category_image = document.getElementById('category_image');
  
        // To check empty form fields.  
        if (product_id == '') {
            document.getElementById('head').innerText = "Please select product"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            brand_name.focus();
            return false;
        }
        if (product_quantity.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Quantity"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            brand_name.focus();
            return false;
        }
       
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(brand_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        // }
        return false;
    }


// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

        function readURL(input) {
            if (input.files && input.files[0]) {
                 
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#profile-img-tag').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".category_image").change(function() {
           
          readURL(this);
        });

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('brand')?>";
            // $('#myModal').modal('hide');
        });

        $("#category_id").on('change',function(){
            var category_id = $(this).val();
            $.ajax({
                url:"<?php echo base_url('resource_product/getSubcategoriesByCategory') ?>",
                type:"post",
                data:{'product_category' : category_id},
                success: function(data){
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#subcategory_id').append('<option value="' + res[i].id + '">' + res[i].sub_cat_name + '</option>');
                         $('#subcategory_id').trigger("chosen:updated");
                        
                    }
                }
            });
        })

        $("#subcategory_id").on('change',function(){
            var subcategory_id = $(this).val();
            $.ajax({
                url:"<?php echo base_url('sales_inventory/getProductsBySubcategory') ?>",
                type:"post",
                data:{'subcategory_id' : subcategory_id},
                success: function(data){
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#product_id').append('<option value="' + res[i].id + '">' + res[i].product_name + '</option>');
                         $('#product_id').trigger("chosen:updated");
                        
                    }
                }
            });
        })

        $("#product_id").on('change',function(){
            var product_id = $(this).val();
            $.ajax({
                url:"<?php echo base_url('sales_inventory/getVarientsByProductId') ?>",
                type:"post",
                data:{'product_id' : product_id},
                success: function(data){
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        var arr = (res[i].product_varient).split(',');
                        for(var j =0; j < arr.length; j++){
                            
                            // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                            $('#product_varient').append('<option value="' + arr[j] + '">' + arr[j] + '</option>');
                            $('#product_varient').trigger("chosen:updated");
                        }
                        
                        
                    }
                }
            });
        })
    </script>
    