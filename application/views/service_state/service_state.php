            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            Service State
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>Manage Locations</li>
                    <li><a href="#">Service State</a></li>
                </ul>
                <!-- END Datatables Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <div>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <script>
                                    swal({
                                        title: "Done",
                                        text: "<?php echo $this->session->flashdata('message'); ?>",
                                        timer: 2000,
                                        showConfirmButton: false,
                                        type: 'success'
                                    });
                                </script>
                            <?php }else{} ?>    
                        </div>
                        <h2><strong>Service State</strong> integration</h2>
                        <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                    </div>
                    <p>Admin Can Add / edit and Delete State and Assign Service State</p>

                    <div class="table-responsive">
                        <table id="example" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php if($states['count'] > 0){ 
                                    $i = 1;
                                    foreach($states['result'] as $row){ ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $row->state_name ?></td>
                                        <td><?php echo $row->state_description ?></td>
                                        <td>
                                            <?php if($row->status_choice == '1'){?>
                                                <label class="switch switch-primary">
                                                    <input type="checkbox" checked onclick="checkActivate(<?php echo $row->state_id ?>)"><span></span>
                                                </label>
                                            <?php }else{ ?>
                                                <label class="switch switch-primary">
                                                    <input type="checkbox" onclick="checkActivate(<?php echo $row->state_id ?>)"><span></span>
                                                </label>
                                            <?php } ?>
                                            
                                        </td>
                                        <td>                                                
                                            <div class="btn-group">
                                                <a href="<?php echo base_url().'service_state/view/'.$row->state_id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                <a href="<?php echo base_url().'service_state/edit/'.$row->state_id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                <a href="<?php echo base_url().'service_state/delete/'.$row->state_id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php $i++; } } ?>               
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <?php $this->load->view('common/footer.php'); ?>
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->

<?php $this->load->view('common/script.php'); ?>

        <!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
  <div class="modal-dialog animation-slideDown">

    <!-- Modal content-->
    <div class="modal-content CustomModal">
      <div class="modal-body">
        <div id="loginCustom">
            <!-- Login Title -->
            <div class="login-title text-right">
                <h1>
                    <i class="Unitsss fa fa-bank pull-left"></i>
                    <strong>Manage</strong> Service State<br>
                    <br>
                </h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block">
                <form method="post" class="form-horizontal form-bordered" id="state_form">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="state_name" name="state_name" class="form-control" placeholder="Enter State name">
                            <span id="head"></span>
                            <span id="p1"></span>
                            <!-- <span class="help-block">Please enter state name</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Description<span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="state_description" name="state_description" class="form-control" placeholder="Enter State description">
                            <span id="head1"></span>
                            <!-- <span class="help-block">Please enter state description</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="status_choice" id="somefield" value="true">
                        <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input id="statusCheck" type="checkbox" checked=""><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Login Block -->
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });
    $("#state_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#state_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('service_state/add') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        window.location.href="<?php echo base_url('service_state') ?>";
                    }
                }
            });
        }
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        var state_name = document.getElementById('state_name');
        var state_description = document.getElementById('state_description');
  
        // To check empty form fields.  
        if (state_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter state Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            state_name.focus();
            return false;
        }
        if (state_description.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter State Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            state_description.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(state_name, "* Please enter valid state name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }

    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        // var alphaExp = /^[A-z\d\-_\s]+$/;
        var alphaExp = /^[a-zA-Z]{3,16}$/;
        if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#p1').delay(2000).hide('slow');
            $('#p1').css('display','block');
            $('#p1').css('color','red');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }

    function checkActivate(id){
        $.ajax({
            url:'<?php echo base_url('service_state/checkActivate') ?>',
            type:"post",
            data:{'id':id},
            success: function(data){
                if(data == 'true'){
                    alert("Record activated/deactivated successfully!");
                }
            }
        });
    }

    $("#state_name").on('blur', function(){
        var state_name = $(this).val();
        $.ajax({
            url:'<?php echo base_url('service_state/checkExists') ?>',
            type:"post",
            data:{'state_name':state_name},
            success: function(data){
                if(data == 'true'){
                    $("#state_name").val('');
                    alert("State name is already exist!");
                }
            }
        });
    });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('service_state') ?>";
        // $('#myModal').modal('hide');
    });
</script>