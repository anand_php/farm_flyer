            <style type="text/css">
                form .error {
                    color: #ff0000;
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Service State
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage States</li>
                        <li><a href="<?php echo base_url('service_state') ?>">Service State</a></li>
                        <li><a href="#">Edit Service State</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Service State</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete State and Assign Service State</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Service State<br>
                                <br>
                            </h1>
                            
                            <form method="post" class="form-horizontal form-bordered" id="edit_service_state">
                                <input type="hidden" name="state_id" value="<?php echo $state->state_id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="state_name" name="state_name" class="form-control" placeholder="Enter state name" value="<?php echo $state->state_name ?>">
                                        <span id="head"></span>
                                        <span id="p1"></span>
                                        <!-- <span class="help-block">Please enter state name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Description<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="state_description" name="state_description" class="form-control" placeholder="Enter state description" value="<?php echo $state->state_description ?>">
                                        <span id="head1"></span>
                                        <!-- <span class="help-block">Please enter state description</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($state->status_choice == '1'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $state->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
   <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

<script type="text/javascript">
    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });
    $("#edit_service_state").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#edit_service_state")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('service_state/update') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Updated Successfully!");
                        window.location.href="<?php echo base_url('service_state') ?>";
                    }
                }
            });
        }
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        var state_name = document.getElementById('state_name');
        var state_description = document.getElementById('state_description');
  
        // To check empty form fields.  
        if (state_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter state Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            state_name.focus();
            return false;
        }
        if (state_description.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter State Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            state_description.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(state_name, "* Please enter valid state name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }

    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        // var alphaExp = /^[A-z\d\-_\s]+$/;
        var alphaExp = /^[a-zA-Z]{3,16}$/;
        if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#p1').delay(2000).hide('slow');
            $('#p1').css('display','block');
            $('#p1').css('color','red');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('service_state') ?>";
        // $('#myModal').modal('hide');
    });
</script>
    