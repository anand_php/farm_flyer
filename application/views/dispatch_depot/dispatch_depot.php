            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Dispatch Depot
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="#">Dispatch Depot</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <div class="alert alert-success " id="successMessage" role="alert"> 
                                        <?= $this->session->flashdata('message') ?> </div>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Dispatch Depot</strong> integration</h2>
                            <?php if($this->session->userdata("logged_in")["user_role"] == 1){ ?> 
                                <p class="AddNew"><b><a href="<?php echo base_url('dispatch_depot/add_dispatch_depot')?>">ADD NEW +</a> </b></p>
                            <?php }else{} ?>
                            
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Dispatch Depot</p>

                        <div class="table-responsive">
                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Depot Code</th>
                                        <th class="text-center">Dispatch Depot Name</th>
                                        <th class="text-center">DD City</th>
                                        <th class="text-center">Address</th>
                                        <!-- <th class="text-center">Map ID</th> -->
                                        <th class="text-center">DD WareHouse</th>
                                        <th class="text-center">Status</th>
                                        <?php if($this->session->userdata("logged_in")["user_role"] == 1){ ?> 
                                            <th class="text-center">Actions</th>
                                        <?php }else{} ?>
                                        
                                    </tr>
                                    </thead>
                                <tbody class="text-center">
                                    <?php if(!empty($dispatch_depots)){
                                        $i = 1;
                                        foreach($dispatch_depots as $row){ ?>
                                            <tr>
                                                <td><?php echo $i ?></td>
                                                <td><?php echo $row->dispatch_depot_code ?></td>
                                                <td><?php echo $row->dispatch_depot_name ?></td>
                                                <td><?php echo $row->city_name ?></td>
                                                <td><?php echo $row->address ?></td>
                                                <!-- <td><?php echo $row->latitude . ' , '. $row->longitude ?></td> -->
                                                <td><?php echo $row->warehouse_name ?></td>
                                                <td>
                                                    <?php if($row->status_choice == 'true'){ ?>
                                                        <label class="switch switch-primary">
                                                            <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                        </label>
                                                    <?php }else{ ?>
                                                        <label class="switch switch-primary">
                                                            <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                        </label>
                                                    <?php } ?>
                                                </td>
                                                <?php if($this->session->userdata("logged_in")["user_role"] == 1){ ?> 
                                                    <td>                                                
                                                        <div class="btn-group">
                                                            <button onclick="getMap('<?php echo $row->latitude .' , ' . $row->longitude; ?>')" data-toggle="tooltip" title="Map" class="btn btn-xs btn-default location"><i class="fa fa-map-marker"></i></button>
                                                            <a href="<?php echo base_url().'dispatch_depot/view/'. $row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                            <a href="<?php echo base_url().'dispatch_depot/edit/'. $row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                            <a href="<?php echo base_url().'dispatch_depot/delete/'. $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </td>
                                                <?php }else{} ?>
                                                
                                            </tr>
                                    <?php $i++; }
                                    } ?>                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

    <?php $this->load->view('common/script.php'); ?>

    <div id="mapModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog animation-slideDown">

            <!-- Modal content-->
            <div class="modal-content CustomModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Map</h4>
                </div>
                <div class="modal-body">
                    <div id="loginCustom">
                        <!-- Login Title -->
                        <!-- <div class="login-title text-right">
                            <h1>
                                <strong>Manage</strong> Units<br>
                                <br>
                            </h1>
                        </div> -->
                        <!-- END Login Title -->

                        <!-- Login Block -->
                        <div class="block">
                            <div id="map" style="width: 100%; height: 300px;"></div>
                        </div>
                        <!-- END Login Block -->
                    </div>
                </div>
            </div>
        </div>
    </div>

            <!-- Modal -->
   
<script type="text/javascript">
    function getMap(latlong){
        var result = latlong.split(',');
        var lat = result[0];
        var long = result[1];

        var latlng = new google.maps.LatLng(lat,long);
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><b>Location</b> : Noida</div></div>';
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
       $('#mapModal').modal('show');
        
    }

google.maps.event.addDomListener(window, 'load', getMap);
</script>
<script type="text/javascript">
$("#address").on('blur', function(){
    var geocoder = new google.maps.Geocoder();
    var address = $("#address").val();

    geocoder.geocode( { 'address': address}, function(results, status) {

    if (status == google.maps.GeocoderStatus.OK) {
        var latitude = results[0].geometry.location.lat();
        var longitude = results[0].geometry.location.lng();
        document.getElementById("map_id").value = latitude + " , " + longitude;
        } 
    }); 
});

</script>
<script type="text/javascript">
    $("#state_id").on('change',function(){
        var state_id = $("#state_id").val();
        
        $.ajax({
            url:'<?php echo base_url("warehouse/getCityByStateId")?>',
            type:"post",
            data:{'state_id' : state_id},
            success: function(data){
                if(data == 'false'){
                    $("#city_id").empty();
                    $('#city_id').trigger("chosen:updated");
                }else{
                    var res = JSON.parse(data);
                    
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#city_id').append('<option value="' + res[i].id + '">' + res[i].city_name + '</option>');
                         $('#city_id').trigger("chosen:updated");
                        
                    }
                }
            }
        });
    });
    $(document).ready(function(){   
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

        $('#dispatch_depot_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#dispatch_depot_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("dispatch_depot/add")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            alert("Record inserted successfully!");
                            window.location.href = "<?php echo base_url('dispatch_depot') ?>";
                        }
                    }
                });
            }
        });
    });

        function formValidation() 
    {
        // Make quick references to our fields.
        var dispatch_depot_code = document.getElementById('dispatch_depot_code');
        var dispatch_depot_name = document.getElementById('dispatch_depot_name');
        var city_id = $("#city_id").val();
        var state_id = $("#state_id").val();
        var address = document.getElementById('address');
        var map_id = document.getElementById('map_id');
        var warehouse_id = $("#warehouse_id").val();
  
        // To check empty form fields.  
        if (dispatch_depot_code.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Dispatch Depot Code"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            dispatch_depot_code.focus();
            return false;
        }
        if (dispatch_depot_name.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Dispatch Depot Name"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            dispatch_depot_name.focus();
            return false;
        }
        if (city_id == '') {
            document.getElementById('head2').innerText = "Please Select City Name"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            city_id.focus();
            return false;
        }
        if (state_id == '') {
            document.getElementById('head6').innerText = "Please Select State Name"; // This segment displays the validation rule for all fields
            $('#head6').delay(2000).hide('slow');
            $('#head6').css('display','block');
            $('#head6').css('color','red');
            city_id.focus();
            return false;
        }
        if (address.value.length == 0) {
            document.getElementById('head3').innerText = "Please Enter Address"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            address.focus();
            return false;
        }
        if (map_id.value.length == 0) {
            document.getElementById('head4').innerText = "Please Enter map id"; // This segment displays the validation rule for all fields
            $('#head4').delay(2000).hide('slow');
            $('#head4').css('display','block');
            $('#head4').css('color','red');
            map_id.focus();
            return false;
        }
        if (warehouse_id == '') {
            document.getElementById('head5').innerText = "Please Select Warehouse"; // This segment displays the validation rule for all fields
            $('#head5').delay(2000).hide('slow');
            $('#head5').css('display','block');
            $('#head5').css('color','red');
            warehouse_id.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(dispatch_depot_name, "* Please enter valid dispatch depot name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#p1').delay(2000).hide('slow');
            $('#p1').css('display','block');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }

        function checkActivate(id){
            $.ajax({
                url:'<?php echo base_url('dispatch_depot/checkActivate') ?>',
                type:"post",
                data:{'id':id},
                success: function(data){
                    alert("Record activated/deactivated successfully!");
                }
            });
        }

        $("#dispatch_depot_code").on('blur', function(){
            var dispatch_depot_code = $(this).val();
            $.ajax({
                url:'<?php echo base_url('dispatch_depot/checkExists') ?>',
                type:"post",
                data:{'dispatch_depot_code':dispatch_depot_code},
                success: function(data){
                    if(data == 'true'){
                        $("#dispatch_depot_code").val('');
                        alert("Depot code already exists!");
                    }
                }
            });
        });

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('dispatch_depot') ?>";            
            // $('#myModal').modal('hide');
        });
    </script>