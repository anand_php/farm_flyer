            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Dispatch Depot
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Dispatch Depot</li>
                        <li><a href="<?php echo base_url('dispatch_depot') ?>">Dispatch Depot</a></li>
                        <li><a href="#">Edit Dispatch Depot</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Dispatch Depot</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Dispatch Depot and Assign Dispatch Depot</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Dispatch Depot<br>
                                <br>
                            </h1>
                            <form action="#" method="post" class="form-horizontal form-bordered" id="edit_dispatch_depot_form">
                                <input type="hidden" name="id" value="<?php echo $depot->id?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Depot Code<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="dispatch_depot_code" name="dispatch_depot_code" class="form-control" placeholder="Enter depot code" value="<?php echo $depot->dispatch_depot_code ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter depot code</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="dispatch_depot_name" name="dispatch_depot_name" class="form-control" placeholder="Enter depot name" value="<?php echo $depot->dispatch_depot_name ?>">
                                        <span id="head1"></span>
                                        <!-- <span class="help-block">Please enter depot name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">State<span>*</span></label>
                                    <div class="col-md-9">
                                        <select id="state_id" name="state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($states['result'] as $state) { ?>
                                                <option value="<?php echo $state->state_id ?>" <?php if($state->state_id == $depot->state_id ) echo 'selected="selected"'; ?> ><?php echo $state->state_name?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head6"></span>
                                        <!-- <span class="help-block">Select Warehouse City</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">City<span>*</span></label>
                                   
                                    <div class="col-md-9">
                                        <select id="city_id" name="city_id" class="select-chosen" data-placeholder="Choose City name" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($cities['result'] as $city) { ?>
                                                <option value="<?php echo $city->id ?>" <?php if($city->id == $depot->city_id ) echo 'selected="selected"'; ?> ><?php echo $city->city_name?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Select depot city</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Address<span>*</span></label>
                                    <div class="col-md-9">
                                        <textarea type="text" rows="3" cols="3" id="address" name="address" class="form-control" placeholder="Enter depot address"><?php echo $depot->address?></textarea>
                                        <span id="head3"></span>
                                        <!-- <span class="help-block">Please enter depot address</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Map ID <span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="map_id" name="map_id" class="form-control" placeholder="Enter dd map id" value="<?php echo $depot->latitude . '  , '. $depot->longitude ?>">
                                        <span id="head4"></span>
                                        <!-- <span class="help-block">Please enter dispatch depot map id</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">DD Warehouse<span>*</span></label>
                                    <div class="col-md-9">
                                        <select id="warehouse_id" name="warehouse_id" class="select-chosen" data-placeholder="Choose Warehouse" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($warehouses['result'] as $warehouse){ ?>
                                                <option value="<?php echo $warehouse->id ?>" <?php if($warehouse->id == $depot->warehouse_id) echo"selected : 'selected'" ?>><?php echo $warehouse->warehouse_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="head5"></span>
                                        <!-- <span class="help-block">Select depot warehouse</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="status_choice" id="somefield" value="true">
                                        <div class="col-md-9">
                                            <?php if($depot->status_choice == 'true'){ ?>
                                                <label class="switch switch-primary">
                                                    <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                                </label>
                                            <?php }else{ ?>
                                                <label class="switch switch-primary">
                                                    <input id="statusCheck" value="" type="checkbox"><span></span>
                                                </label>
                                            <?php } ?>
                                            <!-- <div class="form-group"> -->
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyvW_3aj4NtaihWGsT3GojA2I6ba0rD5w&amp;libraries=geometry,places"></script> -->
        <script type="text/javascript">
        $("#address").on('blur', function(){
            var geocoder = new google.maps.Geocoder();
            var address = $("#address").val();

            geocoder.geocode( { 'address': address}, function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                document.getElementById("map_id").value = latitude + " , " + longitude;
                } 
            }); 
        });

    </script>

    <script type="text/javascript">
        $("#state_id").on('change',function(){
            var state_id = $("#state_id").val();
            
            $.ajax({
                url:'<?php echo base_url("warehouse/getCityByStateId")?>',
                type:"post",
                data:{'state_id' : state_id},
                success: function(data){
                    if(data == 'false'){
                        $("#city_id").empty();
                        $('#city_id').trigger("chosen:updated");
                    }else{
                        var res = JSON.parse(data);
                        
                        var options = "";
                        for (var i = 0; i < res.length; i++) {
                            // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                             $('#city_id').append('<option value="' + res[i].id + '">' + res[i].city_name + '</option>');
                             $('#city_id').trigger("chosen:updated");
                            
                        }
                    }
                }
            });
        });

        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });

            $('#edit_dispatch_depot_form').submit(function(e){

                e.preventDefault();
                var formData = new FormData($("#edit_dispatch_depot_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:'<?php echo base_url("dispatch_depot/update")?>',
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                alert("Record updated successfully!");
                                window.location.href="<?php echo base_url('dispatch_depot') ?>";
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
        {
            var dispatch_depot_code = document.getElementById('dispatch_depot_code');
            var dispatch_depot_name = document.getElementById('dispatch_depot_name');
            var city_id = $("#city_id").val();
            var state_id = $("#state_id").val();
            var address = document.getElementById('address');
            var map_id = document.getElementById('map_id');
            var warehouse_id = $("#warehouse_id").val();
      
            // To check empty form fields.  
            if (dispatch_depot_code.value.length == 0) {
                document.getElementById('head').innerText = "Please Enter Dispatch Depot Code"; // This segment displays the validation rule for all fields
                $('#head').delay(2000).hide('slow');
                $('#head').css('display','block');
                $('#head').css('color','red');
                dispatch_depot_code.focus();
                return false;
            }
            if (dispatch_depot_name.value.length == 0) {
                document.getElementById('head1').innerText = "Please Enter Dispatch Depot Name"; // This segment displays the validation rule for all fields
                $('#head1').delay(2000).hide('slow');
                $('#head1').css('display','block');
                $('#head1').css('color','red');
                dispatch_depot_name.focus();
                return false;
            }
            if (city_id == '') {
                document.getElementById('head2').innerText = "Please Select City Name"; // This segment displays the validation rule for all fields
                $('#head2').delay(2000).hide('slow');
                $('#head2').css('display','block');
                $('#head2').css('color','red');
                city_id.focus();
                return false;
            }
            if (state_id == '') {
                document.getElementById('head6').innerText = "Please Select State Name"; // This segment displays the validation rule for all fields
                $('#head6').delay(2000).hide('slow');
                $('#head6').css('display','block');
                $('#head6').css('color','red');
                city_id.focus();
                return false;
            }
            if (address.value.length == 0) {
                document.getElementById('head3').innerText = "Please Enter Address"; // This segment displays the validation rule for all fields
                $('#head3').delay(2000).hide('slow');
                $('#head3').css('display','block');
                $('#head3').css('color','red');
                address.focus();
                return false;
            }
            if (map_id.value.length == 0) {
                document.getElementById('head4').innerText = "Please Enter map id"; // This segment displays the validation rule for all fields
                $('#head4').delay(2000).hide('slow');
                $('#head4').css('display','block');
                $('#head4').css('color','red');
                map_id.focus();
                return false;
            }
            if (warehouse_id == '') {
                document.getElementById('head5').innerText = "Please Select Warehouse"; // This segment displays the validation rule for all fields
                $('#head5').delay(2000).hide('slow');
                $('#head5').css('display','block');
                $('#head5').css('color','red');
                warehouse_id.focus();
                return false;
            }
            // Check each input in the order that it appears in the form.
            if (inputAlphabet(dispatch_depot_name, "* Please enter valid dispatch depot name.")) {
                // if (isImageValid(category_image, "* Please image in valid format.")) {
                    return true;
                // }
            }
            return false;
        }

        // Function that checks whether input text is an alphabetic character or not.
        function inputAlphabet(inputtext, alertMsg) {
            var alphaExp = /^[A-z\d\-_\s]+$/;
            if (inputtext.value.match(alphaExp)) {
                return true;
            } else {
                document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
                $('#p1').delay(2000).hide('slow');
                $('#p1').css('display','block');
                //alert(alertMsg);
                inputtext.focus();
                return false;
            }
        }

        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('dispatch_depot') ?>";            
            // $('#myModal').modal('hide');
        });
    </script>
    