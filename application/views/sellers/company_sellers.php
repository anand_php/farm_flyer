            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sellers
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Users</li>
                        <li><a href="#">Sellers</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <script>
                                        swal({
                                            title: "Done",
                                            text: "<?php echo $this->session->flashdata('message'); ?>",
                                            timer: 2000,
                                            showConfirmButton: false,
                                            type: 'success'
                                        });
                                    </script>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Sellers</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="<?php echo base_url('buyer/add_buyer') ?>" >ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Sellers </p>

                        <div class="table-responsive">

                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                      <!--   <th class="text-center">Role</th> -->
                                        <th class="text-center">Company Name</th>
                                        <th class="text-center">Mobile</th>
                                     <!--    <th class="text-center">Email</th> -->
                                        <th class="text-center">State</th>
                                        <th class="text-center">City</th>
                                        <th class="text-center">Town</th>
                                        <!-- <th class="text-center">Address</th> -->
                                        <th class="text-center">Warehouse</th>
                                        <th class="text-center">A/C Manager</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php 
                                        if(count($sellers) > 0){ 
                                        $i = 1;
                                        foreach($sellers as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <!-- <td><?php //echo $row->buyer_role ?></td> -->
                                            <td><?php echo $row->CompanyName ?></td>
                                            <td><?php echo $row->PhoneNumber ?></td>
                                          <!--   <td><?php //echo $row->email ?></td> -->
                                            <td><?php echo $row->state_name ?></td>
                                            <td><?php echo $row->city_name ?></td>
                                            <td><?php echo $row->Town ?></td>
                                           <!--  <td><?php //echo $row->Address ?></td> -->
                                            <td><?php echo isset($row->warehouse_name)?$row->warehouse_name:'NA'; ?></td>
                                            <td><?php echo isset($row->accountManagerName)?$row->accountManagerName:'NA'; ?></td>
                                            <td>
                                                <?php if($row->AdminApproved == 1){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->ID ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                   <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->ID ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            </td>
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url().'buyer/viewSeller/'. $row->ID ?>" data-toggle="View" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>
                                                    <a href="#" data-toggle="Edit" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-pencil"></i></a>
                                                    <a href="#" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" id="delete"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    <?php $i++; } } ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>
    <!-- <script src="<?php echo base_url('scripts/add_brand.js')?>"></script> -->
            <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
      <div class="modal-dialog animation-slideDown">

        <!-- Modal content-->
        <div class="modal-content CustomModal">
          <div class="modal-body">
            <div id="loginCustom">
                <!-- Login Title -->
                <div class="login-title text-right">
                <div class="login-title text-right">
                    <h1>
                        <i class="Unitsss fa fa-star-o pull-left"></i>
                        <strong>Manage</strong> Sellers<br>
                        <br>
                    </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block">
                        <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="buyer_form" novalidate>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Role<span>*</span></label>
                                <div class="col-md-9">
                                    <select id="buyer_role" name="buyer_role" class="form-control buyer_role">
                                        <option>Select Role</option>
                                        <option value="purchasor">Purchasor</option>
                                        <option value="stockist">Stockist</option>
                                    </select>
                                    <!-- <input type="text" id="buyer_name" name="buyer_name" class="form-control name" placeholder="Enter Buyer name"> -->
                                    <span id="head"></span>
                                    <!-- <span class="help-block">Please enter buyer name</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="buyer_name" name="buyer_name" class="form-control name" placeholder="Enter Buyer name">
                                    <span id="head1"></span>
                                    <!-- <span class="help-block">Please enter buyer name</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Mobile<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter mobile">
                                    <span id="head2"></span>
                                    <!-- <span class="help-block">Please enter buyer mobile</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Alternate Mobile</label>
                                <div class="col-md-9">
                                    <input type="text" id="alternate_mobile" name="alternate_mobile" class="form-control alternate_mobile" placeholder="Enter alternate mobile">
                                    <!-- <span class="help-block">Please enter altername mobile</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Email<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="email" name="email" class="form-control email" placeholder="Enter Email">
                                    <span id="head3"></span>
                                    <!-- <span class="help-block">Please enter email</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Username<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="username" name="username" class="form-control username" placeholder="Enter username">
                                    <span id="head4"></span>
                                    <!-- <span class="help-block">Please enter username</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Password<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="password" id="password" name="password" class="form-control password" placeholder="Enter password">
                                    <span id="head5"></span>
                                    <!-- <span class="help-block">Please enter password</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Address<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="buyer_address" name="buyer_address" class="form-control buyer_address" placeholder="Enter address">
                                    <span id="head6"></span>
                                    <!-- <span class="help-block">Please enter address</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">State Name<span>*</span></label>
                                <div class="col-md-9">
                                    <select id="state_id" name="state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;" required>
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                             <?php foreach($states['result'] as $state) { ?>
                                                <option value="<?php echo $state->state_id ?>"><?php echo $state->state_name ?></option>
                                            <?php }?>
                                    </select>
                                    <span id="head7"></span>
                                    <!-- <span class="help-block">Please select menu name</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">City<span>*</span></label>
                                <div class="col-md-9">
                                    <select name="city_id" id="city_id" class="select-chosen" data-placeholder="Choose City" style="width: 250px;" required>
                                        <option></option>
                                    </select>
                                    <span id="head8"></span>
                                    <!-- <span class="help-block">Please select city</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Town<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="town" name="town" class="form-control town" placeholder="Enter town">
                                    <span id="head9"></span>
                                    <!-- <span class="help-block">Please enter town</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Pincode<span>*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="pincode" name="pincode" class="form-control pincode" placeholder="Enter pincode">
                                    <span id="head10"></span>
                                    <!-- <span class="help-block">Please enter pincode</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="status_choice" id="somefield" value="true">
                                    <div class="col-md-9">
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
