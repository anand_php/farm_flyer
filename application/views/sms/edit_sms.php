            <style type="text/css">
                .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
                .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                SMS
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Settings</li>
                        <li><a href="<?php echo base_url('sms') ?>">Manage SMS Gateway</a></li>
                        <li><a href="#">Edit SMS</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>SMS</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete SMS</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> SMS<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="edit_sms_form" novalidate>
                                <input type="hidden" name="sms_id" value="<?php echo $sms->sms_id ?>">
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Username <span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="username" name="username" class="form-control" placeholder="Please enter username" value="<?php echo $sms->username ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter category name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Password<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Please enter password" value="<?php echo $sms->password ?>">
                                        <!-- <span class="help-block">Please enter category description</span> -->
                                        <span id="head1"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">SMS Key</label>
                                    <div class="col-md-9">
                                        <input type="text" id="sms_key" name="sms_key" class="form-control sms_key" placeholder="Please enter sms key" value="<?php echo $sms->sms_key ?>">
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please select file</span> -->
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <?php if($sms->is_activate == 'true'){ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                        </label>
                                    <?php }else{ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox"><span></span>
                                        </label>
                                    <?php } ?>
                                    
                                    <input type="hidden" name="status_choice" id="somefield" value="<?php echo $sms->is_activate ?>">
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    
    <script type="text/javascript">
        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });

    $("#edit_sms_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#edit_sms_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('sms/update') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Updated Successfully!");
                        window.location.href = '<?php echo base_url("sms") ?>';
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        // var parent_category_id = $("#parent_category_id").val();
        var username = document.getElementById('username');
        var password = document.getElementById('password');
        var sms_key = document.getElementById('sms_key');
  
        // To check empty form fields.   
        if (sms_key.value.length == 0) {
            document.getElementById('head2').innerText = "Please enter sms key"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            sms_key.focus();
            return false;
        }
        if (username.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter username"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            username.focus();
            return false;
        }
        if (password.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter password"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            password.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(category_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        // }
        return false;
    }




// Function that checks whether input text is an alphabetic character or not.
function inputAlphabet(inputtext, alertMsg) {
var alphaExp = /^[a-zA-Z]{3,16}$/;
if (inputtext.value.match(alphaExp)) {
return true;
} else {
document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
$('#head').delay(2000).hide('slow');
$('#head').css('display','block');
$('#head').css('color','red');
//alert(alertMsg);
inputtext.focus();
return false;
}
}

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('category') ?>";
    });

    </script>