            <style type="text/css">
                form .error {
                    color: #ff0000;
                }
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                            SMS
                    </div>
                </div>
                <ul class="breadcrumb breadcrumb-top">
                    <li>setting</li>
                    <li><a href="#">SMS</a></li>
                </ul>
                <!-- END Datatables Header -->

                <!-- Datatables Content -->
                <div class="block full">
                    <div class="block-title">
                        <div>
                            <?php if ($this->session->flashdata('message')) { ?>
                                <script>
                                    swal({
                                        title: "Done",
                                        text: "<?php echo $this->session->flashdata('message'); ?>",
                                        timer: 2000,
                                        showConfirmButton: false,
                                        type: 'success'
                                    });
                                </script>
                            <?php }else{} ?>    
                        </div>
                        <h2><strong>SMS</strong> integration</h2>
                        <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                    </div>
                    <p>Admin Can Add / edit and Delete SMS details</p>

                    <div class="table-responsive">
                        <table id="example" class="table table-vcenter table-condensed table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">username</th>
                                    <th class="text-center">SMS key</th>
                                    <!-- <th class="text-center">Descroption</th> -->
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php if($sms['count'] > 0 ){
                                    $i =1; 
                                    foreach($sms['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->username ?></td>
                                            <td><?php echo $row->sms_key ?></td>
                                            <!-- <td><?php echo $row->password ?></td> -->
                                            <td>
                                                <?php if($row->is_activate == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->sms_id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->sms_id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                                
                                            </td>
                                            <td>                                              
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url() .'sms/view/'. $row->sms_id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default"><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url() .'sms/edit/'. $row->sms_id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'sms/delete/'. $row->sms_id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                <?php $i++; } }?>                       
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Datatables Content -->
            </div>
            <!-- END Page Content -->

            <?php  $this->load->view('common/footer.php'); ?> 
            
            
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<?php $this->load->view('common/settings') ?>
<!-- END User Settings -->

<?php $this->load->view('common/script.php'); ?>

        <!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
  <div class="modal-dialog animation-slideDown">

    <!-- Modal content-->
    <div class="modal-content CustomModal">
      <div class="modal-body">
        <div id="loginCustom">
            <!-- Login Title -->
            <div class="login-title text-right">
                <h1>
                    <i class="Unitsss fa fa-hourglass pull-left"></i>
                    <strong>Manage</strong> SMS<br>
                    <br>
                </h1>
            </div>
            <!-- END Login Title -->

            <!-- Login Block -->
            <div class="block">
                <form method="post" class="form-horizontal form-bordered" id="sms_form" enctype="multipart/form-data" novalidate>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Username <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="username" name="username" class="form-control" placeholder="Enter username">
                            <!-- <span class="help-block">Please enter category name</span> -->
                            <span id="head"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">Password <span>*</span></label>
                        <div class="col-md-9">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Enter category password">
                            <span id="head2"></span>
                            <!-- <span class="help-block">Please enter category description</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-hf-text">SMS Key <span>*</span></label>
                        <div class="col-md-9">
                            <input type="text" id="sms_key" name="sms_key" class="form-control" placeholder="Enter sms key">
                            <span id="head3"></span>
                            <!-- <span class="help-block">Please enter category description</span> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="status_choice" id="somefield" value="true">
                        <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                            <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Login Block -->
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    $('#statusCheck').change(function(){
        cb = $(this);
        cb.val(cb.prop('checked'));
        $("#somefield").val(this.value);
    });

    $("#sms_form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($("#sms_form")[0]);
        var status = $("#statusCheck").val();
        var isFormValid = formValidation();
        if(isFormValid == true){
            $.ajax({
                url:"<?php echo base_url('sms/add') ?>",
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data){
                    if(data == 'true'){
                        alert("Record Inserted Successfully!");
                        location.reload();
                    }
                }
            });
        }
        
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        // var parent_category_id = $('#parent_category_id').val();
        var username = document.getElementById('username');
        var password = document.getElementById('password');
        var sms_key = document.getElementById('sms_key');
  
        // To check empty form fields.  
        if (sms_key.value.length == 0) {
            document.getElementById('head3').innerText = "Please enter smskey"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            sms_key.focus();
            return false;
        }
        if (username.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter username"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            username.focus();
            return false;
        }
        if (password.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter password"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            password.focus();
            return false;
        }
  
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(category_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
            return true;
        // }
    // }
    return false;
}

function isImageValid(img,alertMsg)
{
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($("#category_image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        //alert("Only formats are allowed : "+fileExtension.join(', '));
        $("#head").html("<span style='color:red;'>Formats Allowd are : </span>"+fileExtension.join(', '));
        $('#head').delay(2000).hide('slow');
        $('#head').css('display','block');

        return false;
    }
    else
    {
        //$('#basicForm')[0].submit();
        return  true;
    }
}

function checkActivate(id){
    $.ajax({
        // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/category/checkActivate',
        url:'<?php echo base_url('category/checkActivate') ?>',
        type:"post",
        data:{'id':id},
        success: function(data){
            alert("Record activated/deactivated successfully!");
        }
    });
}

   

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('sms') ?>";
        // $('#myModal').modal('hide');
    });

   
</script>
    