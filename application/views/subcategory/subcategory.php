                <style type="text/css">
                    .alert {
                       height:40px;    
                    }
                </style>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Datatables Header -->
                    <div class="content-header">
                        <div class="header-section">
                            <h1>
                                    Sub Category
                            </div>
                        </div>
                        <ul class="breadcrumb breadcrumb-top">
                            <li>Manage Resources </li>
                            <li><a href="#">Sub Category</a></li>
                        </ul>
                        <!-- END Datatables Header -->

                        <!-- Datatables Content -->
                        <div class="block full">
                            <div class="block-title">
                                <div>
                                    <?php if ($this->session->flashdata('message')) { ?>
                                        <div class="alert alert-success" id="successMessage"> <?= $this->session->flashdata('message') ?> </div>
                                    <?php }else{} ?>    
                                </div>
                                <h2><strong>Sub Category</strong> integration</h2>
                                <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                            </div>
                            <p>Admin Can Add / edit and Delete Category and Assign Base Category</p>

                            <div class="table-responsive">
                                <table id="example" class="table table-vcenter table-condensed table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">Sub Category Name</th>
                                            <th class="text-center">Category Name</th>
                                            <th class="text-center">Subcategory Image</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <?php if($subcategories['count'] > 0 ){  
                                            $i = 1;
                                            foreach($subcategories['result'] as $row){  ?>
                                                <tr>
                                                    <td><?php echo $i ?></td>
                                                    <td><?php echo $row->sub_cat_name ?></td>
                                                    <td><?php echo $row->category_name ?></td>
                                                    <td>
                                                        <img src="<?php echo base_url() . 'uploads/subcategory/'. $row->sub_cat_Image ?>" alt="avatar" class="img-circle" style="height: 80px; width:100px">
                                                    </td>
                                                    <td>
                                                        <?php if($row->status_choice == 'true'){ ?>
                                                            <label class="switch switch-primary">
                                                                <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                            </label>
                                                        <?php }else{ ?>
                                                            <label class="switch switch-primary">
                                                                <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                            </label>
                                                        <?php } ?>
                                                    </td>
                                                    <td>                                         
                                                        <div class="btn-group">
                                                            <a href="<?php echo base_url() .'subcategory/edit/'. $row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>
                                                            <a href="<?php echo base_url() .'subcategory/delete/'. $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php $i++; } }?> 
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END Datatables Content -->
                    </div>
                    <!-- END Page Content -->

                   <?php $this->load->view('common/footer.php'); ?>
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <?php $this->load->view('common/settings.php') ?>
        <!-- END User Settings -->

        <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
       <?php $this->load->view('common/script.php') ?>
       <!-- <script src="<?php echo base_url('assets/js/add_subcategory.js')?>"></script> -->
        <!-- Modal -->
        <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
          <div class="modal-dialog animation-slideDown">

            <!-- Modal content-->
            <div class="modal-content CustomModal">
              <div class="modal-body">
                <div id="loginCustom">
                    <!-- Login Title -->
                    <div class="login-title text-right">
                        <h1>
                            <i class="Unitsss fa fa-cube pull-left"></i>
                            <strong>Manage</strong> Sub Categories<br>
                            <br>
                        </h1>
                        </div>
                        <!-- END Login Title -->

                        <!-- Login Block -->
                        <div class="block">
                            <form action="" method="post" class="form-horizontal form-bordered" id="subcategory_form" novalidate>  
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Sub Category Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="sub_cat_name" name="sub_cat_name" class="form-control" placeholder="Enter subcategory name">
                                        <span id="head"></span>
                                        <span class="help-block">Please enter Sub Category name</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Sub Category Description</label>
                                    <div class="col-md-9">
                                        <input type="text" id="sub_cat_description" name="sub_cat_description" class="form-control" placeholder="Enter subcategory description" required>
                                        <span id="head1"></span>
                                        <span class="help-block">Please enter Sub Category Description</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Category Name</label>
                                    <div class="col-md-9">
                                        <select id="category_fk_id" name="category_fk_id" class="select-chosen" data-placeholder="Choose Category" style="width: 250px;" required>
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                             <?php foreach($categories['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>"><?php echo $row->category_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head2"></span>
                                        <span class="help-block">Please select category</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Category Image</label>
                                    <div class="col-md-9">
                                        <input type="file" id="sub_cat_Image" name="sub_cat_Image" class="category_image" required>
                                        <img src="<?php echo base_url('assets/img/placeholders/avatars/avatar2.jpg')?>" id="profile-img-tag" width="200px">
                                        <span class="help-block">Please upload category image</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="status_choice" id="somefield" value="">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END Login Block -->
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });
            
            $("#subcategory_form").submit(function(e){
                e.preventDefault();
                var formData = new FormData($("#subcategory_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:"<?php echo base_url('subcategory/add') ?>",
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                alert("Record Inserted Successfully!");
                                location.reload();
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
    {
        // Make quick references to our fields.
        var sub_cat_name = document.getElementById('sub_cat_name');
        var category_fk_id = document.getElementById('category_fk_id');
        var sub_cat_Image = document.getElementById('sub_cat_Image');
        var sub_cat_description = document.getElementById('sub_cat_description');
  
  
        // To check empty form fields.  
        if (sub_cat_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter subcategory Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            sub_cat_name.focus();
            return false;
        }
        if (sub_cat_description.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            sub_cat_description.focus();
            return false;
        }
        if (category_fk_id.value.length == 0) {
            document.getElementById('head2').innerText = "Please select Category"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            category_fk_id.focus();
            return false;
        }

  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(sub_cat_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


        // Function that checks whether input text is an alphabetic character or not.
        function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
        return true;
        } else {
        document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
        $('#p1').delay(2000).hide('slow');
        $('#p1').css('display','block');
        //alert(alertMsg);
        inputtext.focus();
        return false;
        }
        }

        function checkActivate(id){
            $.ajax({
                // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/subcategory/checkActivate',
                url:'<?php echo base_url("subcategory/checkActivate") ?>',
                type:"post",
                data:{'id':id},
                success: function(data){
                    alert("Record activated/deactivated successfully!");
                }
            });
        }

        function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".category_image").change(function() {
       
      readURL(this);
    });

    $("#example-hf-text1").on('blur', function(){
        var sub_cat_name = $(this).val();
        $.ajax({
            url:'<?php echo base_url("subcategory/checkExists") ?>',
            type:"post",
            data:{'sub_cat_name':sub_cat_name},
            success: function(data){
                if(data == 'true'){
                    $("#example-hf-text1").val('');
                    alert('Subcategory name already exists!');
                }
            }
        });
    });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('subcategory')?> ";
        // $('#myModal').modal('hide');
    });
    </script>
