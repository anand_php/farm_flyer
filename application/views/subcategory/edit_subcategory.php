            <style type="text/css">
                .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
                .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Subcategory
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Resources</li>
                        <li><a href="<?php echo base_url('subcategory') ?>">Subcategory</a></li>
                        <li><a href="#">Edit Subcategory</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Subcategory</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Units and Assign Categories</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Subcategory<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                             <form method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="edit_subcategory_form">
                                <input type="hidden" name="id" value="<?php echo $subcategory->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Sub Category Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="sub_cat_name" name="sub_cat_name" class="form-control" placeholder="Enter Subcategory name" value="<?php echo $subcategory->sub_cat_name ?>">
                                        <span id="head"></span>
                                        <span class="help-block">Please enter Sub Category name</span>
                                    </div>
                                </div><div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Sub Category Description</label>
                                    <div class="col-md-9">
                                        <input type="text" id="sub_cat_description" name="sub_cat_description" class="form-control" placeholder="Enter Subcategory Description" value="<?php echo $subcategory->sub_cat_description ?>">
                                        <span id="head1"></span>
                                        <span class="help-block">Please enter Sub Category Description</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Category Name</label>
                                    <div class="col-md-9">
                                        <select id="category_fk_id" name="category_fk_id" class="select-chosen" data-placeholder="Choose Base Unit" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($categories['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $subcategory->id ) echo 'selected="selected"'; ?> ><?php echo $row->category_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head2"></span>
                                        <span class="help-block">Please select base unit</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Category Image</label>
                                    <div class="col-md-9">
                                        <input type="file" id="example-file-input" name="sub_cat_Image" class="category_image">
                                        <img src="<?php echo base_url().'uploads/subcategory/'. $subcategory->sub_cat_Image ?>" id="profile-img-tag" width="200px">
                                        <span class="help-block">Please upload category image</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <?php if($subcategory->status_choice == 'true'){ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                        </label>
                                    <?php }else{ ?>
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox"><span></span>
                                        </label>
                                    <?php } ?>
                                    <!-- <div class="form-group"> -->
                                    <input type="hidden" name="status_choice" id="somefield" value="<?php echo $subcategory->status_choice ?>">
                                    <!-- <div class="col-md-9">
                                        <label class="switch switch-primary">
                                            <input type="checkbox" checked="" name="status_choice"><span></span></label>
                                    </div> -->
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings.php'); ?>
    <!-- END User Settings -->

   <?php $this->load->view('common/script.php'); ?>

    
    <script type="text/javascript">
        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });
            
            $("#edit_subcategory_form").submit(function(e){
                e.preventDefault();
                var formData = new FormData($("#subcategory_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:"<?php echo base_url('subcategory/update') ?>",
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                alert("Record Inserted Successfully!");
                                window.location.href = "<?php echo base_url('subcategory') ?>"
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
    {
        // Make quick references to our fields.
        var sub_cat_name = document.getElementById('sub_cat_name');
        var category_fk_id = document.getElementById('category_fk_id');
        var sub_cat_Image = document.getElementById('sub_cat_Image');
        var sub_cat_description = document.getElementById('sub_cat_description');
  
  
        // To check empty form fields.  
        if (sub_cat_name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter subcategory Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            sub_cat_name.focus();
            return false;
        }
        if (sub_cat_description.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Category Description"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            sub_cat_description.focus();
            return false;
        }
        if (category_fk_id.value.length == 0) {
            document.getElementById('head2').innerText = "Please select Category"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            category_fk_id.focus();
            return false;
        }

  
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(sub_cat_name, "* Please enter valid category name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


        // Function that checks whether input text is an alphabetic character or not.
        function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
        return true;
        } else {
        document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
        $('#p1').delay(2000).hide('slow');
        $('#p1').css('display','block');
        //alert(alertMsg);
        inputtext.focus();
        return false;
        }
        }

        $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".category_image").change(function() {
       
      readURL(this);
    });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('subcategory')?> ";
    });
    </script>