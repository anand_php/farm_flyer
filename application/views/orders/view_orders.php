            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Orders
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Orders</li>
                        <li><a href="<?php echo base_url('orders') ?>">Orders</a></li>
                        <li><a href="#">View Order</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Orders</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Orders</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Orders<br>
                                <br>
                            </h1>
                            <!-- <?php //echo"<pre>"; print_r($order); ?>   -->
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>User Name</td>
                                            <td><?php echo $order->buyer_name ?></td>
                                        </tr>
                                        <tr>
                                            <td>Order Generated Id</td>
                                            <td><?php echo $order->order_generated_id ?></td>
                                        </tr>
                                        <tr>
                                            <td>Order Status</td>
                                            <td>
                                                <?php if($order->order_status == '0'){
                                                        echo"Pending";
                                                    }elseif($order->order_status == '1'){
                                                        echo"Accepted";
                                                    }elseif($order->order_status == '2'){
                                                        echo"Rejected";
                                                    }elseif($order->order_status == '3'){
                                                        echo"Cancelled";
                                                    }elseif($order->order_status == '4'){
                                                        echo"Shipped";
                                                    }elseif($order->order_status == '5'){
                                                        echo"Completed";
                                                    }  
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Order Total</td>
                                            <td><?php echo $order->order_total ?></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Mode</td>
                                            <td><?php echo $order->payment_mode ?></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Status</td>
                                            <td>
                                                <?php if($order->payment_status == '0'){
                                                        echo"Paid";
                                                    }else{
                                                        echo"Unpaid";
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pickup Address</td>
                                            <td><?php echo $order->pickup_address ?></td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Address</td>
                                            <td><?php echo $order->delivery_address ?></td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Option</td>
                                            <td><?php echo $order->delivery_option ?></td>
                                        </tr>
                                        <tr>
                                            <td>Order Placed Date</td>
                                            <td><?php echo $order->created_date ?></td>
                                        </tr>
                                        <tr>
                                            <td><h3 style="text-align: center">Order Details</h3></td>
                                            <table class="table">
                                                <thead>
                                                    <th>Category</th>
                                                    <th>Subcategory</th>
                                                    <th>Product</th>
                                                    <th>Product Varient</th>
                                                    <th>Product Quantity</th>
                                                    <th>Product Price</th>
                                                    <th>Subtotal</th>
                                                </thead>
                                                <tbody>
                                                    <?php if($order_details['count'] > 0){
                                                        foreach($order_details['result'] as $order_detail){ ?>
                                                        <tr>
                                                            <td><?php echo $order_detail->category_id ?></td>
                                                            <td><?php echo $order_detail->subcategory_id ?></td>
                                                            <td><?php echo $order_detail->product_id ?></td>
                                                            <td><?php echo $order_detail->product_varient ?></td>
                                                            <td><?php echo $order_detail->product_quantity ?></td>
                                                            <td><?php echo $order_detail->product_price ?></td>
                                                            <td><?php echo $order_detail->product_subtotal ?></td>
                                                        </tr>   
                                                    <?php }
                                                    } ?>
                                                </tbody>
                                            </table>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>