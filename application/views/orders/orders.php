            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Orders
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Orders</li>
                        <li><a href="#">Orders</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <div class="alert alert-success" id="successMessage"> <?= $this->session->flashdata('message') ?> </div>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Orders</strong> integration</h2>
                            <!-- <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Orders</p>

                        <div class="table-responsive">

                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Sr No</th>
                                        <th class="text-center">Order Id</th>
                                        <th class="text-center">User name</th>
                                        <th class="text-center">Order Status</th>
                                        <th class="text-center">Order Total</th>
                                        <th class="text-center">Order Placed Date</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php 
                                        if($orders['count'] > 0){ 
                                        $i=1;
                                        foreach($orders['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->order_generated_id ?></td>
                                            <td><?php echo $row->buyer_name ?></td>
                                            <td>
                                                <?php if($row->order_status == '0'){
                                                echo"Pending";
                                                }elseif($row->order_status == '1'){
                                                    echo"Accepted";
                                                }elseif($row->order_status == '2'){
                                                    echo"Rejected";
                                                }elseif($row->order_status == '3'){
                                                    echo"Cancelled";
                                                }elseif($row->order_status == '4'){
                                                    echo"Shipped";
                                                }elseif($row->order_status == '5'){
                                                    echo"Completed";
                                                }  ?>
                                                    
                                            </td>
                                            <td><?php echo $row->order_total ?></td>
                                            <td><?php echo date("Y-m-d", strtotime($row->created_date)) ?></td>
                                            <td>
                                                <?php if($row->status_choice == '1'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->order_id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->order_id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            </td>
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url().'orders/view/'. $row->order_id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>
                                                    <!-- <a href="<?php echo base_url().'orders/edit/'. $row->order_id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-pencil"></i></a> -->
                                                    <a href="<?php echo base_url() .'orders/delete/' . $row->order_id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" id="delete"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    <?php $i++; } } ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>
    <!-- <script src="<?php echo base_url('scripts/add_brand.js')?>"></script> -->
            <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
      <div class="modal-dialog animation-slideDown">

        <!-- Modal content-->
        <div class="modal-content CustomModal">
          <div class="modal-body">
            <div id="loginCustom">
                <!-- Login Title -->
                <div class="login-title text-right">
                <div class="login-title text-right">
                    <h1>
                        <i class="Unitsss fa fa-star-o pull-left"></i>
                        <strong>Manage</strong> Admin<br>
                        <br>
                    </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block">
                        <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="admin_form">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Buyer</label>
                                <div class="col-md-9">
                                    <select id="buyer_id" name="buyer_id" class="select-chosen" data-placeholder="Choose Buyer" style="width: 250px;" required>
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                             <?php foreach($buyers['result'] as $buyer) { ?>
                                                <option value="<?php echo $buyer->buyer_id ?>"><?php echo $buyer->buyer_name ?></option>
                                            <?php }?>
                                    </select>
                                    <span id="head1"></span>
                                    <span class="help-block">Please select Buyer</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Name</label>
                                <div class="col-md-9">
                                    <input type="text" id="name" name="name" class="form-control name" placeholder="Enter name">
                                    <span id="head"></span>
                                    <span id="p1"></span>
                                    <span class="help-block">Please enter name</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Email</label>
                                <div class="col-md-9">
                                    <input type="email" id="email" name="email" class="form-control email" placeholder="Enter email">
                                    <span id="head1"></span>
                                    <span class="help-block">Please enter email</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Mobile</label>
                                <div class="col-md-9">
                                    <input type="text" id="mobile" name="mobile" class="form-control mobile" placeholder="Enter mobile" >
                                    <span id="head2"></span>
                                    <span id="mobile-valid"></span>  
                                    <span id="mobile-invalid"></span>
                                    <span class="help-block">Please enter mobile</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Gender</label>
                                <div class="col-md-9">
                                    <!-- <input type="text" id="brand_name" name="brand_name" class="form-control brand_name" placeholder="Enter brand name"> -->
                                    <select class="form-control gender" name="gender" id="gender">
                                        <option value="">Please Select gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                    <span id="head3"></span>
                                    <span class="help-block">Please select gender</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text"> Password</label>
                                <div class="col-md-9">
                                    <input type="password" id="password" name="password" class="form-control password" placeholder="Enter password">
                                    <span id="head4"></span>
                                    <span class="help-block">Please enter password</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Confirm Password</label>
                                <div class="col-md-9">
                                    <input type="password" id="confirm_password" name="confirm_password" class="form-control password" placeholder="R e-Enter password">
                                    <span id="head4"></span>
                                    <span class="help-block">Please re-enter password</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Address</label>
                                <div class="col-md-9">
                                    <input type="text" id="address" name="address" class="form-control address" placeholder="Enter address">
                                    <span id="head5"></span>
                                    <span class="help-block">Please enter address</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Image</label>
                                <div class="col-md-9">
                                    <input type="file" id="admin_image" name="admin_image" class="admin_image" required>
                                    <img src="<?php echo base_url('assets/img/placeholders/avatars/avatar2.jpg')?>" id="profile-img-tag" width="200px">
                                    <span id="head6"></span>
                                    <span class="help-block">Please upload admin image</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="status_choice" id="somefield" value="true">
                                    <div class="col-md-9">
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#admin_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#admin_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("admin/add")?>',
                    // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/brand/add',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            alert("Record inserted successfully!");
                            window.location.href = "<?php echo base_url('admin') ?>";
                        }
                    }
                });
            }
        });
    });

        function formValidation() 
    {
        // Make quick references to our fields.
        var name = document.getElementById('name');
        // var username = document.getElementById('username');
        var email = document.getElementById('email');
        var mobile = document.getElementById('mobile');
        var gender = $("#gender").val();
        var password = document.getElementById('password');
        var address = document.getElementById('address');
        // var category_image = document.getElementById('category_image');
  
        // To check empty form fields.  
        if (name.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            name.focus();
            return false;
        }
        
        if (email.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Email"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            email.focus();
            return false;
        }
        if (mobile.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter Mobil"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            mobile.focus();
            return false;
        }
        if (gender == '') {
            document.getElementById('head3').innerText = "Please select gender"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            gender.focus();
            return false;
        }
        if (password.value.length == 0) {
            document.getElementById('head4').innerText = "Please Enter Password"; // This segment displays the validation rule for all fields
            $('#head4').delay(2000).hide('slow');
            $('#head4').css('display','block');
            $('#head4').css('color','red');
            password.focus();
            return false;
        }
        if (address.value.length == 0) {
            document.getElementById('head5').innerText = "Please Enter Address"; // This segment displays the validation rule for all fields
            $('#head5').delay(2000).hide('slow');
            $('#head5').css('display','block');
            $('#head5').css('color','red');
            address.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(name, "* Please enter valid name.")) {
            // if (isMobileValid(mobile, "* Please enter valid mobile number")) {
                return true;
            // }
        }
        return false;
    }

    $("#mobile").on("blur", function(){
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;

        if (filter.test(mobNum)) {
            if(mobNum.length==10){
                 
            } else {
                document.getElementById('head2').innerText = "Please put 10  digit mobile number";
                $('#head2').delay(2000).hide('slow');
                $('#head2').css('display','block');
                $('#head2').css('color','red');
                $("#mobile").val('');
                return false;
            }
        }else{
            document.getElementById('head2').innerText = "Please enter valid number";
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            $("#mobile").val('');
            return false;
        }
    });


    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[a-zA-Z]{3,16}$/;
        if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#p1').delay(2000).hide('slow');
            $('#p1').css('display','block');
            $('#p1').css('color','red');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }

    $("#confirm_password").on('blur', function(){
        var confirm_password = $("#confirm_password").val();
        var password = $("#password").val();

        if(confirm_password == password){

        }else{
            $("#confirm_password").val('');
            $("#password").val('')
            alert("Password and confirm password must be same");
        }
    });

    function checkActivate(id){
        $.ajax({
            // url:'http://192.168.0.2/dev06/Pooja/Bit/Farm_flyer/brand/checkActivate',
            url:'<?php echo base_url('admin/checkActivate') ?>',
            type:"post",
            data:{'id':id},
            success: function(data){
                alert("Record activated/deactivated successfully!");
            }
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".admin_image").change(function() {
       
      readURL(this);
    });

    $("#username").on('blur', function(){
        var username = $(this).val();

        $.ajax({
            url:'<?php echo base_url('admin/checkExists') ?>',
            type:"post",
            data:{'username':username},
            success: function(data){
                if(data == 'true'){
                    $("#username").val('');
                    alert("username already exists!");
                }
            }
        });
    });

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('admin') ?>";
        // $('#myModal').modal('hide');
    });
</script>