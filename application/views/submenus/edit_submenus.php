            <style type="text/css">
    .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
    .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
</style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sub Menus
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Menus</li>
                        <li><a href="<?php echo base_url('submenus') ?>">Sub Menus</a></li>
                        <li><a href="#">Edit Sub Menu</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Sub Menus</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Sub Menus and Assign Sub Menus</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Sub Menus<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="edit_submenu_form">
                                <input type="hidden" name="id" value="<?php echo $submenu->id ?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Menu Name</label>
                                    <div class="col-md-9">
                                        <select id="menu_id" name="menu_id" class="select-chosen" data-placeholder="Choose Base Unit" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($menus['result'] as $row) { ?>
                                                <option value="<?php echo $row->id ?>" <?php if($row->id == $submenu->menu_id ) echo 'selected="selected"'; ?> ><?php echo $row->menu_name ?></option>
                                            <?php }?>
                                        </select>
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please select menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Sub Menu Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="submenu_name" name="submenu_name" class="form-control submenu_name" placeholder="Enter sub menu name" value="<?php echo $submenu->submenu_name ?>">
                                        <span id="head1"></span>
                                        <!-- <span class="help-block">Please enter sub menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Link<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="link" name="link" class="form-control link" placeholder="Enter menu link" value="<?php echo $submenu->link ?>">
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please enter menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Icon<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="file" id="icon" name="icon" class="form-control icon" placeholder="Enter menu icon" required>
                                        <span id="head3"></span>
                                        <!-- <span class="help-block">Please enter menu name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($submenu->status_choice == 'true'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $submenu->status_choice ?>"> 
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#statusCheck').change(function(){
                cb = $(this);
                cb.val(cb.prop('checked'));
                $("#somefield").val(this.value);
            });

            $('#edit_submenu_form').submit(function(e){

                e.preventDefault();
                var formData = new FormData($("#edit_submenu_form")[0]);
                var isFormValid = formValidation();
                if(isFormValid == true){
                    $.ajax({
                        url:'<?php echo base_url("submenus/update")?>',
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data){
                            if(data == 'true'){
                                alert("Record updated successfully!");
                                window.location.href="<?php echo base_url('submenus') ?>";
                            }
                        }
                    });
                }
            });
        });

        function formValidation() 
    {
        // Make quick references to our fields.submenus_form
        var menu_id = $('#menu_id').val();
        var submenu_name = document.getElementById('submenu_name');
  
        // To check empty form fields.  
        if (menu_id == '') {
            document.getElementById('head').innerText = "Please Select menu Name"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            menu_id.focus();
            return false;
        }
        if (submenu_name.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter submenu Name"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            submenu_name.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(submenu_name, "* Please enter valid submenu name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
            if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#p1').delay(2000).hide('slow');
            $('#p1').css('display','block');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }

    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('submenus') ?>";
        // $('#myModal').modal('hide');
    }); 
</script>
    