           <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sellers
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Users</li>
                        <li><a href="<?php echo base_url('users') ?>">Sellers</a></li>
                        <li><a href="#">Edit User</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Sellers</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Sellers and Assign Sellers</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Sellers<br>
                                <br>
                            </h1>
                            
                            <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="edit_user_form" novalidate>
                                <input type="hidden" name="user_id" value="<?php echo $user->user_id?>">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">User Role<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_role" name="user_role" class="form-control user_role" placeholder="Please enter role" value="<?php echo $user->user_role ?>">
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please enter user role</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_name" name="user_name" class="form-control name" placeholder="Please enter user name" value="<?php echo $user->user_name?>">
                                        <span id="head1"></span>
                                        <span id="p1"></span>
                                        <!-- <span class="help-block">Please enter name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Mobile<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_mobile" name="user_mobile" class="form-control" placeholder="Please enter mobile" value="<?php echo $user->user_mobile?>">
                                        <span id="head2"></span>
                                        <!-- <span class="help-block">Please enter user mobile</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Alternate Mobile</label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_alt_mobile" name="user_alt_mobile" class="form-control" placeholder="Please enter altername mobile" value="<?php echo $user->user_alt_mobile ?>">
                                        <span id="head3"></span>
                                        <!-- <span class="help-block">Please enter users altername mobile</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Address<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_address" name="user_address" class="form-control user_address" placeholder="Please enter Address" value="<?php echo $user->user_address ?>">
                                        <span id="head4"></span>
                                        <!-- <span class="help-block">Please enter users Address</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">State<span>*</span></label>
                                    <div class="col-md-9">
                                        <select name="user_state_id" id="user_state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($states['result'] as $state){ ?>
                                                <option value="<?php echo $state->state_id ?>" <?php if($state->state_id == $user->user_state_id ) echo 'selected="selected"'; ?> ><?php echo $state->state_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="head5"></span>
                                        <!-- <span class="help-block">Please select state</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">City<span>*</span></label>
                                    <div class="col-md-9">
                                        <select name="user_city_id" id="user_city_id" class="select-chosen" data-placeholder="Choose City" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($cities['result'] as $city){ ?>
                                                <option value="<?php echo $city->id ?>" <?php if($city->id == $user->user_city_id ) echo 'selected="selected"'; ?> ><?php echo $city->city_name ?></option>
                                            <?php } ?> 
                                        </select>
                                        <span id="head6"></span>
                                        <!-- <span class="help-block">Please select city</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Town<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_town" name="user_town" class="form-control" placeholder="Please enter town" value="<?php echo $user->user_town?>">
                                        <span id="head7"></span>
                                        <!-- <span class="help-block">Please enter users town</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Landmark<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_landmark" name="user_landmark" class="form-control" placeholder="Please enter landmark" value="<?php echo $user->user_landmark?>">
                                        <span id="head8"></span>
                                        <!-- <span class="help-block">Please enter users landmark</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Pincode<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_pincode" name="user_pincode" class="form-control user_pincode" placeholder="Please enter pincode" value="<?php echo $user->user_id?>">
                                        <span id="head9"></span>
                                        <!-- <span class="help-block">Please enter users pincode</span> -->
                                    </div>
                                </div>
                                <h2>Business Information</h2>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Name<span>*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="business_name" name="business_name" class="form-control business_name" placeholder="Please Enter Business information" value="<?php echo $user->business_name ?>">
                                        <span id="head9"></span>
                                        <!-- <span class="help-block">Please enter business information</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">State</label>
                                    <div class="col-md-9">
                                        <select name="business_state_id" id="business_state_id" class="select-chosen" data-placeholder="Choose State" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($states['result'] as $state){ ?>
                                                <option value="<?php echo $state->state_id ?>" <?php if($state->state_id == $user->user_state_id ) echo 'selected="selected"'; ?> ><?php echo $state->state_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="head10"></span>
                                        <!-- <span class="help-block">Please select state</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">City</label>
                                    <div class="col-md-9">
                                        <select name="business_city_id" id="business_city_id" class="select-chosen" data-placeholder="Choose City" style="width: 250px;">
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <?php foreach($cities['result'] as $city){ ?>
                                                <option value="<?php echo $city->id ?>" <?php if($city->id == $user->user_city_id ) echo 'selected="selected"'; ?> ><?php echo $city->city_name ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="head11"></span>
                                        <!-- <span class="help-block">Please select city</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Town</label>
                                    <div class="col-md-9">
                                        <input type="text" id="business_town" name="business_town" class="form-control business_town" placeholder="Please enter business town" value="<?php echo $user->business_town ?>">
                                        <span id="head12"></span>
                                        <!-- <span class="help-block">Please enter business town</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" id="business_address" name="business_address" class="form-control" placeholder="Please enter business town" value="<?php echo $user->business_address?>">
                                        <span id="head13"></span>
                                        <!-- <span class="help-block">Please enter business address</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Pincode</label>
                                    <div class="col-md-9">
                                        <input type="text" id="business_pincode" name="business_pincode" class="form-control business_pincode" placeholder="Please enter business pincode" value="<?php echo $user->business_pincode ?>">
                                        <span id="head14"></span>
                                        <!-- <span class="help-block">Please enter business pincode</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Bank Beneficiary Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="bank_beneficiary_name" name="bank_beneficiary_name" class="form-control" placeholder="Please enter bank beneficiary name" value="<?php echo $user->bank_beneficiary_name?>">
                                        <span id="head15"></span>
                                        <!-- <span class="help-block">Please enter bank Beneficiary Name</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Bank Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="bank_name" name="bank_name" class="form-control" placeholder="Please enter bank name" value="<?php echo $user->bank_name?>">
                                        <span id="head16"></span>
                                        <!-- <span class="help-block">Please enter bank name</span> -->
                                    </div>
                                </div><div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Bank Account Number</label>
                                    <div class="col-md-9">
                                        <input type="text" id="bank_account_number" name="bank_account_number" class="form-control" placeholder="Please enter business pincode" value="<?php echo $user->bank_account_number?>">
                                        <span id="head17"></span>
                                        <!-- <span class="help-block">Please enter bank account number</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Bank IFSC</label>
                                    <div class="col-md-9">
                                        <input type="text" id="bank_ifsc" name="bank_ifsc" class="form-control" placeholder="Please enter bank IFSC" value="<?php echo $user->bank_ifsc?>">
                                        <span id="head18"></span>
                                        <!-- <span class="help-block">Please enter bank IFSC</span> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <?php if($user->status_choice == '1'){ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" checked><span></span>
                                            </label>
                                        <?php }else{ ?>
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox"><span></span>
                                            </label>
                                        <?php } ?>
                                        <!-- <div class="form-group"> -->
                                        <input type="hidden" name="status_choice" id="somefield" value="<?php echo $user->status_choice ?>"> 
                                    </div>
                                </div>
                                
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

<script type="text/javascript">
    $("#user_state_id").on('change',function(){
        var user_state_id = $("#user_state_id").val();

        $.ajax({
            url:'<?php echo base_url("users/getCityByStateId")?>',
            type:"post",
            data:{'user_state_id' : user_state_id},
            success: function(data){
                if(data == 'false'){
                    $("#user_city_id").empty();
                    $('#user_city_id').trigger("chosen:updated");
                }else{
                    var res = JSON.parse(data);
                
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                         $('#user_city_id').append('<option value="' + res[i].id + '">' + res[i].city_name + '</option>');
                         $('#user_city_id').trigger("chosen:updated");
                        
                    }
                }
            }
        });
    });

    $("#business_state_id").on('change',function(){
        var user_state_id = $("#business_state_id").val();

        $.ajax({
            url:'<?php echo base_url("users/getCityByStateId")?>',
            type:"post",
            data:{'user_state_id' : user_state_id},
            success: function(data){
                if(data == 'false'){
                    $("#business_city_id").empty();
                    $('#business_city_id').trigger("chosen:updated");
                }else{
                    var res = JSON.parse(data);
                
                    var options = "";
                    for (var i = 0; i < res.length; i++) {
                        // $('#sub_category_fk_id').html('<option value="">Select state first</option>'); 
                        $('#business_city_id').append('<option value="' + res[i].id + '">' + res[i].city_name + '</option>');
                        $('#business_city_id').trigger("chosen:updated");
                        
                    }
                }
            }
        });
    });

    $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#edit_user_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#edit_user_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("users/update")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            alert('Record inserted successfully!');
                            window.location.href = "<?php echo base_url('users') ?>"
                        }
                    }
                });
            }
        });
    });

    function formValidation() 
    {
        // Make quick references to our fields.
        var user_role = document.getElementById('user_role');
        var user_name = document.getElementById('user_name');
        var user_mobile = document.getElementById('user_mobile');
        var user_address = document.getElementById('user_address');
        var user_state_id = $("#user_state_id").val();
        var user_city_id = $("#user_city_id").val();
        var user_town = document.getElementById('user_town');
        var user_landmark = document.getElementById('user_landmark');
        var user_pincode = document.getElementById('user_pincode');
        // var business_name = document.getElementById('business_name');
        var business_state_id = $("#business_state_id").val();
        var business_city_id = $("#business_city_id").val();
        // var business_city_id = document.getElementById('business_city_id');
        var business_town = document.getElementById('business_town');
        var business_address = document.getElementById('business_address');
        var business_pincode = document.getElementById('business_pincode');
        var bank_beneficiary_name = document.getElementById('bank_beneficiary_name');
        var bank_name = document.getElementById('bank_name');
        var bank_account_number = document.getElementById('bank_account_number');
        var bank_ifsc = document.getElementById('bank_ifsc');
  
        // To check empty form fields.  
        if (user_role.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter User Role"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            user_role.focus();
            return false;
        }
        if (user_name.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter User Name"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            user_name.focus();
            return false;
        }
        if (user_mobile.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter User Mobile"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            user_mobile.focus();
            return false;
        }
        if (user_address.value.length == 0) {
            document.getElementById('head4').innerText = "Please Enter User Address"; // This segment displays the validation rule for all fields
            $('#head4').delay(2000).hide('slow');
            $('#head4').css('display','block');
            $('#head4').css('color','red');
            user_address.focus();
            return false;
        }
        if (user_state_id == '') {
            document.getElementById('head5').innerText = "Please select State"; // This segment displays the validation rule for all fields
            $('#head5').delay(2000).hide('slow');
            $('#head5').css('display','block');
            $('#head5').css('color','red');
            user_state_id.focus();
            return false;
        }
        if (user_city_id == '') {
            document.getElementById('head6').innerText = "Please select City"; // This segment displays the validation rule for all fields
            $('#head6').delay(2000).hide('slow');
            $('#head6').css('display','block');
            $('#head6').css('color','red');
            // user_city_id.focus();
            return false;
        }
        if (user_town.value.length == 0) {
            document.getElementById('head7').innerText = "Please Enter Users Town"; // This segment displays the validation rule for all fields
            $('#head7').delay(2000).hide('slow');
            $('#head7').css('display','block');
            $('#head7').css('color','red');
            user_town.focus();
            return false;
        }
        if (user_landmark.value.length == 0) {
            document.getElementById('head8').innerText = "Please Enter Landmark"; // This segment displays the validation rule for all fields
            $('#head8').delay(2000).hide('slow');
            $('#head8').css('display','block');
            $('#head8').css('color','red');
            user_landmark.focus();
            return false;
        }if (user_pincode.value.length == 0) {
            document.getElementById('head9').innerText = "Please Enter Pincode"; // This segment displays the validation rule for all fields
            $('#head9').delay(2000).hide('slow');
            $('#head9').css('display','block');
            $('#head9').css('color','red');
            user_pincode.focus();
            return false;
        }
        if (business_state_id == '') {
            document.getElementById('head10').innerText = "Please select State"; // This segment displays the validation rule for all fields
            $('#head10').delay(2000).hide('slow');
            $('#head10').css('display','block');
            $('#head10').css('color','red');
            business_state_id.focus();
            return false;
        }
        if (business_city_id == '') {
            document.getElementById('head11').innerText = "Please select City"; // This segment displays the validation rule for all fields
            $('#head11').delay(2000).hide('slow');
            $('#head11').css('display','block');
            $('#head11').css('color','red');
            business_city_id.focus();
            return false;
        }
        if (business_town.value.length == 0) {
            document.getElementById('head12').innerText = "Please Enter business Town"; // This segment displays the validation rule for all fields
            $('#head12').delay(2000).hide('slow');
            $('#head12').css('display','block');
            $('#head12').css('color','red');
            business_town.focus();
            return false;
        }
        if (business_address.value.length == 0) {
            document.getElementById('head13').innerText = "Please Enter Business Address"; // This segment displays the validation rule for all fields
            $('#head13').delay(2000).hide('slow');
            $('#head13').css('display','block');
            $('#head13').css('color','red');
            business_address.focus();
            return false;
        }
        if (business_pincode.value.length == 0) {
            document.getElementById('head14').innerText = "Please Enter Business pincode"; // This segment displays the validation rule for all fields
            $('#head14').delay(2000).hide('slow');
            $('#head14').css('display','block');
            $('#head14').css('color','red');
            business_pincode.focus();
            return false;
        }
        if (bank_beneficiary_name.value.length == 0) {
            document.getElementById('head15').innerText = "Please Enter Bank Beneficiary Name"; // This segment displays the validation rule for all fields
            $('#head15').delay(2000).hide('slow');
            $('#head15').css('display','block');
            $('#head15').css('color','red');
            bank_beneficiary_name.focus();
            return false;
        }
        if (bank_name.value.length == 0) {
            document.getElementById('head16').innerText = "Please Enter Bank Name"; // This segment displays the validation rule for all fields
            $('#head16').delay(2000).hide('slow');
            $('#head16').css('display','block');
            $('#head16').css('color','red');
            bank_name.focus();
            return false;
        }
        if (bank_account_number.value.length == 0) {
            document.getElementById('head17').innerText = "Please Enter Bank Accoutn Number"; // This segment displays the validation rule for all fields
            $('#head17').delay(2000).hide('slow');
            $('#head17').css('display','block');
            $('#head17').css('color','red');
            bank_account_number.focus();
            return false;
        }
        if (bank_ifsc.value.length == 0) {
            document.getElementById('head18').innerText = "Please Enter IFSC Code"; // This segment displays the validation rule for all fields
            $('#head18').delay(2000).hide('slow');
            $('#head18').css('display','block');
            $('#head18').css('color','red');
            bank_ifsc.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(user_name, "* Please enter valid name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
            return true;
        } else {
            document.getElementById('p1').innerText = alertMsg; // This segment displays the validation rule for name.
            $('#p1').delay(2000).hide('slow');
            $('#p1').css('display','block');
            //alert(alertMsg);
            inputtext.focus();
            return false;
        }
    }

    $("#user_mobile").on("blur", function(){
        
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;

        if (filter.test(mobNum)) {
            if(mobNum.length==10){
                 
            } else {
                document.getElementById('head2').innerText = "Please put 10  digit mobile number";
                $('#head2').delay(2000).hide('slow');
                $('#head2').css('display','block');
                $('#head2').css('color','red');
                $("#user_mobile").val('');
                return false;
            }
        }else{
            document.getElementById('head2').innerText = "Please enter valid number";
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            $("#user_mobile").val('');
            return false;
        }
    });

        function readURL(input) {
            if (input.files && input.files[0]) {
                 
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#profile-img-tag').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".admin_image").change(function() {
           
          readURL(this);
        });

        //function to reset or redirect after click on cancel button
        $("#reset").on('click', function(){
            window.location.href="<?php echo base_url('users') ?>";
        }); 
</script>
    