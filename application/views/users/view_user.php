            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sellers
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Users</li>
                        <li><a href="<?php echo base_url('users') ?>">Sellers</a></li>
                        <li><a href="#">View Seller</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Sellers</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Sellers and Assign Sellers</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>View</strong> Sellers<br>
                                <br>
                            </h1>
                            
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>User Role</td>
                                            <td><?php echo $user->user_role?></td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo $user->user_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile</td>
                                            <td><?php echo $user->user_mobile?></td>
                                        </tr>
                                        <tr>
                                            <td>Alternate Mobile</td>
                                            <td><?php echo $user->user_alt_mobile?></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td><?php echo $user->user_address?></td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td><?php echo $user->user_state?></td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td><?php echo $user->user_city?></td>
                                        </tr>
                                        <tr>
                                            <td>Town</td>
                                            <td><?php echo $user->user_town?></td>
                                        </tr>
                                        <tr>
                                            <td>Landmark</td>
                                            <td><?php echo $user->user_landmark?></td>
                                        </tr>
                                        <tr>
                                            <td>Pincode</td>
                                            <td><?php echo $user->user_pincode?></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h2 style="text-align: center;">Business Information</h2>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo $user->business_name?></td>
                                        </tr>
                                        <tr>
                                            <td>State</td>
                                            <td><?php echo $user->business_state?></td>
                                        </tr>
                                        <tr>
                                            <td>City</td>
                                            <td><?php echo $user->business_city ?></td>
                                        </tr>
                                        <tr>
                                            <td>Town</td>
                                            <td><?php echo $user->business_town?></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td><?php echo $user->business_address?></td>
                                        </tr>
                                        <tr>
                                            <td>Pincode</td>
                                            <td><?php echo $user->business_pincode?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank Beneficiary name</td>
                                            <td><?php echo $user->bank_beneficiary_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank Name</td>
                                            <td><?php echo $user->bank_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank Account Number</td>
                                            <td><?php echo $user->bank_account_number?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $user->bank_ifsc?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>