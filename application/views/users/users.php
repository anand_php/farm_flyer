            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Sellers
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Users</li>
                        <li><a href="#">Sellers</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <div class="alert alert-success" id="successMessage"> <?= $this->session->flashdata('message') ?> </div>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Sellers</strong> integration</h2>
                            <!-- <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Sellers and Assign Sellers</p>

                        <div class="table-responsive">

                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">User Role</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Mobile</th>
                                        <!-- <th class="text-center">Alternate Mobile</th> -->
                                        <th class="text-center">State</th>
                                        <th class="text-center">City</th>
                                        <th class="text-center">Town</th>
                                        <th class="text-center">Address</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php 
                                        if($users['count'] > 0){ 
                                        $i = 1;
                                        foreach($users['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->user_role ?></td>
                                            <td><?php echo $row->user_name ?></td>
                                            <td><?php echo $row->user_mobile ?></td>
                                            <!-- <td><?php if($row->user_alt_mobile == 0){
                                               // echo"NA";
                                            }else{
                                               // echo $row->user_alt_mobile;
                                            }  ?></td> -->
                                            <td><?php echo $row->state_name ?></td>
                                            <td><?php echo $row->city_name ?></td>
                                            <td><?php echo $row->user_town ?></td>
                                            <td><?php echo $row->user_address ?></td>
                                            <td>
                                                <?php if($row->status_choice == '1'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->user_id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->user_id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            </td>
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url().'users/view/'. $row->user_id ?>" data-toggle="View" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url().'users/edit/'. $row->user_id ?>" data-toggle="Edit" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'users/delete/' . $row->user_id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" id="delete"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    <?php $i++; } } ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>
    <!-- <script src="<?php echo base_url('scripts/add_brand.js')?>"></script> -->
            <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
      <div class="modal-dialog animation-slideDown">

        <!-- Modal content-->
        <div class="modal-content CustomModal">
          <div class="modal-body">
            <div id="loginCustom">
                <!-- Login Title -->
                <div class="login-title text-right">
                <div class="login-title text-right">
                    <h1>
                        <i class="Unitsss fa fa-star-o pull-left"></i>
                        <strong>Manage</strong> Sellers<br>
                        <br>
                    </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block">
                        <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="user_form">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">User Role</label>
                                <div class="col-md-9">
                                    <input type="text" id="user_role" name="user_role" class="form-control user_role" placeholder="Please enter user role">
                                    <span id="head"></span>
                                    <!-- <span class="help-block">Please enter user role</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">User Name</label>
                                <div class="col-md-9">
                                    <input type="text" id="user_name" name="user_name" class="form-control name" placeholder="Enter user name">
                                    <span id="head1"></span>
                                    <span class="help-block">Please enter user name</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">User Mobile</label>
                                <div class="col-md-9">
                                    <input type="text" id="user_mobile" name="user_mobile" class="form-control" placeholder="Enter user_mobile">
                                    <span id="head2"></span>
                                    <span class="help-block">Please enter user mobile</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">User  alternate Mobile</label>
                                <div class="col-md-9">
                                    <input type="text" id="user_alt_mobile" name="user_alt_mobile" class="form-control user_alt_mobile" placeholder="Enter users alternate mobile">
                                    <span id="head3"></span>
                                    <span class="help-block">Please enter users altername mobile</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">State Name</label>
                                <div class="col-md-9">
                                    <select id="state_id" name="state_id" class="select-chosen" data-placeholder="Choose Menu" style="width: 250px;" required>
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                             <?php foreach($states['result'] as $state) { ?>
                                                <option value="<?php echo $state->state_id ?>"><?php echo $state->state_name ?></option>
                                            <?php }?>
                                    </select>
                                    <span id="head"></span>
                                    <span class="help-block">Please select menu name</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Users  City</label>
                                <div class="col-md-9">
                                    <select class="form-control user_state_id" name="user_state_id" id="user_state_id">
                                        <?php foreach($cities['result'] as $city){ ?>
                                            <option value="<?php echo $city->city_id ?>"><?php echo $city->city_name ?></option>
                                        <?php } ?>
                                    </select>
                                    <span id="head5"></span>
                                    <span class="help-block">Please select city</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Users Town</label>
                                <div class="col-md-9">
                                    <input type="text" id="user_town" name="user_town" class="form-control user_town" placeholder="Enter users town">
                                    <span id="head6"></span>
                                    <span class="help-block">Please enter users town</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Users Landmark</label>
                                <div class="col-md-9">
                                    <input type="text" id="user_landmark" name="user_landmark" class="form-control user_landmark" placeholder="Enter users landmark">
                                    <span id="head7"></span>
                                    <span class="help-block">Please enter users landmark</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Users Pincode</label>
                                <div class="col-md-9">
                                    <input type="text" id="user_pincode" name="user_pincode" class="form-control user_pincode" placeholder="Enter users pincode">
                                    <span id="head8"></span>
                                    <span class="help-block">Please enter users pincode</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="status_choice" id="somefield" value="true">
                                    <div class="col-md-9">
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#user_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#user_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("users/add")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            $('#modal').modal('hide');
                        }
                    }
                });
            }
        });
    });

        function formValidation() 
    {
        // Make quick references to our fields.
        var user_role = document.getElementById('user_role');
        var user_name = document.getElementById('user_name');
        var user_mobile = document.getElementById('user_mobile');
        var user_address = document.getElementById('user_address');
        var user_state_id = $("#user_state_id").val();
        var user_city_id = $("#user_city_id").val();
        var user_town = document.getElementById('user_town');
        var user_landmark = document.getElementById('user_landmark');
        var user_pincode = document.getElementById('user_pincode');
  
        // To check empty form fields.  
        if (user_role.value.length == 0) {
            document.getElementById('head').innerText = "Please Enter User Role"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            user_role.focus();
            return false;
        }
        if (user_name.value.length == 0) {
            document.getElementById('head2').innerText = "Please Enter User Name"; // This segment displays the validation rule for all fields
            $('#head2').delay(2000).hide('slow');
            $('#head2').css('display','block');
            $('#head2').css('color','red');
            user_name.focus();
            return false;
        }
        if (user_mobile.value.length == 0) {
            document.getElementById('head3').innerText = "Please Enter User Mobile"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            user_mobile.focus();
            return false;
        }
        if (email.value.length == 0) {
            document.getElementById('head1').innerText = "Please Enter Email"; // This segment displays the validation rule for all fields
            $('#head1').delay(2000).hide('slow');
            $('#head1').css('display','block');
            $('#head1').css('color','red');
            email.focus();
            return false;
        }
        
        if (gender == '') {
            document.getElementById('head3').innerText = "Please select gender"; // This segment displays the validation rule for all fields
            $('#head3').delay(2000).hide('slow');
            $('#head3').css('display','block');
            $('#head3').css('color','red');
            gender.focus();
            return false;
        }
        if (password.value.length == 0) {
            document.getElementById('head4').innerText = "Please Enter Password"; // This segment displays the validation rule for all fields
            $('#head4').delay(2000).hide('slow');
            $('#head4').css('display','block');
            $('#head4').css('color','red');
            password.focus();
            return false;
        }
        if (address.value.length == 0) {
            document.getElementById('head5').innerText = "Please Enter Address"; // This segment displays the validation rule for all fields
            $('#head5').delay(2000).hide('slow');
            $('#head5').css('display','block');
            $('#head5').css('color','red');
            address.focus();
            return false;
        }
        // Check each input in the order that it appears in the form.
        if (inputAlphabet(name, "* Please enter valid name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        }
        return false;
    }


    // Function that checks whether input text is an alphabetic character or not.
    function inputAlphabet(inputtext, alertMsg) {
        var alphaExp = /^[A-z\d\-_\s]+$/;
        if (inputtext.value.match(alphaExp)) {
        return true;
        } else {
        document.getElementById('head').innerText = alertMsg; // This segment displays the validation rule for name.
        $('#head').delay(2000).hide('slow');
        $('#head').css('display','block');
        //alert(alertMsg);
        inputtext.focus();
        return false;
        }
    }

        function checkActivate(user_id){
            $.ajax({
                url:'<?php echo base_url('users/checkActivate') ?>',
                type:"post",
                data:{'user_id':user_id},
                success: function(data){
                    alert("Record activated/deactivated successfully!");
                }
            });
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                 
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#profile-img-tag').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".admin_image").change(function() {
           
          readURL(this);
        });

        $("#username").on('blur', function(){
            var username = $(this).val();

            $.ajax({
                url:'<?php echo base_url('admin/checkExists') ?>',
                type:"post",
                data:{'username':username},
                success: function(data){
                    if(data == 'true'){
                        $("#username").val('');
                        alert("username already exists!");
                    }
                }
            });
        });

        $("#reset").on('click', function(){
            $('#myModal').modal('hide');
        });
</script>