            <style type="text/css">
                .alert {
                   height:40px;    
                }
            </style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Role Permissions
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Role Permissions</li>
                        <li><a href="#">Role Permissions</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <div>
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <script>
                                        swal({
                                            title: "Done",
                                            text: "<?php echo $this->session->flashdata('message'); ?>",
                                            timer: 2000,
                                            showConfirmButton: false,
                                            type: 'success'
                                        });
                                    </script>
                                <?php }else{} ?>    
                            </div>
                            <h2><strong>Role Permissions</strong> integration</h2>
                            <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p>
                        </div>
                        <p>Admin Can Add / edit and Delete Role and Assign Permissions</p>

                        <div class="table-responsive">

                            <table id="example" class="table table-vcenter table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Role</th>
                                        <th class="text-center">Menu Permissions</th>
                                        <th class="text-center">Sub Menu Permissions</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <?php 
                                        if($permissions['count'] > 0){ 
                                        $i =1;
                                        foreach($permissions['result'] as $row){ ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $row->roleName ?></td>
                                            <td><?php  echo $row->menus; ?></td>
                                            <td><?php echo $row->submenues; ?></td>
                                            <td>
                                                <?php if($row->status_choice == 'true'){ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" checked onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php }else{ ?>
                                                    <label class="switch switch-primary">
                                                        <input type="checkbox" onclick="checkActivate(<?php echo $row->id ?>)"><span></span>
                                                    </label>
                                                <?php } ?>
                                            </td>
                                            <td> 
                                                <div class="btn-group">
                                                    <a href="<?php echo base_url().'permissions/view/'. $row->id ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-default" ><i class="fa fa-eye"></i></a>
                                                    <a href="<?php echo base_url().'permissions/edit/'. $row->id ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-default" ><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url() .'permissions/delete/' . $row->id ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger" id="delete"><i class="fa fa-times"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    <?php $i++; } } ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>
    <!-- <script src="<?php echo base_url('scripts/add_brand.js')?>"></script> -->
            <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-2" role="dialog" aria-hidden="true">
      <div class="modal-dialog animation-slideDown">

        <!-- Modal content-->
        <div class="modal-content CustomModal">
          <div class="modal-body">
            <div id="loginCustom">
                <!-- Login Title -->
                <div class="login-title text-right">
                <div class="login-title text-right">
                    <h1>
                        <i class="Unitsss fa fa-star-o pull-left"></i>
                        <strong>Manage</strong> Role Permissions<br>
                        <br>
                    </h1>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Block -->
                    <div class="block">
                        <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="permissions_form" novalidate>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Role</label>
                                <div class="col-md-9">
                                    <select id="role_id" name="role_id" class="select-chosen" data-placeholder="Choose Role" style="width: 250px;" required>
                                            <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                            <option value="">Select Role</option>
                                             <?php foreach($roles as $row) { ?>
                                                <option value="<?php echo $row->role_id ?>"><?php echo $row->name ?></option>
                                            <?php }?>
                                        </select>
                                    <span id="head"></span>
                                    <!-- <span class="help-block">Please select name</span> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <?php foreach($menues as $row){  ?>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" value="<?php echo $row->id ?>" name="menues[]" id="menu_id" class="menu_id">
                                                <?php echo $row->menu_name ?>
                                            </label><br>
                                            <span id="head1"></span>
                                        </div>

                                        <div class="col-md-12">
                                            <?php 
                                                $submenu = explode(',', $row->submenu);
                                                $submenu_id = explode(',', $row->submenu_id);
                                                
                                                if(!empty($submenu)){ 
                                                    for($i = 0; $i<count($submenu_id); $i++){ 
                                                        if($submenu_id[$i] == ''){ ?>
                                                            
                                                        <?php }else{ ?>
                                                            <div class="col-md-4">
                                                                <label class="checkbox-inline">
                                                                    <input type="checkbox" value="<?php echo $submenu_id[$i] ?>" name="submenu_id[]" id="submenu_id" class="submenu_id">
                                                                    <?php echo $submenu[$i] ?>
                                                                </label><br>
                                                            </div>  
                                                        <?php } 
                                                    } 
                                                }  
                                            ?>

                                        </div>
                                    </div>
                                    <hr>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                <div class="col-md-9">
                                    <input type="hidden" name="status_choice" id="somefield" value="true">
                                    <div class="col-md-9">
                                        <label class="switch switch-primary">
                                            <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <div class="col-md-9" id="menues">
                                    <?php foreach($menus['result'] as $row){ ?>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" value="<?php echo $row->id ?>" name="menues[]" id="menu_id" class="menu_id">
                                            <?php echo $row->menu_name ?>
                                        </label><br>
                                    <?php } ?>
                                    <span id="head1"></span>
                                    <span class="help-block">Please select Menu</span>
                                </div>
                            </div> -->
                            <div class="form-group form-actions">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                    <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#permissions_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#permissions_form")[0]);
            var isFormValid = formValidation();
            if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("permissions/add")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            alert("Permissions added successfully!");
                            window.location.href="<?php echo base_url('permissions') ?>";
                        }
                    }
                });
            }
        });
    });

    function formValidation() {
        // Make quick references to our fields.
        var role_id = document.getElementById('role_id');
        // var menu_id = document.getElementsByClassName("menu_id");
  
        // To check empty form fields.  
        if (role_id.value.length == 0) {
            document.getElementById('head').innerText = "Please select role"; // This segment displays the validation rule for all fields
            $('#head').delay(2000).hide('slow');
            $('#head').css('display','block');
            $('#head').css('color','red');
            role_id.focus();
            return false;
        }

        // if(menu_id == ''){
        //     document.getElementById('head1').innerText = "Please select atleast one menu"; // This segment displays the validation rule for all fields
        //     $('#head1').delay(2000).hide('slow');
        //     $('#head1').css('display','block');
        //     $('#head1').css('color','red');
        //     menu_id.focus();
        //     return false;
        // }
        // Check each input in the order that it appears in the form.
        // if (inputAlphabet(menu_name, "* Please enter valid menu name.")) {
            // if (isImageValid(category_image, "* Please image in valid format.")) {
                return true;
            // }
        // }
        return false;
    }


    function checkActivate(id){
        $.ajax({
            url:'<?php echo base_url('permissions/checkActivate') ?>',
            type:"post",
            data:{'id':id},
            success: function(data){
                if(data == 'true'){
                    window.location.href = "<?php echo base_url('permissions') ?>";
                }
                // alert("Record activated/deactivated successfully!");
            }
        });
    }

    // $(".menu_id").on('click', function(){
    //     var menu_id = $(this).val();
    //     $.ajax({
    //         url:'<?php echo base_url('permissions/getSubMenus') ?>',
    //         type:"post",
    //         data:{'menu_id':menu_id},
    //         success: function(data){
    //             if(data == 'false'){
    //                 alert("No data found");
    //             }else{
    //                 var res = $.parseJSON(data);
    //                 var trHTML = '';
    //                 $.each(res, function(i, item) {
    //                     // alert();
    //                     // trHTML += '<tr><td>' + item.submenu_name + '</td></tr>';
    //                     trHTML += '<div class="form-group"><div class="col-md-9" id="menues"><input type="checkbox" value="'+ item.menu_id + "."+ item.id +'" name="submenues[]" id="menu_id" class="menu_id">' +item.submenu_name +'</div></div>';
    //                 });
    //                 $('#menues').append(trHTML);
    //             }
    //         }
    //     });
    // });    

    $("#reset").on('click', function(){
        $('#myModal').modal('hide');
    });  
</script>