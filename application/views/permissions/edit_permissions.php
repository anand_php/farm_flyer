            <style type="text/css">
    .btn-default.btn-on.active{background-color: #5BB75B;color: white;}
    .btn-default.btn-off.active{background-color: #DA4F49;color: white;}
</style>
            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Role Permissions
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Role</li>
                        <li><a href="<?php echo base_url('permissions') ?>">Role Permissions</a></li>
                        <li><a href="#">Edit Role Permissions</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <h2><strong>Permissions</strong> integration</h2>
                           <!--  <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <p>Admin Can Add / edit and Delete Role and Assign Permissions</p>

                        <div class="block">
                            <h1>
                                <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Role Permissions<br>
                                <br>
                            </h1>
                            <?php echo validation_errors(); ?>
                            <form action="" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" id="edit_permissions_form" novalidate>
                                <input type="hidden" name="id" value="<?php echo $permission->id ?>">
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Role</label>
                                    <div class="col-md-9">
                                        <select id="role_id" name="role_id" class="select-chosen" data-placeholder="Choose Role" style="width: 250px;" required>
                                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                                <option value="">Select Role</option>
                                                 <?php foreach($roles as $row) { ?>
                                                    <option value="<?php echo $row->role_id ?>" <?php if($row->role_id == $permission->role_id ) echo 'selected="selected"'; ?> ><?php echo $row->name ?></option>
                                                <?php }?>
                                            </select>
                                        <span id="head"></span>
                                        <!-- <span class="help-block">Please select name</span> -->
                                    </div>
                                </div>  
                                <div class="form-group">
                                    <?php foreach($menues as $row){ 
                                        $menu = explode(',', $permission->menus);
                                        ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" value="<?php echo $row->id ?>" name="menues[]" id="menu_id" class="menu_id" <?php if(in_array($row->menu_name, $menu)){ echo 'checked="checked"'; }?> >
                                                    <?php echo $row->menu_name ?>
                                                </label><br>
                                                <span id="head1"></span>
                                            </div>

                                            <div class="col-md-12">
                                                <?php 

                                                    $submenu = explode(',', $row->submenu);
                                                    $submenu_id = explode(',', $row->submenu_id);
                                                    $sub = explode(',', $permission->submenues);
                                                     
                                                    if(!empty($submenu)){ 
                                                        for($i = 0; $i<count($submenu_id); $i++){ 
                                                            if($submenu_id[$i] == ''){ ?>
                                                                
                                                            <?php }else{ ?>
                                                                <div class="col-md-4">
                                                                    <label class="checkbox-inline">
                                                                        <input type="checkbox" value="<?php echo $submenu_id[$i] ?>" name="submenu_id[]" id="submenu_id" class="submenu_id" <?php if(in_array($submenu[$i], $sub)){ echo 'checked="checked"'; }?> >
                                                                        <?php echo $submenu[$i] ?>
                                                                    </label><br>
                                                                </div>  
                                                            <?php } 
                                                        } 
                                                    }  
                                                ?>

                                            </div>
                                        </div>
                                        <hr>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="example-hf-text">Status</label>
                                    <div class="col-md-9">
                                        <input type="hidden" name="status_choice" id="somefield" value="true">
                                        <div class="col-md-9">
                                            <label class="switch switch-primary">
                                                <input id="statusCheck" value="" type="checkbox" data="1" checked=""><span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                        <button type="button" class="btn btn-sm btn-danger" id="reset"><i class="fa fa-times"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php $this->load->view('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>

   <?php $this->load->view('common/script.php'); ?>

    <script type="text/javascript">
        // $('input[type="checkbox"]').click(function(){
        //     if($(this).prop("checked") == true){
        //         alert("Checkbox is checked.");
        //     }
        //     else if($(this).prop("checked") == false){
        //         alert("Checkbox is unchecked.");
        //     }
        // });

        $(document).ready(function(){   
          $('#statusCheck').change(function(){
            cb = $(this);
            cb.val(cb.prop('checked'));
            $("#somefield").val(this.value);
        });
        $('#edit_permissions_form').submit(function(e){

            e.preventDefault();
            var formData = new FormData($("#edit_permissions_form")[0]);
            // var isFormValid = formValidation();
            // if(isFormValid == true){
                $.ajax({
                    url:'<?php echo base_url("permissions/update")?>',
                    type:"post",
                    data:new FormData(this),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    success: function(data){
                        if(data == 'true'){
                            window.location.href = "<?php echo base_url('permissions') ?>"
                        }
                    }
                });
            // }
        });
    });

    $(".menu_id").on('click', function(){
        if($(this).prop("checked") == true){
            var menu_id = $(this).val();
            $.ajax({
                url:'<?php echo base_url('permissions/getSubMenus') ?>',
                type:"post",
                data:{'menu_id':menu_id},
                success: function(data){
                    if(data == 'false'){
                        alert("No data found");
                    }else{
                        var res = $.parseJSON(data);
                        var trHTML = '';
                        $.each(res, function(i, item) {
                            // alert();
                            // trHTML += '<tr><td>' + item.submenu_name + '</td></tr>';
                            trHTML += '<div class="form-group"><div class="col-md-9 submenu" id="menues"><div id="test"><input type="checkbox" value="'+ item.id +'" name="submenues[]" id="menu_id" class="menu_id">' +item.submenu_name +'</div></div></div>';
                        });
                        $('#menues').append(trHTML);
                    }
                }
            });
        }
        else if($(this).prop("checked") == false){
            // $('.submenu').remove();
            // alert("Checkbox is unchecked.");
        }
        
    });

    //function to reset or redirect after click on cancel button
    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('permissions') ?>";
    });  
</script>
    