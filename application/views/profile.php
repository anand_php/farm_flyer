            <!-- Page content -->
            <div id="page-content">
                <!-- Datatables Header -->
                <div class="content-header">
                    <div class="header-section">
                        <h1>
                                Profile
                        </div>
                    </div>
                    <ul class="breadcrumb breadcrumb-top">
                        <li>Manage Profile</li>
                        <li><a href="<?php echo base_url('dashboard/profile') ?>">Profile</a></li>
                    </ul>
                    <!-- END Datatables Header -->

                    <!-- Datatables Content -->
                    <div class="block full">
                        <div class="block-title">
                            <?php if ($this->session->flashdata('message')) { ?>
                                <div class="alert alert-success"> <?= $this->session->flashdata('message') ?> </div>
                            <?php } ?>
                            <h2><strong>Admin</strong> Profile</h2>
                            <!-- <p class="AddNew"><b><a href="#" onclick="$('#myModal').modal('show');">ADD NEW +</a> </b></p> -->
                        </div>
                        <!-- <p>Admin Can Add / edit and Delete Units and Assign Brands</p> -->

                        <div class="block">
                            <h1>
                                <!-- <i class="Unitsss fa fa-star-o pull-left"></i>
                                <strong>Manage</strong> Brand<br>
                                <br> -->
                            </h1>
                            <?php echo validation_errors(); ?>
                            <?php if($this->session->userdata('logged_in')['user_role'] == 1){ ?>
                                <form action="<?php echo base_url('dashboard/update') ?>" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" >
                                    <input type="hidden" name="id" value="<?php echo $profile->admin_id ?>">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">First Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="admin_fname" class="form-control admin_fname" placeholder="Enter first name" value="<?php echo $profile->admin_fname ?>">
                                            <span class="help-block">Please enter first name</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Last Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="admin_lname" class="form-control admin_lname" placeholder="Enter last name" value="<?php echo $profile->admin_lname ?>">
                                            <span class="help-block">Please enter last name</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Gender</label>
                                        <div class="col-md-9">
                                            <select name="admin_gender" class="form-control">
                                                <option>Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                            <!-- <input type="text" id="example-hf-text" name="admin_lname" class="form-control admin_lname" placeholder="Enter brand name" value="<?php echo $profile->admin_lname ?>"> -->
                                            <span class="help-block">Please select gender</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Mobile</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="admin_mobile" class="form-control" placeholder="Enter Mobile" value="<?php echo $profile->admin_mobile ?>">
                                            <span class="help-block">Please enter mobile</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Image</label>
                                        <div class="col-md-9">
                                            <input type="file" id="example-hf-text" name="admin_image_path" class="form-control admin_image" placeholder="Enter Mobile" value="<?php echo $profile->admin_image_path ?>">
                                            <img src="<?php echo base_url(). 'uploads/admin/'. $profile->admin_image_path ?>" id="profile-img-tag" width="200px" />
                                            <span class="help-block">Please select file</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Email</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="admin_email" class="form-control" placeholder="Enter Email" value="<?php echo $profile->admin_email ?>">
                                            <span class="help-block">Please enter email</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Password</label>
                                        <div class="col-md-9">
                                            <input type="password" id="example-hf-text" name="admin_password" class="form-control" placeholder="Enter Password" >
                                            <span class="help-block">Please enter password</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Address</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="admin_address" class="form-control" placeholder="Enter Address" value="<?php echo $profile->admin_address ?>">
                                            <span class="help-block">Please enter address</span>
                                        </div>
                                    </div>

                                    <div class="form-group form-actions">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                            <button type="button" id="reset" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            <?php }else{  ?>
                                <form action="<?php echo base_url('dashboard/update_subadmin') ?>" enctype="multipart/form-data" method="POST" class="form-horizontal form-bordered" >
                                    <input type="hidden" name="id" value="<?php echo $profile->id ?>">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="name" class="form-control name" placeholder="Enter name" value="<?php echo $profile->name ?>">
                                            <!-- <span class="help-block">Please enter name</span> -->
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Gender</label>
                                        <div class="col-md-9">
                                            <select name="gender" class="form-control">
                                                <option>Select Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                            <!-- <input type="text" id="example-hf-text" name="admin_lname" class="form-control admin_lname" placeholder="Enter brand name" value="<?php echo $profile->admin_lname ?>"> -->
                                            <!-- <span class="help-block">Please select gender</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Mobile</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="mobile" class="form-control" placeholder="Enter Mobile" value="<?php echo $profile->mobile ?>">
                                            <!-- <span class="help-block">Please enter mobile</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Image</label>
                                        <div class="col-md-9">
                                            <input type="file" id="example-hf-text" name="admin_image" class="form-control admin_image" placeholder="Enter Mobile" value="<?php echo $profile->admin_image ?>">
                                            <img src="<?php echo base_url(). 'uploads/subadmin/'. $profile->admin_image ?>" id="profile-img-tag" width="200px" />
                                            <!-- <span class="help-block">Please select file</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Email</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="email" class="form-control" placeholder="Enter Email" value="<?php echo $profile->email ?>">
                                            <!-- <span class="help-block">Please enter email</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Password</label>
                                        <div class="col-md-9">
                                            <input type="password" id="example-hf-text" name="password" class="form-control" placeholder="Enter Password" >
                                            <!-- <span class="help-block">Please enter password</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="example-hf-text">Address</label>
                                        <div class="col-md-9">
                                            <input type="text" id="example-hf-text" name="address" class="form-control" placeholder="Enter Address" value="<?php echo $profile->address ?>">
                                            <span class="help-block">Please enter address</span>
                                        </div>
                                    </div>

                                    <div class="form-group form-actions">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="submit" class="btn btn-sm btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                            <button type="button" id="reset" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            <?php } ?>
                            
                        </div>
                    </div>
                    <!-- END Datatables Content -->
                </div>
                <!-- END Page Content -->

               <?php include('common/footer.php'); ?>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->

    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
    <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
    <?php $this->load->view('common/settings') ?>
    <!-- END User Settings -->

   <?php include('common/script.php'); ?>

<script type="text/javascript">
    //function to reset or redirect after click on cancel button
    $("#reset").on('click', function(){
        window.location.href="<?php echo base_url('dashboard') ?>";
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
             
            var reader = new FileReader();
            
            reader.onload = function(e) {
              $('#profile-img-tag').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".admin_image").change(function() {
       
      readURL(this);
    });
</script>