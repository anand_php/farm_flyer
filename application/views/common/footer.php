 <!-- Footer -->
<footer class="clearfix">
    <div class="pull-right">
        <!-- Crafted with <i class="fa fa-heart text-danger"></i> by <a href="https://www.omsoftware.net/" target="_blank" title="omsoftware">omsoftware</a> -->
        Powered by <a href="https://www.omsoftware.net/" target="_blank" title="OMSOFTWARE">OMSOFTWARE</a>
    </div>
    <div class="pull-left">
        <span>&copy; 2019-20</span>
    </div>
</footer>
<script>
 if($('.nav-item li a[href="'+ window.location.href +'"]'))
    {
      $('.br-menu-link').removeClass('active');
      $('li a[href="'+ window.location.href +'"]').addClass('active');
      $('li a[href="'+ window.location.href +'"]').parent().parent().prev().addClass('active');
      $('li a[href="'+ window.location.href +'"]').parent().parent().addClass('show-sub');
      var url = "<?php echo base_url('dashboard/profile') ?>";
      if(window.location.href != url)
      {
        $('li a[href="'+ window.location.href +'"]').parent().parent().css('display','block');
      }
    }
    else if($('a[href="'+ window.location.href +'"]'))
    {
      $('.br-menu-link').removeClass('active');
      $('a[href="'+ window.location.href +'"]').addClass('br-menu-link active');
    }
    
</script>
<!-- END Footer -->
 <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
       