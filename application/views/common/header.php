<!DOCTYPE html>
<html class="no-js" lang="en"> 
    <head>
        <meta charset="utf-8">

        <title>Farmflyer-Admin</title>

        <meta name="description" content="farmflyer-admin by omsoftware">
        <meta name="author" content="omsoftware">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <link rel="icon" href="<?php echo base_url('assets/img/logo.png') ?>">



        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
        <!-- <link rel="stylesheet" href="<?php echo base_url('') ?>"> -->

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/plugins.css') ?>">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>">

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
         <script src="<?php echo base_url('assets/js/vendor/jquery.min.js') ?>"></script>
         <script src="<?php echo base_url('assets/js/vendor/bootstrap.min.js') ?>"></script>
        <!-- Modernizr (browser feature detection library) -->
        <script src="<?php echo base_url('assets/js/vendor/modernizr.min.js') ?>"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
    </head>
    <body>
        <!-- Page Wrapper -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!--
            Available classes:

            'page-loading'      enables page preloader
        -->
        <div id="page-wrapper">
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Farmflyer</strong>Admin</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie10"></div>
                </div>
            </div>
            <!-- END Preloader -->
            <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">

            <!-- Main Sidebar -->
            <div id="sidebar">
                <!-- Wrapper for scrolling functionality -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        <!-- Brand -->

                        <a href="<?php echo base_url('dashboard')?>" class="sidebar-brand">
                            
                            <img src="<?php echo base_url('assets/img/logo.png') ?>" class="CustomLogo">
                        </a>
                        <!-- END Brand -->
                        <?php
                            $user_id = $this->session->userdata("logged_in")['id'];
                            $role = $this->session->userdata("logged_in")['user_role'];

                            if($this->session->userdata("logged_in")['user_role'] == 1){
                                $query = $this->db->select('admin_image_path as image')->from('admin_user')->where('admin_id',$user_id)->get()->row();
                            }else{
                                $query = $this->db->select('admin_image as image')->from('subadmin_user')->where('id',$user_id)->get()->row();
                            }
                            
                            if($role == 1){
                                $image = base_url() .'uploads/admin/'. $query->image;
                            }else{
                                $image = base_url() .'uploads/subadmin/'. $query->image ;
                                
                            }
                           

                        ?>
                        <!-- User Info -->
                        <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                            <div class="sidebar-user-avatar">
                                <a href="#">
                                    <?php if($image == ''){ ?>
                                        <img src="<?php echo base_url('assets/img/icons/profile.png')?>" alt="avatar">
                                    <?php }else{ ?>
                                        <img src="<?php echo $image ?>" alt="avatar">
                                    <?php } ?>
                                </a>
                            </div>
                            <div class="sidebar-user-name"><?php echo $this->session->userdata("logged_in")["fname"] ?></div>
                            <!-- <div class="sidebar-user-name">ADMIN</div> -->
                            <div class="sidebar-user-links">
                                <a href="<?php echo base_url('dashboard/profile') ?>" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                                <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                                <a href="<?php echo base_url('login/logout') ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                            </div>
                        </div>
                        <!-- END User Info -->

                        <!-- Sidebar Navigation -->
                        
                        <?php if($this->session->userdata("logged_in")['user_role'] == 1){

                         ?>
                            <ul class="sidebar-nav">
                            <li>
                                <a href="<?php echo base_url('dashboard')?>" class=" active"><i class="fa fa-dashboard sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                            </li>
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Users</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('buyer/sellers') ?>"><i class="fa fa-user"></i>Sellers</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('buyer') ?>"><i class="fa fa-user"></i>Buyers</a>
                                    </li>
                                </ul>
                            </li>
                           <!--  <li>
                                <a href="<?php //echo base_url('orders')?>" class=" active"><i class="fa fa-list sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Orders</span></a>
                            </li> -->
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Inventory</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('sales_inventory') ?>"><i class="fa fa-user"></i>Sales Inventory</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- created by Pooja on 11_09_2019 -->
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Role</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('role') ?>"><i class="fa fa-cube"></i>Add Role</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('permissions') ?>"><i class="fa fa-hourglass"></i>Add Role Permission</a>
                                    </li>
                                </ul>
                            </li> 
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Subadmins</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('admin') ?>"><i class="fa fa-cube"></i>Add Admin</a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-bars sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Menus</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('menus') ?>"><i class="fa fa-plus-circle"></i>Add Menus</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('submenus') ?>"><i class="fa fa-plus-circle"></i>Add Sub menus</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-suitcase sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Resources</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('units') ?>"><i class="fa fa-cube"></i> Units</a>
                                    </li>
                                    <!-- <li>
                                        <a href="<?php echo base_url('subunits') ?>"><i class="fa fa-cube"></i> Sub Units</a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo base_url('category') ?>"><i class="fa fa-hourglass"></i> Category</a>
                                    </li>
                                    <!-- <li>
                                        <a href="<?php echo base_url('subcategory') ?>"><i class="fa fa-hourglass"></i> Sub Category</a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo base_url('brand') ?>"><i class="fa fa-star-o"></i> Brand</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('resource_product') ?>"><i class="fab fa-product-hunt"></i> Products</a>
                                    </li> 
                                    <!-- <li>
                                        <a href="<?php echo base_url('varient') ?>"><i class="gi gi-playing_dices"></i> Verient</a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo base_url('wastage') ?>"><i class="fa fa-trash" aria-hidden="true"></i> Wastage Type</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-map-marker sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Locations</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('service_state') ?>"><i class="fa fa-bank"></i> Service State</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('service_city') ?>"><i class="fa fa-bank"></i> Service City</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('Warehouse') ?>"><i class="fa gi gi-package"></i> Warehouse</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('cold_storage') ?>"><i class="gi gi-cargo"></i> Cold Storage</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('dispatch_depot
                                        ') ?>"><i class="fa fa-th-large"></i> Dispatch Depot</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-shopping_cart sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">eCommerce</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('ecom_dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="gi gi-shop_window"></i> Orders</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="gi gi-shopping_cart"></i> Order View</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('products') ?>"><i class="gi gi-shopping_bag"></i> Products</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('products/buy_pricing') ?>"><i class="gi gi-pencil"></i> Update Buy Price</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('products/sell_pricing') ?>"><i class="gi gi-pencil"></i> Update Sell Price</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('products/edit') ?>"><i class="gi gi-pencil"></i> Product Edit</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('customers/view') ?>"><i class="gi gi-user"></i> Customer View</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Settings</span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url('sms') ?>"><i class="fa fa-envelope-o"></i>Manage SMS Gateway</a>
                                    </li>
                                    
                                </ul>
                            </li>
                        </ul>
                        <?php }else{ ?>
                            <ul class="sidebar-nav">
                                <?php 
                                    for($i = 0; $i<count($menus); $i++){
                                    // for($j = 0; $j<count($permissions); $j++){
                                        // print_r($menus[$i]->link);
                                        $parmission_menu = explode(',', $permissions->menues);
                                        $per = array_filter($parmission_menu);
                                       
                                        for($k = 0; $k<count($per); $k++){
                                            if($per[$k] == $menus[$i]->menu_name){ 
                                                if($menus[$i]->submenus == ''){
                                                    $url = base_url().$menus[$i]->link;
                                                    $class = '';
                                                    $class2 = "active";
                                                }else{
                                                    $url = '';
                                                    $class = 'fa fa-angle-left';
                                                    $class2 = "activee";
                                                }
                                            ?>
                                                <li>
                                                    <a href="<?php echo $url ?>" class="sidebar-nav-menu <?php echo $class2; ?>"><i class="<?php echo $class ?> sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-settings sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide"><?php echo $per[$k]; ?></span></a>
                                                    <?php 
                                                    if($menus[$i]->submenus == ''){ 

                                                    }else{ 

                                                    ?>
                                                        <ul>
                                                            <?php
                                                                $sub = explode(',', $menus[$i]->submenus);

                                                                $parmission_submenues = explode(',', $permissions->submenues);
                                                                $per_sub = array_filter($parmission_submenues);

                                                                
                                                                for($l = 0; $l<count(array_values(array_filter($sub))); $l++){ 
                                                                    if($sub[$l] == ''){

                                                                    }else{ 
                                                                        for($j = 0; $j<count(array_values($per_sub)); $j++){
                                                                            
                                                                            
                                                                            if($per_sub[$j] == $sub[$l]){ 
                                                                                $link = $this->db->select('link')->from('admin_submenus')->where('submenu_name', $per_sub[$j])->get()->row();

                                                                                
                                                                                $url = base_url().$link->link;
                                                                            
                                                                               
                                                                                ?>
                                                                                <li>
                                                                                    <a href="<?php echo $url ?>"><i class="fa fa-envelope-o"></i><?php echo $per_sub[$j] ?></a>
                                                                                </li> 
                                                                            <?php  }
                                                                        } 
                                                                    ?>
                                                                     
                                                                <?php } } 
                                                            ?>
                                                        </ul>
                                                    <?php } ?>
                                                </li>
                                            <?php }
                                        } ?>
                                    <?php  } ?>
                                    <li>
                                        <a href="#" class="sidebar-nav-menu activee"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-map-marker sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Locations</span></a>
                                        <ul>
                                            <li>
                                                <a href="<?php echo base_url('Warehouse') ?>"><i class="fa gi gi-package"></i> Warehouse</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('cold_storage') ?>"><i class="gi gi-cargo"></i> Cold Storage</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo base_url('dispatch_depot
                                                ') ?>"><i class="fa fa-th-large"></i> Dispatch Depot</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                        <?php } ?>
                        
                        <!-- END Sidebar Notifications -->
                    </div>
                    <!-- END Sidebar Content -->
                </div>
                <!-- END Wrapper for scrolling functionality -->
            </div>
            <!-- END Main Sidebar -->

            <!-- Main Container -->
            <div id="main-container">
                <header class="navbar navbar-default">

                    <ul class="nav navbar-nav-custom">
                        <!-- Main Sidebar Toggle Button -->
                        <li>
                            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                <i class="fa fa-bars fa-fw"></i>
                            </a>
                        </li>
                        <!-- END Main Sidebar Toggle Button -->
                    </ul>

                    <!-- Right Header Navigation -->
                        <ul class="nav navbar-nav-custom pull-right">
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">

                                    <?php if($image == ''){ ?>
                                        <img src="<?php echo base_url('assets/img/icons/profile.png')?>" alt="avatar">
                                    <?php }else{ ?>
                                        <img src="<?php echo $image ?>" alt="avatar">
                                    <?php } ?>
                                   
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <li class="dropdown-header text-center">Account</li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-clock-o fa-fw pull-right"></i>
                                            <span class="badge pull-right">10</span>
                                            Updates
                                        </a>
                                        <a href="#">
                                            <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                            <span class="badge pull-right">5</span>
                                            Messages
                                        </a>
                                        <a href="#"><i class="fa fa-question fa-fw pull-right"></i>
                                            <span class="badge pull-right">11</span>
                                            FAQ
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?php echo base_url('dashboard/profile') ?>">
                                            <i class="fa fa-user fa-fw pull-right"></i>
                                            Profile
                                        </a>
                                        <a href="#modal-user-settings" data-toggle="modal">
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                            Settings
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#"><i class="fa fa-lock fa-fw pull-right"></i> Lock Account</a>
                                        <a href="<?php echo base_url('login/logout') ?>"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                                    </li>
                                    <li class="dropdown-header text-center">Activity</li>
                                    <li>
                                        <div class="alert alert-success alert-alt">
                                            <small>5 min ago</small><br>
                                            <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                        </div>
                                        <div class="alert alert-info alert-alt">
                                            <small>10 min ago</small><br>
                                            <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                        </div>
                                        <div class="alert alert-warning alert-alt">
                                            <small>3 hours ago</small><br>
                                            <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
                                        </div>
                                        <div class="alert alert-danger alert-alt">
                                            <small>Yesterday</small><br>
                                            <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)" class="alert-link">New bug submitted</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- END User Dropdown -->
                        </ul>
                    <!-- END Right Header Navigation -->
                </header>
                <!-- END Header -->