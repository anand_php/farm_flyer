<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog animation-slideDown">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <form action="<?php echo base_url('dashboard/change_password') ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                            <input type="hidden" name="id" id="id" value="<?php echo $this->session->userdata('logged_in')['id']?>">
                            <fieldset>
                                <legend>Farmflyer-Admin Setting</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Username</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo $this->session->userdata("logged_in")['fname']  ?></p>
                                        <!-- .' '. $this->session->userdata("logged_in")['lname'] -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" id="admin_email" name="admin_email" class="form-control" value="<?php echo $this->session->userdata("logged_in")['email'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
                                    <div class="col-md-8">
                                        <label class="switch switch-primary">
                                            <input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="admin_password" name="admin_password" class="form-control" placeholder="Please choose a complex one..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="confirm_pasword" name="confirm_pasword" class="form-control" placeholder="..and confirm it!" required>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->
         <script type="text/javascript">
            $("#confirm_pasword").on('blur',function(){
                var confirm_pasword = $(this).val();
                var admin_password = $("#admin_password").val();
                if(admin_password == confirm_pasword){

                }else{
                    $("#confirm_pasword").val('');
                    alert("Password and confirm password must be same");
                }
            });

            
            // $.ajax({
            //     url: '<?php echo base_url('dashboard/change_password') ?>',
            //     type: 'post',
            //     data: {'admin_password':admin_password, 'admin_email':admin_email, 'id':id},
            //     success: function(response) {
            //         alert(response);
            //        // if(response == 'true'){
            //        //      alert("Record Inserted Successfully!");
            //        //      location.reload();
            //        //  }
            //     }            
            // });
        </script>