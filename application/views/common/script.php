        
        <!-- <script src="<?php //echo base_url('assets/js/vendor/bootstrap.min.js') ?>"></script> -->
        <script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/app.js') ?>"></script>

        <!-- Load and execute javascript code used only in units page -->
        <script src="<?php echo base_url('assets/js/pages/tablesDatatables.js')?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#example').DataTable();
                $('input[type="search"]').attr('placeholder', 'Search here');
                $('input[type="search"]').addClass('searchh');

            } );
        </script>
            
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key="></script> -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyvW_3aj4NtaihWGsT3GojA2I6ba0rD5w&amp;libraries=geometry,places"></script>
        <script src="<?php echo base_url('assets/js/helpers/gmaps.min.js') ?>"></script>

        <!-- Load and execute javascript code used only in ecom pages -->
        <script src="<?php echo base_url('assets/js/pages/ecomDashboard.js') ?>"></script>
        <script>$(function(){ EcomDashboard.init(); });</script>

        <!-- Load and execute javascript code used only in orders page -->
        <script src="<?php echo base_url('assets/js/pages/ecomOrders.js') ?>"></script>

        <!-- Load and execute javascript code used only in products page -->
        <script src="<?php echo base_url('assets/js/pages/ecomProducts.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/helpers/ckeditor/ckeditor.js') ?>"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="<?php echo base_url('assets/js/pages/index.js') ?>"></script>
        <script>$(function(){ Index.init(); });</script>
      
        <!-- <script type="text/javascript" src="<?php echo base_url('assets/js/add_units.js')?>"></script> -->

        <!-- <script type="text/javascript">
            $("#unit_form").submit(function(e){
                alert();
            }
        </script> -->
    </body>
</html>