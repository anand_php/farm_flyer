<?php
	class Menus_model extends CI_Model {

		public function getAllMenus(){
			$query = $this->db->select('*')->from('admin_menus')->where('is_deleted',0)->order_by('id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        
        public function add()
        {
            $config['upload_path']       = 'uploads/icon';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('icon'))
            {
                $data['upload_data'] = $this->upload->data('file_name');
                $icon = $data['upload_data'];
            }
            else
            {
                $icon = '';
            }
            
            $data = array(
                'menu_name'     => $_POST['menu_name'],
                'link'          => $_POST['link'],
            	'icon'          => $icon,
            	'status_choice' => $_POST['status_choice'],
            	'is_deleted'    => 0,
                'created_date'  => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('admin_menus',$data);
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('admin_menus');
        	return $query;
        }

        public function getMenuById($id){
        	$this->db->select('*');
        	$this->db->from('admin_menus');
        	$this->db->where('id', $id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){
        	$id = $_POST['id'];

            $config['upload_path']       = 'uploads/icon';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('icon'))
            {
                $data['upload_data'] = $this->upload->data('file_name');
                $icon = $data['upload_data'];
            }
            else
            {
                $icon = '';
            }

            $data = array(
                'menu_name'     => $_POST['menu_name'],
                'link'          => $_POST['link'],
                'icon'          => $icon,
                'status_choice' => $_POST['status_choice'],
                'is_deleted'    => 0,
                'modified_date' => date("Y-m-d H:i:s")
            );
           
            $this->db->where('id', $id);
            $query = $this->db->update('admin_menus', $data);
            return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('admin_menus')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('admin_menus');
            return $query;
        }

        public function checkExists($menu_name){
            $where = array('menu_name'=> $menu_name, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('admin_menus')
                    ->where($where)
                    ->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

	}
