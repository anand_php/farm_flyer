<?php
	class Wastage_model extends CI_Model {

		public function getAllWastageTypes(){
			$query = $this->db->select('*')
                    ->from('manage_resources_wastagetype')
                    ->where('is_deleted',0)
                    ->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function add(){

            $data =  array(
                'wastage_type_name' => $_POST['wastage_type_name'],
                'description' => $_POST['description'],
                'status_choice' => $_POST['status_choice'],
                'is_deleted' => 0,
                'created_date' => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('manage_resources_wastagetype', $data);
            return $query;
        }

        public function delete($id){
            $this->db->set('is_deleted', 1);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_wastagetype');
            return $query;
        }

        public function getWastageById($id){
            $query = $this->db->select('*')
                    ->from('manage_resources_wastagetype')
                    ->where('id', $id)
                    ->get()->row();

            return $query;
        }

        public function update(){
            $id = $_POST['id'];
            
            $data =  array(
                'wastage_type_name' => $_POST['wastage_type_name'],
                'description' => $_POST['description'],
                'status_choice' => $_POST['status_choice'],
                'is_deleted' => 0,
                'modified_date' => date("Y-m-d H:i:s"),
            );
            
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_wastagetype', $data);
            return $query;
        }

        public function checkActivate($id){

            $status = $this->db->select('status_choice')->from('manage_resources_wastagetype')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_wastagetype');
            return $query;
        }

        public function checkExists($wastage_type_name){
            $where = array('wastage_type_name'=>$wastage_type_name, 'is_deleted'=> 0);
            $query = $this->db->select('*')->from('manage_resources_wastagetype')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }
	}
