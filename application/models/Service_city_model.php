<?php
	class Service_city_model extends CI_Model {

		public function getAllCities(){
			$query = $this->db->select('service_city.*,service_state.state_id, service_state.state_name')
                    ->from('service_city')
                    ->join('service_state','service_state.state_id = service_city.state_id')
                    ->where('service_city.is_deleted',0)
                    ->order_by('service_city.id','DESC')
                    ->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function add(){
            $data =  array(
                'city_name'     => $_POST['city_name'],
                'state_id'      => $_POST['state_id'],
                'description'   => $_POST['description'],
                'status_choice' => $_POST['status_choice'],
                'is_deleted'    => 0,
                'created_date'  => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('service_city', $data);
            return $query;
        }

        public function delete($id){
            $this->db->set('is_deleted', 1);
            $this->db->where('id', $id);
            $query = $this->db->update('service_city');
            return $query;
        }

        public function getServiceCityById($id){
            $query = $this->db->select('service_city.*, service_state.state_id, service_state.state_name')
                    ->from('service_city')
                    ->join('service_state', 'service_state.state_id = service_city.id','left')
                    ->where('id', $id)
                    ->get()->row();

            return $query;
        }

        public function update(){
            $id = $_POST['id'];
            
            $data =  array(
                'city_name'     => $_POST['city_name'],
                'state_id'      => $_POST['state_id'],
                'description'   => $_POST['description'],
                'status_choice' => $_POST['status_choice'],
                'is_deleted'    => 0,
                'modified_date' => date("Y-m-d H:i:s"),
            );
            
            $this->db->where('id', $id);
            $query = $this->db->update('service_city', $data);
            return $query;
        }

        public function checkActivate($id){

            $status = $this->db->select('status_choice')->from('service_city')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('service_city');
            return $query;
        }

        public function checkExists($city_name){
            $where = array('city_name'=>$city_name, 'is_deleted'=> 0);
            $query = $this->db->select('*')->from('service_city')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }
	}
