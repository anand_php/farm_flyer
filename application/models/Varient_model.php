<?php
	class Varient_model extends CI_Model {

		public function getAllVarients(){
			$query = $this->db->select('manage_resources_variant.*,manage_resources_product.id as productId, manage_resources_product.product_name as product')
                    ->from('manage_resources_variant')
                    ->join('manage_resources_product','manage_resources_product.id = manage_resources_variant.product_name','left')
                    ->where('manage_resources_variant.is_deleted',0)
                    ->order_by('id','DESC')
                    ->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function getAllProducts(){
            $query = $this->db->select('*')
                    ->from('manage_resources_product')
                    ->where('is_deleted',0)
                    ->get();
            $data['count'] = $query->num_rows();
            if($data['count'] > 0){
                $data['result'] = $query->result();
            }
            return $data;
        }

        public function add(){
           
            $data =  array(
                'product_name'  => $_POST['product_name'],
                'varient_name'  => $_POST['variant_name'],
                'status_choice' => $_POST['status_choice'],
                'is_deleted'    => 0,
                'created_date'  => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('manage_resources_variant', $data);
            return $query;
        }

        public function delete($id){
            $this->db->set('is_deleted', 1);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_variant');
            return $query;
        }

        public function getVarientById($id){
            $query = $this->db->select('*')->from('manage_resources_variant')->where('id', $id)->get()->row();

            return $query;
        }

        public function update(){
            $id = $_POST['id'];
            // print_r($_POST); 
                $data =  array(
                    'product_name'  => $_POST['product_name'],
                    'varient_name'  => $_POST['varient_name'],
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted'    => 0,
                    'modified_date' => date("Y-m-d H:i:s"),
                );
            
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_variant', $data);
            // print_r($this->db->last_query()); die();
            return $query;
        }

        public function checkActivate($id){

            $status = $this->db->select('status_choice')->from('manage_resources_variant')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_variant');
            return $query;
        }

        public function checkExists($variant_name){
            $where = array('varient_name'=>$variant_name, 'is_deleted'=> 0);
            $query = $this->db->select('*')->from('manage_resources_variant')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }
	}
