<?php
	class Warehouse_model extends CI_Model {

		public function getAllWarehouses(){

			$query = $this->db->select('warehouse.*, service_city.id as cityId, service_city.city_name')
                    ->from('warehouse')
                    ->join('service_city','service_city.id = warehouse.city_id')
                    ->where('warehouse.is_deleted',0)
                    ->order_by('warehouse.id','DESC')
                    ->get()->result();
			// $data['count'] = $query->num_rows();
			// if($data['count'] > 0){
			// 	$data['result'] = $query->result();
			// }
			return $query;
		}

        public function getAllCities(){
            $query = $this->db->select('*')
                    ->from('service_city')
                    ->where('is_deleted',0)
                    ->get()->result();
            return $query;
        }
        
        public function add()
        {
            $add = explode(',', $_POST['map_id']);
            
            $data = array(
            	'warehouse_code'    => $_POST['warehouse_code'],
            	'warehouse_name'    => $_POST['warehouse_name'],
                'warehouse_address' => $_POST['warehouse_address'],
                'state_id'          => $_POST['state_id'],
                'city_id'           => $_POST['city_id'],
                'latitude'            => $add[0],
                'longitude'           => $add[1],
            	'status_choice'     => $_POST['status_choice'],
            	'is_deleted'        => 0,
                'created_date'      => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('warehouse',$data);
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('warehouse');
        	return $query;
        }

        public function getWarehouseById($id){
        	$this->db->select('warehouse.*,service_state.state_id, service_state.state_name,service_city.id, service_city.city_name');
        	$this->db->from('warehouse');
            $this->db->join('service_state','service_state.state_id = warehouse.state_id','left');
            $this->db->join('service_city','service_city.id = warehouse.city_id','left');
        	$this->db->where('warehouse.id', $id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){
        	$id = $_POST['id'];
            $add = explode(',', $_POST['map_id']);

            $data = array(
                'warehouse_code'    => $_POST['warehouse_code'],
                'warehouse_name'    => $_POST['warehouse_name'],
                'warehouse_address' => $_POST['warehouse_address'],
                'state_id'          => $_POST['state_id'],
                'city_id'           => $_POST['city_id'],
                'latitude'          => $add[0],
                'longitude'         => $add[1],
                'status_choice'     => $_POST['status_choice'],
                'is_deleted'        => 0,
                'modified_date' => date("Y-m-d H:i:s")
            );
        	
            $this->db->where('id', $id);
            $query = $this->db->update('warehouse', $data);
           
            return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('warehouse')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('warehouse');
            return $query;
        }

        public function checkExists($warehouse_code){
            $where = array('warehouse_code'=> $warehouse_code, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('warehouse')
                    ->where($where)
                    ->get()->row();
            
            if($query){
                return true;
            }else{
                return false;
            }
        }

        public function getCityByStateId($state_id){
            $query = $this->db->select('*')->from('service_city')->where('state_id', $state_id)->get()->result();
            if($query){
                return $query;
            }else{
                return false;
            }
        }

        //created by Pooja on 17_09_2019
        public function getParticularUserWarehouses(){
            $id = $this->session->userdata("logged_in")["id"];
            $query = $this->db->select('*')->from('subadmin_user')->where('id', $id)->get()->row();

            return $query;
        }

	}
