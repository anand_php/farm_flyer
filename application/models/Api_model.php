<?php
	class Api_model extends CI_Model {
		//Anand check 14_11_2019
		public function delete_all($tableName,$where)
	    {
	        $this->db->where($where);
	        $this->db->delete($tableName); 
	        return $this->db->affected_rows();
	    }
		public function selectMaxId($user_type)
		{
			$maxid = 0;
			$row = $this->db->query('SELECT MAX(OrderID) AS `OrderID` FROM `ff_buyorder`')->row();
			if ($row) 
			{
			    $maxid = $row->OrderID; 
			}
			return $maxid;
		}
		//Anand check 14_11_2019
		//Anand check 13_11_2019
		public function getAllCartItems($user_id,$company_id)
		{
			$this->db->select('ff_tempcart.*,ff_tempcartitem.*,ff_tempcartitem.ID as temp_cart_item_id');
			$this->db->from('ff_tempcartitem');
			$this->db->join('ff_tempcart','ff_tempcart.ID = ff_tempcartitem.CartID');
			$this->db->where(array('ff_tempcartitem.Company_id'=>$company_id));
			$query = $this->db->get();
			if($query != null){
				return $query->result();
			}else{
				return false;
			}
		}
		//Anand check 13_11_2019

		//Anand check 05_11_2019
		public function selectAllByWhere($table,$where)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get();
			if($query != null){
				return $query->result();
			}else{
				return false;
			}
		}
		public function selectRow($table)
		{
			$this->db->from($table);
			$query = $this->db->get();
			return $query->result();
		}
		//Anand check 05_11_2019
		//Anand check 06_11_2019
		public function insert_all($table,$data){
			$this->db->insert($table,$data);
    		return $this->db->insert_id();
		}
		public function selectRowByWhere($tableName,$where)
	    {
	       $this->db->from($tableName);
		   $this->db->where($where);
		   $result = $this->db->get()->row();
		   return $result;
	    }
	    public function update_all($table, $where, $data){
			$this->db->where($where);
			$query = $this->db->update($table, $data);
			return $query;
		}
		public function update_password($table, $where, $data){
			$this->db->where($where);
			$this->db->update($table, $data);
			if ($this->db->affected_rows() > 0) {
			    return true; // your code
			} else {
			    return false; // your code
			}
			//return $query;
		}
		//Anand check 06_11_2019

		public function getAllStates(){
			$this->db->select('*');
			$this->db->from('service_state');
			$this->db->where('is_deleted','0');
			$query = $this->db->get()->result_array();
			return $query;
		}

		//created by Pooja on 11_09_2019 to get cities by state id
		public function getAllCitiesByStateId($state_id){
			$this->db->select('*');
			$this->db->from('service_city');
			$this->db->where('state_id', $state_id);
			$query = $this->db->get()->result_array();
			return $query;
		}

		//created by Pooja on 11_09_2019 to check mobile exists or not
		public function checkMobileExists($mobile){
			$this->db->select('*');
			$this->db->from('otp_table');
			$this->db->where('mobile', $mobile);
			$query = $this->db->get()->row();

			return $query;
		}

		//created by Pooja on 11_09_2019 to insert record to the database
		public function insert($table,$data){
			$query = $this->db->insert($table, $data);
			return $query;
		}

		//created by Pooja on 11_09_2019 to select records from database
		public function select($table, $where){
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get()->row();
			return $query;
		}
		// Created By Ganesh 30/10/2019
		public function selectbuyerprofile($table, $where){
			$this->db->select('buyer_id,buyer_name,email,mobile,alternate_mobile,buyer_address,landmark');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get()->row();
			return $query;
		}
		// Created By Ganesh 30/10/2019
		public function selectsellerprofile($table, $where){
			$this->db->select('user_id,user_name,user_email,user_mobile,user_alt_mobile,user_state_id,user_city_id,user_town,user_landmark,user_address');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get()->row();
			return $query;
		}

		//Created By Ganesh 30/10/2019
		public function selectCategory($table, $where){
			$this->db->select('id,category_name');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get()->result();
			return $query;
		}

		public function selectProduct($table, $where){
			$this->db->select('id,product_name,product_sku_code,product_image,unit_fk_key,product_description as description,product_varient,product_specofication,"" as productprice');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get()->result();
			return $query;
		}
		public function getCategoryListById($table, $where){
			$this->db->select('id,category_name,description,category_image');
			$this->db->from($table);
			$this->db->where($where);
			$query = $this->db->get()->result();
			return $query;
		}
		

		//Created By Ganesh Gore 30/10/2019
		public function getProductDetailsById($table_name,$table_name2,$on,$where)
	    {
	        $this->db->select('manage_resources_product.id,manage_resources_product.product_name,manage_resources_product.product_image,manage_resources_product.unit_fk_key,manage_resources_unit.id as weightid, manage_resources_unit.unit_name as weight');
	        $this->db->from($table_name);
	        $this->db->join($table_name2,$on);
	        $this->db->where($where);
	        $result=$this->db->get();
	      /*  echo $this->db->last_query();
	        exit();*/
	        return $result->result();
	    }

		//created by Pooja on 11_09_2019 to update record
		public function update($table, $where, $data){
			$this->db->where($where);
			$query = $this->db->update($table, $data);
			
			return $query;
		}

		//created by Pooja on 13_09_2019
		public function checkToken($token){
			$this->db->select('*');
			$this->db->from('token_table');
			$this->db->where('token', $token);
			$query = $this->db->get()->row();
			if($query != ''){
				return true;
			}else{
				return false;
			}
		}

		//created by Pooja on 13_09_2019
		public function selectOrWhere($table, $where, $or_where){
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$this->db->or_where($or_where);
			$query = $this->db->get()->row();
			return $query;
		}
		//Anand check 05_11_2019
		public function getProductListByCategoryId($table,$table2,$joinOn,$where){
			$this->db->select('manage_resources_product.product_sku_code,manage_resources_product.product_name,manage_resources_category.category_name,manage_resources_product.product_description,manage_resources_product.product_specofication,manage_resources_product.unit_fk_key');
			$this->db->from($table);
			$this->db->join($table2,$joinOn);
			$this->db->where($where);
			$query = $this->db->get()->result();
			return $query;
		}
		//Anand check 05_11_2019
	}
