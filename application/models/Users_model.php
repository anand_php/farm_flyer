<?php
	class Users_model extends CI_Model {

		public function getAllUsers(){
			$query = $this->db->select('users.*,service_state.state_id, service_state.state_name, service_city.id, service_city.city_name')
                    ->from('users')
                    ->join('service_state','service_state.state_id = users.user_state_id')
                    ->join('service_city','service_city.id = users.user_city_id','left')
                    ->where('users.is_deleted','0')
                    ->order_by('users.user_id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        
        // public function add()
        // {
            
        //     $config['upload_path']       = 'uploads/subadmin';
        //     $config['allowed_types']     = 'gif|jpg|jpeg|png';
        //     $this->load->library('upload',$config);
        //     $this->upload->initialize($config);
        //     if ($this->upload->do_upload('admin_image'))
        //     {
            	
        //         $data['upload_data'] = $this->upload->data('file_name');
        //         $admin_image = $data['upload_data'];
        //     }
        //     else
        //     {
        //         $admin_image = '';
        //     }

        //     $data = array(
        //         'name'           => $_POST['name'],
        //         'email'          => $_POST['email'],
        //         'mobile'         => $_POST['mobile'],
        //         'gender'         => $_POST['gender'],
        //         'address'        => $_POST['address'],
        //         'username'       => $_POST['username'],
        //         'user_role'      => 2,
        //     	'password'       => base64_decode($_POST['password']),
        //     	'admin_image'    => $admin_image,
        //     	'status_choice'  => $_POST['status_choice'],
        //     	'is_deleted'     => 0,
        //         'created_date'   => date("Y-m-d H:i:s")
        //     );
        //     $query = $this->db->insert('subadmin_user',$data);
        //     return $query;
        // }

        public function delete($user_id)
        {
        	$this->db->set('is_deleted', '1');
        	$this->db->where('user_id', $user_id);
        	$query = $this->db->update('users');
        	return $query;
        }

        public function getUserById($user_id){
        	$this->db->select('users.*,service_state.state_id, ss.state_name AS business_state,service_state.state_name AS user_state, service_city.id, service_city.city_name AS user_city, sc.city_name AS business_city, user_business_information.business_name , user_business_information.business_state_id, user_business_information.business_city_id, user_business_information.business_town, user_business_information.business_address, user_business_information.business_pincode, user_business_information.bank_beneficiary_name, user_business_information.bank_name, user_business_information.bank_account_number, user_business_information.bank_ifsc');
        	$this->db->from('users');
            $this->db->join('user_business_information','user_business_information.user_id = users.user_id','left');
            $this->db->join('service_state','service_state.state_id = users.user_state_id','left');
            $this->db->join('service_state AS ss','ss.state_id = user_business_information.business_state_id');
            $this->db->join('service_city','service_city.id = users.user_city_id','left');
            $this->db->join('service_city AS sc','sc.id = user_business_information.business_city_id','left');
            
        	$this->db->where('users.user_id', $user_id);
        	$query = $this->db->get()->row();

            
        	return $query;
        }

        public function update(){
            
        	$user_id = $_POST['user_id'];

                $data = array(
                    'user_role' => $_POST['user_role'],
                    'user_name' => $_POST['user_name'],
                    'user_mobile' => $_POST['user_mobile'],
                    'user_alt_mobile' => $_POST['user_alt_mobile'],
                    'user_address' => $_POST['user_address'],
                    'user_state_id' => $_POST['user_state_id'],
                    'user_city_id' => $_POST['user_city_id'],
                    'user_town' => $_POST['user_town'],
                    'user_landmark' => $_POST['user_landmark'],
                    'user_pincode' => $_POST['user_pincode'],
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted' => '0',
                    'modified_date' => date("Y-m-d H:i:s")
                );
        	
            $this->db->where('user_id', $user_id);
            $query = $this->db->update('users', $data);
            if($query == 1){
                $data2 = array(
                    'user_id'              => $_POST['user_id'],
                    'business_name'        => $_POST['business_name'],
                    'business_state_id'    => $_POST['business_state_id'],
                    'business_city_id'     => $_POST['business_city_id'],
                    'business_town'        => $_POST['business_town'],
                    'business_address'     => $_POST['business_address'],
                    'business_pincode'     => $_POST['business_pincode'],
                    'bank_beneficiary_name'=> $_POST['bank_beneficiary_name'],
                    'bank_name'            => $_POST['bank_name'],
                    'bank_account_number'  => $_POST['bank_account_number'],
                    'bank_ifsc'            => $_POST['bank_ifsc'],
                );
                $this->db->insert('user_business_information', $data2);
            }
            return $query;
        }

        public function checkActivate($user_id){
            $status = $this->db->select('status_choice')->from('users')->where('user_id', $user_id)->get()->row();
            if($status->status_choice == '1'){
                $final_status = '0';
            }else{
                $final_status = '1';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('user_id', $user_id);
            $query = $this->db->update('users');
            return $query;
        }

        public function checkExists($username){
            $where = array('username'=> $username, 'is_deleted'=>'0');
            $query = $this->db->select('*')
                    ->from('users')
                    ->where($where)
                    ->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

        public function getCityByStateId($user_state_id){
            $query = $this->db->select('*')->from('service_city')->where('state_id', $user_state_id)->get()->result();
            if($query){
                return $query;
            }else{
                return false;
            }
        }

	}
