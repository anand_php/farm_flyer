<?php
	class Role_model extends CI_Model {

		public function getAllRoles(){
			$query = $this->db->select('*')
                    ->from('user_roles')
                    ->where('is_deleted','0')
                    ->order_by('role_id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function add(){
            $config['upload_path']       = 'uploads/roles';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('icon'))
            {
                
                $data['upload_data'] = $this->upload->data('file_name');
                $icon = $data['upload_data'];
            }
            else
            {
                $icon = '';
            }

            $data =  array(
                'name'         => $_POST['name'],
                'description'  => $_POST['description'],
                'icon'         => $icon,
                'status'       => $_POST['status_choice'],
                'is_deleted'   => '0',
                'created_date' => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('user_roles', $data);
            return $query;
        }

        public function delete($role_id){
            $this->db->set('is_deleted', '1');
            $this->db->where('role_id', $role_id);
            $query = $this->db->update('user_roles');
            return $query;
        }

        public function getRoleById($role_id){
            $query = $this->db->select('*')->from('user_roles')->where('role_id', $role_id)->get()->row();

            return $query;
        }

        public function update(){

            $role_id = $_POST['role_id'];
            
            if($_FILES['icon']['name'] == ''){ 
                $data =  array(
                    'name'          => $_POST['name'],
                    'description'   => $_POST['description'],
                    'status'        => $_POST['status_choice'],
                    'is_deleted'    => '0',
                    'modified_date' => date("Y-m-d H:i:s"),
                );
               
            }else{
                $config['upload_path']       = 'uploads/roles';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('icon'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $icon = $data['upload_data'];
                }
                else
                {
                    $icon = '';
                }

                $data =  array(
                    'name'          => $_POST['name'],
                    'description'   => $_POST['description'],
                    'status'        => $_POST['status_choice'],
                    'icon'          => $icon,
                    'is_deleted'    => '0',
                    'modified_date' => date("Y-m-d H:i:s"),
                );
            }
            
            $this->db->where('role_id', $role_id);
            $query = $this->db->update('user_roles', $data);
            return $query;
        }

        public function checkActivate($role_id){

            $status = $this->db->select('status')->from('user_roles')->where('role_id', $role_id)->get()->row();
            if($status->status == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status', $final_status);
            $this->db->where('role_id', $role_id);
            $query = $this->db->update('user_roles');
            return $query;
        }

        public function checkExists($category_name){
            $where = array('category_name'=>$category_name, 'is_deleted'=> 0);
            $query = $this->db->select('*')->from('user_roles')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }
	}
