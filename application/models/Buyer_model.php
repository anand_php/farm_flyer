<?php
	class Buyer_model extends CI_Model {
        //Anand check 13_11_2019
        public function getAccountManagerData($account_manager_id,$id)
        {
            $this->db->select('ff_companyusers.*,ff_comapnydetails.*,ff_userlogindetails.*');
            $this->db->from('ff_companyusers');
            $this->db->join('ff_comapnydetails','ff_comapnydetails.ID=ff_companyusers.Company_id');
            $this->db->join('ff_userlogindetails','ff_userlogindetails.UserID = ff_companyusers.User_id');
            $this->db->where(array('ff_companyusers.ID'=>$account_manager_id));
            $query=$this->db->get()->row();
            return $query;
        }
        
        public function getDispatchDepos($tableName)
        {
           $this->db->from($tableName);
           $query = $this->db->get();
            if($query != null)
                return $query->result();
            else
                return false;
        }
        public function getAccountManagers()
        {
            $query = $this->db->select('ff_companyusers.*, ff_usertyperole.*,ff_userlogindetails.*')
                    ->from('ff_companyusers')
                    ->join('ff_userlogindetails','ff_userlogindetails.UserID = ff_companyusers.User_id')
                    ->join('ff_usertyperole','ff_usertyperole.UserRoleID = ff_companyusers.UserRoleID')
                    ->where('ff_companyusers.UserRoleID',2)
                    ->where('ff_companyusers.UserRoleID !=',0)
                    ->get();
            
           if($query != null)
                return $query->result();
            else
                return false;
        }
        public function getAllCompanySellers()
        {
            $this->db->select('ff_userlogindetails.*, ff_comapnydetails.*,warehouse.warehouse_name,service_state.state_id, service_state.state_name, service_city.id, service_city.city_name');
            $this->db->from('ff_userlogindetails');
            $this->db->join('ff_comapnydetails','ff_comapnydetails.CompanyMainAccountID = ff_userlogindetails.UserID');

            $this->db->join('warehouse','warehouse.id = ff_comapnydetails.warehouse_id');


            $this->db->join('service_state','service_state.state_id = ff_comapnydetails.State');
            $this->db->join('service_city','service_city.id = ff_comapnydetails.City');
            $this->db->where('ff_userlogindetails.UserType',2);
            $this->db->order_by('ff_userlogindetails.Created_on','DESC');

            $query=$this->db->get();

            if($query != null)
                return $query->result();
            else
                return false;
        }
        public function getWarehouse($tableName)
        {
           $this->db->from($tableName);
           $query = $this->db->get();
            if($query != null)
                return $query->result();
            else
                return false;
        }
        public function getWarehouseData($warehouse_id,$id)
        {
            $this->db->select('warehouse.*,ff_comapnydetails.*');
            $this->db->from('warehouse');
            $this->db->join('ff_comapnydetails','ff_comapnydetails.warehouse_id=warehouse.id');
            $this->db->where(array('warehouse.id'=>$warehouse_id));
            $query=$this->db->get()->row();
            return $query;
        }
        

		public function getAllBuyers()
        {
			$query = $this->db->select('buyers.*, service_state.state_id, service_state.state_name, service_city.id, service_city.city_name')
                    ->from('buyers')
                    ->join('service_state','service_state.state_id = buyers.state_id','left')
                    ->join('service_city','service_city.id = buyers.city_id','left')
                    ->where('buyers.is_deleted','0')
                    ->order_by('buyer_id','DESC')
                    ->get();
            
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        public function selectRowByWhere($tableName,$where)
        {
           $this->db->from($tableName);
           $this->db->where($where);
           $result = $this->db->get()->row();
           return $result;
        }
      
        public function getAllCompanyBuyers()
        {
            $this->db->select('ff_userlogindetails.*, ff_comapnydetails.*,dispatch_depot.dispatch_depot_code,dispatch_depot.dispatch_depot_name,service_state.state_id, service_state.state_name, service_city.id, service_city.city_name');
            $this->db->from('ff_userlogindetails');
            $this->db->join('ff_comapnydetails','ff_comapnydetails.CompanyMainAccountID = ff_userlogindetails.UserID');
            $this->db->join('dispatch_depot','dispatch_depot.id = ff_comapnydetails.dispatch_depot_id');

            $this->db->join('service_state','service_state.state_id = ff_comapnydetails.State');
            $this->db->join('service_city','service_city.id = ff_comapnydetails.City');
            $this->db->where('ff_userlogindetails.UserType',1);
            $this->db->order_by('ff_userlogindetails.Created_on','DESC');

            $query=$this->db->get();

            if($query != null)
                return $query->result();
            else
                return false;
        }
        public function getBuyerRecord($id)
        {
            $this->db->select('ff_userlogindetails.*, ff_comapnydetails.*,dispatch_depot.dispatch_depot_code,dispatch_depot.dispatch_depot_name,service_state.state_id, service_state.state_name, service_city.id, service_city.city_name');
            $this->db->from('ff_comapnydetails');
            $this->db->join('ff_userlogindetails','ff_comapnydetails.CompanyMainAccountID = ff_userlogindetails.UserID');
            $this->db->join('dispatch_depot','dispatch_depot.id = ff_comapnydetails.dispatch_depot_id');
            $this->db->join('service_state','service_state.state_id = ff_comapnydetails.State');
            $this->db->join('service_city','service_city.id = ff_comapnydetails.City');
            $this->db->where('ff_comapnydetails.ID',$id);
            $this->db->order_by('ff_userlogindetails.Created_on','DESC');

            $query=$this->db->get()->row();

            if($query != null)
                return $query;
            else
                return false;
        }
        public function update_all($table, $where, $data)
        {
            $this->db->where($where);
            $this->db->update($table, $data);
            if ($this->db->affected_rows() > 0) {
                return true; 
            } else {
                return false; 
            }
        }
        public function getAllUsersOfCompany($ID)
        {
            $this->db->select('ff_companyusers.*,ff_userlogindetails.*,ff_usertyperole.*');
            $this->db->from('ff_companyusers');
            $this->db->join('ff_userlogindetails','ff_companyusers.user_id = ff_userlogindetails.UserID');
            $this->db->join('ff_usertyperole','ff_usertyperole.UserRoleID = ff_companyusers.UserRoleID');
            $this->db->where('ff_companyusers.Company_id',$ID);
            $this->db->order_by('ff_userlogindetails.Created_on','DESC');

            $query=$this->db->get();

            if($query != null)
                return $query->result();
            else
                return false;

        }
        public function selectAllByWhere($table,$where)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where);
            $query = $this->db->get();
            if($query != null){
                return $query->result();
            }else{
                return false;
            }
        }
        //Anand check 13_11_2019
        public function add()
        {
            if($_POST['status_choice'] == true){
                $status_choice = '1';
            }else{
                $status_choice = '0';
            }

            $data = array(
                'buyer_role'       => $_POST['buyer_role'],
            	'buyer_name'       => $_POST['buyer_name'],
                'mobile'           => $_POST['mobile'],
                'alternate_mobile' => $_POST['alternate_mobile'],
                'email'            => $_POST['email'],
                'username'         => $_POST['username'],
                'password'         => $_POST['password'],
                'buyer_address'    => $_POST['buyer_address'],
                'state_id'         => $_POST['state_id'],
                'city_id'          => $_POST['city_id'],
                'town'             => $_POST['town'],
            	'pincode'          => $_POST['pincode'],
            	'status_choice'    => $status_choice,
            	'is_deleted'       => '0',
                'created_date'     => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('buyers',$data);
            return $query;
        }

        public function delete($buyer_id)
        {
        	$this->db->set('is_deleted', '1');
        	$this->db->where('buyer_id', $buyer_id);
        	$query = $this->db->update('buyers');
        	return $query;
        }

        public function getBuyerById($buyer_id){
        	$this->db->select('buyers.*, service_state.state_id, service_state.state_name, service_city.id, service_city.city_name');
        	$this->db->from('buyers');
            $this->db->join('service_state','service_state.state_id = buyers.state_id','left');
            $this->db->join('service_city','service_city.id = buyers.city_id','left');
        	$this->db->where('buyer_id', $buyer_id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){
        	$buyer_id = $_POST['buyer_id'];

            if($_POST['status_choice'] == true){
                $status_choice = '1';
            }else{
                $status_choice = '0';
            }

            $data = array(
                'buyer_role'       => $_POST['buyer_role'],
                'buyer_name'       => $_POST['buyer_name'],
                'mobile'           => $_POST['mobile'],
                'alternate_mobile' => $_POST['alternate_mobile'],
                'email'            => $_POST['email'],
                'username'         => $_POST['username'],
                'password'         => $_POST['password'],
                'buyer_address'    => $_POST['buyer_address'],
                'state_id'         => $_POST['state_id'],
                'city_id'          => $_POST['city_id'],
                'town'             => $_POST['town'],
                'pincode'          => $_POST['pincode'],
                'status_choice'    => $status_choice,
                'is_deleted'       => '0',
                'modified_date'    => date("Y-m-d H:i:s")
            );
        	
            $this->db->where('buyer_id', $buyer_id);
            $query = $this->db->update('buyers', $data);
            return $query;
        }

        public function checkActivate($buyer_id){
            $status = $this->db->select('status_choice')->from('buyers')->where('buyer_id', $buyer_id)->get()->row();
            if($status->status_choice == '1'){
                $final_status = '0';
            }else{
                $final_status = '1';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('buyer_id', $buyer_id);
            $query = $this->db->update('buyers');
            return $query;
        }

        public function checkExists($username){
            $where = array('username'=> $username, 'is_deleted'=>'0');
            $query = $this->db->select('*')
                    ->from('buyers')
                    ->where($where)
                    ->get()->row();
                    
            if($query){
                return true;
            }else{
                return false;
            }
        }

        public function getCityByStateId($state_id){
            $query = $this->db->select('*')->from('service_city')->where('state_id', $state_id)->get()->result();
            if($query){
                return $query;
            }else{
                return false;
            }
        }

	}
