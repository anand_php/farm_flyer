<?php
    class Subcategory_model extends CI_Model {

        public function getAllSubcategories(){
            $query = $this->db->select('manage_resources_subcategory.*, manage_resources_category.id as cat_id, manage_resources_category.category_name')
                    ->from('manage_resources_subcategory')
                    ->join('manage_resources_category','manage_resources_category.id = manage_resources_subcategory.category_fk_id','left')
                    ->where('manage_resources_subcategory.is_deleted',0)
                    ->order_by('manage_resources_subcategory.id','DESC')
                    ->get();
            $data['count'] = $query->num_rows();
            if($data['count'] > 0){
                $data['result'] = $query->result();
            }
            return $data;
        }

        public function add(){
            $config['upload_path']       = 'uploads/subcategory';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('sub_cat_Image'))
            {
                
                $data['upload_data'] = $this->upload->data('file_name');
                $sub_cat_Image = $data['upload_data'];
            }
            else
            {
                $sub_cat_Image = '';
            }

            $data =  array(
                'sub_cat_name' => $_POST['sub_cat_name'],
                'sub_cat_description' => $_POST['sub_cat_description'],
                'sub_cat_Image' => $sub_cat_Image,
                'status_choice' => $_POST['status_choice'],
                'category_fk_id' => $_POST['category_fk_id'],
                'is_deleted' => 0,
                'created_date' => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('manage_resources_subcategory', $data);
            return $query;
        }

        public function delete($id){
            $this->db->set('is_deleted', 1);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_subcategory');
            return $query;
        }

        public function getSubcategoryById($id){
            $query = $this->db->select('*')->from('manage_resources_subcategory')->where('id', $id)->get()->row();
            
            return $query;
        }

        public function update(){
            $id = $_POST['id'];

            if($_FILES['sub_cat_Image']['name'] == ''){
                $data =  array(
                'sub_cat_name' => $_POST['sub_cat_name'],
                'sub_cat_description' => $_POST['sub_cat_description'],
                // 'sub_cat_Image' => $sub_cat_Image,
                'status_choice' => $_POST['status_choice'],
                'category_fk_id' => $_POST['category_fk_id'],
                'is_deleted' => 0,
                'modified_date' => date("Y-m-d H:i:s"),
            );

            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_subcategory', $data);
            return $query;
            }else{
                $config['upload_path']       = 'uploads/subcategory';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('sub_cat_Image'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $sub_cat_Image = $data['upload_data'];
                }
                else
                {
                    $sub_cat_Image = '';
                }
            
                $data =  array(
                    'sub_cat_name' => $_POST['sub_cat_name'],
                    'sub_cat_description' => $_POST['sub_cat_description'],
                    'sub_cat_Image' => $sub_cat_Image,
                    'status_choice' => $_POST['status_choice'],
                    'category_fk_id' => $_POST['category_fk_id'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s"),
                );

                $this->db->where('id', $id);
                $query = $this->db->update('manage_resources_subcategory', $data);
                return $query;
            }
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('manage_resources_subcategory')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_subcategory');
            return $query;
        }

        public function checkExists($sub_cat_name){
            $where = array('sub_cat_name'=>$sub_cat_name, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('manage_resources_subcategory')
                    ->where($where)
                    ->get()->row(); 
            
            if($query){
                return true;
            }else{
                return false;
            }
        }
    }
