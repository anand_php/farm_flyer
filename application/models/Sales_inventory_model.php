<?php
	class Sales_inventory_model extends CI_Model {

		public function getAllSalesInventory(){
			$query = $this->db->select('sales_inventory.*, manage_resources_product.id as productId, manage_resources_product.product_name, users.user_id, users.user_name, manage_resources_unit.unit_name, manage_resources_unit.base_unit_name, manage_resources_unit.unit_ratio ')
                    ->from('sales_inventory')
                    ->join('manage_resources_product','manage_resources_product.id = sales_inventory.product_id','left')
                    ->join('users','users.user_id = sales_inventory.seller_id','left')
                    ->join('manage_resources_unit','manage_resources_unit.id = sales_inventory.unit_id','left')
                    // ->where('is_deleted',0)
                    ->order_by('sale_inventory_id','DESC')
                    ->get();

			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        
        public function add()
        {
        	if($_POST['status_choice'] == true){
                $status_choice = '1';
            }else{
                $status_choice = '0';
            }

            $data = array(
                'seller_id'        => $_POST['seller_id'],
                'category_id'      => $_POST['category_id'],
                'subcategory_id'   => $_POST['subcategory_id'],
                'product_id'       => $_POST['product_id'],
            	'product_varient'  => $_POST['product_varient'],
                'product_quantity' => $_POST['product_quantity'],
                'unit_id'          => $_POST['unit_id'],
            	'order_type'       => $_POST['order_type'],
            	'status_choice'    => $status_choice,
            	'is_deleted'       => '0',
                'created_date'     => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('sales_inventory',$data);
            return $query;
        }

        // public function delete($id)
        // {
        // 	$this->db->set('is_deleted', 1);
        // 	$this->db->where('id', $id);
        // 	$query = $this->db->update('sales_inventory');
        // 	return $query;
        // }

        public function getSaleInventoryById($sale_inventory_id){
        	$this->db->select('sales_inventory.*, manage_resources_product.id as productId, manage_resources_product.product_name, manage_resources_unit.id as unitId, manage_resources_unit.unit_name, manage_resources_category.id as catID, manage_resources_category.category_name, manage_resources_subcategory.id, manage_resources_subcategory.sub_cat_name, users.user_id, users.user_name');
        	$this->db->from('sales_inventory');
            $this->db->join('manage_resources_product','manage_resources_product.id = sales_inventory.product_id','left');
            $this->db->join('manage_resources_unit','manage_resources_unit.id = sales_inventory.unit_id','left');
            $this->db->join('manage_resources_category','manage_resources_category.id = sales_inventory.category_id','left');
            $this->db->join('manage_resources_subcategory','manage_resources_subcategory.id = sales_inventory.subcategory_id','left');
            $this->db->join('users','users.user_id = sales_inventory.seller_id','left');
        	$this->db->where('sale_inventory_id', $sale_inventory_id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){
        	$sale_inventory_id = $_POST['sale_inventory_id'];

            if($_POST['status_choice'] == 'true'){
                $status_choice = '1';
            }else{
                $status_choice = '0';
            }
            
            $data = array(
                'seller_id'        => $_POST['seller_id'],
                'category_id'      => $_POST['category_id'],
                'subcategory_id'   => $_POST['subcategory_id'],
                'product_id'       => $_POST['product_id'],
                'product_varient'  => $_POST['product_varient'],
                'product_quantity' => $_POST['product_quantity'],
                'unit_id'          => $_POST['unit_id'],
                'order_type'       => $_POST['order_type'],
                'status_choice'    => $status_choice,
                'is_deleted'       => '0',
                'created_date'     => date("Y-m-d H:i:s")
            );
        	
            $this->db->where('sale_inventory_id', $sale_inventory_id);
            $query = $this->db->update('sales_inventory', $data);
            return $query;
        }

        public function checkActivate($sale_inventory_id){
            $status = $this->db->select('status_choice')->from('sales_inventory')->where('sale_inventory_id', $sale_inventory_id)->get()->row();
            if($status->status_choice == '1'){
                $final_status = '0';
            }else{
                $final_status = '1';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('sale_inventory_id', $sale_inventory_id);
            $query = $this->db->update('sales_inventory');
            return $query;
        }

        public function getProductsBySubcategory($subcategory_id){
            $query = $this->db->select('*')->from('manage_resources_product')->where('sub_category_fk_id', $subcategory_id)->get()->result();
            return $query;
        }

        public function getVarientsByProductId($product_id){
            $query = $this->db->select('*')->from('manage_resources_product')->where('id', $product_id)->get()->result();
            return $query;
        }

        //created by Pooja on 11_09_2019
        public function getUnitAsBaseUnit($unit_id){
            $this->db->select('*');
            $this->db->from('manage_resources_unit');
            $this->db->where('base_unit_name', $unit_id);
            $query = $this->db->get()->row();
            return $query;
        }

	}
