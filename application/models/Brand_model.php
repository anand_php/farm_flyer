<?php
	class Brand_model extends CI_Model {

		public function getAllBrands(){
			$query = $this->db->select('*')->from('manage_resources_brand')->where('is_deleted',0)->order_by('id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        
        public function add()
        {
        	
            $config['upload_path']       = 'uploads/brand';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('brand_image'))
            {
            	
                $data['upload_data'] = $this->upload->data('file_name');
                $brand_image = $data['upload_data'];
            }
            else
            {
                $brand_image = '';
            }

            $data = array(
            	'brand_name' => $_POST['brand_name'],
            	'brand_image' => $brand_image,
            	'status_choice' => $_POST['status_choice'],
            	'is_deleted' => 0,
                'created_date' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('manage_resources_brand',$data);
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('manage_resources_brand');
        	return $query;
        }

        public function getBrandById($id){
        	$this->db->select('*');
        	$this->db->from('manage_resources_brand');
        	$this->db->where('id', $id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){
        	$id = $_POST['id'];

            if($_FILES['brand_image']['name'] == ''){ 
                $data = array(
                    'brand_name' => $_POST['brand_name'],
                    // 'brand_image' => $brand_image,
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );
            }else{
                $config['upload_path']       = 'uploads/brand';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('brand_image'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $brand_image = $data['upload_data'];
                }
                else
                {
                    $brand_image = '';
                }

                $data = array(
                    'brand_name' => $_POST['brand_name'],
                    'brand_image' => $brand_image,
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );

            }
        	
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_brand', $data);
            return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('manage_resources_brand')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_brand');
            return $query;
        }

        public function checkExists($brand_name){
            $where = array('brand_name'=> $brand_name, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('manage_resources_brand')
                    ->where($where)
                    ->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

	}
