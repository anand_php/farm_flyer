<?php
	class Permissions_model extends CI_Model {

        // public function getAllPermissions(){
        //     $this->db->select('auth_user_user_permissions.*, subadmin_user.id as subadminId, subadmin_user.name as subadminName,admin_submenus.id as submenuId, admin_submenus.submenu_name,admin_menus.id as menuId, admin_menus.menu_name');
        //     $this->db->from('auth_user_user_permissions');
        //     $this->db->join('subadmin_user','subadmin_user.id = auth_user_user_permissions.subadmin_id');
            // $this->db->join('admin_submenus','admin_submenus.id = auth_user_user_permissions.submenu_id');
            // $this->db->join('admin_menus','admin_menus.id = auth_user_user_permissions.menu_id');
        //     $this->db->where('auth_user_user_permissions.is_deleted',0);
        //     $query = $this->db->get();
        //     $data['count'] = $query->num_rows();
        //     if($data['count'] > 0){
        //         $data['result'] = $query->result();
        //     }
        //     return $data;
        // }

        public function getAllPermissions(){
            $this->db->select('auth_user_user_permissions.*, user_roles.name as roleName, user_roles.icon, admin_submenus.id as submenuId, admin_submenus.submenu_name,admin_menus.id as menuId, admin_menus.menu_name, "" AS menus, "" AS submenues');
            $this->db->from('auth_user_user_permissions');
            $this->db->join('user_roles','user_roles.role_id = auth_user_user_permissions.role_id','left');
            $this->db->join('admin_submenus','admin_submenus.id = auth_user_user_permissions.submenu_id');
            $this->db->join('admin_menus','admin_menus.id = auth_user_user_permissions.menu_id');
            $this->db->where('auth_user_user_permissions.is_deleted','0');
            $query = $this->db->get();
            $data['count'] = $query->num_rows();
            if($data['count'] > 0){
                $data['result'] = $query->result();
            }
            return $data;
        }

		public function getAllAdmins(){

			$query = $this->db->select('id,name, admin_menus_id, email, mobile,gender, password, address, admin_image, admin_menus_id, admin_submenus_id,status_choice, "" as menus, "" as submenus')
                    ->from('subadmin_user')
                    ->where('is_deleted',0)
                    // ->where('admin_menus_id !=','')
                    ->order_by('id','DESC')
                    ->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function getMenuById($id){
            $query = $this->db->select('*')->from('admin_menus')->where('id',$id)->get()->result();
            return $query;
        }

        public function getSubmenuById($id){
            $query = $this->db->select('*')->from('admin_submenus')->where('id',$id)->get()->row();
            return $query;
        }

        public function getSubMenus($menu_id){
            $query = $this->db->select('*')
                    ->from('admin_submenus')
                    ->where('is_deleted',0)
                    ->where('menu_id',$menu_id)
                    ->order_by('id','DESC')
                    ->get()->result();
           
            if($query){
                return $query;
            }else{
                return false;
            }
        }
        
        public function add(){
           
            // comma in the array 
            // print_r($_POST['submenu_id'][0]); die();
            // if($_POST['submenu_id'][0] != ''){
            //     echo"<pre>"; print_r($_POST['submenu_id']); die();
            // }else{
            //     echo"submenu blanks"; die();
            // }
            $menus = implode(', ', $_POST['menues']); 
            $submenus = implode(', ', $_POST['submenu_id']); 
            
            $permissions = array(
                'role_id'       => $_POST['role_id'],
                'menu_id'       => $menus,
                'submenu_id'    => $submenus,
                'status_choice' => $_POST['status_choice'],
                'is_deleted'    => 0,
                'created_date'  => date("Y-m-d H:i:s")
            );
            // $this->db->where('id', $_POST['id']);
            $query = $this->db->insert('auth_user_user_permissions',$permissions);
           
            return $query;
        }

        //function to update role permissions
        public function update(){
            
            $menus = implode(', ', $_POST['menues']); 
            $submenus = implode(', ', $_POST['submenu_id']); 
            
            $permissions = array(
                'role_id'       => $_POST['role_id'],
                'menu_id'       => $menus,
                'submenu_id'    => $submenus,
                'status_choice' => $_POST['status_choice'],
                'is_deleted'    => 0,
                'modified_date' => date("Y-m-d H:i:s")
            );
            $this->db->where('id', $_POST['id']);
            $query = $this->db->update('auth_user_user_permissions', $permissions);
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted',1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('auth_user_user_permissions');
        	return $query;
        }

        public function getPermissionById($id){
        	$this->db->select('auth_user_user_permissions.*, user_roles.name, "" AS menus, "" AS submenues');
        	$this->db->from('auth_user_user_permissions');
            $this->db->join('user_roles','user_roles.role_id = auth_user_user_permissions.role_id');
        	$this->db->where('auth_user_user_permissions.id', $id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('auth_user_user_permissions')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('auth_user_user_permissions');
            return $query;
        }

        // public function checkExists($username){
        //     $where = array('username'=> $username, 'is_deleted'=>0);
        //     $query = $this->db->select('*')
        //             ->from('subadmin_user')
        //             ->where($where)
        //             ->get()->row();
        //     if($query){
        //         return true;
        //     }else{
        //         return false;
        //     }
        // }

        //created by Pooja on 13_09_2019 to get all roles 
        public function getAllRoles(){
            $this->db->select('*');
            $this->db->from('user_roles');
            $this->db->where('is_deleted','0');
            $query = $this->db->get()->result();
           
            return $query;
        }

        //created by Pooj aon 13_09_2019 to get all menus
        public function getAllMenues(){
            $this->db->select('id, menu_name, status_choice, is_deleted, created_date, modified_date, "" AS submenu, "" AS submenu_id ');
            $this->db->from('admin_menus');
            $this->db->where('is_deleted',0);
            $query = $this->db->get()->result();
            return $query;
        }

        //created by Pooja on 13_09_2019
        public function getSubMenusById($menu_id){
            $this->db->select('*');
            $this->db->from('admin_submenus');
            $this->db->where('menu_id', $menu_id);
            $query = $this->db->get()->result();
            return $query;
        }

	}
