<?php
	class Sms_model extends CI_Model {

		public function getAllSmsDetails(){
			$query = $this->db->select('*')
                    ->from('sms')->where('is_deleted','0')
                    ->order_by('sms_id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function add(){
            $data =  array(
                'username' => $_POST['username'],
                'password'      => $_POST['password'],
                'sms_key'        => $_POST['sms_key'],
                'is_activate'      => $_POST['status_choice'],
                'is_deleted'         => '0',
                'created_date'       => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('sms', $data);
            return $query;
        }

        public function delete($sms_id){
            $this->db->set('is_deleted', '1');
            $this->db->where('sms_id', $sms_id);
            $query = $this->db->update('sms');
            return $query;
        }

       
        public function update(){
            $sms_id = $_POST['sms_id'];
            $data =  array(
                'username'      => $_POST['username'],
                'password'      => $_POST['password'],
                'sms_key'       => $_POST['sms_key'],
                'is_activate'   => $_POST['status_choice'],
                'is_deleted'    => '0',
                'modified_date' => date("Y-m-d H:i:s"),
            );
            
            $this->db->where('sms_id', $sms_id);
            $query = $this->db->update('sms', $data);
            return $query;
        }

        public function getSmsById($sms_id){
            $this->db->select('*');
            $this->db->from('sms');
            $this->db->where('sms_id', $sms_id);
            $query = $this->db->get()->row();
            return $query;
        }

        public function checkActivate($sms_id){

            $status = $this->db->select('is_activate')->from('sms')->where('sms_id', $sms_id)->get()->row();
            if($status->is_activate == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('is_activate', $final_status);
            $this->db->where('sms_id', $sms_id);
            $query = $this->db->update('sms');
            return $query;
        }
	}
