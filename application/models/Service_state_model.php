<?php
	class Service_state_model extends CI_Model {

		public function getAllStates(){
			$query = $this->db->select('*')
                    ->from('service_state')
                    ->where('is_deleted','0')
                    ->order_by('state_id','DESC')
                    ->get();
            
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function add(){
            if($_POST['status_choice'] == 'true'){
                $status = '1';
            }else{
                $status = '0';
            }
            
            $data =  array(
                'state_name'        => $_POST['state_name'],
                'country_id'        => 1,
                'state_description' => $_POST['state_description'],
                'status_choice'     => $status,
                'is_deleted'        => '0',
                'created_date'      => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('service_state', $data);
            
            return $query;
        }

        public function delete($state_id){
            $this->db->set('is_deleted', '1');
            $this->db->where('state_id', $state_id);
            $query = $this->db->update('service_state');
            return $query;
        }

        public function getServiceStateById($id){
            $query = $this->db->select('*')->from('service_state')->where('state_id', $id)->get()->row();

            return $query;
        }

        public function update(){
            $state_id = $_POST['state_id'];
            
            if($_POST['status_choice'] == 'true'){
                $status = '1';
            }else{
                $status = '0';
            }
            
            $data =  array(
                'state_name'        => $_POST['state_name'],
                'country_id'        => 1,
                'state_description' => $_POST['state_description'],
                'status_choice'     => $status,
                'is_deleted'        => '0',
                'created_date'      => date("Y-m-d H:i:s"),
            );
            
            $this->db->where('state_id', $state_id);
            $query = $this->db->update('service_state', $data);
            return $query;
        }

        public function checkActivate($id){

            $status = $this->db->select('status_choice')->from('service_state')->where('state_id', $id)->get()->row();
            if($status->status_choice == '1'){
                $final_status = '0';
            }else{
                $final_status = '1';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('state_id', $id);
            $query = $this->db->update('service_state');
            if($query == 1){
                return true;
            }else{
                return false;
            }
        }

        public function checkExists($state_name){
            $where = array('state_name'=>$state_name, 'is_deleted'=> '0');
            $query = $this->db->select('*')->from('service_state')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }
	}
