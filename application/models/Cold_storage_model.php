<?php
	class Cold_storage_model extends CI_Model {

		public function getAllStorages(){
			$query = $this->db->select('cold_storage.*, service_state.state_id, service_state.state_name, service_city.id as cityId, service_city.city_name, warehouse.id as warehouseId, warehouse.warehouse_name')
                    ->from('cold_storage')
                    ->join('service_state','service_state.state_id = cold_storage.state_id')
                    ->join('service_city','service_city.id = cold_storage.city_id')
                    ->join('warehouse','warehouse.id = cold_storage.warehouse_id')
                    ->where('cold_storage.is_deleted','0')
                    ->order_by('cold_storage.cold_storage_id','DESC')
                    ->get()->result();
			// $data['count'] = $query->num_rows();
			// if($data['count'] > 0){
			// 	$data['result'] = $query->result();
			// }
			return $query;
		}

        public function getAllCities(){
            $query = $this->db->select('*')
                    ->from('service_city')
                    ->where('is_deleted',0)
                    ->get()->result();
            return $query;
        }
        
        public function add()
        {
           
            $add = explode(',', $_POST['map_id']);
        	
            $data = array(
            	'cold_storage_code'   => $_POST['cold_storage_code'],
            	'name'                => $_POST['name'],
                'city_id'             => $_POST['city_id'],
                'state_id'            => $_POST['state_id'],
                'address'             => $_POST['address'],
                // 'map_id'              => $_POST['map_id'],
                'latitude'            => $add[0],
                'longitude'           => $add[1],
                'warehouse_id'        => $_POST['warehouse_id'],
            	'status_choice'       => $_POST['status_choice'],
            	'is_deleted'          => 0,
                'created_date'        => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('cold_storage',$data);
            return $query;
        }

        public function delete($cold_storage_id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('cold_storage_id', $cold_storage_id);
        	$query = $this->db->update('cold_storage');
        	return $query;
        }

        public function getStorageById($cold_storage_id){
        	$this->db->select('cold_storage.*, service_state.state_id, service_state.state_name, service_city.id, service_city.city_name, warehouse.id as warehouse_id, warehouse.warehouse_name');
        	$this->db->from('cold_storage');
            $this->db->join('service_state','service_state.state_id = cold_storage.state_id','left');
            $this->db->join('service_city','service_city.id = cold_storage.city_id','left');
            $this->db->join('warehouse','warehouse.id = cold_storage.warehouse_id','left');
        	$this->db->where('cold_storage_id', $cold_storage_id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){
        	$cold_storage_id = $_POST['cold_storage_id'];
            $add = explode(',', $_POST['map_id']);

            $data = array(
                'cold_storage_code'   => $_POST['cold_storage_code'],
                'name'                => $_POST['name'],
                'city_id'             => $_POST['city_id'],
                'state_id'            => $_POST['state_id'],
                'address'             => $_POST['address'],
                // 'map_id'              => $_POST['map_id'],
                'latitude'            => $add[0],
                'longitude'           => $add[1],
                'warehouse_id'        => $_POST['warehouse_id'],
                'status_choice'       => $_POST['status_choice'],
                'is_deleted'          => 0,
                'modified_date'       => date("Y-m-d H:i:s")
            );
        	
            $this->db->where('cold_storage_id', $cold_storage_id);
            $query = $this->db->update('cold_storage', $data);
            
            return $query;
        }

        public function checkActivate($cold_storage_id){
            $status = $this->db->select('status_choice')->from('cold_storage')->where('cold_storage_id', $cold_storage_id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('cold_storage_id', $cold_storage_id);
            $query = $this->db->update('cold_storage');
            return $query;
        }

        public function checkExists($dispatch_depot_code){
            $where = array('dispatch_depot_code'=> $dispatch_depot_code, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('cold_storage')
                    ->where($where)
                    ->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

        //created by Pooja to get particular subuser cold storage
        public function getParticularUserStorages(){
            $id = $this->session->userdata("logged_in")["id"];
            $query = $this->db->select('*')->from('subadmin_user')->where('id', $id)->get()->row();

            return $query;
        }

	}
