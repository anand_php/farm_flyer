<?php
	class Resource_product_model extends CI_Model {

		public function getAllResourceProducts(){
			$query = $this->db->select('manage_resources_product.*, manage_resources_category.id as catId, manage_resources_category.category_name, manage_resources_subcategory.id as subCatId, manage_resources_subcategory.sub_cat_name, manage_resources_brand.id as brandId, manage_resources_brand.brand_name, manage_resources_unit.id as unitID, manage_resources_unit.unit_name')
                    ->from('manage_resources_product')
                    ->join('manage_resources_category','manage_resources_category.id = manage_resources_product.product_category','left')
                    ->join('manage_resources_subcategory','manage_resources_subcategory.id = manage_resources_product.sub_category_fk_id','left')
                    ->join('manage_resources_brand','manage_resources_brand.id = manage_resources_product.brand_fk_id','left')
                    ->join('manage_resources_unit','manage_resources_unit.id = manage_resources_product.unit_fk_key','left')
                    ->where('manage_resources_product.is_deleted',0)
                    ->order_by('manage_resources_product.id','DESC')
                    ->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function add(){
            $product_varient = implode(",", $_POST['product_varient']);

            
            $config['upload_path']       = 'uploads/resource_product';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('product_image'))
            {
                
                $data['upload_data'] = $this->upload->data('file_name');
                $product_image = $data['upload_data'];
            }
            else
            {
                $product_image = '';
            }

            $data =  array(
                'product_category'      => $_POST['product_category'],
                'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
                'brand_fk_id'           => $_POST['brand_fk_id'],
                'product_name'          => $_POST['product_name'],
                'product_description'   => $_POST['product_description'],
                'product_specofication' => $_POST['product_specofication'],
                'unit_fk_key'           => $_POST['unit_fk_key'],
                'product_varient'       => $product_varient,
                'product_image'         => $product_image,
                'status_choice'         => $_POST['status_choice'],
                'is_deleted'            => 0,
                'created_date'          => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('manage_resources_product', $data);
            return $query;
        }

        public function delete($id){
            $this->db->set('is_deleted', 1);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_product');
            if($query == 1){
                return true;
            }else{
                return false;
            }
        }

        public function getResourceProductById($id){
            $query = $this->db->select('manage_resources_product.*, manage_resources_category.id as cat_id, manage_resources_category.category_name, manage_resources_subcategory.id as sub_cat_id, manage_resources_subcategory.sub_cat_name, manage_resources_brand.id as brandId, manage_resources_brand.brand_name, manage_resources_unit.id as unitID, manage_resources_unit.unit_name')
                    ->from('manage_resources_product')
                    ->join('manage_resources_category','manage_resources_category.id = manage_resources_product.product_category','left')
                    ->join('manage_resources_subcategory','manage_resources_subcategory.id = manage_resources_product.sub_category_fk_id','left')
                    ->join('manage_resources_brand','manage_resources_brand.id = manage_resources_product.brand_fk_id','left')
                    ->join('manage_resources_unit','manage_resources_unit.id = manage_resources_product.unit_fk_key','left')
                    ->where('manage_resources_product.id', $id)
                    ->get()
                    ->row();

            return $query;
        }

        public function update(){
            
            $id = $_POST['id']; 

            if(empty($_POST['product_varient'])){
                $product_varient = '';
            }else{
                $product_varient = implode(",", $_POST['product_varient']);
            }
            
            if($_FILES['product_image']['name'] == ''){ 
                if($_POST['sub_category_fk_id'] == ''){
                    $data =  array(
                        'product_category'      => $_POST['product_category'],
                        'brand_fk_id'           => $_POST['brand_fk_id'],
                        'product_name'          => $_POST['product_name'],
                        'product_description'   => $_POST['product_description'],
                        'product_specofication' => $_POST['product_specofication'],
                        'unit_fk_key'           => $_POST['unit_fk_key'],
                        'product_varient'       => $product_varient,
                        'status_choice'         => $_POST['status_choice'],
                        'is_deleted'            => 0,
                        'modified_date' => date("Y-m-d H:i:s"),
                    );
                }else{
                    $data =  array(
                        'product_category'      => $_POST['product_category'],
                        'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
                        'brand_fk_id'           => $_POST['brand_fk_id'],
                        'product_name'          => $_POST['product_name'],
                        'product_description'   => $_POST['product_description'],
                        'product_specofication' => $_POST['product_specofication'],
                        'unit_fk_key'           => $_POST['unit_fk_key'],
                        'product_varient'       => $product_varient,
                        'status_choice'         => $_POST['status_choice'],
                        'is_deleted'            => 0,
                        'modified_date' => date("Y-m-d H:i:s"),
                    );
                }
                
               
            }else{
                $config['upload_path']       = 'uploads/resource_product';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('product_image'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $product_image = $data['upload_data'];
                }
                else
                {
                    $product_image = '';
                }

                if($_POST['sub_category_fk_id'] == ''){
                    $data =  array(
                        'product_category'      => $_POST['product_category'],
                        // 'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
                        'brand_fk_id'           => $_POST['brand_fk_id'],
                        'product_name'          => $_POST['product_name'],
                        'product_description'   => $_POST['product_description'],
                        'product_specofication' => $_POST['product_specofication'],
                        'unit_fk_key'           => $_POST['unit_fk_key'],
                        'product_varient'       => $product_varient,
                        'product_image'         => $product_image,
                        'status_choice'         => $_POST['status_choice'],
                        'is_deleted'            => 0,
                        'modified_date' => date("Y-m-d H:i:s"),
                    );
                }else{
                    $data =  array(
                        'product_category'      => $_POST['product_category'],
                        'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
                        'brand_fk_id'           => $_POST['brand_fk_id'],
                        'product_name'          => $_POST['product_name'],
                        'product_description'   => $_POST['product_description'],
                        'product_specofication' => $_POST['product_specofication'],
                        'unit_fk_key'           => $_POST['unit_fk_key'],
                        'product_varient'       => $product_varient,
                        'product_image'         => $product_image,
                        'status_choice'         => $_POST['status_choice'],
                        'is_deleted'            => 0,
                        'modified_date' => date("Y-m-d H:i:s"),
                    );
                }

                
            }
            
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_product', $data);
            
            return $query;
        }

        public function checkActivate($id){

            $status = $this->db->select('status_choice')->from('manage_resources_product')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_product');
            return $query;
        }

        public function checkExists($category_name){
            $where = array('category_name'=>$category_name, 'is_deleted'=> 0);
            $query = $this->db->select('*')->from('manage_resources_product')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

        public function getSubcategoriesByCategory($product_category){
            $query = $this->db->select('*')->from('manage_resources_subcategory')->where('category_fk_id', $product_category)->get()->result();
            return $query;
        }
	}
