<?php
class Products_model extends CI_Model {
	public function getAllProducts(){
		$query = $this->db->select('manage_resources_product.*, manage_resources_category.id as catId, manage_resources_category.category_name, manage_resources_subcategory.id as subCatId, manage_resources_subcategory.sub_cat_name, manage_resources_brand.id as brandId, manage_resources_brand.brand_name, manage_resources_unit.id as unitID, manage_resources_unit.unit_name ,"" AS varient')
                ->from('manage_resources_product')
                ->join('manage_resources_category','manage_resources_category.id = manage_resources_product.product_category','left')
                ->join('manage_resources_subcategory','manage_resources_subcategory.id = manage_resources_product.sub_category_fk_id','left')
                ->join('manage_resources_brand','manage_resources_brand.id = manage_resources_product.brand_fk_id','left')
                ->join('manage_resources_unit','manage_resources_unit.id = manage_resources_product.unit_fk_key','left')
                // ->join('manage_resources_variant','manage_resources_variant.product_sku_code = manage_resources_product.product_sku_code','left')
                ->where('manage_resources_product.is_deleted',0)
                ->order_by('manage_resources_product.id','DESC')
                ->get();
		$data['count'] = $query->num_rows();
		if($data['count'] > 0){
			$data['result'] = $query->result();
		}
		return $data;
	}

    public function add(){
       
        $data = array();
        
        // Count total files
        $countfiles = count($_FILES['files']['name']);

        $status = json_decode($_POST['cover_image']);
        for($j = 0; $j<count($status); $j++){
            if($status[$j]->image_status == 'cover'){
                $cover_image = $status[$j]->image_name;
            }else{
                // Looping all files
                for($i=0;$i<$countfiles;$i++){

                    if(!empty($_FILES['files']['name'][$i])){

                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                        // Set preference
                        $config['upload_path'] = 'uploads/resource_product/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        // $config['max_size'] = '5000'; // max_size in kb
                        $config['file_name'] = $_FILES['files']['name'][$i];

                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            // Get data about the file
                            $uploadData = $this->upload->data();
                            $filename = $uploadData['file_name'];
                            // Initialize array
                            $data['filenames'][] = $filename;
                        }
                    }
                }
           }
        }
        
        // echo"<pre>"; print_r($admin_image); die();
        $images = implode(',', $data['filenames']);

        $data =  array(
            'product_sku_code'      => $_POST['product_sku_code'],
            'product_name'          => $_POST['product_name'],
            'product_description'   => $_POST['product_description'],
            'product_specofication' => $_POST['product_specofication'],
            'product_category'      => $_POST['product_category'],
            'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
            'brand_fk_id'           => $_POST['brand_fk_id'],
            'unit_fk_key'           => $_POST['unit_fk_key'],
            'selling_unit'          => $_POST['selling_unit'],
            'tax'                   => $_POST['tax'],
            'alert_quantity'        => $_POST['alert_quantity'],
            'product_image'         => $images,
            'cover_image'           => $cover_image,
            'is_deleted'            => 0,
            'status_choice'         => $_POST['status_choice'],
            'created_date'          => date("Y-m-d H:i:s"),
        );

        $query = $this->db->insert('manage_resources_product', $data);
        return $query;
    }

    public function delete($id){
        $this->db->set('is_deleted', 1);
        $this->db->where('id', $id);
        $query = $this->db->update('manage_resources_product');
        return $query;
    }

    public function getProductById($id){
        // $query = $this->db->select('manage_resources_product.*, manage_resources_category.id as cat_id, manage_resources_category.category_name, manage_resources_subcategory.id as sub_cat_id, manage_resources_subcategory.sub_cat_name, manage_resources_brand.id as brandId, manage_resources_brand.brand_name, manage_resources_unit.id as unitID, manage_resources_unit.unit_name')
        //         ->from('manage_resources_product')
        //         ->join('manage_resources_category','manage_resources_category.id = manage_resources_product.product_category','left')
        //         ->join('manage_resources_subcategory','manage_resources_subcategory.id = manage_resources_product.sub_category_fk_id','left')
        //         ->join('manage_resources_brand','manage_resources_brand.id = manage_resources_product.brand_fk_id','left')
        //         ->join('manage_resources_unit','manage_resources_unit.id = manage_resources_product.unit_fk_key','left')
        //         ->where('manage_resources_product.id', $id)
        //         ->get()
        //         ->row();

        // return $query;

        $this->db->select('manage_resources_product.*, manage_resources_category.category_name, subcat.category_name as subcategory_name, manage_resources_brand.brand_name, manage_resources_unit.unit_name, unit.unit_name as selling_unit_name, tax_rate.tax_name');
        $this->db->from('manage_resources_product');
        $this->db->join('manage_resources_category','manage_resources_category.id = manage_resources_product.product_category','left');
        $this->db->join('manage_resources_category subcat','subcat.parent_category_id = manage_resources_product.product_category','left');
        $this->db->join('manage_resources_brand','manage_resources_brand.id = manage_resources_product.brand_fk_id','left');
        $this->db->join('manage_resources_unit','manage_resources_unit.id = manage_resources_product.unit_fk_key','left');
        $this->db->join('manage_resources_unit unit','unit.base_unit_name = manage_resources_product.selling_unit','left');
        $this->db->join('tax_rate','tax_rate.tax_id = manage_resources_product.tax','left');
        $this->db->where('manage_resources_product.id', $id);
        $query = $this->db->get()->row();

        return $query;
    }

    public function update(){
        
        $id = $_POST['id']; 

        if(empty($_POST['product_varient'])){
            $product_varient = '';
        }else{
            $product_varient = implode(",", $_POST['product_varient']);
        }
        
        if($_FILES['product_image']['name'] == ''){ 
            if($_POST['sub_category_fk_id'] == ''){
                $data =  array(
                    'product_category'      => $_POST['product_category'],
                    'brand_fk_id'           => $_POST['brand_fk_id'],
                    'product_name'          => $_POST['product_name'],
                    'product_description'   => $_POST['product_description'],
                    'product_specofication' => $_POST['product_specofication'],
                    'unit_fk_key'           => $_POST['unit_fk_key'],
                    'product_varient'       => $product_varient,
                    'status_choice'         => $_POST['status_choice'],
                    'is_deleted'            => 0,
                    'modified_date' => date("Y-m-d H:i:s"),
                );
            }else{
                $data =  array(
                    'product_category'      => $_POST['product_category'],
                    'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
                    'brand_fk_id'           => $_POST['brand_fk_id'],
                    'product_name'          => $_POST['product_name'],
                    'product_description'   => $_POST['product_description'],
                    'product_specofication' => $_POST['product_specofication'],
                    'unit_fk_key'           => $_POST['unit_fk_key'],
                    'product_varient'       => $product_varient,
                    'status_choice'         => $_POST['status_choice'],
                    'is_deleted'            => 0,
                    'modified_date' => date("Y-m-d H:i:s"),
                );
            }
            
           
        }else{
            $config['upload_path']       = 'uploads/resource_product';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('product_image'))
            {
                
                $data['upload_data'] = $this->upload->data('file_name');
                $product_image = $data['upload_data'];
            }
            else
            {
                $product_image = '';
            }

            if($_POST['sub_category_fk_id'] == ''){
                $data =  array(
                    'product_category'      => $_POST['product_category'],
                    // 'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
                    'brand_fk_id'           => $_POST['brand_fk_id'],
                    'product_name'          => $_POST['product_name'],
                    'product_description'   => $_POST['product_description'],
                    'product_specofication' => $_POST['product_specofication'],
                    'unit_fk_key'           => $_POST['unit_fk_key'],
                    'product_varient'       => $product_varient,
                    'product_image'         => $product_image,
                    'status_choice'         => $_POST['status_choice'],
                    'is_deleted'            => 0,
                    'modified_date' => date("Y-m-d H:i:s"),
                );
            }else{
                $data =  array(
                    'product_category'      => $_POST['product_category'],
                    'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
                    'brand_fk_id'           => $_POST['brand_fk_id'],
                    'product_name'          => $_POST['product_name'],
                    'product_description'   => $_POST['product_description'],
                    'product_specofication' => $_POST['product_specofication'],
                    'unit_fk_key'           => $_POST['unit_fk_key'],
                    'product_varient'       => $product_varient,
                    'product_image'         => $product_image,
                    'status_choice'         => $_POST['status_choice'],
                    'is_deleted'            => 0,
                    'modified_date' => date("Y-m-d H:i:s"),
                );
            }

            
        }
        
        $this->db->where('id', $id);
        $query = $this->db->update('manage_resources_product', $data);
        
        return $query;
    }

    public function checkActivate($id){

        $status = $this->db->select('status_choice')->from('manage_resources_product')->where('id', $id)->get()->row();
        if($status->status_choice == 'true'){
            $final_status = 'false';
        }else{
            $final_status = 'true';
        }
        
        $this->db->set('status_choice', $final_status);
        $this->db->where('id', $id);
        $query = $this->db->update('manage_resources_product');
        return $query;
    }

    public function getAllTaxes(){
        $this->db->select('*');
        $this->db->from('tax_rate');
        $this->db->where('is_deleted',0);
        $query = $this->db->get();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }

    //created by Pooja on 26_09_2019
    public function getSellingUnits($unit_fk_key){
        $this->db->select('*');
        $this->db->from('manage_resources_unit');
        $this->db->where('base_unit_name', $unit_fk_key);
        $query = $this->db->get()->result();
        if(!empty($query)){
            return $query;
        }else{
            return false;
        }
    }

    //created by Pooja on 26_09_2019
    public function getSubcategoriesByCategory($product_category){
        $this->db->select('*');
        $this->db->from('manage_resources_category');
        $this->db->where('parent_category_id', $product_category);
        $query = $this->db->get()->result();

        if(!empty($query)){
            return $query;
        }else{
            return false;
        }
    }

    //created by Pooja on 26_09_2019
    public function getLastRecord(){
        $id = $this->db->select('id')->order_by('id','desc')->limit(1)->get('manage_resources_product')->row('id');
        return $id;
    }

    //created by Pooja on 26_09_2019
    public function checkExists($product_name){
        $this->db->select('*');
        $this->db->from('manage_resources_product');
        $this->db->where('product_name', $product_name);
        $query = $this->db->get()->row();
        if(!empty($query)){
            return true;
        }else{
            return false;
        }
    }

    //created by Pooja on 26_09_2019
    public function getProductName($product_sku_code){
        $this->db->select('*');
        $this->db->from('manage_resources_product');
        $this->db->where('product_sku_code', $product_sku_code);
        $query = $this->db->get()->row();
        return $query;
    }

    //created by Pooja on 26_09_2019
    public function getVarient(){
        $id = $this->db->select('id')->order_by('id','desc')->limit(1)->get('ff_prouduct_variant')->row('id');
        if(!empty($id)){
            return $id;
        }else{
            return false;
        }
        // return $id;
    }

    //created by Pooja on 26_09_2019
    public function addVarient(){

        // $product = $this->db->select('product_name')->from('manage_resources_product')->where('id', $_POST['product_id'])->get()->row();

        // $variant = $this->db->select('varient_name')->from('ff_verient')->where('varient_id', $_POST['varient_id'])->get()->row();

        // $product_variant_name = $product->product_name .'-'. $variant->varient_name;
        
        if(!empty($_POST['product_variant_name'])){
            $data= array(
                'product_id'           => $_POST['product_id'],
                'product_sku_code'     => $_POST['product_sku_code'],
                // 'varient_id'           => $_POST['varient_id'],
                'product_variant_name' => $_POST['product_variant_name'],
                'varient_sku_code'     => $_POST['varient_sku_code'],
                'status_choice'        => $_POST['status_choice'],
                'is_deleted'           => 0,
                'created_date'         => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('ff_prouduct_variant', $data);
            // print_r($this->db->last_query()); die();
            return $query;
        }
        
    }

    //created by Pooja on 26_09_2019
    public function getProductVarients($product_id){
        $query = $this->db->select('product_sku_code')->from('manage_resources_product')->where('id', $product_id)->get()->row();

        $this->db->select('*');
        $this->db->from('ff_prouduct_variant');
        $this->db->where('product_sku_code', $query->product_sku_code);
        $query2 = $this->db->get()->result();
        if(!empty($query2)){
            return $query2;
        }else{
            return false;
        }
    }

    //created by Pooja on 26_09_2019
    public function addPricing(){
        // echo"<pre>"; print_r($_POST); die();
        $data = array(
            'product_id'   => $_POST['product_id'],
            'warehouse_id' => $_POST['warehouse_id'],
            'varient_id'   => $_POST['varient_id'],
            'sale_price'   => $_POST['sale_price'],
            'buy_price'    => $_POST['buy_price'],
        );
        $query = $this->db->insert('ff_pricing', $data);
        return $query;
    }

    //created by Pooja on 27_09_2019
    public function updateProductBasic(){
        $id = $_POST['id'];

        $data = array();

        // Count total files
        $countfiles = count($_FILES['files']['name']);

        $status = json_decode($_POST['cover_image']);
        for($j = 0; $j<count($status); $j++){
            if($status[$j]->image_status == 'cover'){
                $cover_image = $status[$j]->image_name;
            }else{
                // Looping all files
                for($i=0;$i<$countfiles;$i++){

                    if(!empty($_FILES['files']['name'][$i])){

                        // Define new $_FILES array - $_FILES['file']
                        $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                        // Set preference
                        $config['upload_path'] = 'uploads/resource_product/'; 
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        // $config['max_size'] = '5000'; // max_size in kb
                        $config['file_name'] = $_FILES['files']['name'][$i];

                        //Load upload library
                        $this->load->library('upload',$config); 
                        // File upload
                        if($this->upload->do_upload('file')){
                            // Get data about the file
                            $uploadData = $this->upload->data();
                            $filename = $uploadData['file_name'];
                            // Initialize array
                            $data['filenames'][] = $filename;
                        }
                    }
                }
           }
        }
        $images = implode(',', $data['filenames']);
        
        $data = array(
            'product_sku_code'      => $_POST['product_sku_code'],
            'product_name'          => $_POST['product_name'],
            'product_description'   => $_POST['product_description'],
            'product_specofication' => $_POST['product_specofication'],
            'product_category'      => $_POST['product_category'],
            'sub_category_fk_id'    => $_POST['sub_category_fk_id'],
            'brand_fk_id'           => $_POST['brand_fk_id'],
            'unit_fk_key'           => $_POST['unit_fk_key'],
            'selling_unit'          => $_POST['selling_unit'],
            'tax'                   => $_POST['tax'],
            'alert_quantity'        => $_POST['alert_quantity'],
            'product_image'         => $images,
            'cover_image'           => $cover_image,
            'is_deleted'            => 0,
            'status_choice'         => $_POST['status_choice'],
            'modified_date'         => date("Y-m-d H:i:s")
        );
        $this->db->where('id', $id);
        $query = $this->db->update('manage_resources_product', $data);
        
        return $query;
    }

    //created by Pooja on 27_09_2019
    public function updateProduct2(){
        $id = $_POST['id'];
        $data = array(
            'unit_fk_key'    => $_POST['unit_fk_key'],
            'selling_unit'   => $_POST['selling_unit'],
            'tax'            => $_POST['tax'],
            'alert_quantity' => $_POST['alert_quantity'],
            'modified_date'         => date("Y-m-d H:i:s")
        );
        $this->db->where('id', $id);
        $query = $this->db->update('manage_resources_product', $data);
        return $query;
    }

    // public function getProductPricing($product_id){
    //     // $this->db->distinct();
    //     $this->db->select('product_pricing.*, warehouse.warehouse_code, warehouse.warehouse_name, warehouse.city_id, service_city.city_name');
    //     $this->db->from('product_pricing');
    //     $this->db->join('warehouse','warehouse.id = product_pricing.warehouse_id','left');
    //     $this->db->join('service_city','service_city.id = warehouse.city_id','left');
    //     $this->db->where('product_id', $product_id);
    //     $this->db->where('product_pricing.is_deleted',0);
    //     // $this->db->group_by('product_pricing.product_id'); 
    //     $this->db->order_by('product_pricing.warehouse_id', 'asc'); 
    //     $query = $this->db->get()->result();
    //     // echo"<pre>"; print_r($this->db->last_query()); die();
    //     return $query;
    // }

    //created by Pooja on 27_09_2019
    public function updatePricing(){
        
        $sell_price   = $_POST['sell_price'];
        $buy_price    = $_POST['buy_price'];
        $product_id   = $_POST['product_id'];
        $warehouse_id = $_POST['warehouse_id'];
        $varient_id   = $_POST['varient_id'];

        $where = array('product_id'=>$product_id, 'warehouse_id' => $warehouse_id,'varient_id'=>$varient_id);

        $query2 = $this->db->select('*')->from('ff_pricing')->where($where)->get()->row();

        if(!empty($query2)){
            if(!empty($buy_price)){
                if(!empty($sell_price)){
                    $data  = array(
                        'buy_price'  => $buy_price,
                        'sell_price' => $sell_price,
                        'is_deleted' => 0,
                        'updtaed_on' => date("Y-m-d H:i:s"),
                        'updtaed_by' => $this->session->userdata()["logged_in"]["id"]
                    );
                }else{
                    $data  = array(
                        'buy_price'  => $buy_price,
                        // 'sell_price' => $sell_price,
                        'is_deleted' => 0,
                        'updtaed_on' => date("Y-m-d H:i:s"),
                        'updtaed_by' => $this->session->userdata()["logged_in"]["id"]
                    );
                }
                
            }else{
                if(!empty($sell_price)){
                    $data  = array(
                        // 'buy_price'  => $buy_price,
                        'sell_price' => $sell_price,
                        'is_deleted' => 0,
                        'updtaed_on' => date("Y-m-d H:i:s"),
                        'updtaed_by' => $this->session->userdata()["logged_in"]["id"]
                    );
                }else{
                    $data  = array(
                        // 'buy_price'  => $buy_price,
                        // 'sell_price' => $sell_price,
                        'is_deleted' => 0,
                        'updtaed_on' => date("Y-m-d H:i:s"),
                        'updtaed_by' => $this->session->userdata()["logged_in"]["id"]
                    );
                }
            }
            
           
            $this->db->where($where);
            $query = $this->db->update('ff_pricing', $data);
            // print_r($this->db->last_query()); die();
        }else{
            if(!empty($buy_price)){
                if(!empty($sell_price)){
                    $data = array(
                        'buy_price'    => $_POST['buy_price'],
                        'sell_price'    => $_POST['sell_price'],
                        'product_id'   => $_POST['product_id'],
                        'warehouse_id' => $_POST['warehouse_id'],
                        'varient_id'   => $_POST['varient_id'],
                        'is_deleted'   => 0,
                        'created_on'   => date("Y-m-d H:i:s")
                    ); 
                }else{
                    $data = array(
                        'buy_price'    => $_POST['buy_price'],
                        // 'sell_price'    => $_POST['sell_price'],
                        'product_id'   => $_POST['product_id'],
                        'warehouse_id' => $_POST['warehouse_id'],
                        'varient_id'   => $_POST['varient_id'],
                        'is_deleted'   => 0,
                        'created_on'   => date("Y-m-d H:i:s")
                    ); 
                }
            }else{
                if(!empty($sell_price)){
                    $data = array(
                        // 'buy_price'    => $_POST['buy_price'],
                        'sell_price'    => $_POST['sell_price'],
                        'product_id'   => $_POST['product_id'],
                        'warehouse_id' => $_POST['warehouse_id'],
                        'varient_id'   => $_POST['varient_id'],
                        'is_deleted'   => 0,
                        'created_on'   => date("Y-m-d H:i:s")
                    ); 
                }else{
                    $data = array(
                        // 'buy_price'    => $_POST['buy_price'],
                        // 'sell_price'    => $_POST['sell_price'],
                        'product_id'   => $_POST['product_id'],
                        'warehouse_id' => $_POST['warehouse_id'],
                        'varient_id'   => $_POST['varient_id'],
                        'is_deleted'   => 0,
                        'created_on'   => date("Y-m-d H:i:s")
                    ); 
                }
            }
            $query = $this->db->insert('ff_pricing', $data);
            // $id = $this->db->
        }
        // for($i = 0; $i<count($_POST['price_id']); $i++){
        //     $price_id = $_POST['price_id'][$i];
        //     $data = array(
        //         'buy_price' => $_POST['buy_price'][$i],
        //         'sale_price' => $_POST['sale_price'][$i],
        //         'updtaed_on' => date("Y-m-d H:i:s"),
        //         'updtaed_by' => $this->session->userdata()["logged_in"]["id"]
        //     );
        //     $this->db->where('price_id',$price_id);
        //     $query = $this->db->update('product_pricing', $data);
        // }
        return $query;
        
    }

    //Created By Ganesh 24102019
    public function updateBuyPricing(){
        
        $buy_price    = $_POST['buy_price'];
        $product_id   = $_POST['product_id'];
        $warehouse_id = $_POST['warehouse_id'];
        $varient_id   = $_POST['varient_id'];

        $where = array('product_id'=>$product_id, 'warehouse_id' => $warehouse_id,'varient_id'=>$varient_id);

        $query2 = $this->db->select('*')->from('ff_pricing_buying')->where($where)->get()->row();

        if(!empty($query2)){
            
                
                $data  = array(
                    'buy_price'  => $buy_price,
                    'updated_on' => date("Y-m-d H:i:s"),
                    'updated_by' => $this->session->userdata()["logged_in"]["id"]
                );
                
            
           
            $this->db->where($where);
            $query = $this->db->update('ff_pricing_buying', $data);
            // print_r($this->db->last_query()); die();
        }else{
               
                $data = array(
                    'buy_price'    => $_POST['buy_price'],
                    'product_id'   => $_POST['product_id'],
                    'warehouse_id' => $_POST['warehouse_id'],
                    'varient_id'   => $_POST['varient_id'],
                    'updated_on' => date("Y-m-d H:i:s"),
                    'updated_by' => $this->session->userdata()["logged_in"]["id"]
                ); 
                  
            $query = $this->db->insert('ff_pricing_buying', $data);
            // $id = $this->db->
        }
        return $query;
    
    }

    //Created By Ganesh 24102019
    public function updateSellPricing(){
        
        $buy_price    = $_POST['sell_price'];
        $product_id   = $_POST['product_id'];
        $warehouse_id = $_POST['dispatch_depot_id'];
        $varient_id   = $_POST['varient_id'];

        $where = array('product_id'=>$product_id, 'dispatch_depot_id' => $warehouse_id,'varient_id'=>$varient_id);

        $query2 = $this->db->select('*')->from('ff_pricing_selling')->where($where)->get()->row();

        if(!empty($query2)){
            
                
                $data  = array(
                    'sell_price'  => $buy_price,
                    'updated_on' => date("Y-m-d H:i:s"),
                    'updated_by' => $this->session->userdata()["logged_in"]["id"]
                );
                
            
           
            $this->db->where($where);
            $query = $this->db->update('ff_pricing_selling', $data);

        }else{
               
                $data = array(
                    'sell_price'    => $_POST['sell_price'],
                    'product_id'   => $_POST['product_id'],
                    'dispatch_depot_id' => $_POST['dispatch_depot_id'],
                    'varient_id'   => $_POST['varient_id'],
                    'updated_on' => date("Y-m-d H:i:s"),
                    'updated_by' => $this->session->userdata()["logged_in"]["id"]
                ); 
                  
            $query = $this->db->insert('ff_pricing_selling', $data);
        }
        return $query;
        
    }
    //created by Pooja on 27_09_2019
    public function getProductVarients2($product_sku_code){
        $this->db->select('*');
        $this->db->from('ff_prouduct_variant');
        $this->db->where('product_sku_code',$product_sku_code);
        $query = $this->db->get()->result();
        return $query;
    }

    // //created by Pooja on 27_09_2019
    /*public function getAllProducts2(){
        $this->db->select('manage_resources_product.*, ff_prouduct_variant.id as varientId, ff_prouduct_variant.varient_sku_code');
        $this->db->from('manage_resources_product');
        $this->db->join('ff_prouduct_variant','ff_prouduct_variant.product_sku_code = manage_resources_product.product_sku_code','left');
        $this->db->where('manage_resources_product.is_deleted',0);
        $query = $this->db->get();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }*/

    //modified By Ganesh 24102019
    public function getAllProducts2(){
        $this->db->select('manage_resources_product.*, ff_prouduct_variant.id as varientId, ff_prouduct_variant.varient_sku_code,manage_resources_unit.id as unitid,manage_resources_unit.unit_name');
        $this->db->from('manage_resources_product');
        $this->db->join('ff_prouduct_variant','ff_prouduct_variant.product_sku_code = manage_resources_product.product_sku_code');
        $this->db->join('manage_resources_unit','manage_resources_unit.id = manage_resources_product.unit_fk_key');
        $this->db->where('manage_resources_product.is_deleted',0);
        $query = $this->db->get();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }

    //created by Pooja on 30_09_2019
    public function getProductPricing(){
        $this->db->select('ff_pricing.*, manage_resources_product.id as productId, manage_resources_product.product_sku_code as productSKU, manage_resources_product.product_name, warehouse.warehouse_code, warehouse.warehouse_address, ff_prouduct_variant.varient_sku_code, service_city.city_name');
        $this->db->from('ff_pricing');
        $this->db->join('manage_resources_product', 'manage_resources_product.id = ff_pricing.product_id','left');
        $this->db->join('warehouse','warehouse.id = ff_pricing.warehouse_id');
        $this->db->join('ff_prouduct_variant','ff_prouduct_variant.id = ff_pricing.varient_id','left');
        $this->db->join('service_city','service_city.id = warehouse.city_id');
        $this->db->where('ff_pricing.is_deleted',0);
        $query = $this->db->get();
        // print_r($this->db->last_query()); die();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }

    public function getSellProductPricing(){
        $this->db->select('ff_pricing_selling.*, manage_resources_product.id as productId, manage_resources_product.product_sku_code as productSKU, manage_resources_product.product_name, dispatch_depot.dispatch_depot_code, dispatch_depot.address, ff_prouduct_variant.varient_sku_code, service_city.city_name');
        $this->db->from('ff_pricing_selling');
        $this->db->join('manage_resources_product', 'manage_resources_product.id = ff_pricing_selling.product_id','left');
        $this->db->join('dispatch_depot','dispatch_depot.id = ff_pricing_selling.dispatch_depot_id');
        $this->db->join('ff_prouduct_variant','ff_prouduct_variant.id = ff_pricing_selling.varient_id','left');
        $this->db->join('service_city','service_city.id = dispatch_depot.city_id');
        $query = $this->db->get();
        // print_r($this->db->last_query()); die();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }

    //Created By Ganesh 2410019

    public function getBuyProductPricing(){

        $this->db->select('ff_pricing_buying.*, manage_resources_product.id as productId, manage_resources_product.product_sku_code as productSKU, manage_resources_product.product_name, warehouse.warehouse_code, warehouse.warehouse_address, ff_prouduct_variant.varient_sku_code, service_city.city_name');
        $this->db->from('ff_pricing_buying');
        $this->db->join('manage_resources_product', 'manage_resources_product.id = ff_pricing_buying.product_id','left');
        $this->db->join('warehouse','warehouse.id = ff_pricing_buying.warehouse_id');
        $this->db->join('ff_prouduct_variant','ff_prouduct_variant.id = ff_pricing_buying.varient_id','left');
        $this->db->join('service_city','service_city.id = warehouse.city_id');
        $query = $this->db->get();
        // print_r($this->db->last_query()); die();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    } 

    public function getBuyBaseUnit(){

        $this->db->select('ff_pricing_buying.product_id, manage_resources_product.id as productId, manage_resources_product.unit_fk_key as unitkey, manage_resources_unit.id as baseunitid, manage_resources_unit.unit_name');
        $this->db->from('ff_pricing_buying');
        $this->db->join('manage_resources_product', 'manage_resources_product.id = ff_pricing_buying.product_id','left');
        $this->db->join('manage_resources_unit','manage_resources_unit.id = manage_resources_product.unit_fk_key');
        $query = $this->db->get();
        // print_r($this->db->last_query()); die();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }

    //created by Pooja on 30_09_2019
    public function getAllWarehouses(){
        $this->db->select('warehouse.*, service_city.city_name');
        $this->db->from('warehouse');
        $this->db->where('warehouse.is_deleted',0);
        $this->db->join('service_city','service_city.id = warehouse.city_id');
        // $this->db->group_by('warehouse_id');
        $query = $this->db->get()->result();

        return $query;
    }

    public function getAllWarehousesByCity($city){
        $this->db->select('warehouse.*, service_city.city_name');
        $this->db->from('warehouse');
        if($city ==''){
        $this->db->where('warehouse.is_deleted','0');
        }
        else{
            $this->db->where('warehouse.is_deleted','0');
          $this->db->where('warehouse.city_id',$city);  
        }
        $this->db->join('service_city','service_city.id = warehouse.city_id');
        $query = $this->db->get()->result();
        return $query;
    }

    public function getAllDispatchdepo($table){
        $this->db->select('dispatch_depot.*, service_city.city_name');
        $this->db->from('dispatch_depot');
        $this->db->where('dispatch_depot.is_deleted',0);
        $this->db->join('service_city','service_city.id = dispatch_depot.city_id');
        $query = $this->db->get()->result();

        return $query;
    }
    public function getAllDispatchdepoByCity($city){
        $this->db->select('dispatch_depot.*, service_city.city_name');
        $this->db->from('dispatch_depot');
        if($city ==''){
        $this->db->where('dispatch_depot.is_deleted','0');
        }
        else{
            $this->db->where('dispatch_depot.is_deleted','0');
          $this->db->where('dispatch_depot.city_id',$city);  
        }
        $this->db->join('service_city','service_city.id = dispatch_depot.city_id');
        $query = $this->db->get()->result();

        return $query;
    }

    //created by Pooja on 30_09_2019
    public function getProductVarientsBySkuCode($product_sku_code){
        $this->db->select('*');
        $this->db->from('ff_prouduct_variant');
        $this->db->where('product_sku_code', $product_sku_code);
        $query = $this->db->get()->result();
        return $query;
    }

    //created by Pooja on 30_09_2019
    public function addInventory(){
        $data = array(
            'product_sku'   => $_POST['product_sku'],
            'varient_sku'   => $_POST['varient_sku'],
            'warehouse_id'  => $_POST['warehouse_id'],
            'quantity'      => $_POST['quantity'],
            'is_deleted'    => 0,
            'status_choice' => $_POST['status_choice'],
            'created_date'  => date("Y-m-d H:i:s")
        );
        $query = $this->db->insert('ff_inventory', $data);
        return $query;
    }

    //ccreated by Pooja on 30_09_2019
    public function getAllInventory(){
        $this->db->select('ff_inventory.*, manage_resources_product.product_name, warehouse.warehouse_code, warehouse.city_id, service_city.city_name');
        $this->db->from('ff_inventory');
        $this->db->join('manage_resources_product','manage_resources_product.product_sku_code = ff_inventory.product_sku');
        $this->db->join('warehouse','warehouse.id = ff_inventory.warehouse_id');
        $this->db->join('service_city','service_city.id = warehouse.city_id');
        $this->db->where('ff_inventory.is_deleted',0);
        $query = $this->db->get();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }

    //created by Pooja on 30_09_2019
    public function updateInaventory(){
        
        $quantity     = $_POST['quantity'];
        $product_sku   = $_POST['product_id'];
        $warehouse_id = $_POST['warehouse_id'];
        $varient_sku   = $_POST['varient_id']; 

        $where = array('product_sku'=>$product_sku, 'warehouse_id'=> $warehouse_id, 'varient_sku'=> $varient_sku);

        $query2 = $this->db->select('*')->from('ff_inventory')->where($where)->get()->row();
        if(!empty($query2)){
            $data = array(
                'quantity' => $quantity,
                'is_deleted' => 0,
                'modified_date' => date("Y-m-d H:i:s")
            );
            $this->db->where($where);
            $query = $this->db->update('ff_inventory',$data);
        }else{
            $data = array(
                'quantity' => $quantity,
                'product_sku' => $product_sku,
                'warehouse_id' => $warehouse_id,
                'varient_sku' => $varient_sku,
                'is_deleted' => 0,
                'created_date' => date("Y-m-d H:i:s"),
            );
            $query = $this->db->insert('ff_inventory',$data);
        }

        return $query;
    }

    //created by Pooja on 30_09_2019
    public function getProductSubCategory($product_category){
        $this->db->select('*');
        $this->db->from('manage_resources_category');
        $this->db->where('parent_category_id', $product_category);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get()->result();
        return $query;
    }

    // created by Pooja on 01_10_2019
    public function getInventoryByProductAndVarient($product_sku, $varient_sku){
        $where = array('product_sku'=> $product_sku, 'varient_sku'=>$varient_sku);
        $query = $this->db->select('*')->from('ff_inventory')->where($where)->get()->result();
        return $query;
    }

    //created by Pooja on 02_10_2019
    public function getAllVariants(){
        $this->db->select('*');
        $this->db->from('ff_verient');
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();
        $data['count'] = $query->num_rows();
        if($data['count'] > 0){
            $data['result'] = $query->result();
        }
        return $data;
    }

    public function getAllStates(){
            $query = $this->db->select('*')->from('service_state')->where('is_deleted','0')->order_by('state_id','ASEC')->get();
            //echo $query;
            $data['count'] = $query->num_rows();
            if($data['count'] > 0){
                $data['result'] = $query->result();
            }
            return $data;
        }
    public function getAllCities(){
            $query = $this->db->select('*')->from('service_city')->where('is_deleted','0')->order_by('id','ASEC')->get();
            //echo $query;
            $data['count'] = $query->num_rows();
            if($data['count'] > 0){
                $data['result'] = $query->result();
            }
            return $data;
        }
     public function getAllDDCities(){
            $query = $this->db->select('*')->from('service_city')->where('is_deleted','0')->order_by('id','ASEC')->get();
            //echo $query;
            $data['count'] = $query->num_rows();
            if($data['count'] > 0){
                $data['result'] = $query->result();
            }
            return $data;
        }
}
