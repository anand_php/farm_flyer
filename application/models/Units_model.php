<?php
	class Units_model extends CI_Model {

		public function getAllUnits(){
			$query = $this->db->select('*')->from('manage_resources_unit')->where('is_deleted',0)->order_by('id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        
        public function add()
        {
            
            $data = array(
                'unit_name'      => $_POST['unit_name'],
                'unit_ratio'     => $_POST['unit_ratio'],
            	'base_unit_name' => $_POST['base_unit_name'],
            	'status_choice'  => $_POST['status_choice'],
            	'is_deleted'     => 0,
                'created_date'   => date("Y-m-d H:i:s")
            );
            
            $query = $this->db->insert('manage_resources_unit',$data);
           
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('manage_resources_unit');
        	return $query;
        }

        public function getUnitById($id){
        	$this->db->select('manage_resources_unit.*');
        	$this->db->from('manage_resources_unit');
        	$this->db->where('manage_resources_unit.id', $id);
        	$query = $this->db->get()->row();

        	return $query;
        }

        public function update(){
        	$id = $_POST['id'];

            $data = array(
                'unit_name'      => $_POST['unit_name'],
                'base_unit_name' => $_POST['base_unit_name'],
                'unit_ratio'     => $_POST['unit_ratio'],
                'status_choice'  => $_POST['status_choice'],
                'is_deleted'     => 0,
                'modified_date'  => date("Y-m-d H:i:s")
            );

            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_unit', $data);
            return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('manage_resources_unit')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_unit');
            return $query;
        }

        public function getUnitNameByID($id){
            
            $query = $this->db->select('unit_name as base_unit')->from('manage_resources_unit')->where('id', $id)->get()->row();
            return $query;
        }

        public function checkExists($unit_name){
            $where = ['unit_name' => $unit_name, 'is_deleted' => 0];
            $query = $this->db->select('*')->from('manage_resources_unit')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
            
        }
	}
