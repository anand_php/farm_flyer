<?php
	class Category_model extends CI_Model {

		public function getAllCategories(){
			$query = $this->db->select('*')->from('manage_resources_category')->where('is_deleted',0)->order_by('id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}

        public function add(){
            $config['upload_path']       = 'uploads/category';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('category_image'))
            {
                
                $data['upload_data'] = $this->upload->data('file_name');
                $category_image = $data['upload_data'];
            }
            else
            {
                $category_image = '';
            }

            $data =  array(
                'parent_category_id' => $_POST['parent_category_id'],
                'category_name'      => $_POST['category_name'],
                'description'        => $_POST['description'],
                'category_image'     => $category_image,
                'status_choice'      => $_POST['status_choice'],
                'is_deleted'         => 0,
                'created_date'       => date("Y-m-d H:i:s"),
            );

            $query = $this->db->insert('manage_resources_category', $data);
            return $query;
        }

        public function delete($id){
            $this->db->set('is_deleted', 1);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_category');
            return $query;
        }

        public function getCategoryById($id){
            $query = $this->db->select('*')->from('manage_resources_category')->where('id', $id)->get()->row();

            return $query;
        }

        public function update(){
            $id = $_POST['id'];
            
            if($_FILES['category_image']['name'] == ''){ 
                $data =  array(
                    'parent_category_id' => $_POST['parent_category_id'],
                    'category_name'      => $_POST['category_name'],
                    'description'        => $_POST['description'],
                    'status_choice'      => $_POST['status_choice'],
                    'is_deleted'         => 0,
                    'modified_date'      => date("Y-m-d H:i:s"),
                );
               
            }else{
                $config['upload_path']       = 'uploads/category';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('category_image'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $category_image = $data['upload_data'];
                }
                else
                {
                    $category_image = '';
                }

                $data =  array(
                    'parent_category_id' => $_POST['parent_category_id'],
                    'category_name'      => $_POST['category_name'],
                    'description'        => $_POST['description'],
                    'category_image'     => $category_image,
                    'status_choice'      => $_POST['status_choice'],
                    'is_deleted'         => 0,
                    'modified_date'      => date("Y-m-d H:i:s"),
                );
            }
            
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_category', $data);
            return $query;
        }

        public function checkActivate($id){

            $status = $this->db->select('status_choice')->from('manage_resources_category')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('manage_resources_category');
            return $query;
        }

        public function checkExists($category_name){
            $where = array('category_name'=>$category_name, 'is_deleted'=> 0);
            $query = $this->db->select('*')->from('manage_resources_category')->where($where)->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }
	}
