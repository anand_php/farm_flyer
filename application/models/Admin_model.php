<?php
	class Admin_model extends CI_Model {

		public function getAllAdmins(){
			$query = $this->db->select('*')->from('subadmin_user')->where('is_deleted',0)->order_by('id','DESC')->get();
			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        
        public function add()
        {   
           
            $config['upload_path']       = 'uploads/subadmin';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('admin_image'))
            {
            	
                $data['upload_data'] = $this->upload->data('file_name');
                $admin_image = $data['upload_data'];
            }
            else
            {
                $admin_image = '';
            }

            $data = array(
                'user_role'      => $_POST['user_role'],
                'name'           => $_POST['name'],
                'email'          => $_POST['email'],
                'mobile'         => $_POST['mobile'],
                'gender'         => $_POST['gender'],
                'address'        => $_POST['address'],
                'username'       => $_POST['username'],
                'user_role'      => 2,
            	'password'       => base64_encode($_POST['password']),
            	'admin_image'    => $admin_image,
            	'status_choice'  => $_POST['status_choice'],
            	'is_deleted'     => 0,
                'created_date'   => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('subadmin_user',$data);
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('subadmin_user');
        	return $query;
        }

        public function getAdminById($id){
        	$this->db->select('subadmin_user.*, user_roles.name as role, "" AS warehouse, "" AS cold_storage, "" AS dispatch_depot');
        	$this->db->from('subadmin_user');
            $this->db->join('user_roles','user_roles.role_id = subadmin_user.user_role');
        	$this->db->where('subadmin_user.id', $id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){

        	$id = $_POST['id'];
            if($_POST['password'] == ''){
                $password = '';
            }else{
                $password = base64_encode($_POST['password']);
            }
            
            if($_FILES['admin_image']['name'] == ''){ 
                $data = array(
                    'user_role'      => $_POST['user_role'],
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['mobile'],
                    'gender' => $_POST['gender'],
                    'address' => $_POST['address'],
                    'username' => $_POST['username'],
                    
                    'password' => $password,
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );
            }else{
                $config['upload_path']       = 'uploads/subadmin';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('admin_image'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $admin_image = $data['upload_data'];
                }
                else
                {
                    $admin_image = '';
                }

                $data = array(
                    'user_role'      => $_POST['user_role'],
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['mobile'],
                    'gender' => $_POST['gender'],
                    'address' => $_POST['address'],
                    'username' => $_POST['username'],
                    
                    'password' => $password,
                    'admin_image' => $admin_image,
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );

            }
        	
            $this->db->where('id', $id);
            $query = $this->db->update('subadmin_user', $data);
            return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('subadmin_user')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('subadmin_user');
            return $query;
        }

        public function checkExists($username){
            $where = array('username'=> $username, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('subadmin_user')
                    ->where($where)
                    ->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

        //created by Pooja on 14_09_2019
        public function assign(){
            $id = $_POST['id'];
            $location = isset($_POST['warehouse_id'])?$_POST['warehouse_id']:'';
            $location2 = isset($_POST['cold_storage_id'])?$_POST['cold_storage_id']:'';
            $location3 = isset($_POST['dispatch_depot_id'])?$_POST['dispatch_depot_id']:'';

            if(!empty($location))
            {
                $warehouse_id = implode(',', $location);
            }
            if(!empty($location2)){
                $cold_storage_id = implode(',', $location2);
            }
            if(!empty($location3)){
                $dispatch_depot_id = implode(',', $location3);
            }
            
            
            $data = array(
                'warehouse_id'      => isset($warehouse_id)?$warehouse_id:'',
                'cold_storage_id'   => isset($cold_storage_id)?$cold_storage_id:'',
                'dispatch_depot_id' => isset($dispatch_depot_id)?$dispatch_depot_id:'',
            );
            $this->db->where('id', $id);
            $query = $this->db->update('subadmin_user', $data);
            return $query;
        }


        //created by Pooja on 17_09_2019
        public function getLocationByID($table,$id){
            $query = $this->db->select('*')->from($table)->where('id', $id)->get()->row();
            return $query;
        }

        //created by Pooja on 17_09_2019
        public function getStorageByID($id){
            $query = $this->db->select('*')->from('cold_storage')->where('cold_storage_id', $id)->get()->row();
            return $query;
        }

	}
