<?php
	class Orders_model extends CI_Model {

		public function getAllOrders(){
			$query = $this->db->select('order_table.*, buyers.buyer_id, buyers.buyer_name')
                    ->from('order_table')
                    ->join('buyers','buyers.buyer_id = order_table.buyer_id','left')
                    ->where('order_table.is_deleted','0')
                    ->order_by('order_id','DESC')
                    ->get();

			$data['count'] = $query->num_rows();
			if($data['count'] > 0){
				$data['result'] = $query->result();
			}
			return $data;
		}
        
        public function add()
        {
        	
            $config['upload_path']       = 'uploads/brand';
            $config['allowed_types']     = 'gif|jpg|jpeg|png';
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('brand_image'))
            {
            	
                $data['upload_data'] = $this->upload->data('file_name');
                $brand_image = $data['upload_data'];
            }
            else
            {
                $brand_image = '';
            }

            $data = array(
            	'brand_name' => $_POST['brand_name'],
            	'brand_image' => $brand_image,
            	'status_choice' => $_POST['status_choice'],
            	'is_deleted' => 0,
                'created_date' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('order_table',$data);
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('order_table');
        	return $query;
        }

        public function getOrderById($order_id){
        	$this->db->select('order_table.*, buyers.buyer_id, buyers.buyer_name');
        	$this->db->from('order_table');
            $this->db->join('buyers','buyers.buyer_id = order_table.buyer_id','left');
        	$this->db->where('order_table.order_id', $order_id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function getOrderDetailsById($order_id){
            $this->db->select('*');
            $this->db->from('order_details_table');
            $this->db->where('order_id', $order_id);
            $query = $this->db->get();

            $data['count'] = $query->num_rows();
            if($data['count'] > 0){
                $data['result'] = $query->result();
            }
            return $data;
        }

        public function update(){
        	$id = $_POST['id'];

            if($_FILES['brand_image']['name'] == ''){ 
                $data = array(
                    'brand_name' => $_POST['brand_name'],
                    // 'brand_image' => $brand_image,
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );
            }else{
                $config['upload_path']       = 'uploads/brand';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('brand_image'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $brand_image = $data['upload_data'];
                }
                else
                {
                    $brand_image = '';
                }

                $data = array(
                    'brand_name' => $_POST['brand_name'],
                    'brand_image' => $brand_image,
                    'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );

            }
        	
            $this->db->where('id', $id);
            $query = $this->db->update('order_table', $data);
            return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('order_table')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('order_table');
            return $query;
        }

        public function checkExists($brand_name){
            $where = array('brand_name'=> $brand_name, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('order_table')
                    ->where($where)
                    ->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

	}
