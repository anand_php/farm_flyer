<?php
	class Login_model extends CI_Model {

        public function checkUserExists(){
            $where = ['admin_email' => $_POST['login-email'], 'admin_password' => base64_encode($_POST['login-password'])];

            $query = $this->db->select('*')
                    ->from('admin_user')
                    ->where($where) 
                    ->get()->row();

            // if(!$query){
            //     $where2 = ['email' => $_POST['login-email'], 'password' => base64_encode($_POST['login-password'])];

                // $query2 = $this->db->select('*')
                //     ->from('subadmin_user')
                //     ->where($where2) 
                //     ->get()->row();
                // return $query2;
            // }else{
                return $query;
            // }
   
        }

        public function checkSubadminExists(){

            $where = ['email' => $_POST['login-email'], 'password' => base64_encode($_POST['login-password'])];

            $query = $this->db->select('subadmin_user.*, user_roles.name as role')
                    ->from('subadmin_user')
                    ->join('user_roles','user_roles.role_id = subadmin_user.user_role')
                    ->where($where) 
                    ->get()->row();
            
                return $query;
        }

	}
