<?php
	class Dashboard_model extends CI_Model {

        public function getProfileDataById(){
            if($this->session->userdata('logged_in')['user_role'] == 1){
                $query = $this->db->select('*')
                    ->from('admin_user')
                    ->where('admin_id', $this->session->userdata("logged_in")['id'])
                    ->get()->row();
            }else{
                $query = $this->db->select('*')
                    ->from('subadmin_user')
                    ->where('id', $this->session->userdata("logged_in")['id'])
                    ->get()->row();
            }
            
            return $query;
        }

		
        public function update(){
        	$id = $_POST['id'];
            if($_FILES['admin_image_path']['name'] == ''){
                if($_POST['admin_password'] == ''){
                    $data = array(
                        'admin_fname' => $_POST['admin_fname'],
                        'admin_lname' => $_POST['admin_lname'],
                        'admin_gender' => $_POST['admin_gender'],
                        'admin_mobile' => $_POST['admin_mobile'],
                        'admin_email' => $_POST['admin_email'],
                        // 'admin_password' => base64_encode($_POST['admin_password']),
                        'admin_address' => $_POST['admin_address'],
                        // 'admin_image_path' => $admin_image_path,
                        // 'is_deleted' => 0,
                        'created_date' => date("Y-m-d H:i:s")
                    );
                }else{
                    $data = array(
                        'admin_fname' => $_POST['admin_fname'],
                        'admin_lname' => $_POST['admin_lname'],
                        'admin_gender' => $_POST['admin_gender'],
                        'admin_mobile' => $_POST['admin_mobile'],
                        'admin_email' => $_POST['admin_email'],
                        'admin_password' => base64_encode($_POST['admin_password']),
                        'admin_address' => $_POST['admin_address'],
                        // 'admin_image_path' => $admin_image_path,
                        // 'is_deleted' => 0,
                        'created_date' => date("Y-m-d H:i:s")
                    );  
                }

                
            }else{
                $config['upload_path']       = 'uploads/admin';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('admin_image_path'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $admin_image_path = $data['upload_data'];
                }
                else
                {
                    $brand_image = '';
                }

                if($_POST['admin_password'] == ''){
                    $data = array(
                        'admin_fname' => $_POST['admin_fname'],
                        'admin_lname' => $_POST['admin_lname'],
                        'admin_gender' => $_POST['admin_gender'],
                        'admin_mobile' => $_POST['admin_mobile'],
                        'admin_email' => $_POST['admin_email'],
                        // 'admin_password' => base64_encode($_POST['admin_password']),
                        'admin_address' => $_POST['admin_address'],
                        'admin_image_path' => $admin_image_path,
                        // 'is_deleted' => 0,
                        'created_date' => date("Y-m-d H:i:s")
                    );
                }else{
                    $data = array(
                        'admin_fname' => $_POST['admin_fname'],
                        'admin_lname' => $_POST['admin_lname'],
                        'admin_gender' => $_POST['admin_gender'],
                        'admin_mobile' => $_POST['admin_mobile'],
                        'admin_email' => $_POST['admin_email'],
                        'admin_password' => base64_encode($_POST['admin_password']),
                        'admin_address' => $_POST['admin_address'],
                        'admin_image_path' => $admin_image_path,
                        // 'is_deleted' => 0,
                        'created_date' => date("Y-m-d H:i:s")
                    );  
                }
            }

            $this->db->where('admin_id', $id);
            $query = $this->db->update('admin_user', $data);
            return array('msg'=>'Record Inserted Successfully', $query);
        }

        public function change_password(){
            $data = array(
                // 'admin_email' => $_POST['email'],
                'admin_password' => base64_encode($_POST['admin_password']),
            );
            $this->db->where('admin_id',$_POST['id']);
            $query = $this->db->update('admin_user', $data);
            return $query;
        }

        //created by Pooja on 14_09_2019 
        public function getPermissions(){
            $query = $this->db->select('role_id')->from('user_roles')->where('name',$this->session->userdata("logged_in")["user_role"])->get()->row();
           
            $query2 = $this->db->select('id, role_id, menu_id, submenu_id, status_choice, is_deleted, created_date, modified_date, "" AS menues, "" AS submenues')
                        ->from('auth_user_user_permissions')
                        ->where('role_id', $query->role_id)
                        ->get()->row();

            return $query2; 
        }

        //created by Pooja on 14_09_2019
        public function getMenuById($id){
            $query = $this->db->select('*')
                    ->from('admin_menus')
                    ->where('id', $id)
                    ->get()->row();

            return $query;
        }

        //created by Pooja on 14_09_2019
        public function getSubmenuByMenuId($menu_id){

            $query = $this->db->select('*')
                    ->from('admin_submenus')
                    ->where('menu_id', $menu_id)
                    ->get()->result();
            
                return $query;
            
            
        }

        public function update_subadmin(){
            // echo"<pre>";print_r($_POST); die();
            $id = $_POST['id'];
            if($_POST['password'] == ''){
                $password = '';
            }else{
                $password = base64_encode($_POST['password']);
            }
            
            if($_FILES['admin_image']['name'] == ''){ 
                $data = array(
                    // 'user_role'      => $_POST['user_role'],
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['mobile'],
                    'gender' => $_POST['gender'],
                    'address' => $_POST['address'],
                    'username' => $_POST['username'],
                    
                    'password' => $password,
                    // 'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );
            }else{
                $config['upload_path']       = 'uploads/subadmin';
                $config['allowed_types']     = 'gif|jpg|jpeg|png';
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('admin_image'))
                {
                    
                    $data['upload_data'] = $this->upload->data('file_name');
                    $admin_image = $data['upload_data'];
                }
                else
                {
                    $admin_image = '';
                }

                $data = array(
                    // 'user_role'      => $_POST['user_role'],
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['mobile'],
                    'gender' => $_POST['gender'],
                    'address' => $_POST['address'],
                    'username' => $_POST['username'],
                    
                    'password' => $password,
                    'admin_image' => $admin_image,
                    // 'status_choice' => $_POST['status_choice'],
                    'is_deleted' => 0,
                    'modified_date' => date("Y-m-d H:i:s")
                );

            }
            
            $this->db->where('id', $id);
            $query = $this->db->update('subadmin_user', $data);
            return $query;
        }

        //created by Pooja on 14_09_2019
        public function getAllMenues(){
            $this->db->select('id, menu_name, status_choice, is_deleted, created_date, modified_date, "" AS submenus, link,icon, "" AS sub_link ');
            $this->db->from('admin_menus');
            $this->db->where('is_deleted',0);
            $query = $this->db->get()->result();
            return $query;
        }

        //created by Pooja on 16_09_2019 to get permission submenu name
        public function getSubmenuById($id){
            $query = $this->db->select('*')
                    ->from('admin_submenus')
                    ->where('id',$id)
                    ->get()->row();
            // print_r($this->db->last_query()); die();
            return $query;
        }
	}
