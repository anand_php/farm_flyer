<?php
	class Dispatch_depot_model extends CI_Model {

		public function getAllDispathDepots(){
			$query = $this->db->select('dispatch_depot.*, service_city.id as cityId, service_city.city_name, warehouse.id as warehouseId, warehouse.warehouse_name')
                    ->from('dispatch_depot')
                    ->join('service_city','service_city.id = dispatch_depot.city_id')
                    ->join('warehouse','warehouse.id = dispatch_depot.warehouse_id')
                    ->where('dispatch_depot.is_deleted',0)
                    ->order_by('dispatch_depot.id','DESC')
                    ->get()->result();
			// $data['count'] = $query->num_rows();
			// if($data['count'] > 0){
			// 	$data['result'] = $query->result();
			// }
			return $query;
		}

        public function getAllCities(){
            $query = $this->db->select('*')
                    ->from('service_city')
                    ->where('is_deleted',0)
                    ->get()->result();
            return $query;
        }
        
        public function add()
        {
            $add = explode(',', $_POST['map_id']);
        	
            $data = array(
            	'dispatch_depot_code' => $_POST['dispatch_depot_code'],
            	'dispatch_depot_name' => $_POST['dispatch_depot_name'],
                'city_id'             => $_POST['city_id'],
                'state_id'            => $_POST['state_id'],
                'address'             => $_POST['address'],
                'map_id'              => $_POST['map_id'],
                'latitude'            => $add[0],
                'longitude'           => $add[1],
                'warehouse_id'        => $_POST['warehouse_id'],
            	'status_choice'       => $_POST['status_choice'],
            	'is_deleted'          => 0,
                'created_date'        => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('dispatch_depot',$data);
            return $query;
        }

        public function delete($id)
        {
        	$this->db->set('is_deleted', 1);
        	$this->db->where('id', $id);
        	$query = $this->db->update('dispatch_depot');
        	return $query;
        }

        public function getDepotById($id){
        	$this->db->select('dispatch_depot.*, service_state.state_id, service_state.state_name, service_city.id,service_city.city_name, warehouse.id, warehouse.warehouse_name');
            $this->db->from('dispatch_depot');
            $this->db->join('service_state','service_state.state_id = dispatch_depot.state_id');
            $this->db->join('service_city','service_city.id = dispatch_depot.city_id');
            $this->db->join('warehouse','warehouse.id = dispatch_depot.warehouse_id');
            $this->db->where('dispatch_depot.id', $id);
        	$query = $this->db->get()->row();
        	return $query;
        }

        public function update(){
        	$id = $_POST['id'];
            $add = explode(',', $_POST['map_id']);

            $data = array(
                'dispatch_depot_code' => $_POST['dispatch_depot_code'],
                'dispatch_depot_name' => $_POST['dispatch_depot_name'],
                'state_id'            => $_POST['state_id'],
                'city_id'             => $_POST['city_id'],
                'address'             => $_POST['address'],
                'map_id'              => $_POST['map_id'],
                'latitude'            => $add[0],
                'longitude'           => $add[1],
                'warehouse_id'        => $_POST['warehouse_id'],
                'status_choice'       => $_POST['status_choice'],
                'is_deleted'          => 0,
                'modified_date'       => date("Y-m-d H:i:s")
            );
        	
            $this->db->where('id', $id);
            $query = $this->db->update('dispatch_depot', $data);
            return $query;
        }

        public function checkActivate($id){
            $status = $this->db->select('status_choice')->from('dispatch_depot')->where('id', $id)->get()->row();
            if($status->status_choice == 'true'){
                $final_status = 'false';
            }else{
                $final_status = 'true';
            }
            
            $this->db->set('status_choice', $final_status);
            $this->db->where('id', $id);
            $query = $this->db->update('dispatch_depot');
            return $query;
        }

        public function checkExists($dispatch_depot_code){
            $where = array('dispatch_depot_code'=> $dispatch_depot_code, 'is_deleted'=>0);
            $query = $this->db->select('*')
                    ->from('dispatch_depot')
                    ->where($where)
                    ->get()->row();
            if($query){
                return true;
            }else{
                return false;
            }
        }

        //created by Pooja on 17_09_2019
        public function getParticularUserDepot(){
            $id = $this->session->userdata("logged_in")["id"];
            $query = $this->db->select('*')->from('subadmin_user')->where('id', $id)->get()->row();

            return $query;
        }

	}
